var fs = require('fs');
var app = require("../server");
const { Pool, Client } = require('pg')

var fileName = app.get('env') + ".json";
var configFile = fs.readFileSync('./config/' + fileName, 'utf8');
var config = JSON.parse(configFile);

const pool = new Pool(config.pgConnection);

var CollectionRoutes = function (router) {
    router.post('/getCollectionsTree', getCollectionData);
    router.post('/addNewCollection', addNewCollection);
    router.post('/updateNewCollection', updateNewCollection);
    return router;
}

var getCollectionData = function (req, res, next) {
    try {
        pool.query("SELECT * from tdg_collections WHERE owner = '" + req.body.userEmail + "'", (error, results) => {
            if (error) {
                console.log("error")
                console.log(error)
              return next(error);
            }

            res.json(results.rows);
          });
    } catch (errorLog) {
        console.log(errorLog)
    }
}

var addNewCollection = function (req, res, next) {
    try {
        const {name, parent_id, document_info, owner, created_date, modified_by, modified_date} = req.body;
        var insertQuery = "INSERT INTO tdg_collections (id, name, parent_id, document_info, owner, created_date, modified_by, modified_date) VALUES (DEFAULT, $1, $2, $3, $4, $5, $6, $7) RETURNING id";
        pool.query(insertQuery, [name, parent_id, document_info, owner, created_date, modified_by, modified_date], (error, results) => {
            if (error) {
                console.log("error")
                console.log(error)
                return next(error);
            }

            res.json(results.rows);
          });
    } catch (errorLog) {
        console.log(errorLog)
    }
}

var updateNewCollection = function (req, res, next) {
    try {
        const {id, name, parent_id, document_info, owner, created_date, modified_by, modified_date} = req.body;
        var insertQuery = "UPDATE tdg_collections SET name=$2, parent_id=$3, document_info=$4, owner=$5, created_date=$6, modified_by=$7, modified_date=$8 WHERE id=$1";
        pool.query(insertQuery, [id, name, parent_id, document_info, owner, created_date, modified_by, modified_date], (error, results) => {
            if (error) {
                console.log("error")
                console.log(error)
              return next(error);
            }

            res.json(results.rows);
          });
    } catch (errorLog) {
        console.log(errorLog)
    }
}

module.exports = CollectionRoutes;