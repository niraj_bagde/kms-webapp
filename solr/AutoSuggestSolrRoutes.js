var fs = require('fs');
var SolrNode = require('solr-node');
var app = require("../server");

var fileName = app.get('env') + ".json";
var configFile = fs.readFileSync('./config/' + fileName, 'utf8');
var config = JSON.parse(configFile);

var client = new SolrNode({
    host: config.SearchSolrHost,
    port: config.SearchSolrPort,
    core: config.SearchSolrCore,
    protocol: 'http',
    debugLevel: 'ERROR',
});
client.SEARCH_PATH = config.AutoSuggestSolrHandler;

var autosuggestSolrRoutes = function (router) {
    router.post('/getAutoSuggestResults', getAutoSuggestResults);
    return router;  
}

var getAutoSuggestResults = function (req, res, next) {
    var clientQry;
        clientQry = client.query()
        .q(req.body.query)
        .addParams({
                    wt: 'json',
                    indent: true,
                    category:'',
                    profile:'',
                    facet:true,
                    spellcheck:true})
        clientQry.params.push("suggest.count="+req.body.count);
        clientQry.params.push("suggest.cfq=("+ req.body.datasets +")");
                   
    client.search(clientQry, function (err, result) {
        try {
            if (err) {
                return next(err);
            }
            var resultData = (result);
            if(resultData) {
                res.json(resultData);
            }
        } catch(err) {
            res.json(err);
        }
    }); 
}

module.exports = autosuggestSolrRoutes;
