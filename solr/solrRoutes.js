var solrClient = {};

var solrRoutes = function (router) {
    router.post('/getUserData', getUserData);
    router.post('/setUserData', setUserData);
    router.post('/removeUser', removeUser);
    return router;  
}

var getUserData = function (req, res, next){
    solrClient = res.locals.solrLiveNode;

    var clientQry = solrClient.query()
    .q(req.body.prop + ':' + req.body.value)
    .start("0")
    .rows("100");

    solrClient.search(clientQry, function (err, result) {
        if (err) {
            return next(err);
        }
        var resultData = (result);
        if(resultData && resultData.response) {
            res.json(resultData.response);
        } else {
            res.json(resultData);
        }
        
    }); 
}

var setUserData = function (req, res, next){
    var data = req.body.data;
    solrClient = res.locals.solrLiveNode;

    solrClient.update(data, function(err, result) {
        if (err) {
            console.log(err);
            return;
        }
        var resultData = (result);
        if(resultData && resultData.response) {
            res.json(resultData.response);
        } else {
            res.json(resultData);
        }
    });
}


var removeUser = function (req, res, next){
    // Delete Query
    // var strQuery = 'id:9b74c915-acf4-4b92-a49d-0552e4e67eae'
    // solrClient.delete(strQuery);

    // strQuery = 'digital_email:richa.sinha@thedigitalgroup.com'
    // solrClient.delete(strQuery);
    // strQuery = 'id:29e05ab7-42a2-49f7-9ac6-ef5832b1e5fe'
    // solrClient.delete(strQuery);




}

 


module.exports = solrRoutes;