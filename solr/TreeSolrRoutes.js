var fs = require('fs');
var SolrNode = require('solr-node');
var app = require("../server");

var fileName = app.get('env') + ".json";
var configFile = fs.readFileSync('./config/' + fileName, 'utf8');
var config = JSON.parse(configFile);

var client = new SolrNode({
    host: config.SearchSolrHost,
    port: config.SearchSolrPort,
    core: config.SearchSolrCore,
    // rootPath: "/solr/alnylam-data/3rdi-edocs",
    protocol: 'http',
    debugLevel: 'ERROR',
    user: 'solr',
    password: 'scoutarmy'
});
client.SEARCH_PATH = config.TreeSolrHandler;

var getTreeStructFromSolr = function (router) {
    router.post('/getTreeStructFromSolr', callSolrTreeStructApi);
    router.post('/getTreeStructFromGroupedSolr', callGroupedSolrTreeStructApi);
    return router;
}

var callSolrTreeStructApi = function (req, res, next) {
    try {
    var clientQry;
    // console.log("req.body")
    // console.log(req.body)
    clientQry = client.query()
        // .q(req.body.searchQuery)
        // .fq("doc_path_hierarchy: 'edocs/Library-Alnylam'")
        // .rows("100")
        .addParams({
            wt: 'json',
            group: true,
            facet: false,
            indent: true,
        });
    let manifoldUser;
    var serviceTypeString = "";
    /* if (req.body.source_type !== undefined) {
        var splitServiceType = req.body.source_type.split(",");
        splitServiceType.forEach((serviceTypes, index) => {
            // if (serviceTypes === "manifold") {
            //     serviceTypeString = serviceTypeString + "sharepoint";
            // } else {
                serviceTypeString = serviceTypeString + serviceTypes;
            // }
            if (index != (splitServiceType.length -1)) {
                serviceTypeString =  serviceTypeString + " OR ";
            }
        })
    } */

    serviceTypeString = "(box OR sharepoint)";
    if (req.body.manifoldUsername !== undefined && req.body.manifoldUsername !== null) {
        // manifoldUser = "AuthenticatedUserName=richas@dgi"
        manifoldUser = "permission.manifold.AuthenticatedUserName=" + req.body.manifoldUsername
        clientQry.params.push(manifoldUser);
        clientQry.params.push("personalization.explicit.enable=false");
        clientQry.params.push("personalization.enableIrisExpansions=false");
    } 
    // let serviceType= "permission.service.type=" + req.body.serviceType;
    let serviceType= "permission.service.type=manifold,box";
    clientQry.params.push(serviceType);
    if (req.body.accessToken !== undefined && req.body.accessToken !== null) {
        let authenticationString;
        authenticationString = 'permission.box.UserACLs=[{"allowAcessDocumentTokens":[';
        for (let i = 0; i < req.body.acl.length; i++) {
            authenticationString += '"' + req.body.acl[i] + '"';
            if (i < req.body.acl.length - 1) {
                authenticationString += ',';
            }
        }
        if (req.body.acl.length == 0) {
            authenticationString += '""'
        }
        authenticationString += '],"tokensFound":' + req.body.tokensFound + ',"dataset":"' +
            req.body.dataset + '","authorityType":"box","datasetType":"Box","authorityInfoExist":true}]'
        clientQry.params.push(authenticationString);
        
    }
    // console.log("req.body.docPath")
    // console.log(req.body.docPath)
    if (req.body.docPath !== undefined && req.body.docPath != "" && req.body.docPath != "*" ) {
        let docPathHierarchy = req.body.docPath.replace(/\s/g, '\\ ');
        clientQry.fq([{field: "doc_path_hierarchy", value: docPathHierarchy}, {field: "dataset_key", value: "nba-contract-lib-edocs-data"}])
    } else {
        // console.log("I am called")
       // clientQry.params.push('fq=source_type:(' + serviceTypeString + ')');
       clientQry.params.push('fq=dataset_key:"nba-contract-lib-edocs-data"');
        // clientQry.params.push('facet=false');
        // clientQry.params.push('group=true');
    }
    
    clientQry.q("*:*");
    clientQry.rows("1000");
 
    client.search(clientQry, function (err, result) {
        if (err) {
            return next(err);
        }
        var resultData = (result);
        if (resultData) {
            res.json(resultData);
        }
    });
    

    } catch (err) {
        console.log(err);
    }
}

var callGroupedSolrTreeStructApi = function (req, res, next) {
    try {
    var clientQry;
    clientQry = client.query()
        // .q(req.body.searchQuery)
        // .fq("doc_path_hierarchy: 'edocs/Library-Alnylam'")
        // .rows("100")
        .addParams({
            wt: 'json',
            group: true,
            facet: false,
            indent: true,
        });
    let manifoldUser;
    var serviceTypeString = "";
    /* if (req.body.source_type !== undefined) {
        var splitServiceType = req.body.source_type.split(",");
        splitServiceType.forEach((serviceTypes, index) => {
            // if (serviceTypes === "manifold") {
            //     serviceTypeString = serviceTypeString + "sharepoint";
            // } else {
                serviceTypeString = serviceTypeString + serviceTypes;
            // }
            if (index != (splitServiceType.length -1)) {
                serviceTypeString =  serviceTypeString + " OR ";
            }
        })
    } */
    console.log("serviceTypeString");
    console.log(serviceTypeString);
    serviceTypeString = "(box OR sharepoint)";
    if (req.body.manifoldUsername !== undefined && req.body.manifoldUsername !== null) {
        // manifoldUser = "AuthenticatedUserName=richas@dgi"
        manifoldUser = "permission.manifold.AuthenticatedUserName=" + req.body.manifoldUsername
        clientQry.params.push(manifoldUser);
        clientQry.params.push("personalization.explicit.enable=false");
        clientQry.params.push("personalization.enableIrisExpansions=false");
    } 
    // let serviceType= "permission.service.type=" + req.body.serviceType;
    let serviceType= "permission.service.type=manifold,box";
    clientQry.params.push(serviceType);
    if (req.body.accessToken !== undefined && req.body.accessToken !== null) {
        let authenticationString;
        authenticationString = 'permission.box.UserACLs=[{"allowAcessDocumentTokens":[';
        for (let i = 0; i < req.body.acl.length; i++) {
            authenticationString += '"' + req.body.acl[i] + '"';
            if (i < req.body.acl.length - 1) {
                authenticationString += ',';
            }
        }
        if (req.body.acl.length == 0) {
            authenticationString += '""'
        }
        authenticationString += '],"tokensFound":' + req.body.tokensFound + ',"dataset":"' +
            req.body.dataset + '","authorityType":"box","datasetType":"Box","authorityInfoExist":true}]'
        clientQry.params.push(authenticationString);
        
    }
    // console.log("req.body.docPath")
    // console.log(req.body.docPath)
    if (req.body.docPath !== undefined && req.body.docPath != "" && req.body.docPath != "*") {
        let docPathHierarchy = req.body.docPath.replace(/\s/g, '\\ ');
        clientQry.fq([{field: "doc_path_hierarchy", value: docPathHierarchy}, {field: "dataset_key", value: "nba-contract-lib-edocs-data"}])
    } else {
        console.log("I am called")
      //  clientQry.params.push('fq=source_type:(' + serviceTypeString + ')');
      clientQry.params.push('fq=dataset_key: "nba-contract-lib-edocs-data" ');
        clientQry.params.push('facet=false');
        clientQry.params.push('group=true');
    }
    
    clientQry.q("*:*");
    clientQry.rows("1000");
 
    client.search(clientQry, function (err, result) {
        if (err) {
            return next(err);
        }
        var resultData = (result);
        if (resultData) {
            res.json(resultData);
        }
    });
    

    } catch (err) {
        console.log(err);
    }
}

module.exports = getTreeStructFromSolr;
