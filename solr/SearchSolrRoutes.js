var fs = require('fs');
var SolrNode = require('solr-node');
var app = require("../server");

var fileName = app.get('env') + ".json";
var configFile = fs.readFileSync('./config/' + fileName, 'utf8');
var config = JSON.parse(configFile);

var client = new SolrNode({
    host: config.SearchSolrHost,
    port: config.SearchSolrPort,
    core: config.SearchSolrCore,
    // rootPath: rootPath,
    protocol: 'http',
    debugLevel: 'ERROR',
    // user: 'solr',
    // password: 'scoutarmy'
});
client.SEARCH_PATH = config.SearchSolrHandler;

var searchSolrRoutes = function (router) {
    router.post('/getResults', getResults);
    return router;  
}

var getResults = function (req, res, next) {
    var clientQry;
    var query = req.body.searchQuery;
    if(req.body.searchWithinQuery) {
        if(query=="*:*") {
            query = req.body.searchWithinQuery;
        } else {
            query = query + " " + req.body.searchWithinQuery;
        }
    }
    if(req.body.filterObj && req.body.filterObj.length==0) {
        clientQry = client.query()
                    .q(query)
                    .addParams({
                        wt: 'json',
                        indent: true,
                        category:'',
                        profile:'',
                        facet:true,
                        spellcheck:true,
                    })
                    .start(req.body.pageOffset)
                    .rows(req.body.pageSize);
    } else {
        clientQry = client.query()
                    .q(query)
                    .fq(req.body.filterObj)
                    .addParams({
                        wt: 'json',
                        indent: true,
                        category:'',
                        profile:'',
                        facet:true,
                        spellcheck:true,
                    })
                    .start(req.body.pageOffset)
                    .rows(req.body.pageSize);
    }
    clientQry.params.push("sort="+req.body.sort);
    // if(req.body.searchWithinQuery) {
    //     var swsParam = "fq=" + req.body.searchWithinQuery;
    //     clientQry.params.push(swsParam);
    //     let hlq = "hl.q=" + req.body.searchWithinQuery;
    //     clientQry.params.push(hlq);
    // }
    if(req.body.autosuggestCategory!="") {
        let categoryParam = "suggest.category=" + req.body.autosuggestCategory;
        clientQry.params.push(categoryParam);
    }
    clientQry.params.push("personalization.explicit.enable=false");
    clientQry.params.push("personalization.enableIrisExpansions=false");
    clientQry.params.push("spellcheck.collate=true");
    let manifoldUser;
    if(req.body.manifoldUsername) {
        manifoldUser= "permission.manifold.AuthenticatedUserName=" + req.body.manifoldUsername;
        clientQry.params.push(manifoldUser);
    }
    clientQry.params.push("spellcheck.collate=true");
    let serviceType= "permission.service.type=" + req.body.serviceType;
    clientQry.params.push(serviceType);

    if(req.body.serviceType.indexOf('box')>-1) {
        let authenticationString;
        authenticationString ='permission.box.UserACLs=[{"allowAcessDocumentTokens":[';
        for(let i=0; i< req.body.acl.length; i++) {
            authenticationString += '"' + req.body.acl[i] + '"';
            if(i<req.body.acl.length-1) {
                authenticationString += ',';
            }
        }
        if(req.body.acl.length==0) {
            authenticationString += '""'
        }
        authenticationString +='],"tokensFound":'+ req.body.tokensFound + ',"dataset":"' +
                                req.body.dataset + '","authorityType":"box","datasetType":"Box","authorityInfoExist":true}]'
        clientQry.params.push(authenticationString);
    }

    client.search(clientQry, function (err, result) {
        try {
            if (err) {
                return next(err);
            }
            var resultData = (result);
            if(resultData) {
                res.json(resultData);
            }
        } catch(err) {
            res.json(err);
        }
    }); 
}

module.exports = searchSolrRoutes;
