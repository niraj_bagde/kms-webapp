
var fs = require('fs');
var SolrNode = require('solr-node');
var solrCloudClient = require('node-solr-smart-client');

module.exports = function (env,spl, callback) {

    var fileName = env + ".json";
    var configFile = fs.readFileSync('./config/' + fileName, 'utf8');
    var config = JSON.parse(configFile);
    var solrClientNode;

    var corename = config.SOLR_CORE;
    solrCloudClient.createClient(corename, config, function (err, cloudClient) {
        process.on('uncaughtException', (err) => {
        // logger.error(err.message);
        });
        if (err) {
            // logger.error(err);
            callback(null, err);
        }
        else {
            if (typeof cloudClient === "undefined") {
                // logger.error(err);
                callback(null, err);
            }
            else {

                var cloudClientObj = cloudClient.options;

                var pathString = cloudClientObj.path;
                pathString = pathString.split('/');

                var coreString = pathString[pathString.length - 1];
                var rootPath = pathString[pathString.length - 2];

                solrClientNode = new SolrNode({
                    host: cloudClientObj.host,
                    port: cloudClientObj.port,
                    core: coreString,
                    rootPath: rootPath,
                    protocol: 'http',
                    debugLevel: 'ERROR',
                    user: 'solr',
                    password: 'scoutarmy'
                });
                solrClientNode.SEARCH_PATH = "select";

                callback(solrClientNode);
            }
        }
    });
}
