var apiAnnotation = require("./apiAnnotation");

var annotationRouter = function (router) {
  router.post("/save", performSave);
  router.get("/fetch/:doc_id", getTags);
  return router;
};

var performSave = function (req, res, next) {
  var apiAnnotationObj = new apiAnnotation();
  apiAnnotationObj.performSave(req, function (result, error) {
    res.json(result);
  });
};

var getTags = function (req, res, next) {
  var apiAnnotationObj = new apiAnnotation();
  apiAnnotationObj.getTags(req, function (result, error) {
    res.json(result);
  });
};

module.exports = annotationRouter;
