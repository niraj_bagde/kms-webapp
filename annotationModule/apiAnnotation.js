var request = require("request");
var app = require("../server");
var apiAnnotaion = function () {};

apiAnnotaion.prototype.performSave = function (req, callback) {
  let body = req.body;
 
  body = JSON.stringify(body);

  var options = {
      uri: app.get('URL_SaveMetaData'),
      body: body,
      headers: { 'content-type' : 'application/json' }
  };
  request.post(options, function (error, response, body) {
      console.log(body)
      if (callback) {
          try {
              if(body) {
                  callback(JSON.parse(body));
              } else {
                  callback(JSON.parse("{}"));
              }
              callback = null;
            }
            catch(err) {
              callback(JSON.parse("{}"));
              callback = null;
            }
      }
  });
};

apiAnnotaion.prototype.getTags = function (req, callback) {
  let doc_id = req.params.doc_id;

  var uri = app.get('URL_GetMetaData') + doc_id;

  request.get(uri, function (error, response, body) {
      console.log(body)
      if (callback) {
          try {
              if(body) {
                  callback(JSON.parse(body));
              } else {
                  callback(JSON.parse("{}"));
              }
              callback = null;
            }
            catch(err) {
              callback(JSON.parse("{}"));
              callback = null;
            }
      }
  });
};

module.exports = apiAnnotaion;
