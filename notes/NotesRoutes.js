var fs = require('fs');
var app = require("../server");
var NoteController = require("../database/controllers")
const { Pool, Client } = require('pg')
var fileName = app.get('env') + ".json";
var configFile = fs.readFileSync('./config/' + fileName, 'utf8');
var config = JSON.parse(configFile);

const pool = new Pool(config.pgConnection);
var currentParentIds = [];

var NotesRoutes = function (router) {
    // router.post('/createNote', NoteController.createNote);
    // router.post('/createNewNoteLine', NoteController.createNote);
    router.post('/createNewNoteLine', createNote);
    router.post('/updateNoteLine', NoteController.updateNote);
    router.post('/getAllNotes', NoteController.getAllNotes);
    router.post('/getNote', NoteController.getSingleNote);
    // router.post('/getUserNote', NoteController.getUserNote);
    router.post('/getUserNote', getUserNote);
    router.post('/deleteNote', NoteController.deleteNote);

    router.post("/updateInsertNotes", updateInsertNotes);
    return router;
}

var createNote = function (req, res, next) {
    console.log("I am called");
}


var getUserNote = function (req, res, next) {
    try {
        pool.query("SELECT * from tdg_usernotes WHERE author_id = '" + req.body.userEmail + "'", (error, results) => {
            if (error) {
                console.log("error")
                console.log(error)
                return next(error);
            }

            res.json(results.rows);
        });
    } catch (errorLog) {
        console.log(errorLog)
    }
}

var updateInsertNotes = function (req, res, next) {
    var allNotes = req.body.allNotes;
    var modifiedBy = req.body.userEmail;
    updateAllNotes(allNotes, modifiedBy).then(updateInfo => {
        res.json(updateInfo);
    })
}

var updateAllNotes = async function  (allNotes, modifiedBy) {
    var updateInfo = allNotes.map(async note => {
        if (note.name1 !== undefined && note.name !== null) {
            if (note.name1.trim() !== "") {
                currentParentIds.forEach(eachParent => {
                    if (Number(eachParent.idInNote) == Number(note.id)) {
                        note.id = eachParent.idInDB;
                    }
                })
                return getNoteDetails(note, modifiedBy).then((updatedInfo) => {
                    if (updatedInfo.rows.length > 0) {
                        return updateNote(note, modifiedBy).then((updateStatus) => {
                            let idsArray = {
                                "idInNote": note.id,
                                "idInDB": updateStatus.rows[0]["id"]
                            }
                            currentParentIds.push(idsArray)
                            return idsArray;
                        }) 
                    } else {
                        return insertNewNote(note, modifiedBy).then((insertDetails) => {
                            let idsArray = {
                                "idInNote": note.id,
                                "idInDB": insertDetails.rows[0]["id"]
                            }
                            currentParentIds.push(idsArray)
                            return idsArray;
                        })
                    }
                })
            }
        }
    });

    return Promise.all(updateInfo) 
}

var getNoteDetails = async function  (note, modifiedBy) {
    var noteDetails = await pool.query("SELECT * from tdg_usernotes WHERE id = '" + note.id + "'");
    return noteDetails;
}

var updateNote = async function (note, modifiedBy) {
    var updateQuery = "UPDATE tdg_usernotes SET "
                    + "note_id=$1, "
                    + "parent_line_id=$2, "
                    + "author_id=$3, "
                    + "line_data=$4, "
                    + "created_date=$5, "
                    + "modified_by=$6, "
                    + "modified_date=$7, "
                    + "line_type=$8 "
                    + "WHERE id=$9 RETURNING id";
    let newParentId = note.parentId; 
    currentParentIds.forEach(eachParent => {
        if (Number(eachParent.idInNote) == Number(note.parentId)) {
            newParentId = Number(eachParent.idInDB);
        }
    });
    var updateVariables = [note.noteId, newParentId, modifiedBy, note.name1, getTodaysDate(), modifiedBy, getTodaysDate(), note.type, note.id];
    var updateStatus = await pool.query(updateQuery, updateVariables);
    return updateStatus;
}

var insertNewNote = async function (note, modifiedBy) {
    var insertQuery = "INSERT INTO tdg_usernotes (id, note_id, parent_line_id, author_id, line_data, created_date, modified_by, modified_date, line_type) "
                                + "VALUES (DEFAULT, $1, $2, $3, $4, $5, $6, $7, $8) RETURNING id";
    let newParentId = note.parentId; 
    currentParentIds.forEach(eachParent => {
        if (Number(eachParent.idInNote) == Number(note.parentId)) {
            newParentId = Number(eachParent.idInDB);
        }
    });
    var insertVariables = [note.noteId, newParentId, modifiedBy, note.name1, getTodaysDate(), modifiedBy, getTodaysDate(), note.type];
    var insertStatus = await pool.query(insertQuery, insertVariables);
    return insertStatus;
}

/* var updateInsertNotesPromise = function (req, res, next) {
    try {
        currentParentIds=[];
        
        var allPromises = [];
        allNotes.forEach(async (note, index) => {
            if (note.name1 !== undefined && note.name !== null) {
                if (note.name1.trim() !== "") {
                    allPromises.push(await updateNotes(note, modifiedBy));
                }
            }
        });
        Promise.all(allPromises).then((allResults) => {
            console.log("currentParentIds")
            console.log(currentParentIds)
            console.log("allResults")
            console.log(allResults)
            console.log("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
            res.json(currentParentIds);
        })
    } catch (error) {
        console.log(error);
    }
    
} */

/* var updateNotes = async function  (note, modifiedBy) {
    return new Promise((resolve, reject) => {
        var tdgUserNote = pool.query("SELECT * from tdg_usernotes WHERE id = '" + note.id + "'", (error, results) => {
            if (error) {
                console.log("error")
                console.log(error)
                return next(error);
            }
            
            if (results.rows.length > 0) {
                var updateQuery = "UPDATE tdg_usernotes SET "
                                + "note_id=$1, "
                                + "parent_line_id=$2, "
                                + "author_id=$3, "
                                + "line_data=$4, "
                                + "created_date=$5, "
                                + "modified_by=$6, "
                                + "modified_date=$7 "
                                + "WHERE id=$8";
                var updateVariables = [note.noteId, note.parentId, note.authorId, note.name1, note.createdDate, modifiedBy, getTodaysDate(), note.id];
                pool.query(updateQuery, updateVariables, (error, result) => {
                    if (error) {
                        console.log("error");
                        console.log(error);
                    } else {
                        currentParentIds.push({
                            idInTree: note.id,
                            idInDb: note.id
                        });
                        console.log("currentParentIds")
                        console.log(currentParentIds)
                        console.log("========================================")
                        resolve(currentParentIds)
                    }
                })
            } else {
                var insertQuery = "INSERT INTO tdg_usernotes (id, note_id, parent_line_id, author_id, line_data, created_date, modified_by, modified_date) "
                                + "VALUES (DEFAULT, $1, $2, $3, $4, $5, $6, $7) RETURNING id";
                let newParentId = 0;
                currentParentIds.forEach(eachParent => {
                    if (eachParent.idInTree == note.parentId) {
                        newParentId = eachParent.idInDb;
                    }
                });
                var insertVariables = [note.noteId, newParentId, modifiedBy, note.name1, getTodaysDate(), modifiedBy, getTodaysDate()];
                pool.query(insertQuery, insertVariables, (error, result) => {
                    if (error) {
                        console.log("error");
                        console.log(error);
                    } else {
                        currentParentIds.push({
                            idInTree: note.id,
                            idInDb: result.rows[0]["id"]
                        });
                        console.log("currentParentIds")
                        console.log(currentParentIds)
                        console.log("----------------------------------------------")
                        resolve(currentParentIds)
                    }
                })
            }
            // res.json(results.rows);
        });
    })
} */

var getTodaysDate = function () {
    const monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"];
    const dateObj = new Date();
    const month = dateObj.getMonth();
    const day = dateObj.getDate();
    const year = dateObj.getFullYear();
    const output = year + "-" + month + "-" + day; //month  + '\n'+ day  + ',' + year;

    return output
} 

module.exports = NotesRoutes;