var apiSearch = require('./apiSearch');

var searchRouter = function (router) {
    router.post('/getSearchResults', performSearch);
    router.post('/autoSuggest', autoSuggest);
    router.post('/saveSearchEventData', saveSearchEventData);
    router.post('/getHLData', getHLData);
    return router;
};

var performSearch = function (req, res, next) {
    var apiSearchObj = new apiSearch();
    apiSearchObj.performSearch(req, function (result, error) {
        res.json(result);
    });

};

var saveSearchEventData = function (req, res, next) {
    var apiSearchObj = new apiSearch();
    apiSearchObj.saveSearchEventData(req, function (result, error) {
        res.json(result);
    });
};


var autoSuggest = function (req, res, next) {
    var apiSearchObj = new apiSearch();
    apiSearchObj.autoSuggest(req, function (result, error) {
        res.json(result);
    });

};

var getHLData = function (req, res, next) {
    var apiSearchObj = new apiSearch();
    apiSearchObj.getHLData(req, function (result, error) {
        res.json(result);
    });

};
module.exports = searchRouter;