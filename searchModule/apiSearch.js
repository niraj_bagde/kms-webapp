var request = require('request');
var app = require('../server');
var apiSearch = function () {
};

apiSearch.prototype.performSearch = function (req, callback) {
    var body = JSON.stringify(req.body);
    var client  = req.body.client;

    var url = client=='alnylam' ? app.get('URL_GetAlnylamSearchUrl') : app.get('URL_GetSearchUrl');
    
    var options = {
        url: url,
        body: body,
        headers: { 'content-type' : 'application/json', 'api-key' : '310aeb7d-5cb2-41b5-a619-41b44e536702' }
    };
    request.post(options, function (error, response, body) {
        if (callback) {
            try {
                if(body) {
                    callback(JSON.parse(body));
                } else {
                    callback(JSON.parse("{}"));
                }
                callback = null;
              }
              catch(err) {
                callback(JSON.parse("{}"));
                callback = null;
              }
        }
    });
};


apiSearch.prototype.autoSuggest = function (req, callback) {
    var body = JSON.stringify(req.body);
    var client = req.body.client;
    // var url = app.get('URL_AutoSuggestUrl');
    var url = client=='alnylam' ? app.get('URL_AlnylamAutoSuggestUrl') : app.get('URL_AutoSuggestUrl');
    var options = {
        url: url,
        body: body,
        headers: { 'content-type' : 'application/json', 'api-key' : '310aeb7d-5cb2-41b5-a619-41b44e536702' }
    };
    request.post(options, function (error, response, body) {
        if (callback) {
            try {
                if(body) {
                    callback(JSON.parse(body));
                } else {
                    callback(JSON.parse("{}"));
                }
                callback = null;
              }
              catch(err) {
                callback(JSON.parse("{}"));
                callback = null;
              }
        }
    });
};

apiSearch.prototype.saveSearchEventData = function (req, callback) {
    var param = JSON.stringify(req.body);
    var request = require('request');
    var eventURL = app.get('URL_GetEventUrl');

    var options = {
        url: eventURL,
        body: param,
        headers: { 'content-type': 'application/json' }
    };

    request.post(options, function (error, response, body) {
        if (callback) {
            try {
                if(!error && response) {
                    callback(JSON.parse(body));
                } else {
                    callback(JSON.parse("{}"));
                }
                callback = null;
              }
              catch(err) {
                callback(JSON.parse("{}"));
                callback = null;
              }
        }
    });
}

apiSearch.prototype.getHLData = function (req, callback) {
    var reqBody = JSON.stringify(req.body);
    var request = require('request');
    var HLURL = app.get('URL_GetHLUrl');

    var options = {
        url: HLURL,
        body: reqBody,
        headers: { 'content-type': 'application/json' }
    };

    request.post(options, function (error, response, body) {
        if (callback) {
            try {
                if(!error && response) {
                    callback(JSON.parse(body));
                } else {
                    callback(JSON.parse("{}"));
                }
                callback = null;
              }
              catch(err) {
                callback(JSON.parse("{}"));
                callback = null;
              }
        }
    });
}
module.exports = apiSearch;