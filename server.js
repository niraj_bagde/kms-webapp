var express = require('express');
var bodyParser = require('body-parser');
var http = require('http');
var https = require('https');
var fs = require('fs');
// var path = require('path');
// var configFile = fs.readFileSync('./config/config.json', 'utf8');
// var config = JSON.parse(configFile);

var solr = require('./solr/solrClient');
var app = module.exports = express();

var env = process.env.NODE_ENV;

if(env == undefined){
    env = "cloud";
}


app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true, parameterLimit: 50000 }));
app.use(bodyParser.json());  
app.use(bodyParser.urlencoded({ 
    extended: true
}));

app.use(express.static(__dirname + '/'));
app.set("env",env);

var fileName = './config/'+env + ".json";
var envFile = fs.readFileSync(fileName, 'utf8');
var envconfig = JSON.parse(envFile);

app.set('view engine', 'html');
app.set('port', envconfig.port);
app.set('httpsport', envconfig.httpsport);


app.set('Service_URL', envconfig.serviceUrl);
app.set('Alnylam_ServiceURL', envconfig.alnylamServiceurl);
app.set('URL_BackendAPI', envconfig.edocsBackendAPI);
app.set('Event_URL', envconfig.eventUrl);
app.set('HL_Url', envconfig.HLUrl);

app.set('URL_DotNetCoreBackendAPI', envconfig.edocsDotNetCoreBackendAPI);
app.set("URL_DotNetCoreDocumentPreviewUrl",app.get("URL_DotNetCoreBackendAPI") + "api/docuvieware");

app.set("URL_GetSearchUrl",app.get("Service_URL") + "/nbascout/service/process/search");
app.set("URL_GetAlnylamSearchUrl",app.get("Alnylam_ServiceURL") + "/nbascout/service/process/search");
app.set("URL_GetDocumentTreeUrl",app.get("URL_BackendAPI") + "api/v1/document");
app.set("URL_AutoSuggestUrl",app.get("Service_URL") + "/nbascout/service/suggest");
app.set("URL_AlnylamAutoSuggestUrl",app.get("Alnylam_ServiceURL") + "/nbascout/service/suggest");
app.set("URL_GetEventUrl",app.get("Event_URL") + "/signal/record/event");
app.set("URL_GetHLUrl",app.get("HL_Url") + "/text-highlighter-service/hilighted-file-content/");

app.set("URL_DocumentPreviewUrl",app.get("URL_BackendAPI") + "api/v1/doccontent");
app.set("URL_VersionPreviewUrl",app.get("URL_BackendAPI") + "api/v1/docversioncontent");
app.set("URL_EmailPreviewUrl",app.get("URL_BackendAPI") + "api/v2/document");


//21-01-2021
app.set("URL_GetDocumentVersions",app.get("URL_BackendAPI") + "api/v1/documentversions");
app.set("URL_GetDocument",app.get("URL_BackendAPI") + "api/v1/document/");
app.set("URL_DocVersionHistory",app.get("URL_BackendAPI") + "api/v1/docversionhistory");
app.set("URL_GetDocProperties",app.get("URL_BackendAPI") + "api/v1/getdocproperties");
app.set("URL_DocProperties",app.get("URL_BackendAPI") + "api/v1/docproperties");
app.set("URL_SaveMetaData",app.get("URL_BackendAPI") + "api/v1/metadata");
app.set("URL_GetMetaData",app.get("URL_BackendAPI") + "api/v1/metadata/");
app.set("URL_GetDocPath",app.get("URL_BackendAPI") + "api/v1/document/getdocpath");
app.set("URL_MailVersionContent",app.get("URL_BackendAPI") + "api/v1/mailversioncontent");




var solrRoutes = require('./solr/solrRoutes.js')(express.Router());

var solrClientHandler = function(req,res,next){   
    solr(app.get('env'),false,function(liveNode,err){
        if(liveNode != null){
            solrClientNode = liveNode;
            res.locals.solrLiveNode = solrClientNode;
            next();
        }else{
            next(err);
        }
    });
};

app.use('/Solr',solrClientHandler,solrRoutes);

var SearchSolrRoutes = require('./solr/SearchSolrRoutes.js')(express.Router());
app.use('/SearchSolr',SearchSolrRoutes);

var AutoSuggestSolrRoutes = require('./solr/AutoSuggestSolrRoutes.js')(express.Router());
app.use('/AutoSuggestSolr',AutoSuggestSolrRoutes);

var TreeSolrRoutes = require('./solr/TreeSolrRoutes.js')(express.Router());
app.use('/solrTree', TreeSolrRoutes);

var CollectionRoutes = require('./collections/CollectionRoutes.js')(express.Router());
app.use('/collections', CollectionRoutes);

app.get('/', function(req, res) {
	 res.header("Content-Type", "text/html;charset=utf-8 ");
	 res.sendFile(__dirname + '/public/views/index.html');
});

app.get('/clientConfig/:client', function(req, res) {
    let client = req.params.client;
    var clientConfigString = fs.readFileSync('./config/'+client+'.json', 'utf8');
    var clientConfig = JSON.parse(clientConfigString);
	 res.send(clientConfig);
});


app.get('/getEnvConfigs', function(req, res) {
	 res.send(envconfig);
});

app.get('/getDocContent/:filePath', function(req,res){
    var filePath = req.params.filePath.toString();
    function getFileName(url) {
        var matches = url.match(/\/([^\/?#]+)[^\/]*$/);
        if (matches && matches.length > 1) {
          let fileName = matches[1]
            return fileName;
        }
        return null;
      }
      var fileName = getFileName(filePath);
    let header = {
        // "Content-Type": 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        // "Content-Type": "application/vnd.openxmlformats-officedocument.presentationml.presentation",
        "Content-Type": "application/vnd.openxmlformats-officedocument.wordprocessing",
        "Content-Disposition": "inline;filename="+ fileName,
    };
    res.writeHead(200, header);
    request.get(filePath, function(error, response, body) {}).pipe(res);
});


app.get('/search', function(req, res) {
	 res.header("Content-Type", "text/html;charset=utf-8 ");
	 res.sendFile(__dirname + '/public/views/index.html');
});

var NotesRoutes = require('./notes/NotesRoutes.js')(express.Router());
app.use('/notes', NotesRoutes);

// app.get('/alnylam', function(req, res) {
//     res.header("Content-Type", "text/html;charset=utf-8 ");
//     res.sendFile(__dirname + '/public/views/index.html');
// });

// app.get('/nba', function(req, res) {
//     res.header("Content-Type", "text/html;charset=utf-8 ");
//     res.sendFile(__dirname + '/public/views/index.html');
// });

app.get('/login', function(req, res) {
    res.header("Content-Type", "text/html;charset=utf-8 ");
    res.sendFile(__dirname + '/public/views/index.html');
});

var request = require('request');

app.get('/getVersionPreview/:docId/:name/:url/:source/:client',function (req, res) {
    var docId = req.params.docId;
    var name = req.params.name;
    var url = req.params.url;
    var ConnectorSourceName = req.params.source;
    var client = req.params.client;
    var req = {
        "VersionLabel":name,
        "DocumentId": docId,
        "EncodedAbsUrl": url,
        "ConnectorSourceName": ConnectorSourceName,
        "ClientName": client
        }
        var body = JSON.stringify(req)
        var options = {
            url: app.get('URL_MailVersionContent'),
            body: body,
            headers: { 'content-type' : 'application/json'}
        };

    try {
        request.post(options, function (error, response, body) {
            var resBody = JSON.parse(body);
            var filepath = resBody.FilePath;
            request.get(filepath, function (error, response, body) {
             }).pipe(res);
         })
      }
      catch(err) {
        res.send("{}");
      }
})


app.get('/getDocPreview/:docId/:source/:client',function (req, res) {
    var docId = req.params.docId;
    var ConnectorSourceName = req.params.source;
    var client = req.params.client;
    try {
        var req = {
            "DocumentId": docId,
            "ConnectorSourceName": ConnectorSourceName,
            "ClientName": client
            }
        var body = JSON.stringify(req)
        var options = {
            url: app.get('URL_GetDocPath'),
            body: body,
            headers: { 'content-type' : 'application/json'}
        };
        request.post( options, function (error, response, body) {
        // request.get("https://sp16-nba.thedigitalgroup.com:9091/api/v1/document/" + docId, function (error, response, body) {
            var resBody = JSON.parse(body);
            var filepath = resBody.FilePath;
            request.get(filepath, function (error, response, body) {
             }).pipe(res);
         })
      }
      catch(err) {
        res.send("{}");
      }
})

var path = require('path');
app.use(express.static(path.join(__dirname, 'public/src')));

app.all('*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
});

var boxRouter = require('./BoxConnector/BoxAuthorization.js')(express.Router());
app.use('/Box',boxRouter);

var searchRouter = require('./searchModule/searchRouter.js')(express.Router());
app.use('/search',searchRouter);

var docTreeRouter = require('./docTreeModule/docTreeRouter.js')(express.Router());
var annotationRouter = require('./annotationModule/annotationRouter.js')(express.Router());
app.use('/docTree', docTreeRouter);
app.use('/annotation',annotationRouter);

var previewRouter = require('./docPreviewModule/docPreviewRouter.js')(express.Router());
app.use('/preview',previewRouter);

app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.send({"Error" : err});
});

http.createServer(app).listen(app.get('port'), function () {
    console.log('Express server listening on http port ' + app.get('port'));
});

// var sslOptions = {
//     key: fs.readFileSync('./ssl/3rdisearch_key.key'),
//     cert: fs.readFileSync('./ssl/3rdisearch_crt.crt')
// };

// https.createServer(sslOptions,app).listen(app.get('httpsport'), function () {
//     console.log('Express server listening on https port ' + app.get('httpsport'));
// });

module.exports = app;