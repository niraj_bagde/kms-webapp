'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class tdg_sharenotes_transaction extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            /* tdg_sharenotes_transaction.belongsTo(Model.tdg_usernotes, {
                foreignKey: 'id',
                as: "sharedWith",
                onDelete: 'CASCADE'
            }) */
        }
    };
    tdg_sharenotes_transaction.init({
        line_id: { type: DataTypes.INTEGER, allowNull: false },
        shared_with: DataTypes.STRING,
        shared_permissions: DataTypes.TEXT,
        shared_by: DataTypes.STRING,
        shared_date: DataTypes.DATE,
        share_notes: DataTypes.TEXT
    }, {
        sequelize,
        modelName: 'tdg_sharenotes_transaction',
    });
    return tdg_sharenotes_transaction;
};