'use strict';
const {
    Model
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class tdg_usernotes extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            /* tdg_usernotes.hasMany(Model.tdg_sharenotes_transaction, {
                foreignKey: 'line_id',
                as: "sharedWith",
                onDelete: 'CASCADE'
            })
            tdg_usernotes.belongsTo(Model.tdg_usernotes, {
                foreignKey: 'line_id',
                as: 'parent_line',
                onDelete: 'CASCADE'
            }) */
        }
    };
    tdg_usernotes.init({
        note_id: DataTypes.INTEGER,
        line_id: DataTypes.INTEGER,
        parent_line_id: DataTypes.INTEGER,
        author_id: DataTypes.STRING,
        line_data: DataTypes.BLOB,
        created_date: DataTypes.DATE,
        modified_by: DataTypes.STRING,
        modified_date: DataTypes.DATE
    }, {
        sequelize,
        modelName: 'tdg_usernotes',
    });
    return tdg_usernotes;
};