require('dotenv').config()

module.exports = {
  development: {
    url: "postgres://sa:''@10.10.1.39:5432/3RDICollaboration",
    dialect: 'postgres',
  },
  test: {
    url: "postgres://sa:''@10.10.1.39:5432/3RDICollaboration",
    dialect: 'postgres',
  },
  production: {
    url: "postgres://sa:''@10.10.1.39:5432/3RDICollaboration",
    dialect: 'postgres',
  },
}