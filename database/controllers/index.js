const models = require('../models');

const createNote = async (req, res) => {
    try {
        const notes = await models.tdg_usernotes.create(req.body);
        return res.status(201).json({
            notes,
        });
    } catch (error) {
        return res.status(500).json({ error: error.message })
    }
}

const getAllNotes = async (req, res) => {
    try {
        const notes = await models.tdg_usernotes.findAll({
            include: [
                {
                    model: models.tdg_sharenotes_transaction,
                    as: 'sharedWith'
                },
                {
                    model: models.tdg_usernotes,
                    as: 'parent_line'
                }
            ]
        });
        return res.status(200).json({ notes });
    } catch (error) {
        return res.status(500).send(error.message);
    }
}

const getSingleNote = async (req, res) => {
    try {
        const { noteId } = req.params;
        const note = await models.tdg_usernotes.findOne({
            where: { note_id: noteId },
            include: [
                {
                    model: models.tdg_sharenotes_transaction,
                    as: 'sharedWith',
                },
                {
                    model: models.tdg_usernotes,
                    as: 'parent_line'
                }
            ]
        });
        if (note) {
            return res.status(200).json({ note });
        }
        return res.status(404).send('note with the specified ID does not exists');
    } catch (error) {
        return res.status(500).send(error.message);
    }
}

const getUserNote = async (req, res) => {
    try {
        console.log("req.params")
        console.log(req.body)
        const userEmail = req.body.value;
        console.log("userEmail")
        console.log(userEmail)
        const note = await models.tdg_usernotes.findOne({
            where: { author_id: userEmail },
            /* include: [
                {
                    model: models.tdg_sharenotes_transaction,
                    as: 'sharedWith',
                },
                {
                    model: models.tdg_usernotes,
                    as: 'parent_line'
                }
            ] */
        });
        if (note) {
            console.log("note")
            console.log(note)
            return res.status(200).json({ note });
        }
        return res.status(404).send('note with the specified ID does not exists');
    } catch (error) {
        console.log("error")
        console.log(error)
        return res.status(500).send(error.message);
    }
}

const updateNote = async (req, res) => {
    try {
        const { noteId } = req.params;
        const [updated] = await models.tdg_usernotes.update(req.body, {
            where: { note_id: noteId }
        });
        if (updated) {
            const updatedNote = await models.tdg_usernotes.findOne({ where: { id: postId } });
            return res.status(200).json({ note: updatedNote });
        }
        throw new Error('Note not found');
    } catch (error) {
        return res.status(500).send(error.message);
    }
};

const deleteNote = async (req, res) => {
    try {
        const { noteId } = req.params;
        const deleted = await models.tdg_usernotes.destroy({
            where: { note_id: noteId }
        });
        if (deleted) {
            return res.status(204).send("Note deleted");
        }
        throw new Error("Note not found");
    } catch (error) {
        return res.status(500).send(error.message);
    }
};

module.exports = {
    createNote,
    getAllNotes,
    getSingleNote,
    getUserNote,
    updateNote,
    deleteNote
}