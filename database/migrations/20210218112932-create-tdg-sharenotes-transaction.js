'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('tdg_sharenotes_transactions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      line_id: {
        type: Sequelize.INTEGER
      },
      shared_with: {
        type: Sequelize.STRING
      },
      shared_permissions: {
        type: Sequelize.TEXT
      },
      shared_by: {
        type: Sequelize.STRING
      },
      shared_date: {
        type: Sequelize.DATE
      },
      share_notes: {
        type: Sequelize.TEXT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('tdg_sharenotes_transactions');
  }
};