var request = require('request');
var app = require('../server');
var apiDocTree = function () {
};

apiDocTree.prototype.getDocTreeNodes = function (req, callback) {
    var body = JSON.stringify(req.body);
    var docTreeUrl = app.get('URL_GetDocumentTreeUrl');
    request.get(docTreeUrl, function (error, response, body) {
        if (callback) {
            try {
                if(body) {
                    callback(JSON.parse(body));
                } else {
                    callback(JSON.parse("{}"));
                }
                callback = null;
              }
              catch(err) {
                callback(JSON.parse("{}"));
                callback = null;
              }
        }
    });
};

apiDocTree.prototype.getDocTreeWithVersions = function (req, callback) {
    var body = JSON.stringify(req.body);
     var docTreeUrl = app.get('URL_GetDocumentVersions');
    request.get(docTreeUrl, function (error, response, body) {
        if (callback) {
            try {
                if(body) {
                    callback(JSON.parse(body));
                } else {
                    callback(JSON.parse("{}"));
                }
                callback = null;
              }
              catch(err) {
                callback(JSON.parse("{}"));
                callback = null;
              }
        }
    });
};

apiDocTree.prototype.getDocPreview = function (req, callback) {
    var body = JSON.stringify(req.body);
    var docPreviewUrl = app.get('URL_GetDocument');
    request.post(docPreviewUrl + req.query.documentId, function (error, response, body) {
        // console.log("body");
        // console.log(body);
        if (callback) {
            try {
                if(body) {
                    callback(JSON.parse(body));
                } else {
                    callback(JSON.parse("{}"));
                }
                callback = null;
            } catch(err) {
                callback(JSON.parse("{}"));
                callback = null;
            }
        }
    });
}


apiDocTree.prototype.getDocVersions = function (req, callback) {
    var body = JSON.stringify(req.body);
    var options = {
        url: app.get('URL_DocVersionHistory'),
        body: body,
        headers: { 'content-type' : 'application/json'}
    };
    request.post(options, function (error, response, body) {
        if (callback) {
            try {
                if(body) {
                    callback(JSON.parse(body));
                } else {
                    callback(JSON.parse("{}"));
                }
                callback = null;
            } catch(err) {
                callback(JSON.parse("{}"));
                callback = null;
            }
        }
    });
}

apiDocTree.prototype.getDocProperties = function (req, callback) {
    var body = JSON.stringify(req.body);
    var options = {
        url: app.get('URL_GetDocProperties'),
        body: body,
        headers: { 'content-type' : 'application/json'}
    };
    request.post(options, function (error, response, body) {
        if (callback) {
            try {
                if(body) {
                    callback(JSON.parse(body));
                } else {
                    callback(JSON.parse("{}"));
                }
                callback = null;
            } catch(err) {
                callback(JSON.parse("{}"));
                callback = null;
            }
        }
    });
}


apiDocTree.prototype.saveDocProperties = function (req, callback) {
    var body = JSON.stringify(req.body);

    var options = {
        url: app.get('URL_DocProperties'),
        body: body,
        headers: { 'content-type' : 'application/json'}
    };
    request.post(options, function (error, response, body) {
        if (callback) {
            try {
                if(body) {
                    callback(JSON.parse(body));
                } else {
                    callback(JSON.parse("{}"));
                }
                callback = null;
            } catch(err) {
                callback(JSON.parse("{}"));
                callback = null;
            }
        }
    });
}
module.exports = apiDocTree;