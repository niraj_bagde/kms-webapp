var apiDocTree = require('./apiDocTree');

var searchRouter = function (router) {
    router.get('/getDocTreeNodes', getDocTreeNodes);
    router.get('/getDocTreeWithVersions', getDocTreeWithVersions);
    router.get('/getDocPreview', getDocPreview);
    router.post('/getDocVersions', getDocVersions);
    router.post('/getDocProperties', getDocProperties); //
    router.post('/saveDocProperties', saveDocProperties);
    return router;
};

var getDocTreeNodes = function (req, res, next) {
    var apiDocTreeObj = new apiDocTree();
    apiDocTreeObj.getDocTreeNodes(req, function (result, error) {
        res.json(result);
    });

};

var getDocTreeWithVersions = function (req, res, next) {
    var apiDocTreeObj = new apiDocTree();
    apiDocTreeObj.getDocTreeWithVersions(req, function (result, error) {
        res.json(result);
    });
};

var getDocPreview = function (req, res, next) {
    var apiDocTreeObj = new apiDocTree();
    apiDocTreeObj.getDocPreview(req, function (result, error) {
        res.json(result);
    });
};

var getDocVersions = function (req, res, next) {
    var apiDocTreeObj = new apiDocTree();
    apiDocTreeObj.getDocVersions(req, function (result, error) {
        res.json(result);
    });
};

var getDocProperties = function (req, res, next) {
    var apiDocTreeObj = new apiDocTree();
    apiDocTreeObj.getDocProperties(req, function (result, error) {
        res.json(result);
    });
};

var saveDocProperties = function (req, res, next) {
    var apiDocTreeObj = new apiDocTree();
    apiDocTreeObj.saveDocProperties(req, function (result, error) {
        res.json(result);
    });
};
module.exports = searchRouter;