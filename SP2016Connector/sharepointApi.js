var request = require('request');
var app = require('../server');
var apiDocPreview = function () {
};

apiDocPreview.prototype.docPreview = function (req, callback) {
    var body = JSON.stringify(req.body);
  
    // url:app.get('URL_DocumentPreviewUrl')//URL_DotNetCoreDocumentPreviewUrl
    var options = {
        url:app.get('URL_DocumentPreviewUrl'), 
        body: body,
        headers: { 'content-type' : 'application/json'}
    };
    request.post(options, function (error, response, body) {
        if (callback) {
            try {
                if(body) {
                    callback(JSON.parse(body));
                } else {
                    callback(JSON.parse("{}"));
                }
                //callback = null;
            } catch(err) {
                callback(JSON.parse("{}"));
                callback = null;
            }
        }
    });
}

apiDocPreview.prototype.versionPreview = function (req, callback) {
    var body = JSON.stringify(req.body);

    var options = {
        url: app.get('URL_VersionPreviewUrl'),
        body: body,
        headers: { 'content-type' : 'application/json'}
    };
    request.post(options, function (error, response, body) {
        if (callback) {
            try {
                if(body) {
                    callback(JSON.parse(body));
                } else {
                    callback(JSON.parse("{}"));
                }
                callback = null;
            } catch(err) {
                callback(JSON.parse("{}"));
                callback = null;
            }
        }
    });
}


apiDocPreview.prototype.emailPreview = function (req, callback) {
    var body = JSON.stringify(req.body);

    var options = {
        url: app.get('URL_EmailPreviewUrl'),
        body: body,
        headers: { 'content-type' : 'application/json'}
    };
    request.post(options, function (error, response, body) {
        if (callback) {
            try {
                if(body) {
                    callback(JSON.parse(body));
                } else {
                    callback(JSON.parse("{}"));
                }
                callback = null;
            } catch(err) {
                callback(JSON.parse("{}"));
                callback = null;
            }
        }
    });
}

module.exports = apiDocPreview;