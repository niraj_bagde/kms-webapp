var apiDocPreview = require('./../SP2016Connector/sharepointApi');
var fs = require("fs");

var app = require("../server");
var fileName = app.get('env') + ".json";
var configFile = fs.readFileSync('./config/' + fileName, 'utf8');
var config = JSON.parse(configFile);

apiDocPreview = new apiDocPreview();
var previewRouter = function (router) {
    router.post('/docPreview', docPreview);
    router.post('/versionPreview', versionPreview);
    router.post('/emailPreview', emailPreview);
    return router;
};

//Document Preview
var docPreview = function (req, res, next) {
    req.body["ConnectorSourceName"]= req.body["source_type"];
    req.body["ClientID"]=config.clientID;
    req.body["ClientSecret"]=config.clientSecret;
    apiDocPreview.docPreview(req, function (result, error) {
          res.json(result);
      });
};

//Version Preview
var versionPreview = function (req, res, next) {

    req.body["ConnectorSourceName"]= req.body["source_type"];
    req.body["ClientID"]=config.clientID;
    req.body["ClientSecret"]=config.clientSecret;
     apiDocPreview.versionPreview(req, function (result, error) {
        res.json(result);
    });
};

//Email Preview
var emailPreview = function (req, res, next) {

    req.body["ConnectorSourceName"]= req.body["source_type"];
    req.body["ClientID"]=config.clientID;
    req.body["ClientSecret"]=config.clientSecret;
     apiDocPreview.emailPreview(req, function (result, error) {
        res.json(result);
    });  
};

module.exports = previewRouter;