import React, { useState, Component } from 'react';
// import 'bootstrap/dist/css/bootstrap.min.css';
// import './app.login.css';
import logo from '../images/logo-blue.png';
//import hero from '../images/hero-login.svg';
import { useHistory } from "react-router-dom";


const Login = () => {
  const [modal, setModal] = useState(true);
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [domain, setDomain] = useState("");
  const history = useHistory();

  const handleClick = () => {
    history.push("/masterdata");
  }

  const handleSubmit = () => {

  }

  return (
    <div  className="mylogincomponent">
      <div className="modal-header" style={{ padding: "14px 15px 15px", marginTop: "-15px", borderBottom: "0px" }}>
        <button type="button" className="close" data-dismiss="modal" onClick={()=> setModal(false)} aria-label="Close" style={{ outline: "none" }}>
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 className="modal-title" style={{ fontSize: "20px", color: "#fff", fontWeight: "500" }}>Login </h4>
      </div>
      <div className="modal-body" style={{ border: "none", paddingTop: "0px", background: "#fff", height: "190px" }}>
        <form name="loginForm">
          <div className="row form_one" style={{ margin: "50px 0px 20px 0px" }}>
            <div className="col-lg-3 text-right">
              <label>User name<span className="requiredRed">*</span>:
                              </label>
            </div>
            <div className="col-lg-9" style={{ paddingLeft: "0px" }}>
              <input type="text" maxLength="32" className="form-control textbox_radius" value={username} onChange={(e) => setUsername(e.target.value)}
                required />
            </div>
          </div>
          <div className="row form_one" style={{ margin: "20px 0px" }}>
            <div className="col-lg-3 text-right">
              <label>Password<span className="requiredRed">*</span>:
                              </label>
            </div>
            <div className="col-lg-9" style={{ paddingLeft: "0px" }}>
              <input type="password" maxLength="20" className="form-control textbox_radius" value={password} onChange={(e) => setPassword(e.target.value)}
                required />
            </div>
          </div>
          <div className="row form_one" style={{ margin: "20px 0px 50px 0px" }}>
            <div className="col-lg-3 text-right">
              <label>Domain<span className="requiredRed">*</span>:
                              </label>
            </div>
            <div className="col-lg-9" style={{ paddingLeft: "0px" }}>
              <select className="form-control textbox_radius" required>
                <option value={domain} onChange={(e) => setDomain(e.target.value)}>Select</option>
                <option value="thedigitalgroup">THEDIGITALGROUP</option>
                <option value="nba">NBA</option>
              </select>
            </div>
            <div className="col-lg-1"></div>
          </div>
        </form>
      </div>
      <div className="modal-footer" style={{ padding: "10px 0px" }}>
        <button id="Button2" type="button" className="btn btn-default pull-right" style={{ borderRadius: "30px" }}
          onClick={()=>setModal(false)}>
          Close
              </button>
        <button type="button" className="btn btn-primary pull-right" style={{ borderRadius: "30px", marginRight: "15px", fontSize: "14px" }}
          onClick={handleSubmit}>
          Login
        </button>

      </div>
    </div>
  );

}
export default Login;