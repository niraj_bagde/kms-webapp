import React, { Component, useState } from "react";
import { Modal, Row, Container, Col, Button, Form } from "react-bootstrap";
import "./app.settings.css";
import axios from "axios";
import Tree, { TreeNode } from 'rc-tree';
import TreeDataHelper from '../helpers/TreeDataHelper';
import 'rc-tree/assets/index.css';
import DocumentLibrarySolr from "../app.documentLibrary/app.documentLibrarySolr";

class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      spAuthorized: this.props.spAuthorized ? this.props.spAuthorized : false,
      username: "",//"richas@dgi",
      password:"",//"GY@ca49nst",
      activeTab: "box",
      showSpAuthForm: false,
      solrTreeData: [],
      confirmDisconnect: false,
      boxAuthorized: props.boxAuthorized,
      SPUserName: props.SPUserName
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value, spAuthorized: false });
  }
  handleSubmit() {
    this.props.onSuccess(this.state.username,this.state.password);
    this.setState({showSpAuthForm: false, spAuthorized: true, SPUserName: this.state.username});
  }

  componentDidMount() {
    this.getSolrTreeData();
    // this.set
  }

  componentWillMount() {
  }

  componentWillUnmount() {}

  getSolrTreeData = () => {
    let dgiUsername = this.props.dgiUserName.split("@")[0];
    let AT = this.props.accessToken.toString();
    let acl = [];
    if (this.props.boxUserId != '') {
        acl.push("U_" + this.props.boxUserId);
    }

    if (this.props.boxUserGroupIds.length !== 0) {
        acl = acl.concat(this.props.boxUserGroupIds);
    }
    let spUserName; // = "richas@dgi";
    if (this.props.SPUserName !== undefined && this.props.SPUserName !== "" && this.props.SPUserName !== null) {
        spUserName = this.props.SPUserName;
    }
    var solrRequest = {
        "searchQuery": "*:*",
        "accessToken": [AT],
        // "accessToken": ["nioA5tgsn6TPD8gT8wAyRoj63cGzoN6f"],
        "dataset": this.props.solrCollection,
        "client": this.props.client,
        "acl": acl,
        // "acl": ["U_13589537292"],
        "tokensFound": true,
        "docPath": "*",
        "manifoldUsername": spUserName
        // "manifoldUsername": "richas@dgi"
    }
    var compThis = this;
    axios.post("/solrTree/getTreeStructFromSolr", solrRequest).then((response) => {
        try {
            // return this.processResponseFromSolr(response, selectedKey, nodeTitle, serviceType, resolve);
            let treeRoots = response.data.facet_counts.facet_fields.doc_path_hierarchy;
            let treeLeafNodes = response.data.response.docs;
            let toBeExpanded = [];
            let formattedTreeNodes = TreeDataHelper.formatSolrDataForTree(treeRoots, toBeExpanded, this.props.serviceTypes, treeLeafNodes, true);
            
            let nodeToBeselected = {
                node: formattedTreeNodes.docTreeRoots[0]
            } 
            this.treeNodeSelected("", nodeToBeselected);
            compThis.setState({
                isLoading: false,
                solrTreeData: formattedTreeNodes.docTreeRoots,
                defaultExpanded: [formattedTreeNodes.docTreeRoots[0]["key"]],
                checkedKeys: ['0-0'],
            }, function () {
                console.log("I am called")
            });
        }
        catch (err) {
            console.log(err);
            this.setState(
                {
                    isLoading: false,
                });
        }
    })
  }

  AuthorizeBoxUser = () => {
    axios.get("/Box/authorizeUser").then((response, error) => {
      if(response.data) {
        window.open(response.data, "_self")

      }
    })
  }

  showSPCredsFormClick = () => {
    this.setState({showSpAuthForm: true})
  }
  handleCancel = () => {
    this.setState({showSpAuthForm: false})
  }

  treeNodeSelected = () => {
    console.log("I am called")
  }

  showConfirmDisconnect = () => {
    // this.setState({confirmDisconnect: true})
    if (confirm("Are you sure want to disconnect from Box.com?")) {
      this.disconnectBox()
    } else {
      return false;
    }
  }
  showConfirmSPDisconnect = () => {
    // this.setState({confirmDisconnect: true})
    if (confirm("Are you sure want to disconnect from SharePoint 2016?")) {
      this.disconnectSharePoint()
    } else {
      return false;
    }
  }

  disconnectBox = () => {
    this.setState({boxAuthorized: false});
    this.props.removeBoxAuth("");
    // this.getSolrTreeData();
  }

  disconnectSharePoint = () => {
    this.setState({spAuthorized: false, SPUserName: ""});
    this.props.removeSharePointAuth()
  }
  
  render() {
    // console.log("this.state.boxAuthorized")
    // console.log(this.props.boxAuthorized)
    let mainTreeStructure = <img src={this.props.loaderIcon} className="loader"></img>
    if (this.state.solrTreeData.length > 0) {
      // firstColumnClass = "col-md-3 doc-lib-col doc-lib-first-col";
      const { solrTreeData } = this.state;
      mainTreeStructure = <Tree
          className="myCls"
          showLine
          autoExpandParent={true}
          // onExpand={this.onExpand}
          treeData={solrTreeData}
          onSelect={this.treeNodeSelected}
          defaultExpandedKeys={this.state.defaultExpanded}
          // loadData={this.loadTreeData}
      />
    }

    let showDocumentLib = false;
    if (this.state.boxAuthorized || this.state.spAuthorized) {
      showDocumentLib = true;
    }
    return (
      <Modal
        {...this.props}
        aria-labelledby="contained-modal-title-vcenter"
        size="lg"
        dialogClassName="settings-dialog"
        centered
      >
        <Modal.Header closeButton className="Settingspopup">
          <Modal.Title id="contained-modal-title-vcenter">Integrations</Modal.Title>
        </Modal.Header>
        <Modal.Body className="settings">
        
        {/*<div style={{textAlign: "right"}}>
        <span> 
          <a href="public/src/downloadfiles/publish_outlook.zip" download>
          <img style={{display:"none"}} src="public/src/downloadfiles/publish_outlook.zip" />Download Outlook Add-In Version 17-Feb-2021</a>
          </span>
          </div>*/}

          <table class="table table-bordered">
  <thead>
    {/* <tr>
      <th scope="col">Dataset</th>
      <th scope="col">Username</th>
      <th scope="col">Password</th>
      <th scope="col">Status</th>
    </tr> */}
    <tr>
      <th scope="col" className="settings-header">Dataset</th>
      <th scope="col" className="settings-header action-column">Action</th>
      <th scope="col" className="settings-header status-column">Status</th>
    </tr>
  </thead>
  <tbody>
    {this.props.storage.indexOf('Box') > -1 && 
      <tr>
      <th scope="row"><span className="box-logo"></span></th>
      {/* {<td>
        {this.props.boxAuthorized && <span>{this.props.boxAuthUser}</span>}
      </td>} */}
      <td className="centered-aligned-text">
        {!this.state.boxAuthorized && <Button className="btn btn-primary connect-to-datasource" onClick={(e) => this.AuthorizeBoxUser()} title="Connect to SharePoint" >Connect</Button> }
        {this.state.boxAuthorized && <Button onClick={this.showConfirmDisconnect} className="btn btn-danger connect-to-datasource">Disconnect</Button>
        }
      </td>
      {this.state.boxAuthorized && <td><div><span className="green-text">Connected</span> ({this.props.boxAuthUser})</div></td>}
      {!this.state.boxAuthorized && <td><div><span className="red-text">Not Connected</span></div></td>}
      {/* <td style={{"textAlign": "center"}}>
       { this.props.boxAuthorized==true 
        ?  
        <a 
          title="You are currently authorized to access Box"
          style={{"cursor": "inline","font-size": "20px","color":"#029000","display":"inline-block"}}
        >
        <i class="fa fa-sign-in" aria-hidden="true" 
          style={{"cursor": "pointer", "marginRight": "10px", "color": "#4b4646"}}
          onClick={(e) => this.AuthorizeBoxUser()} 
          title="Click here to authorize for Box"
        ></i>
        <i class="fa fa-check-square-o" aria-hidden="true"></i></a>
         : 
         <span
          onClick={(e) => this.AuthorizeBoxUser()} 
          title="Click here to authorize for Box"
          style={{"cursor": "pointer"}}
        >
          <i class="fa fa-sign-in" aria-hidden="true" 
            style={{"cursor": "pointer", "marginRight": "10px", "color": "#4b4646", "font-size": "20px"}}
          ></i>
        </span> 
       } 
        </td> */}
      </tr>
    }
    {this.props.storage.indexOf('Sharepoint') > -1 && 
    <tr>
      <th scope="row"><span className="sharepoint-logo"></span></th>
      
      {!this.state.showSpAuthForm && <td  className="centered-aligned-text">
        {!this.state.spAuthorized && <Button className="btn btn-primary connect-to-datasource" onClick={this.showSPCredsFormClick} title="Connect to SharePoint" >Connect</Button>}
        {this.state.spAuthorized && <Button onClick={this.showConfirmSPDisconnect} className="btn btn-danger connect-to-datasource">Disconnect</Button>}
      </td>}
      {this.state.showSpAuthForm && <td><div>
        <Form.Control
          type="text"
          placeholder="username"
          name="username"
          value={this.state.username}
          onChange={this.handleChange}
          className="bottom-margin"
        />
        <Form.Control
            type="password"
            placeholder="password"
            name="password"
            value={this.state.password}
            onChange={this.handleChange}
            className="bottom-margin"
          />
        </div>
        <span
          onClick={this.handleSubmit}
          title="Click here to authorize for SharePoint"
          style={{"cursor": "pointer", marginLeft: "5px"}}
        >
          <i class="fa fa-sign-in" aria-hidden="true" 
            style={{"cursor": "pointer", "marginRight": "10px", "color": "#4b4646", "font-size": "20px"}}
           ></i>
        </span>
        <span
          onClick={this.handleCancel}
          title="Cancel"
          style={{"cursor": "pointer", marginLeft: "5px"}}
        >
          <i class="fa fa-times" aria-hidden="true" 
            style={{"cursor": "pointer", "marginRight": "10px", "color": "#4b4646", "font-size": "20px"}}
           ></i>
        </span>
        
        </td>}
      {this.state.spAuthorized && <td><div><span className="green-text">Connected</span> ({this.props.SPUserName})</div></td>}
      {!this.state.spAuthorized &&  <td><div><span className="red-text">Not Connected</span></div></td>}
      {/* <td>
        <Form.Control
          type="text"
          placeholder="james@dgi"
          name="username"
          value={this.state.username}
          onChange={this.handleChange}
        />
        </td> */}
      {/* <td>
      <Form.Control
            type="password"
            placeholder="password"
            name="password"
            value={this.state.password}
            onChange={this.handleChange}
          />
      </td> */}
      {/* <td style={{"textAlign": "center"}}>
       { this.state.spAuthorized==true 
        ?  
        <a style={{"cursor": "inline","font-size": "20px","color":"#029000","display":"inline-block"}}
           title="You are currently authorized to access SharePoint"
        >
          <i class="fa fa-check-square-o" aria-hidden="true"></i>
        </a>
         : 
         <span
          onClick={this.handleSubmit}
          title="Click here to authorize for SharePoint"
          style={{"cursor": "pointer"}}
        >
          <i class="fa fa-sign-in" aria-hidden="true" 
            style={{"cursor": "pointer", "marginRight": "10px", "color": "#4b4646", "font-size": "20px"}}
           ></i>
        </span>
      } 
      </td> */}
    </tr>
    }
  </tbody>
</table>
        {/*<ul className="nav nav-tabs" style={{"marginTop": "-10px"}}>
            <li onClick={()=>this.setState({activeTab: 'box'})}
                className={this.state.activeTab == 'box' ? "active" : ""}
                style={{"textAlign":"center", "width":"200px"}}
                >
                <a><span className="box-logo"></span></a>
            </li>
            <li onClick={()=>this.setState({activeTab: 'sharepoint'})}
                className={this.state.activeTab == 'sharepoint' ? "active" : ""}
                style={{"textAlign":"center", "width":"200px"}}
                >
                <a><span className="sharepoint-logo"></span></a>
            </li>
        </ul>*/}
        {/* { this.state.activeTab=='sharepoint' ?  */}
          {/* <Form className="inner-form">
            <p style={{marginBottom:"20px", 'marginTop':"25px"}}>Please enter credentials for Sharepoint</p>
          <Form.Group as={Row} controlId="formHorizontalEmail">
            <Form.Label column md={3}>
              Username
            </Form.Label>
            <Col sm={8}>
              
            </Col>
          </Form.Group>
          <Form.Group as={Row} controlId="formHorizontalPassword">
            <Form.Label column md={3}>
              Password
            </Form.Label>
            <Col sm={8}>
              <Form.Control
                type="password"
                placeholder="anything"
                name="password"
                value={this.state.password}
                onChange={this.handleChange}
              />
            </Col>
          </Form.Group>
        </Form> */}
{/* // :

        // <Form className="inner-form">
        // <Form.Group as={Row} controlId="formHorizontalEmail">
        // <Col sm={12}>
        //  <p style={{'marginTop':"20px"}}>Please <a onClick={(e) => this.AuthorizeBoxUser()} style={{"cursor": "pointer"}}><strong>click here</strong></a> to authorize Box</p>
        // <Button variant="primary" size="lg" style={{"marginLeft": "170px"}} onClick={(e) => this.AuthorizeBoxUser()}>
        //      authorize box
        // </Button>
        // </Col>
        // </Form.Group>
        // </Form>

        // } */}
        {showDocumentLib && <div className="tree-container">
          {/* {mainTreeStructure} */}
          <DocumentLibrarySolr  
          /* documentID={this.state.selectedDoc.file_id} */
            SPUserName={this.state.SPUserName}
            eventAppId={this.props.eventAppId}
            loaderIcon={this.props.loaderIcon}
            serviceType={this.props.serviceType}
            solrCollection={this.props.solrCollection}
            userEmail={this.props.userEmail}
            dgiUserName={this.props.dgiUserName} 
            mainTreeData={this.props.mainTreeList}
            source={this.props.sources}
            client={this.props.client}
            versionTreeData={this.props.versionTreeList}
            pageNo={this.props.pageNo}
            searchQuery={this.props.searchQuery}
            userIP={this.props.userIP}
            sessionId={this.props.sessionId}
            documentID={this.props.documentID}
            
            boxUserGroupIds={this.props.boxUserGroupIds}
            boxUserId={this.props.boxUserId}
            accessToken={this.props.accessToken}
            refreshToken={this.props.refreshToken}
            edocsBackendAPI={this.props.edocsBackendAPI}
            isFromSettings={true}
          />
        </div>}
        </Modal.Body>
        <Modal.Footer>
          <Form.Group as={Row}>
            <Col md={12} right>
              {/* <Button variant="primary" onClick={this.handleSubmit}>
                OK
              </Button> */}
              {/* <Button variant="secondary" onClick={this.props.onHide}>
                Cancel
              </Button> */}
            </Col>
          </Form.Group>
        </Modal.Footer>
        {this.state.confirmDisconnect && <div id="confirm" class="modal">
            <div class="modal-body">
                Are you sure?
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-danger" onClick={this.disconnectBox} id="delete">Disconnect</button>
                <button type="button" data-dismiss="modal" class="btn">Cancel</button>
            </div>
        </div>}
      </Modal>
    );
  }
}

export default Settings;
