import React from 'react';
import TreeDataHelper from './TreeDataHelper';
var allRandonStrings = [];
class CollectionHelper {

    static collectionTreeAssembly(collectionData, addRootCollection, addChildCollection, showAddBtns) {
        let collectionTree = [], mappedArray = {}, arrElement, mappedElement;

        if (showAddBtns) {
            let rootCollectionAdd = <span><button title="Add Collection" onClick={addRootCollection} className="root-collection-add">
                &lt; + Add Collection&gt; </button>
            </span>

            collectionTree = [{
                "id": "0",
                "key": this.randomStringGenerator(),
                "title": rootCollectionAdd,
                "parentId": "",
                "isLeaf": false,
                "isCollectionNode": true,
                "icon": <i className="fa fa-bars collection-bars" />
            }]
        }


        collectionData.forEach((collection, index) => {
            let children = [];
            if (showAddBtns) {
                let childCollectionAdd = <span><button title="Add Collection" onClick={() => addChildCollection(collection)} className="root-collection-add">
                    &lt; + Add Collection&gt; </button>
                </span>
                children = [
                    {
                        "id": "0",
                        "key": this.randomStringGenerator(),
                        "title": childCollectionAdd,
                        "parentId": "",
                        "isLeaf": false,
                        "isCollectionNode": true,
                        "icon": <i className="fa fa-bars collection-bars" />
                    }
                ];
            }

            let collectionNode = {
                "id": collection.id,
                "key": collection.id,
                "title": collection.name,
                "parentId": collection.parent_id,
                "owner": collection.owner,
                "documentInfo": collection.document_info,
                "createdDate": collection.created_date,
                "modifiedBy": collection.modified_by,
                "modifiedDate": collection.modified_date,
                "isLeaf": false,
                "isCollectionNode": true,
                "icon": <i className="fa fa-bars collection-bars" />
            }

            arrElement = collectionNode;
            mappedArray[collection.id] = arrElement;
            mappedArray[collection.id]["children"] = children;
        });

        for (var id in mappedArray) {
            if (mappedArray.hasOwnProperty(id)) {
                mappedElement = mappedArray[id];

                if (mappedElement.parentId !== "0") {
                    if (mappedArray[mappedElement["parentId"]] !== undefined) {
                        mappedArray[mappedElement["parentId"]]["children"].push(mappedElement);
                    }
                } else {
                    /* console.log("collection.document_info")
                    console.log(collection.document_info) */
                    collectionTree.push(mappedElement);
                }
            }
        }

        collectionTree = this.addDocumentsToTree(collectionData, collectionTree);

        return collectionTree;
    }

    static addDocumentsToTree(collectionData, collectionTree) {
        collectionData.forEach(collection => {
            if (collection.document_info.length > 0) {
                collectionTree.forEach(treeNode => {
                    let collectionTreeNode = this.searchTreeWithId(treeNode, collection.id);
                    if (collectionTreeNode !== null) {
                        collection.document_info.forEach((document, index) => {
                            let docNode = document;
                            docNode.key = document.file_id + collection.id + index
                            docNode.title = document.NameInCollection;
                            docNode.isCollectionNode = true;
                            let fileNameExt = document.doc_path.split(".");
                            let fileExt = fileNameExt[fileNameExt.length - 1];
                            let nodeIcon = TreeDataHelper.getFileExtIcon(fileExt);
                            docNode.icon = nodeIcon;
                            docNode.isLeaf = true;
                            docNode.isAddLeaf = false;
                            docNode.serviceType = document.source_type;

                            collectionTreeNode.children.push(docNode);
                        })
                    }
                })
            }
        });

        return collectionTree;
    }

    static searchTreeWithId(element, searchId) {
        if (element !== undefined) {
            if (element.id == searchId) {
                return element;
            } else if (element.children != null) {
                var i;
                var result = null;
                for (i = 0; result == null && i < element.children.length; i++) {
                    result = this.searchTreeWithId(element.children[i], searchId);
                }
                return result;
            }
        }
        return null;
    }

    static findAllParents(node, searchForKey) {

        // If current node name matches the search name, return
        // empty array which is the beginning of our parent result
        if (node.key === searchForKey) {
            return []
        }

        // Otherwise, if this node has a tree field/value, recursively
        // process the nodes in this tree array
        if (Array.isArray(node.children)) {

            for (var treeNode of node.children) {

                // Recursively process treeNode. If an array result is
                // returned, then add the treeNode.name to that result
                // and return recursively
                const childResult = this.findAllParents(treeNode, searchForKey)

                if (Array.isArray(childResult)) {
                    return [treeNode.key].concat(childResult);
                }
            }
        }
    }


    static randomStringGenerator() {
        let length = 5;
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        if (allRandonStrings.indexOf(result) > -1) {
            this.randomStringGenerator();
        }
        allRandonStrings.push(result);
        return result;
    }

}

export default CollectionHelper;