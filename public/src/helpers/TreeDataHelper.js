
import React from 'react';

class TreeDataHelper {
    constructor(con) {
        this.config = con;
    }

    static formatDocListForRcTree(docTreeData, currentFileName) {
        docTreeData.forEach((node, index) => {
            node.key = node["$id"];
            node.title = node["Name"];
            if (node.Members.length > 0) {
                node.children = node.Members
                this.formatDocListForRcTree(node.children, currentFileName);
            } else {
                let fileNExt = node.Name.split(".");
                if (fileNExt.length > 1) {
                    let fileExt = fileNExt[1];
                    if (fileExt === "msg") {
                        node.icon = <i className="fa fa-envelope blue-icons" aria-hidden="true"></i>
                    } else if (fileExt === "doc" || fileExt === "docx") {
                        node.icon = <i className="fa fa-file-word-o blue-icons" aria-hidden="true"></i>
                    } else if (fileExt === "xls" || fileExt === "xlsx") {
                        node.icon = <i className="fa fa-file-excel-o blue-icons" aria-hidden="true"></i>
                    } else {
                        node.icon = <i className="fa fa-file-pdf-o blue-icons" aria-hidden="true"></i>
                    }
                }

                if (node.Name === currentFileName) {
                    node.className = "file-node-from-tree"
                }
            }
        });

        return docTreeData;
    }

    static formatSolrDataForTree(solrDocFolders, toBeExpanded, serviceType, treeDocuments, isFromSearch) {
        let docTreeRoots = [];
        let usedFlds = [];

        solrDocFolders.forEach((eachFolder, index) => {
            if (isNaN(eachFolder)) {
                let sourceFolder; // = eachFolder.doclist.docs[0]["source_type"];
                let folderpathString; // = sourceFolder + "/" + eachFolder.groupValue
                let splitted; // = folderpathString.split("/");
                if (!isFromSearch) {
                    sourceFolder = eachFolder.doclist.docs[0]["source_type"];
                    if (sourceFolder === "box") {
                        sourceFolder = "Box.com";
                    } else if (sourceFolder === "sharepoint") {
                        sourceFolder = "SharePoint 2016";
                    }
                    folderpathString = sourceFolder + "/" + eachFolder.groupValue
                    splitted = folderpathString.split("/");
                } else {
                    sourceFolder = this.getSourceType(eachFolder, treeDocuments);
                    folderpathString = sourceFolder + "/" + eachFolder;
                    splitted = folderpathString.split("/");
                }
                
                splitted.forEach((eachNode, index2) => {
                        if (usedFlds.indexOf(eachNode) === -1) {
                            usedFlds.push(eachNode);
                            if (index2 === 0) {
                                if (eachNode !== "") {
                                    
                                    let nodeObj = {
                                        key: index + eachNode + index2,
                                        title: decodeURI(eachNode),
                                        isLeaf: false,
                                        allFldPath: eachFolder.groupValue,
                                        serviceType: serviceType,
                                        Source: "folder.png",
                                        // docSize: eachDocument.doc_size,
                                        // fileSize: fileSize + " KB",
                                        children: []
                                    }
                                    if (eachNode === "Box.com") {
                                        nodeObj.icon = <img src="../images/box-logo.png" height="13px" style={{"marginTop": "-4px"}} className="box-image-in-tree"/>
                                        nodeObj.style = {"color": "rgba(0, 0, 0, 0.0)"};
                                    }
                                    if (eachNode === "SharePoint 2016") {
                                        nodeObj.icon = <img src="../images/sharepoint-logo.png" style={{"marginTop": "-5px"}} height="16px" className="box-image-in-tree"/>
                                        nodeObj.style = {"color": "rgba(0, 0, 0, 0.0)"};
                                    }
                                    toBeExpanded.push(index + eachNode + index2)
                                    docTreeRoots.push(nodeObj);
                                }
                            } else {
                                if (eachNode !== "") {
                                    let totalSize = 0; //eachNode//solrDocFolders[index + 1];
                                    if (!isFromSearch) {
                                        totalSize = eachFolder.doclist.numFound
                                    } else {
                                        totalSize = solrDocFolders[index + 1];
                                    }
                                    let fldPath = "";
                                    let currentFldLocation = splitted.indexOf(eachNode);
                                    for (let i = 1; i <= currentFldLocation; i++) {
                                        fldPath = fldPath + splitted[i] + "/";
                                    }
                                    let childNodeObj = {
                                        key: index + eachNode + index2,
                                        title: decodeURI(eachNode),
                                        isLeaf: false,
                                        allFldPath: fldPath, //eachFolder.groupValue,
                                        serviceType: serviceType,
                                        Source: "folder.png",
                                        docSize: totalSize, //eachFolder.doclist.numFound,
                                        fileSize: totalSize, //eachFolder.doclist.numFound,
                                        children: []
                                    };
                                    toBeExpanded.push(index + eachNode + index2);
                                    if (docTreeRoots.length > 0) {
                                        docTreeRoots.forEach(docTreeRoot => {
                                            let parentNode = this.searchTree(docTreeRoot, splitted[index2 - 1]);
                                            if (parentNode !== null) {
                                                parentNode.children.push(childNodeObj);
                                            }
                                        })
                                    }
                                    
                                }
                            }
                        }
                })
            }
        });
        treeDocuments.forEach((eachDocument, index) => {
            let docPath = eachDocument.doc_path_hierarchy;
            let allPaths = docPath.split("/");
            let parentNodeTitle = allPaths[allPaths.length - 2];
            
            let fileNameExt = eachDocument.doc_path.split(".");
            let fileExt = fileNameExt[fileNameExt.length - 1];
            let fileTypeIcon = this.getFileExtIcon(fileExt);
            let currentDocId;
            if (eachDocument.doc_path_id !== undefined) {
                let docPathIds = eachDocument.doc_path_id.split("/");
                currentDocId = docPathIds[docPathIds.length - 1];
            } else {
                currentDocId = index
            }
            let fileSize = "";
            if (eachDocument.doc_size !== undefined) {
                fileSize = (eachDocument.doc_size / 1024).toFixed(0);
            }
            
            let fileNameHeirarchy = eachDocument.doc_path.split("/");
            let fileName = fileNameHeirarchy[fileNameHeirarchy.length - 1];
            fileName = decodeURI(fileName);
            // let fileNameElement
            docTreeRoots.forEach(eachParent => {
                let parent = this.searchTree(eachParent, parentNodeTitle);
                if (parent !== undefined && parent !== null) {
                    parent.children.push({
                        key: currentDocId,
                        title: fileName,
                        isLeaf: true,
                        icon: fileTypeIcon,
                        documentPath: eachDocument.doc_path,
                        documentPathId: eachDocument.doc_path_id,
                        solrId: eachDocument.id,
                        lastAuthor: eachDocument.last_author,
                        last_author: eachDocument.last_author,
                        serviceType: eachDocument.source_type,
                        modifiedDate: eachDocument.doc_modified,
                        docSize: eachDocument.doc_size,
                        fileSize: this.getFileSizeFormatted(eachDocument.doc_size),
                        fileExtType: fileExt.toLowerCase(),
                        tags: eachDocument.tags,
                        keyWords: eachDocument.keywords
                    })
                }
            })
        })

        return {docTreeRoots, toBeExpanded};
    }

    static getSourceType(eachFolder, treeDocuments) {
        let sourceOfFld = "";
        for (var i = 0; i < treeDocuments.length; i++) {
            if (eachFolder === treeDocuments[i]["doc_path_hierarchy"]) {
                sourceOfFld = treeDocuments[i]["source_type"];
                break;
            }
        }
        if (sourceOfFld === "box") {
            sourceOfFld = "Box.com";
        } else if (sourceOfFld === "sharepoint") {
            sourceOfFld = "SharePoint 2016";
        }
        return sourceOfFld;
    }

    static getFileSizeFormatted(fileSize, decimals = 2) {
        if (fileSize === 0) return '0 Bytes';

        const kilo = 1024;
        const dm = decimals < 0 ? 0 : decimals;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

        const i = Math.floor(Math.log(fileSize) / Math.log(kilo));

        return parseFloat((fileSize / Math.pow(kilo, i)).toFixed(dm)) + ' ' + sizes[i];
        
    }

    static searchTree(element, matchingTitle){
        if (element !== undefined) {
            if(element.title == matchingTitle){
                return element;
           }else if (element.children != null){
                var i;
                var result = null;
                for(i=0; result == null && i < element.children.length; i++){
                     result = this.searchTree(element.children[i], matchingTitle);
                }
                return result;
           }
        }
        return null;
   }

   static getFileExtIcon(fileExt) {
       let fileTypeIcon = ""
        if (fileExt.toLowerCase() === "png" || fileExt.toLowerCase() === "jpg" || fileExt.toLowerCase() === "jpeg" || fileExt.toLowerCase() === "gif" || fileExt.toLowerCase() === "bmp" || fileExt.toLowerCase() === "xbm" || fileExt.toLowerCase() === "ppm") {
            fileTypeIcon = <i className="fa fa-file-image-o blue-icons" aria-hidden="true" title="Image file"></i>
        } else if (fileExt.toLowerCase() === "doc" || fileExt.toLowerCase() === "docx") {
            fileTypeIcon = <i className="fa fa-file-word-o blue-icons no-left-margin-icon" aria-hidden="true" title="Word document"></i>
        } else if (fileExt.toLowerCase() === "xls" || fileExt.toLowerCase() === "xlsx") {
            fileTypeIcon = <i className="fa fa-file-excel-o blue-icons no-left-margin-icon" aria-hidden="true" title="Excel sheets"></i>
        } else if (fileExt.toLowerCase() === "ppt" || fileExt.toLowerCase() === "pptx") {
            fileTypeIcon = <i className="fa fa-file-powerpoint-o blue-icons" aria-hidden="true" title="Powerpoint presentation"></i>
        } else if (fileExt.toLowerCase() === "pdf") {
            fileTypeIcon = <i className="fa fa-file-pdf-o blue-icons no-left-margin-icon" aria-hidden="true" title="PDF File"></i>
        } else if (fileExt.toLowerCase() === "msg") {
            fileTypeIcon = <i className="fa fa-envelope blue-icons" aria-hidden="true" title="Email attachment"></i>
        } else if (fileExt.toLowerCase() === "zip") {
            fileTypeIcon = <i className="fa fa-file-archive-o blue-icons" aria-hidden="true" title="Zipped files"></i>
        } else if (fileExt.toLowerCase() === "flv" || fileExt.toLowerCase() === "mp4" || fileExt.toLowerCase() === "avi" || fileExt.toLowerCase() === "mx" || fileExt.toLowerCase() === "wbmp" || fileExt.toLowerCase() === "asf" || fileExt.toLowerCase() === "mov") {
            fileTypeIcon = <i className="fa fa-file-video-o blue-icons" aria-hidden="true" title="Video file"></i>
        } else if (fileExt.toLowerCase() === "boxnote" || fileExt.toLowerCase() === "txt") {
            fileTypeIcon = <i className="fa fa-file-text blue-icons" aria-hidden="true" title="Text document"></i>
        } else if (fileExt.toLowerCase() === "mp3" || fileExt.toLowerCase() === "wav") {
            fileTypeIcon = <i className="fa fa-file-audio-o blue-icons" aria-hidden="true" title="Audio file"></i>
        } else {
            fileTypeIcon = <i className="fa fa-question-circle blue-icons" aria-hidden="true" title="Unknown filetype"></i>
        }

        return fileTypeIcon
   }

    static dateToString(dateString) {
        let dateModified = "", dateToShow = "";
        let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        if (dateString !== undefined) {
            dateModified = new Date(dateString);
            let month = '' + (dateModified.getMonth());
            let day = '' + dateModified.getDate();
            let year = dateModified.getFullYear();
            let hour = dateModified.getHours();
            let minutes = dateModified.getMinutes();
            let ampm = hour >= 12 ? 'PM' : 'AM';
            hour = hour % 12;
            hour = hour ? hour : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0'+minutes : minutes;
            let strTime = hour + ':' + minutes + ' ' + ampm;
            // dateToShow = day + "-" + month + "-" + year + " " + hour + ':' + minutes
            dateToShow = months[month]  + " " + day + ", " + year + " " + strTime;
        }

        return dateToShow;
    }

    static getAllTags(allTags) {
        let allTagsString = "";
        allTags.forEach(tag => {
            allTagsString = allTagsString + this.capitalizeFirstLetter(tag) + ", "
        });
        allTagsString = allTagsString.substring(0, allTagsString.length - 2)
        return allTagsString;
    }

    static capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
      }
}

export default TreeDataHelper;