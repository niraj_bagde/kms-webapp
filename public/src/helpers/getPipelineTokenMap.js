var tokenMap = {};

export default function getPipelineTokenMap(tokenArray){
    tokenMap = {};
    for( var itemIndex=0; itemIndex < tokenArray.length; itemIndex++){
        if(tokenArray[itemIndex].concept && tokenArray[itemIndex].concept.graph)
            generateExpansions(tokenArray[itemIndex].concept.graph, tokenArray[itemIndex].token);

        if(tokenArray[itemIndex].entity && tokenArray[itemIndex].entity.extra)
            generateExpansions(tokenArray[itemIndex].entity.extra, tokenArray[itemIndex].token)

        if(tokenArray[itemIndex].relations && tokenArray[itemIndex].relations.length > 0) {
            for ( var i=0; i < tokenArray[itemIndex].relations.length; i++ ) {
                generateExpansions(tokenArray[itemIndex].relations[i].graph, tokenArray[itemIndex].token)
            }
        }
        //extracting smart expansions
        if(tokenArray[itemIndex].iris) {
            var smartExpansions = [];
                if(tokenArray[itemIndex].iris['variants'].length>0)
                    smartExpansions = createSourceExpansions(tokenArray[itemIndex].iris['variants'], smartExpansions, 'variants', tokenArray[itemIndex].token);

                if(tokenArray[itemIndex].iris['acronyms'].length>0)
                    smartExpansions = createSourceExpansions(tokenArray[itemIndex].iris['acronyms'], smartExpansions, 'acronyms', tokenArray[itemIndex].token);

                if(tokenArray[itemIndex].iris['related'].length>0)
                    smartExpansions = createSourceExpansions(tokenArray[itemIndex].iris['related'], smartExpansions, 'related', tokenArray[itemIndex].token);
            if(smartExpansions.length>0 ){
                pushInTokenmap(smartExpansions, 'Machine Learning Enrichment', tokenArray[itemIndex].token);
            }
        }
    }
    return tokenMap;
}

function generateExpansions (tokenGraphObject, tokenName) {
    for ( var i=0; i < tokenGraphObject.length; i++ ) {
        extractExpansions(tokenGraphObject[i], tokenName);
    }
}

function extractExpansions (graphObject, tokenName) {
    var sourceExpansions = [];
    var expansionObject = {};
    if(graphObject.expansions) {
        expansionObject = graphObject.expansions
    } else if (graphObject.expansion){
        expansionObject = graphObject.expansion
    }
    for(var key in expansionObject){
          if(key.match('standardname$') !== null){
              var standardObject = expansionObject[key] ? expansionObject[key] : null;
              if(standardObject) {
                  sourceExpansions = createSourceExpansions(standardObject, sourceExpansions, 'standardname', tokenName);
              }
          }else if(key.match('synonyms$') !== null){
              sourceExpansions = createSourceExpansions(expansionObject[key], sourceExpansions, 'synonyms', tokenName);
          }else if(key.match('related$') !== null){
              sourceExpansions = createSourceExpansions(expansionObject[key], sourceExpansions, 'related', tokenName);
          }else if(key.match('narrower$') !== null){
              sourceExpansions = createSourceExpansions(expansionObject[key], sourceExpansions, 'narrower', tokenName)
          }else{
              sourceExpansions = createSourceExpansions(expansionObject[key], sourceExpansions, 'related', tokenName);
          }
    }
    if(sourceExpansions.length>0) {
        pushInTokenmap(sourceExpansions, graphObject.sourceTitle, tokenName);
    }

}

function createSourceExpansions (expansionObject, sourceExpansions, type, tokenName) {
    var index = -1;
    for( var i=0; i < expansionObject.length; i++) {
        index = sourceExpansions.map(function (d) { return d['label']; }).indexOf(expansionObject[i]);
        if(index == -1) {
            if(tokenName.toLowerCase() !== expansionObject[i].toLowerCase())
                sourceExpansions.push({'label': expansionObject[i], 'type': type})
        }
    }
    return sourceExpansions;
}

function pushInTokenmap (sourceExpansions, sourceTitle, tokenName) {
    if(tokenMap && tokenMap[tokenName]) {
        if(containsKey(tokenMap[tokenName], sourceTitle)){
            for ( var i=0; i < sourceExpansions.length; i++ ) {
                var index = tokenMap[tokenName][sourceTitle].indexOf(sourceExpansions[i])
                if(index == -1) {
                    tokenMap[tokenName][sourceTitle].push(sourceExpansions[i]);
                    tokenMap[tokenName]['show'] = false
                    if(sourceTitle=='Machine Learning Enrichment') {
                        tokenMap[tokenName]['showRelated'] = false
                    }
                }
            }
        } else {
            tokenMap[tokenName][sourceTitle] = sourceExpansions;
            tokenMap[tokenName]['show'] = false
            if(sourceTitle=='Machine Learning Enrichment') {
                tokenMap[tokenName]['showRelated'] = false
            };
        }
    } else {
        tokenMap[tokenName] = {};
        tokenMap[tokenName][sourceTitle] = sourceExpansions;
        tokenMap[tokenName]['show'] = false
        if(sourceTitle=='Machine Learning Enrichment') {
            tokenMap[tokenName]['showRelated'] = false
        }
    }
}

function containsKey (object, key) {
    return !!Object.keys(object).find(k => k.toLowerCase() === key.toLowerCase());
}
