
class NotesHelper {
    static formatNotesData(notesData) {
        let notesDataFormatted = [], mappedArray = {}, arrElement, mappedElement;

        notesData.forEach((note, index) => {
            let noteItem = {
                "id": Number(note.id),
                "name": note.line_data,
                "parentId": Number(note.parent_line_id),
                "focus": false,
                "type": note.line_type,
                "eventId": "",
                "linkData": [],
                "nodes": [],
                "name1": note.line_data,
                "noteId": Number(note.note_id),
                "authorId": note.author_id,
                "createdDate": note.created_date,
                "modifiedBy": note.modified_by,
                "modifiedDate": note.modified_date
            };

            arrElement = noteItem;
            mappedArray[note.id] = arrElement;
        });
        for (var id in mappedArray) {
            if (mappedArray.hasOwnProperty(id)) {
                mappedElement = mappedArray[id];
                if (mappedElement.parentId !== 0 && mappedElement.parentId != null ) {
                    if (mappedArray[mappedElement["parentId"]] !== undefined) {
                        mappedElement.index = mappedArray[mappedElement["parentId"]]["nodes"].length;
                        mappedArray[mappedElement["parentId"]]["nodes"].push(mappedElement);
                    }
                } else {
                    delete mappedElement.parentId;
                    notesDataFormatted.push(mappedElement);
                }
            }
        }

        return notesDataFormatted;
    }

    static getFlatArray(currentNode, treeview, allUpsertedIds) {
        var tempChildren = [];
        currentNode.nodes.forEach(child => {
            child.parentId = currentNode.id;
            child.noteId = currentNode.noteId;
            if (allUpsertedIds !== undefined) {
                allUpsertedIds.forEach(eachUpsert => {
                    if (eachUpsert !== undefined && eachUpsert !== null) {
                        if (Number(child.id) === Number(eachUpsert.idInNote)) {
                            child.id = eachUpsert.idInDB;
                        }
                    }
                })
            }
            
            tempChildren.push(this.getFlatArray(child, treeview, allUpsertedIds));
        });
        treeview.push(currentNode);
      
        return treeview;
    }

    static createFlatArray(notes, flatArray, allUpsertedIds) {
        if (notes.nodes !== undefined && notes.nodes.length > 0) {
            notes.nodes.forEach(note => {

                note.parentId = notes.id;
                note.noteId = notes.noteId;
                if (allUpsertedIds !== undefined) {
                    allUpsertedIds.forEach(eachUpsert => {
                        if (eachUpsert !== undefined && eachUpsert !== null) {
                            if (Number(note.id) === Number(eachUpsert.idInNote)) {
                                note.id = eachUpsert.idInDB;
                            }
                        }
                    })
                }

                flatArray.push(note);
                if (note.nodes.length > 0) {
                    this.createFlatArray(note, flatArray, allUpsertedIds);
                }
            })
        }

        return flatArray;
    }
}

export default NotesHelper;