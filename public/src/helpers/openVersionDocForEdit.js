import axios from "axios";

export  function editVersionDocument (docid, versionid, filepath, source, client,backendURL) {
    let request;
    let requestUrl="";
    if(source.indexOf('Sharepoint') >-1) {
        request = {
        "ConnectorSourceName":"SharePoint",
        "DocumentId": docid,
        "VersionDocumentId": versionid
        };

        requestUrl = backendURL+"api/v1/downloadVersionHistorydoc";
    }
    request["ClientName"]= client;
    axios.post(requestUrl, request).then(response => {
    // var searchRequest = {
    //     DocumentId: docid,
    //     VersionDocumentId: versionid,
    //   };
  
    //   axios
    //     .post(
    //       "https://sp16-nba.thedigitalgroup.com:9091/api/v1/downloadVersionHistorydoc",
    //       searchRequest
    //     )
    //     .then((response) => {
          if (response && response.data && response.data.FilePath) {
           
            var url = "";
            let fileNExt = filepath.split(".");
  
            if (fileNExt.length > 1) {
              let fileExt = fileNExt[fileNExt.length - 1];
              if (fileExt == "doc" || fileExt == "docx") {
                url = "ms-word:ofe|u|" + response.data.FilePath;
              } else if (fileExt == "xls" || fileExt == "xlsx") {
                url = "ms-excel:ofe|u|" + response.data.FilePath;
              } else if (fileExt == "ppt" || fileExt == "pptx") {
                url = "ms-powerpoint:ofe|u|" + response.data.FilePath;
              } else {
                url = "ms-word:ofe|u|" + response.data.FilePath;
              }
              var mylink = document.getElementById("MyLink");
              mylink.setAttribute("href", url);
              mylink.click();
            }
          }
        });
    

}