import axios from "axios";

export default  function editDocument (editNode, source, client,backendURL) {
    var documentId = editNode.DocumentId ? editNode.DocumentId : editNode.DoucumentId;
     var url = "";
     let request;
     if(source.indexOf('Sharepoint') >-1) {
         request = {
             "ConnectorSourceName":"SharePoint",
             "DocumentId": documentId
         };

         url = backendURL+"api/v1/downloaddocument/";
     }
     request["ClientName"] = client;
     axios.post(url, request).then(response => {
    //  axios.get(url).then(response => {
        if(response && response.data && response.data.FilePath) {
            var url = "";
            let fileNExt = editNode.VersionHistoryUrl ? 
                            editNode.VersionHistoryUrl.split(".") : 
                                editNode.Name.split(".");
            if (fileNExt.length > 1) {
            let fileExt = fileNExt[fileNExt.length-1];
            if(fileExt=='doc' || fileExt=='docx') {
                url ='ms-word:ofe|u|'+ response.data.FilePath;
            } else if (fileExt=='xls' || fileExt=='xlsx') {
                url ='ms-excel:ofe|u|'+ response.data.FilePath;
            } else if (fileExt=='ppt' || fileExt=='pptx') {
                url ='ms-powerpoint:ofe|u|'+ response.data.FilePath;
            } else {
                url ='ms-word:ofe|u|'+ response.data.FilePath;
            }
            var mylink = document.getElementById("MyLink");
            mylink.setAttribute("href", url);
            mylink.click();
            }

         }

    })
}
