function allTrim(desc) {
    return desc.replace(/\s+/g, ' ')
        .replace(/^\s+|\s+$/, '');
}

function getAuthors(authorsRawData) {

    var authors = "";

    var uniqueAuthors = authorsRawData.filter(function (item, pos, self) {
        return self.indexOf(item) == pos;
    });

    if (uniqueAuthors && uniqueAuthors.length > 0) {
        authors = uniqueAuthors.join(', ');
        //authors = authors.replace(/\\/g,'');
        authors = authors.replace(/(?!([^<]+>))[:\\]/g, '');
    }

    return authors;
}
function isDuplicate(sourceArray, sourceString) {
    var foundDuplicate = false;
    var elem = document.createElement('textarea');
    var stringItem = sourceString.replace(/<(.|\n)*?>/g, '');
    elem.innerHTML = stringItem;
    stringItem = elem.value;
    for (var i = 0; i < sourceArray.length; i++) {
        //removing the html tags
        elem.innerHTML = sourceArray[i];
        var sourceItem = elem.value;
        sourceItem = sourceItem.replace(/<(.|\n)*?>/g, '');
        if (sourceItem.trim() == stringItem) {
            foundDuplicate = true;
        }
    }
    return foundDuplicate;
}

function refineDesc(descData) {
    descData = descData.replace(/[\\]/g, '');
    var txt = document.createElement("textarea");
    txt.innerHTML = descData;
    return txt.value;
}
function refineTitle(titleData) {
    titleData = titleData.replace(/[\\]/g, '');
    var txt = document.createElement("textarea");
    txt.innerHTML = titleData;
    return txt.value;
}
function refineTeams(highlight, usual, type) {
    var displayString = "";
    if (highlight != undefined && highlight[type] != undefined) {
        let keys = [highlight[type][0]];
        highlight[type].forEach(function (term, index) {
            if (index > 0) {
                if (isDuplicate(keys, term) == false) {
                    keys.push(term);
                }
            }
        });
        usual.forEach(function (term) {
            if (isDuplicate(keys, term) == false) {
                keys.push(term);
            }
        });
        displayString = keys.join(", ");
    } else if (usual != undefined) {
        let keys = [usual[0]];
        usual.forEach(function (term, index) {
            if (index > 0) {
                if (isDuplicate(keys, term) == false) {
                    keys.push(term);
                }
            }
        });
        displayString = keys.join(", ");
    } else {
        displayString = "";
    }
    return displayString;
}

function getContentDesc (contentBody) {
    let spanIndex;
    let desc;
    if(contentBody.length > 600) {
        spanIndex = contentBody.indexOf('<span');
        if (spanIndex > 30) {
            desc =  "..." + contentBody.substr(spanIndex-30, spanIndex+569)  + '...'
        } else {
            desc = contentBody.substr(0, 599);
        }
    } else {
        desc = contentBody.substr(0, 599);
    }
    return desc;
}

function getBiggerContent (record) {
    let contentBody = "";
        if (record.highlight != undefined && record.highlight.content != undefined) {
            if (record.highlight.content[0] && record.highlight.content[0] !== '') {
                contentBody = record.highlight.content[0];
            } else if (record.highlight.content[1] && record.highlight.content[1] !== '') {
                contentBody = record.highlight.content[1];
            }
        } else if (record.highlight != undefined && record.highlight.abstract != undefined) {
            contentBody = record.highlight.abstract[0];
        } else {
            contentBody = "";
        }
    return contentBody;
}

function getShorterContent (record) {
    let contentBody = "";
    if (record.highlight != undefined && record.highlight.abstract_short != undefined) {
        contentBody = record.highlight.abstract_short[0];
    } else if (record.highlight != undefined && record.highlight.content_short != undefined) {
        contentBody = record.highlight.content_short[0];
    } else if (record.abstract_short != undefined) {
        contentBody = record.abstract_short[0];
    } else if (record.content_short != undefined) {
        if (record.content_short[0] && record.content_short[0] !== '') {
            contentBody = record.content_short[0];
        } else if (record.content_short[1] && record.content_short[1] !== '') {
            contentBody = record.content_short[1];
        }
    } else {
        contentBody = "";
    }
    return contentBody;
}

export default function getFormattedResultforSolr(result,dataStore){

    var highlightData = result.searchResponse.highlighting;
    var dataResult = [];


    dataResult = result.searchResponse.response.docs;

    var srno = 0, count = 0;

    for (var key in dataResult) {
        count++;
        dataStore.displayCount++;
        var recordID = dataResult[key].id;

        for (var highlightKey in highlightData) {

            if (highlightKey == recordID) {
                dataResult[key]['highlight'] = highlightData[highlightKey];
                break;
            }
        }

        dataResult[key].highlightedKeyWords = [];

        dataResult[key]['count'] = parseInt(parseInt(srno + 1) + (dataStore.itemsPerPage * (dataStore.pageNo - 1)));
        dataResult[key]['displayCount'] = dataStore.displayCount;
        // dataResult[key].contentType = (dataResult[key].content_type != undefined) ? dataResult[key].content_type : "";    
        let contentTitle = (dataResult[key].highlight != undefined && dataResult[key].highlight.title != undefined) ? 
                            dataResult[key].highlight.title[0] : 
                            (dataResult[key].highlight != undefined && dataResult[key].highlight.title_substitute != undefined) ? 
                            dataResult[key].highlight.title_substitute[0] :
                            dataResult[key].title != undefined ? dataResult[key].title[0] : 
                            dataResult[key].title_substitute != undefined ? dataResult[key].title_substitute : "";
        
        dataResult[key].title = (dataResult[key].highlight != undefined && dataResult[key].highlight.file_name != undefined) ? dataResult[key].highlight.file_name[0] : dataResult[key].file_name != undefined ? dataResult[key].file_name[0] : "";
        // dataResult[key].displyDate = (dataResult[key].hbs_display_date != undefined) ? dataResult[key].hbs_display_date[0] : "";
        // dataResult[key].sourceFacet = (dataResult[key].source != undefined) ? dataResult[key].source : "";
        // dataResult[key].tdg_geoName = (dataResult[key].tdg_geonames_prime != undefined) ? getGeoAndCompanyNames(dataResult[key].tdg_geonames_prime):"";
        // dataResult[key].tdg_companyName = (dataResult[key].tdg_capiq_companyname != undefined) ? getGeoAndCompanyNames(dataResult[key].tdg_capiq_companyname):"";
        dataResult[key].url = (dataResult[key].url != undefined) ? dataResult[key].url : "";
        dataResult[key].id = (dataResult[key].id != undefined) ? dataResult[key].id : "";

        if (dataResult[key].highlight != undefined && dataResult[key].highlight.creator != undefined) {
            dataResult[key].authorsStr = getAuthors(dataResult[key].highlight.creator);
        } else if (dataResult[key].creator != undefined) {
            dataResult[key].authorsStr = getAuthors(dataResult[key].creator);
        }  else if (dataResult[key].author != undefined) {
            dataResult[key].authorsStr = getAuthors(dataResult[key].author);
        } else {
            dataResult[key].authorsStr = "";
        }

       if (dataResult[key].image_url) {
            dataResult[key].image_url = dataResult[key].image_url.split(',')[0];
        }

        if (dataResult[key].highlight != undefined && dataResult[key].highlight.keywords != undefined) {
            let keys = [dataResult[key].highlight.keywords[0]];
            dataResult[key].highlight.keywords.forEach(function (keyword, index) {
                if (index > 0) {
                    if (isDuplicate(keys, keyword) == false) {
                        if (keyword.toLowerCase() !== "n/a")
                            keys.push(keyword);
                    }
                }
            });
            dataResult[key].keywords.forEach(function (keyword) {
                if (isDuplicate(keys, keyword) == false) {
                    if (keyword.toLowerCase() !== "n/a")
                        keys.push(keyword);
                }
            });
            dataResult[key].displayKeywords = keys.join(", ");
        } else if (dataResult[key].keywords != undefined) {
            let keys = [dataResult[key].keywords[0]];
            dataResult[key].keywords.forEach(function (keyword, index) {
                if (index > 0) {
                    if (isDuplicate(keys, keyword) == false) {
                        if (keyword.toLowerCase() !== "n/a")
                            keys.push(keyword);
                    }
                }
            });

            dataResult[key].displayKeywords = keys.join(", ");
        } else {
            dataResult[key].displayKeywords = "";
        }


        if (dataResult[key].highlight != undefined && dataResult[key].highlight.email_to != undefined) {
            dataResult[key].emailToStr = dataResult[key].highlight.email_to;
        } else if (dataResult[key].email_to != undefined) {
            dataResult[key].emailToStr = dataResult[key].email_to;
        } else {
            dataResult[key].emailToStr = "";
        }

        if (dataResult[key].highlight != undefined && dataResult[key].highlight.email_from != undefined) {
            dataResult[key].emailFromStr = dataResult[key].highlight.email_from;
        } else if (dataResult[key].email_from != undefined) {
            dataResult[key].emailFromStr = dataResult[key].email_from;
        } else {
            dataResult[key].emailFromStr = "";
        }

        dataResult[key].displayTeam = refineTeams(dataResult[key].highlight, dataResult[key].teams, 'teams');
        dataResult[key].displayTeam = refineTeams(dataResult[key].highlight, dataResult[key].teams, 'teams');
        dataResult[key].displayOfficials = refineTeams(dataResult[key].highlight, dataResult[key].officials, 'officials');
        dataResult[key].displayPlayers = refineTeams(dataResult[key].highlight, dataResult[key].players, 'players');
        dataResult[key].displayGames = refineTeams(dataResult[key].highlight, dataResult[key].games, 'games');
        dataResult[key].displayPresident = refineTeams(dataResult[key].highlight, dataResult[key].president, 'president');

        dataResult[key].displayAnatomies = refineTeams(dataResult[key].highlight, dataResult[key].anatomies, 'anatomies');
        dataResult[key].displayHealthConditions = refineTeams(dataResult[key].highlight, dataResult[key].health_conditions, 'health_conditions');
        dataResult[key].displayDrugs = refineTeams(dataResult[key].highlight, dataResult[key].drugs, 'drugs');
        dataResult[key].displayOrganizations = refineTeams(dataResult[key].highlight, dataResult[key].organizations, 'organizations');
        
        dataResult[key].detailedDesc = "";
        dataResult[key].shortDesc = "";
        dataResult[key].showMore = false;
        var contentBody = "";

        // if (dataResult[key].highlight != undefined && dataResult[key].highlight.content != undefined) {
        //     if (dataResult[key].highlight.content[0] && dataResult[key].highlight.content[0] !== '') {
        //         contentBody = dataResult[key].highlight.content[0];
        //     } else if (dataResult[key].highlight.content[1] && dataResult[key].highlight.content[1] !== '') {
        //         contentBody = dataResult[key].highlight.content[1];
        //     }
        // } else if (dataResult[key].highlight != undefined && dataResult[key].highlight.abstract != undefined) {
        //     contentBody = dataResult[key].highlight.abstract[0];
        // }else if (dataResult[key].highlight != undefined && dataResult[key].highlight.abstract_short != undefined) {
        //     contentBody = dataResult[key].highlight.abstract_short[0];
        // } else if (dataResult[key].highlight != undefined && dataResult[key].highlight.content_short != undefined) {
        //     contentBody = dataResult[key].highlight.content_short[0];
        // } else if (dataResult[key].abstract_short != undefined) {
        //     contentBody = dataResult[key].abstract_short[0];
        // } else if (dataResult[key].content_short != undefined) {
        //     if (dataResult[key].content_short[0] && dataResult[key].content_short[0] !== '') {
        //         contentBody = dataResult[key].content_short[0];
        //     } else if (dataResult[key].content_short[1] && dataResult[key].content_short[1] !== '') {
        //         contentBody = dataResult[key].content_short[1];
        //     }
        // } else {
        //     contentBody = "";
        // }

        // let team = dataResult[key].displayTeam.split(",");
        // if (dataResult[key]["dataset_key"] == 'nba-grs') {
        //     contentBody = "<em> Game Id: " + dataResult[key]['game_id'] + " and Period: " + dataResult[key]['periodname'] +
        //         ". Game between " + team[0] + " and " + team[1] + ".</em>"
        // }

        // if (dataResult[key]["dataset_key"] == 'nba-team-data') {
        //     contentBody += "<br/> <em>" + "2019 Revenue: " + dataResult[key]['revenue_2019'] + ".</em>"
        // }

        dataResult[key].desc = getBiggerContent(dataResult[key])//getContentDesc(contentBody);
        dataResult[key].shortDesc = getShorterContent(dataResult[key])//contentBody.substr(0, 599);

        if(dataResult[key].desc!==""){
            if(dataResult[key].desc !== dataResult[key].shortDesc) {
                dataResult[key].showExpandIcon = true;
                dataResult[key].showMore = false;
            }

        }
        // if (contentBody.length > 300) {
        //     dataResult[key].showMore = true;
        //     dataResult[key].detailedDesc = contentBody;
        // }

        if (dataResult[key].desc != undefined && dataResult[key].desc != "") {
            dataResult[key].desc = refineDesc(dataResult[key].desc);
            dataResult[key].desc = allTrim(dataResult[key].desc);
            // let tempDesc = dataResult[key].desc;
            // if(contentTitle!=="") {
            //     dataResult[key].desc = "<strong>" + contentTitle + "</strong></br>" + tempDesc
            // }
        }

        if (dataResult[key].shortDesc != undefined && dataResult[key].shortDesc != "") {
            dataResult[key].shortDesc = refineDesc(dataResult[key].shortDesc);
            dataResult[key].shortDesc = allTrim(dataResult[key].shortDesc);
            let tempDesc = dataResult[key].shortDesc;
            if(contentTitle!=="") {
                dataResult[key].shortDesc = "<strong>" + contentTitle + "</strong></br>" + tempDesc
            }
        }

        if (dataResult[key].resultBody != undefined && dataResult[key].resultBody != "") {
            dataResult[key].resultBody = refineDesc(dataResult[key].resultBody);
            dataResult[key].resultBody = allTrim(dataResult[key].resultBody);
        }

        if (dataResult[key].title && dataResult[key].title != "") {
            dataResult[key].title = refineTitle(dataResult[key].title);
        }

        // if(dataResult[key].highlight != undefined && dataResult[key].highlight.keywords != undefined){
        //    angular.copy(dataResult[key].highlight.keywords,dataResult[key].highlightedKeyWords);
        // }else if(dataResult[key].keywords != undefined){
        //    angular.copy(dataResult[key].keywords,dataResult[key].highlightedKeyWords);
        // }else{
        //    dataResult[key].highlightedKeyWords = []; 
        // }
        // if(dataResult[key].highlight) {
        //     getAllKeywords(dataResult[key],dataResult[key].highlight);
        // }
        srno++;
    }
    return dataResult;
}