var utils = function () {
};

utils.prototype.getFileExt = function (fileName){
    var extension = fileName.split('.').pop(); 
    return extension
}

utils.prototype.getFileName = function (fileName){
    var fname = fileName;
    if(fileName.indexOf('.')>-1) {
        fname = fileName.split('.').slice(0, -1).join('.')
    }
    return fname;
}

utils.prototype.titleCase = function (str) {
    str = str.toLowerCase();
    str = str.split(' ');

    for (var i = 0; i < str.length; i++) {
      str[i] = str[i].charAt(0).toUpperCase() + str[i].slice(1); 
    }

    return str.join(' '); 
}


module.exports = utils;