module.exports = {
    context: __dirname,
    entry: "./index.js",
    output: {
        path: __dirname + "/BuildJS",
        filename: "edocsReactApp.min.js",
    },
    optimization: {
        splitChunks: {
            chunks: 'all'
        }
    },
    watch: true,
    mode: 'none', //... Possible values ('none', development', 'production')
    module: {
            rules: [
                {
                    test: /\.js/,
                    exclude: /(node_modules)/, //... Comment this to support IE 10 and below.
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: ['babel-preset-env', 'babel-preset-react'],
                            plugins: ['transform-class-properties', 'transform-object-rest-spread']
                        }
                    }
                },
                {
                    test: /\.css$/,
                    use: ['style-loader', 'css-loader']
                },
                {
                    test: /\.less$/,
                    use: ['style-loader', 'css-loader', 'less-loader']
                },
                {
                    test: /\.(jpe?g|png|gif|woff|woff2|eot|ttf|svg)(\?[a-z0-9=.]+)?$/,
                    loader: 'url-loader?limit=100000'
                }
            ]
        },
}