import React from "react";
import "./app.documentPreview.css";
import DocTree from "../app.docTree/app.docTree";
import Axios from "axios";
import AppDocVersions from "../app.versions/app.versions";
import AppDocProperties from "../app.docProperties/app.docProperties";
import SharedWith from "../app.sharedWith/app.sharedWith";
import Annotation from "../app.annotation/app.annotation";
 import Demo from "../app.boxdoctree/app.boxdoctree";
 import TreeDataHelper from "../helpers/TreeDataHelper";

class DocumentPreview extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pdfPath: "",
      errorInPdf: false,
      currentFileType: "",
      currentItem: {},
      versionArray: [],
      docProperties: {},
      activeTab: props.activeTab ? props.activeTab : "Preview",
      loader: true,
      folderList: [],
      documentList: [],
      showDocumentPreview: true,
      infoFileName:"",
      infoFileSize:"",
      infoFileModifiedBy:"",
      infoFileModifiedOn:""
    };

    this.refreshDocumentPreview = this.refreshDocumentPreview.bind(this);
  }

  componentDidMount() {
    //get doc versions
    this.getDocVersions(this.props.documentId);

    //get doc properties
    this.getDocProperties(this.props.documentId);
    var list = document.getElementById("miniContainer");
    if (list && list.childNodes && list.childNodes[0]) {
      list.removeChild(list.childNodes[0]);
    }
    if (this.props.fileType !== "msg") {
      this.setState({
        currentFileType: this.props.fileType,
        currentItem: this.props.item,
      });

     this.documentPreview();

    } else if(this.props.fileType == "msg" || this.props.fileType == "doc") {
      this.documentPreview();
    }
  }

  sendDocumentClickPreviewEvent = (fileId, url, title, source_type) => {
    var params = {};
    var multiParams = {
      source_type_ss: this.props.source
      };
    params.recommandation_b = false;
    params.ContentType_s = "";
    params.SourceType_s = source_type;
    params.docurl_s = url;
    params.pageno_i = "";
    this.searcheventRequest = {};
    this.searcheventRequest.event_status = 1;
    this.searcheventRequest.sessionid = this.props.sessionId;
    params.UserSubCategory_s = "";
    params.UserCategory_s = "";
    params.numofresults = "";
    params.search_responsetime = 0;
    var clickEventData = {
      "documentStartTime": new Date().toISOString(),
      "documentEndTime": new Date().toISOString(),
      "query": title,
      // "documentPosition": this.docPosition,
      "docId": fileId,
      "userIP": this.props.userIP,
      "eventStatus": this.searcheventRequest.event_status,
      "eventTimestamp": new Date().toISOString(),
      "appId": this.props.eventAppId,
      "user" : this.props.userEmail,
      "application": this.props.eventAppId,
      "sessionId": this.searcheventRequest.sessionid,
      "eventType": "Document Click",
      "params": params,
      "multiParams": multiParams
    };
    Axios.post("/search/saveSearchEventData", clickEventData);
    }

  documentPreview(fileInfo) {
    let fileId, fileName, sourceType;
    var url = "";
    this.setState({showDocumentPreview: true})
    if (fileInfo === undefined) {
      fileId = this.props.fileid;
      fileName = this.props.item.title;
      sourceType = this.props.source_type;
      url = this.props.item.url;
      this.setState({infoFileName: this.props.item.file_name[0],
       infoFileModifiedOn: this.props.selectedDocInfo.modified,
       infoFileModifiedBy:this.props.selectedDocInfo.last_author,
       infoFileSize:TreeDataHelper.getFileSizeFormatted(this.props.selectedDocInfo.doc_size)
      });
      
    } else {
      if (fileInfo.isCollectionNode) {
        fileName = fileInfo.file_name[0]; 
        fileId = fileInfo.file_id;
      } else {
        fileName = fileInfo.title;
        fileId = fileInfo.key;
      }
      url = fileInfo.solrId;
      
      sourceType = fileInfo.serviceType;

      this.setState(
        {infoFileName: fileInfo.title, 
        infoFileSize: fileInfo.fileSize, 
        infoFileModifiedOn: fileInfo.modifiedDate, 
        infoFileModifiedBy: fileInfo.last_author})
    }
    var id = this.props.documentId ? this.props.documentId : fileId;
    this.sendDocumentClickPreviewEvent(id, url, fileName, sourceType);
    let request;
    let docID = this.props.documentId == null ? 0 : this.props.documentId;
    request = {
      source_type: sourceType, //this.props.source_type,
      title: fileName, //this.props.item.title,
      documentId: docID,
      ViewedBy: this.props.userEmail,
      AccessToken: this.props.accessToken.toString(),
      // AccessToken: "BOlcq6Jc1W4wYW4t3igjuuiEEanXagG0",
      RefreshToken: this.props.refreshToken,
      // RefreshToken: "NyagaTwWPGnxQFUfjgX61hk5ysqf2r5evTAtbxIah16TJA3ko6f6l1C1wTk45kEa",
      FileID: docID //this.props.fileid,
    };
    request["searchText"] = this.props.searchQuery == "" ? "":this.props.searchQuery;
    request["ClientName"]= this.props.client;
    Axios.post("/preview/docPreview", request).then(response => {
    
        this.setState({
          loader: false,
        });
        console.log("response")
        console.log(response)
        if (response.data.Message !== undefined && response.data.Message !== "") {

          if(response.data.Message == "DocuVieware Failed.")
          {
          document.getElementById("dvContainer").innerHTML = "<div style='text-align:center;margin-top: 200px'><span style='fontWeight:bold;color:gray'>Preview of this file format is not supported.</span></div>";
           }
          else{
           document.getElementById("dvContainer").innerHTML = "<div style='text-align:center;margin-top: 200px''><span style='fontWeight:bold;color:gray'>Unable to load preview.</span></div>";
         }


        } else {
          const fragment = document
            .createRange()
            .createContextualFragment(response.data["HtmlContent"]);
          document.getElementById("dvContainer").appendChild(fragment);
        }
        
      })
      .catch((error) => {
        this.setState({
          loader: false,
        });
      });
  }

  checkIframeLoaded = () => {
    var iframe = document.getElementById("pdfIframe");
    if (!iframe) {
      setTimeout(this.checkIframeLoaded, 100);
    } else {
      this.setState({
        loader: false,
      });
    }
  };

  removeHTML = (sourceString) => {
    var elem = document.createElement("textarea");
    var stringItem = "";
    if(sourceString) {
      stringItem = sourceString.replace(/<(.|\n)*?>/g, "");
      elem.innerHTML = stringItem;
      stringItem = elem.value;
    }
    return stringItem;
  };

  getDocProperties = (documentId) => {
    var request = {
      "DocumentId": documentId,
    }
    if(this.props.source.indexOf('Sharepoint') >-1) {
      request["ConnectorSourceName"] = "SharePoint";
    }
    request["ClientName"]= this.props.client;
    Axios.post("/docTree/getDocProperties/", request).then((response) => {
      let docProperties = response && response.data ? response.data : {};
      this.setState({
        docProperties: docProperties,
      });
    });
  };

  getDocVersions = (documentId) => {
    let request;
    let requestUrl="";
    if(this.props.source.indexOf('Sharepoint') >-1) {
        request = {
            "ConnectorSourceName":"SharePoint",
            "DocumentId": documentId
        }
        requestUrl = this.props.edocsBackendAPI+"api/v1/docversionhistory/";
    }
    request["ClientName"]= this.props.client;
    //get doc versions
    Axios.post(requestUrl, request).then(response => {
    // Axios.get("/docTree/getDocVersions/" + documentId).then((response) => {
      let versionArray = response && response.data ? response.data : [];
      this.setState({
        versionArray: versionArray,
      });
    });
  };

  sortVersions = (ver1, ver2) => {
    const date1 = new Date(ver1.Created);
    const date2 = new Date(ver2.Created);
    let comparison = 0;
    if (date1 > date2) {
      comparison = 1;
    } else if (date1 < date2) {
      comparison = -1;
    }
    return comparison;
  };

  refreshDocumentPreview(documentId, fileExt, item) {
    var list = document.getElementById("dvContainer");
    if (list && list.childNodes && list.childNodes[0]) {
      list.removeChild(list.childNodes[0]);
    }
    this.setState({
      showDocumentPreview: true,
      loader: true,
    });
    //get doc versions
    // this.getDocVersions(documentId);

    //get doc properties
    // this.getDocProperties(documentId);
    this.setState({
      activeTab: "Preview",
    });

    // if (item.VersionHistoryUrl) {
    //   this.versionPreview(documentId, fileExt, item);
    //   return;
    // }

    if (fileExt !== "msg") {
      this.setState({
        currentFileType: fileExt,
        currentItem: item,
      });

     this.documentPreview(item)
    } else if(this.props.fileType == "msg" || this.props.fileType == "doc") {
     
       setTimeout(() => {
         this.documentPreview(item);
      //   let request;
      //   let requestUrl="";
      //   if(this.props.source.indexOf('Sharepoint') >-1) {
      //     request = {
      //       "ConnectorSourceName":"SharePoint",
      //       Title: this.removeHTML(this.props.item.title),
      //       documentId: this.props.documentId,
      //       ViewedBy: this.props.userEmail,
      //     };

      //    // requestUrl = "https://sp16-nba.thedigitalgroup.com:9091/api/v2/document";
      // }
      // else{
      //   request = {
      //     "ConnectorSourceName":"Box",
      //     Title: this.removeHTML(this.props.item.title),
      //     documentId: 0,
      //     ViewedBy: this.props.userEmail,
      //     AccessToken:this.props.accessToken,
      //     RefreshToken:this.props.refreshToken,
      //     FileID:742638002096
      //   };
      // }
      // request["ClientName"]= this.props.client;
      // Axios.post("/preview/emailPreview", request).then(response => {
     
      //     this.setState({
      //       loader: false,
      //     });
      //     this.setState({
      //       pdfPath: response.data.FilePath,
      //       currentFileType: this.props.fileType,
      //       currentItem: this.props.item,
      //     });
      //   })
      //   .catch((error) => {
      //     this.setState({
      //       loader: false,
      //     });
      //   });
       }, 1000);


    }
    
  }

  versionPreview = (documentId, fileExt, item) => {
    var list = document.getElementById("dvContainer");
    if (list && list.childNodes && list.childNodes[0]) {
      list.removeChild(list.childNodes[0]);
    }
    this.setState({
      activeTab: "Preview",
    });
    this.setState({
      loader: true,
    });
    var pattern = new RegExp("version", "gi");
    var versionLabel = item.Name.replace(pattern, "");
    var pattern1 = new RegExp(":", "gi");
    versionLabel = versionLabel.replace(pattern1, "");
    var pattern2 = new RegExp(" ", "gi");
    versionLabel = versionLabel.replace(pattern2, "");

    if (fileExt !== "msg") {
      this.setState({
        currentFileType: fileExt,
        currentItem: item,
      });

      let request;
      let requestUrl="";
        let docID = documentId == null ? 0 : documentId;
         request = {
          source_type:this.props.source_type,
          "VersionLabel": versionLabel,
          "DocumentId": docID,
          "EncodedAbsUrl": item.VersionHistoryUrl,
          AccessToken: this.props.accessToken,
          RefreshToken: this.props.refreshToken,
          FileID: this.props.fileId,
        };
        request["ClientName"]= this.props.client;
        Axios.post("/preview/versionPreview", request).then(response => {
     
          this.setState({
            loader: false,
          });
          const fragment = document
            .createRange()
            .createContextualFragment(response.data["HtmlContent"]);
          document.getElementById("dvContainer").appendChild(fragment);
        })
        .catch(function (error) {});


    } else {
      this.setState({
        currentFileType: fileExt,
        currentItem: item,
      });
      setTimeout(this.checkIframeLoaded, 100);
      let VersionHistoryUrl = encodeURIComponent(item.VersionHistoryUrl);
      // var url = "http://localhost:8985/getVersionPreview/" + documentId + "/" + versionLabel + "/" + VersionHistoryUrl ;
      var url =
        "https://" +
        window.location.hostname +
        "/getVersionPreview/" +
        documentId +
        "/" +
        versionLabel +
        "/" +
        VersionHistoryUrl;
        if(this.props.source.indexOf('Sharepoint') >-1) {
          url += "/" + "SharePoint";
        }
        url += '/'+'this.props.client';
      this.setState({
        pdfPath: url,
        currentFileType: fileExt,
      });
    }
  };

  showExplorerView = (folderList, documentList) => {
    this.setState({
      folderList: folderList,
      documentList: documentList,
      showDocumentPreview: false
    })
  }

  render() {
    let documentPre = (
      <img src={this.props.loaderIcon} className="loader"></img>
    );
    let columnClass = "col-md-8 no-padding-both-sides center-align-content";
    if (this.state.errorInPdf) {
      documentPre = <h3>Unable to load preview of document.</h3>;
    }
    if (this.state.pdfPath !== "") {
      // documentPre = <iframe src={this.state.pdfPath} height="600px" width="100%" id="pdfIframe"/>
      columnClass = "col-md-8 no-padding-both-sides";
    }

    let folderDocumentList;

    if (!this.state.showDocumentPreview) {
      let today = new Date();
        let dd = String(today.getDate()).padStart(2, '0');
        let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        let yyyy = today.getFullYear();

        today = mm + '/' + dd + '/' + yyyy;
        let folderList = "", fileList = "", modifiedBy = "Amit D";
        if (this.state.folderList.length > 0) {
            folderList = this.state.folderList.map(folder => {
                return <div key={folder.key} className="row doc-list-row" onDoubleClick={() => this.folderDblClick(folder)}>
                    <div className="col-md-5 mouse-pointer">
                        <i className="fa fa-folder folder-icon" aria-hidden="true"></i> &nbsp;{shortStringWithTitle(folder.title, 20)}
                    </div>
                    <div className="col-md-2 mouse-pointer">
                        {folder.fileSize} files
                    </div>
                    <div className="col-md-3 mouse-pointer">
                        {/* {today} */}
                        {shortStringWithTitle(folder.dateMod, 10)}
                    </div>
                    <div className="col-md-2 mouse-pointer">
                        {/* {modifiedBy} */}
                        {shortStringWithTitle(folder.Author, 5)}
                    </div>
                </div>
            })
        }

        if (this.state.documentList.length > 0) {

            fileList = this.state.documentList.map(file => {
                let fileNode = {};
                fileNode.node = file;
                let fileNameNExt = file.title.split(".");
                let fileExt = fileNameNExt[(fileNameNExt.length - 1)];
                let fileIcon = TreeDataHelper.getFileExtIcon(fileExt);
                return <div key={file.key} id={file.key} onClick={() => this.showDocPreview(fileNode)} className="row doc-list-row">
                    <div className="col-md-5 mouse-pointer">
                        {fileIcon} &nbsp;{shortStringWithTitle(fileNameNExt[0], 30)}
                    </div>
                    <div className="col-md-2 mouse-pointer">
                        {/* {modifiedBy} */}
                        {file.fileSize}
                    </div>
                    <div className="col-md-3 mouse-pointer">
                        {/* {today} */}
                        {TreeDataHelper.dateToString(file.modifiedDate)}
                    </div>
                    <div className="col-md-2 mouse-pointer">
                        {shortStringWithTitle(file.last_author[0], 10)}
                    </div>
                </div>
            })
        }

        folderDocumentList = <div className="col-md-12 doc-lib-col">
                        <div className="row">
                            <div className="col-md-5 header-row">Name</div>
                            <div className="col-md-2 header-row">File Size</div>
                            <div className="col-md-3 header-row">Modified Date</div>
                            <div className="col-md-2 header-row">Modified By</div>
                        </div>
                        {folderList}
                        {fileList}
                    </div>
    }

    return (
      <div>
        <div className="col-md-4 document-tree-wrapper no-padding-both-sides">
          <DocTree
            loaderIcon={this.props.loaderIcon}
            docUrl={this.props.docUrl}
            dgiUserName={this.props.dgiUserName}
            mainTreeData={this.props.mainTreeData}
            versionTreeData={this.props.versionTreeData}
            refreshDocumentPreview={this.refreshDocumentPreview}
            source={this.props.source}
            client={this.props.client}
            SPUserName={this.props.SPUserName}
            boxUserGroupIds={this.props.boxUserGroupIds}
            boxUserId={this.props.boxUserId}
            accessToken={this.props.accessToken}
            refreshToken={this.props.refreshToken}
            userEmail={this.props.userEmail}
            selectedDocInfo={this.props.selectedDocInfo}
            showExplorerView={this.showExplorerView}
            edocsBackendAPI={this.props.edocsBackendAPI}
            solrCollection={this.props.solrCollection}
          />
          {/* <Demo></Demo> */}
        </div>
        <div
          className={columnClass}
          style={{ height: "650px", overflowY: "auto", textAlign: "left" }}
        >
          {this.state.activeTab == "Preview" &&
            this.state.currentFileType == "msg" && (
              <div style={{ textAlign: "center" }}>
                {/* this.state.loader - {this.state.loader} */}
                {this.state.loader == true && (
                        <img src={this.props.loaderIcon} className="loader"></img>
                )}
                <iframe
                  src={this.state.pdfPath}
                  height="600px"
                  width="100%"
                  id="pdfIframe"
                />
              </div>
            )}
          {this.state.activeTab == "Preview" &&
            this.state.currentFileType !== "msg" &&
            this.state.showDocumentPreview === true &&
            this.state.loader == false && (
              <div>
              <div style={{ fontWeight: "450", paddingLeft:"10px", paddingBottom:"5px" }}>
              <span style={{color:"#2c8ada"}}> Title:  </span> 
                <span> {this.state.infoFileName}
                </span>
                </div>
                <div style={{ fontWeight: "450", paddingLeft:"10px" }}>
                <span style={{color:"#2c8ada"}}>   Modified By: </span> <span>
                {this.state.infoFileModifiedBy} | 
                </span>
                <span style={{color:"#2c8ada"}}>   Modified On:</span> <span>
                {TreeDataHelper.dateToString(this.state.infoFileModifiedOn)} | 
                </span>  <span style={{color:"#2c8ada"}}> 
                    Size: </span> <span> {this.state.infoFileSize} 
                </span> 
                </div>
              <div
                id="dvContainer"
                style={{ height: "100vh", width: "100%" }}
              ></div>
              </div>
            )}
          {this.state.activeTab == "Preview" &&
            this.state.currentFileType !== "msg" &&
            this.state.showDocumentPreview === true &&
            this.state.loader == true && (
              <div style={{ textAlign: "center" }}>
                  <img src={this.props.loaderIcon} className="loader"></img>
              </div>
            )}

            {this.state.activeTab == "Preview" &&
            this.state.currentFileType !== "msg" &&
            this.state.showDocumentPreview === false &&
            this.state.loader !== true && (
              <div style={{ height: "100vh", width: "100%" }}>
                  {folderDocumentList}
              </div>
            )}
          
          {this.state.activeTab == "Versions" && (
            <AppDocVersions
              versions={this.state.versionArray}
              documentId={this.props.documentId}
              source={this.props.source}
              client={this.props.client}
              edocsBackendAPI={this.props.edocsBackendAPI}
            />
          )}
          {this.state.activeTab == "Properties" && (
            <AppDocProperties docProperties={this.state.docProperties} source={this.props.source} client={this.props.client}/>
          )}
          {this.state.activeTab == "Share" && (
            <SharedWith
              selectedDoc={this.state.currentItem}
              fileType={this.state.currentFileType}
              documentId={this.state.currentItem.DocumentId}
              userEmail={this.props.userEmail}
              source={this.props.source}
              client={this.props.client}
              edocsBackendAPI={this.props.edocsBackendAPI}
            />
          )}
          {this.state.activeTab == "Tags" && (
            <Annotation
              loaderIcon={this.props.loaderIcon}
              source_id={
                this.state.currentItem.DocumentId
                  ? this.state.currentItem.DocumentId
                  : this.state.currentItem.sp_doc_id
              }
              item={this.state.currentItem}
              source={this.props.source}
              client={this.props.client}
              selectedDocInfo={this.props.selectedDocInfo}
              edocsBackendAPI={this.props.edocsBackendAPI}
            />
          )}
          {/* {documentPre} */}
        </div>
        <div
          style={{
            position: "absolute",
            top: "34px",
            right: "25px",
            zIndex: 1,
            display:"none"
          }}
        >
          <div
            className={
              this.state.activeTab == "Preview" ? "view-act-btn" : "view-btn"
            }
            onClick={() => {
              this.setState({ activeTab: "Preview" });
              this.refreshDocumentPreview(
                this.props.documentId,
                this.props.fileType,
                this.state.currentItem
              );
            }}
            title="Preview"
          >
            <i class="fa fa-eye" aria-hidden="true"></i>
          </div>
          <div
            className={
              this.state.activeTab == "Versions" ? "view-act-btn" : "view-btn"
            }
            /* onClick={() => this.setState({ activeTab: "Versions" })} */
            title="Versions"
          >
            <i class="fa fa-file-text-o" aria-hidden="true"></i>
          </div>
          <div
            className={
              this.state.activeTab == "Properties" ? "view-act-btn" : "view-btn"
            }
            /* onClick={() => this.setState({ activeTab: "Properties" })} */
            title="Properties"
          >
            <i class="fa fa-tasks" aria-hidden="true"></i>
          </div>
          <div
            className={
              this.state.activeTab == "Share" ? "view-act-btn" : "view-btn"
            }
            /* onClick={() => this.setState({ activeTab: "Share" })} */
            title="Share/Shared with"
          >
            <i class="fa fa-share-alt" aria-hidden="true"></i>
          </div>
          <div
            className={
              this.state.activeTab == "Tags" ? "view-act-btn" : "view-btn"
            }
            onClick={() => this.setState({ activeTab: "Tags" })}
            title="Tags"
          >
            <i class="fa fa-tags" aria-hidden="true"></i>
          </div>
        </div>
      </div>
    );
  }
}

function shortStringWithTitle(stringToShow, charCount) {
  /* if (charCount === undefined) {
      charCount = 20
  } */
  if (stringToShow !== undefined) {
      let stringDisplay = (stringToShow.length > charCount) ? stringToShow.substr(0, charCount - 1) + '...' : stringToShow;
      return <span title={stringToShow}>{stringDisplay}</span>
  } else {
      return ""
  }

}


function dateToString(dateString) {
  let dateModified = "", dateToShow = "";
  if (dateString !== undefined) {
      dateModified = new Date(dateString);
      let month = '' + (dateModified.getMonth() + 1);
      let day = '' + dateModified.getDate();
      let year = dateModified.getFullYear();
      let hour = dateModified.getHours();
      let minutes = dateModified.getMinutes();
      dateToShow = day + "-" + month + "-" + year + " " + hour + ':' + minutes
  }

  return dateToShow;
}

export default DocumentPreview;
