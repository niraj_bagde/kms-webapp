import React, { Component } from "react";
import axios from "axios";
import "./app.home.css";
import ReactToolTip from "./reactToolTip";

class AppHome extends React.Component {
  constructor(props) {
    super(props);

    this.userName = props.user;
    this.userEmail = props.userEmail;
    this.dgiUserName = props.dgiUserName;

    this.state = {
      userList: [],
      sharedWithmeDocList: [],
      sharedBymeDocList: [],
      modifiedDocumentList: [],
      viewedDocumentList: [],
    };
    if (props.user == "") {
      this.props.history.push("/login");
    } else {
      this.getRecentlyViewedDocList();
      // this.getUsersList();
      this.getShareByMeDocument();
      this.getShareWithMeDocument(this.dgiUserName);
      this.getModifiedDocList(this.dgiUserName);
    }

    this.getFileExtension = this.getFileExtension.bind(this);
  }

  getShareWithMeDocument = (dgiUserName) => {
    var sharedWithmeDoc = [];
    var searchRequest = {
      SharedTo: dgiUserName + ".com",
    };
    let requestUrl = "";
    if (this.props.source.indexOf("Sharepoint") > -1) {
      searchRequest.ConnectorSourceName = "SharePoint";
      requestUrl =
      this.props.edocsBackendAPI+"api/v1/sharedwithmeitems";
    }
    searchRequest["ClientName"]= this.props.client;
    axios
      .post(
        requestUrl,
        searchRequest
      )
      .then((response) => {
        if (response.data) {
          sharedWithmeDoc = response.data;
        }
        this.setState({
          sharedWithmeDocList: sharedWithmeDoc,
        });

      // });
    });
  };

  getShareByMeDocument = () => {
    var sharedBymeDoc = [];
    var searchRequest = {
      SharedByEmail: this.props.userEmail,
    };
    let requestUrl = "";
    if (this.props.source.indexOf("Sharepoint") > -1) {
      searchRequest.ConnectorSourceName = "SharePoint";
      requestUrl =
      this.props.edocsBackendAPI+"api/v1/sharedbymeitems";
    }
    searchRequest["ClientName"]= this.props.client;
    axios
      .post(
        requestUrl,
        searchRequest
      )
      .then((response) => {
        if (response.data) {
          sharedBymeDoc = response.data;
        }
      this.setState({
        sharedBymeDocList: sharedBymeDoc,
      });
    });
  };

  getModifiedDocList = (dgiUserName) => {
    var modifiedDocList = [];
    var searchRequest = {
      DGIUserName: dgiUserName.split("@dgi")[0],
    };
    axios
      .post(
        this.props.edocsBackendAPI+"api/v1/modifieddocslist",
        searchRequest
      )
      .then((response) => {
        if (response.data) {
          modifiedDocList = response.data;
        }
        this.setState({
          modifiedDocumentList: modifiedDocList,
        });
      });
  };

  getRecentlyViewedDocList = () => {
    var viewedDocList = [];
    var searchRequest = {
      ViewedBy: this.props.userEmail,
      ConnectorSourceName: "SharePoint",
    };
    searchRequest["ClientName"]= this.props.client;
    axios
      .post(
        this.props.edocsBackendAPI+"api/v1/vieweditems",
        searchRequest
      )
      .then((response) => {
        if (response.data) {
          viewedDocList = response.data;
        }
        this.setState({
          viewedDocumentList: viewedDocList,
        });
      });
  };

  getFileExtension(url) {
    var matches = url.match(/\/([^\/?#]+)[^\/]*$/);
    if (matches && matches.length > 1) {
      let fileName = matches[1].split(".");
      if (fileName !== undefined && fileName.length > 0) {
        let fileExtension = fileName[fileName.length - 1];
        return fileExtension;
      } else {
        return matches[1];
      }
    }
    return null;
  }

  render() {
    return (<div>
      {
        this.props.client=='alnylam' &&
        <div className="col-md-12 col-lg-12 col-xs-12 col-sm-12 pl-0 pr-0" style={{'marginTop':'15px'}}>
          {/*<div className="watermark-wrapper"><div className="watermarktext">MVP 1.0 Release</div></div>*/}
          <div class="accordion" id="accordionExample">
  <div class="card">
    <div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          <i class="fa fa-bell" aria-hidden="true"></i>&nbsp;My Alerts
        </button>
      </h5>
    </div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
      <div class="card-body">
                <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                  <div className="row-icon">
                    <i
                      className="fa fa-file-word-o"
                      aria-hidden="true"
                    ></i>
                    &nbsp;
                    <a href="#">
                      Alnylam Pharmaceuticals, Inc. (ALNY) Investor Meeting
                    </a>
                  </div>
                  <div className="date-section">Tomorrow</div>
                </div>
                <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                  <div className="row-icon">
                    <i
                      className="fa fa-file-pdf-o"
                      aria-hidden="true"
                    ></i>
                    &nbsp;
                    <a href="#">Treatment of Transthyretin-Mediated Amyloidosis</a>
                  </div>
                  <div className="date-section">10/01</div>
                </div>
                <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                  <div className="row-icon">
                    <i
                      className="fa fa-file-word-o"
                      aria-hidden="true"
                    ></i>
                    &nbsp;
                    <a href="#">
                      Givosiran An Investigational RNAi Therapeutic
                    </a>
                  </div>
                  <div className="date-section">10/01</div>
                </div>
                <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                  <div className="row-icon">
                    <i
                      className="fa fa-file-pdf-o"
                      aria-hidden="true"
                    ></i>
                    &nbsp;
                    <a href="#">Alnylam prepares to land first RNAi drug approval</a>
                  </div>
                  <div className="date-section">10/01</div>
                </div>
                <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                  <div className="row-icon">
                    <i
                      className="fa fa-file-word-o"
                      aria-hidden="true"
                    ></i>
                    &nbsp;
                    <a href="#">
                      Givosiran An Investigational RNAi Therapeutic
                    </a>
                  </div>
                  <div className="date-section">10/01</div>
                </div>
                <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                  <div className="row-icon">
                    <i
                      className="fa fa-file-pdf-o"
                      aria-hidden="true"
                    ></i>
                    &nbsp;
                    <a href="#">Treatment of Transthyretin-Mediated Amyloidosis</a>
                  </div>
                  <div className="date-section">10/01</div>
                </div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingTwo">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          <i class="fa fa-clock-o" aria-hidden="true"></i>&nbsp;My Reminders
        </button>
      </h5>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
      <div class="card-body">
        <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                  <div className="row-icon">
                    <i
                      class="fa fa-envelope"
                      aria-hidden="true"
                    ></i>
                    <a href="#">
                      {" "}
                      Respond to RNAi-Factsheet
                    </a>
                  </div>
                  <div className="date-section">10/07</div>
                </div>
                <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                  <div className="row-icon">
                    <i
                      class="fa fa-envelope"
                      aria-hidden="true"
                    ></i>
                    <a href="#">
                      {" "}
                      Respond to Genetic Variation at the Sulfonylurea
                    </a>
                  </div>
                  <div className="date-section">10/08</div>
                </div>
                <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                  <div className="row-icon">
                    <i
                      class="fa fa-envelope"
                      aria-hidden="true"
                    ></i>
                    <a href="#">
                      {" "}
                      Respond to DDNews-Alnylam and Regeneron
                    </a>
                  </div>
                  <div className="date-section">10/08</div>
                </div>
                <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                  <div className="row-icon">
                    <i
                      class="fa fa-envelope"
                      aria-hidden="true"
                    ></i>
                    <a href="#">
                      {" "}
                      Givosiran An Investigational
                    </a>
                  </div>
                  <div className="date-section">10/08</div>
                </div>
                <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                  <div className="row-icon">
                    <i
                      class="fa fa-envelope"
                      aria-hidden="true"
                    ></i>
                    <a href="#">
                      {" "}
                      Respond to DDNews-Alnylam and Regeneron
                    </a>
                  </div>
                  <div className="date-section">10/08</div>
                </div>
                 <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                  <div className="row-icon">
                    <i
                      class="fa fa-envelope"
                      aria-hidden="true"
                    ></i>
                    <a href="#">
                      {" "}
                      Respond to RNAi-Factsheet
                    </a>
                  </div>
                  <div className="date-section">10/07</div>
                </div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingThree">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          <i class="fa fa-eye" aria-hidden="true"></i>&nbsp;Recently Viewed
        </button>
      </h5>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
      <div class="card-body">
        <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                    <div className="row-icon">
                      <i
                        className="fa fa-file-word-o"
                        aria-hidden="true"
                      ></i>
                      &nbsp;
                      <a href="#">
                        Alnylam Pharmaceuticals, Inc. (ALNY) Investor Meeting 
                      </a>
                      <br />
                      <span class="file-path">
                        Mark Taylor's OneDrive for
                        Business>>...>>WIP
                      </span>
                    </div>
                    <div className="date-section">10/05</div>
                  </div>
                  <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                    <div className="row-icon">
                      <i
                        className="fa fa-file-pdf-o"
                        aria-hidden="true"
                      ></i>
                      &nbsp;
                      <a href="#">Treatment of Transthyretin-Mediated Amyloidosis</a>
                      <br />
                      <span class="file-path">
                        Mark Taylor's OneDrive for
                        Business>>...>>WIP
                      </span>
                    </div>
                    <div className="date-section">10/03</div>
                  </div>
                  <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                    <div className="row-icon">
                      <i
                        className="fa fa-file-word-o"
                        aria-hidden="true"
                      ></i>
                      &nbsp;
                      <a href="#">
                        Givosiran An Investigational RNAi Therapeutic
                      </a>
                      <br />
                      <span class="file-path">
                        Mark Taylor's OneDrive for
                        Business>>...>>WIP
                      </span>
                    </div>
                    <div className="date-section">10/01</div>
                  </div>
                  <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                    <div className="row-icon">
                      <i
                        className="fa fa-file-pdf-o"
                        aria-hidden="true"
                      ></i>
                      &nbsp;
                      <a href="#">
                        Alnylam prepares to land first RNAi drug approval
                      </a>
                      <br />
                      <span class="file-path">
                        Mark Taylor's OneDrive for
                        Business>>...>>WIP
                      </span>
                    </div>
                    <div className="date-section">10/01</div>
                  </div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingFour">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          <i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;Recently Edited
        </button>
      </h5>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
      <div class="card-body">
        <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                    <div className="row-icon">
                      <i
                        className="fa fa-file-word-o"
                        aria-hidden="true"
                      ></i>
                      &nbsp;
                      <a href="#">
                        Alnylam Pharmaceuticals, Inc. (ALNY) Investor Meeting 
                      </a>
                      <br />
                      <span class="file-path">
                        Mark Taylor's OneDrive for
                        Business>>...>>WIP
                      </span>
                    </div>
                    <div className="date-section">10/05</div>
                  </div>
                  <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                    <div className="row-icon">
                      <i
                        className="fa fa-file-pdf-o"
                        aria-hidden="true"
                      ></i>
                      &nbsp;
                      <a href="#">Treatment of Transthyretin-Mediated Amyloidosis</a>
                      <br />
                      <span class="file-path">
                        Mark Taylor's OneDrive for
                        Business>>...>>WIP
                      </span>
                    </div>
                    <div className="date-section">10/03</div>
                  </div>
                  <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                    <div className="row-icon">
                      <i
                        className="fa fa-file-word-o"
                        aria-hidden="true"
                      ></i>
                      &nbsp;
                      <a href="#">
                        Givosiran An Investigational RNAi Therapeutic
                      </a>
                      <br />
                      <span class="file-path">
                        Mark Taylor's OneDrive for
                        Business>>...>>WIP
                      </span>
                    </div>
                    <div className="date-section">10/01</div>
                  </div>
                  <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                    <div className="row-icon">
                      <i
                        className="fa fa-file-pdf-o"
                        aria-hidden="true"
                      ></i>
                      &nbsp;
                      <a href="#">
                        Alnylam prepares to land first RNAi drug approval
                      </a>
                      <br />
                      <span class="file-path">
                        Mark Taylor's OneDrive for
                        Business>>...>>WIP
                      </span>
                    </div>
                    <div className="date-section">10/01</div>
                  </div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingFive">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          <i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp;Shared With Me
        </button>
      </h5>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
      <div class="card-body">
        <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                    <div className="row-icon">
                      <i
                        className="fa fa-file-word-o"
                        aria-hidden="true"
                      ></i>
                      &nbsp;
                      <a href="#">
                        Alnylam Pharmaceuticals, Inc. (ALNY) Investor Meeting 
                      </a>
                      <br />
                      <span class="file-path">
                        Mark Taylor's OneDrive for
                        Business>>...>>WIP
                      </span>
                    </div>
                    <div className="date-section">10/05</div>
                  </div>
                  <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                    <div className="row-icon">
                      <i
                        className="fa fa-file-pdf-o"
                        aria-hidden="true"
                      ></i>
                      &nbsp;
                      <a href="#">Treatment of Transthyretin-Mediated Amyloidosis</a>
                      <br />
                      <span class="file-path">
                        Mark Taylor's OneDrive for
                        Business>>...>>WIP
                      </span>
                    </div>
                    <div className="date-section">10/03</div>
                  </div>
                  <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                    <div className="row-icon">
                      <i
                        className="fa fa-file-word-o"
                        aria-hidden="true"
                      ></i>
                      &nbsp;
                      <a href="#">
                        Givosiran An Investigational RNAi Therapeutic
                      </a>
                      <br />
                      <span class="file-path">
                        Mark Taylor's OneDrive for
                        Business>>...>>WIP
                      </span>
                    </div>
                    <div className="date-section">10/01</div>
                  </div>
                  <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                    <div className="row-icon">
                      <i
                        className="fa fa-file-pdf-o"
                        aria-hidden="true"
                      ></i>
                      &nbsp;
                      <a href="#">
                        Alnylam prepares to land first RNAi drug approval
                      </a>
                      <br />
                      <span class="file-path">
                        Mark Taylor's OneDrive for
                        Business>>...>>WIP
                      </span>
                    </div>
                    <div className="date-section">10/01</div>
                  </div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingSix">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
          <i class="fa fa-share-alt" aria-hidden="true"></i>&nbsp;Shared By Me
        </button>
      </h5>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
      <div class="card-body">
        <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                    <div className="row-icon">
                      <i
                        className="fa fa-file-word-o"
                        aria-hidden="true"
                      ></i>
                      &nbsp;
                      <a href="#">
                        Alnylam Pharmaceuticals, Inc. (ALNY) Investor Meeting 
                      </a>
                      <br />
                      <span class="file-path">
                        Mark Taylor's OneDrive for
                        Business>>...>>WIP
                      </span>
                    </div>
                    <div className="date-section">10/05</div>
                  </div>
                  <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                    <div className="row-icon">
                      <i
                        className="fa fa-file-pdf-o"
                        aria-hidden="true"
                      ></i>
                      &nbsp;
                      <a href="#">Treatment of Transthyretin-Mediated Amyloidosis</a>
                      <br />
                      <span class="file-path">
                        Mark Taylor's OneDrive for
                        Business>>...>>WIP
                      </span>
                    </div>
                    <div className="date-section">10/03</div>
                  </div>
                  <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                    <div className="row-icon">
                      <i
                        className="fa fa-file-word-o"
                        aria-hidden="true"
                      ></i>
                      &nbsp;
                      <a href="#">
                        Givosiran An Investigational RNAi Therapeutic
                      </a>
                      <br />
                      <span class="file-path">
                        Mark Taylor's OneDrive for
                        Business>>...>>WIP
                      </span>
                    </div>
                    <div className="date-section">10/01</div>
                  </div>
                  <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                    <div className="row-icon">
                      <i
                        className="fa fa-file-pdf-o"
                        aria-hidden="true"
                      ></i>
                      &nbsp;
                      <a href="#">
                        Alnylam prepares to land first RNAi drug approval
                      </a>
                      <br />
                      <span class="file-path">
                        Mark Taylor's OneDrive for
                        Business>>...>>WIP
                      </span>
                    </div>
                    <div className="date-section">10/01</div>
                  </div>
      </div>
    </div>
  </div>
</div>
        <div className="col-md-12 col-lg-12 col-xs-12 col-sm-12 pl-0 pr-0" style={{ display: "none" }}>
          <div className="col-md-6 col-lg-6 col-xs-6 col-sm-6 pl-0">
            <div className="dashbcards">
              <div className="dashbcards-heading">
                <span className="dashbcards-heading-text"><i className="fa fa-bell" aria-hidden="true"></i>
                &nbsp;&nbsp;My Alerts</span>
              </div>
              <div className="dashbcards-cardcontent">
                <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0">
                  <div className="row-icon">
                    <i
                      className="fa fa-file-word-o"
                      aria-hidden="true"
                    ></i>
                    &nbsp;
                    <a href="#">
                      Alnylam Pharmaceuticals, Inc. (ALNY) Investor Meeting
                    </a>
                  </div>
                  <div className="date-section">Tomorrow</div>
                </div>
                <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                  <div className="row-icon">
                    <i
                      className="fa fa-file-pdf-o"
                      aria-hidden="true"
                    ></i>
                    &nbsp;
                    <a href="#">Treatment of Transthyretin-Mediated Amyloidosis</a>
                  </div>
                  <div className="date-section">10/01</div>
                </div>
                <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                  <div className="row-icon">
                    <i
                      className="fa fa-file-word-o"
                      aria-hidden="true"
                    ></i>
                    &nbsp;
                    <a href="#">
                      Givosiran An Investigational RNAi Therapeutic
                    </a>
                  </div>
                  <div className="date-section">10/01</div>
                </div>
                <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                  <div className="row-icon">
                    <i
                      className="fa fa-file-pdf-o"
                      aria-hidden="true"
                    ></i>
                    &nbsp;
                    <a href="#">Alnylam prepares to land first RNAi drug approval</a>
                  </div>
                  <div className="date-section">10/01</div>
                </div>
                <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                  <div className="row-icon">
                    <i
                      className="fa fa-file-word-o"
                      aria-hidden="true"
                    ></i>
                    &nbsp;
                    <a href="#">
                      Givosiran An Investigational RNAi Therapeutic
                    </a>
                  </div>
                  <div className="date-section">10/01</div>
                </div>
                <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                  <div className="row-icon">
                    <i
                      className="fa fa-file-pdf-o"
                      aria-hidden="true"
                    ></i>
                    &nbsp;
                    <a href="#">Treatment of Transthyretin-Mediated Amyloidosis</a>
                  </div>
                  <div className="date-section">10/01</div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-6 col-lg-6 col-xs-6 col-sm-6 pl-0">
            <div className="dashbcards">
              <div className="dashbcards-heading">
                <span className="dashbcards-heading-text"><i
                  className="fa fa-clock-o"
                  aria-hidden="true"
                ></i>
                &nbsp;&nbsp;My Reminders</span>
              </div>
              <div className="dashbcards-cardcontent">
                <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                  <div className="row-icon">
                    <i
                      class="fa fa-envelope"
                      aria-hidden="true"
                    ></i>
                    <a href="#">
                      {" "}
                      Respond to RNAi-Factsheet
                    </a>
                  </div>
                  <div className="date-section">10/07</div>
                </div>
                <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                  <div className="row-icon">
                    <i
                      class="fa fa-envelope"
                      aria-hidden="true"
                    ></i>
                    <a href="#">
                      {" "}
                      Respond to Genetic Variation at the Sulfonylurea
                    </a>
                  </div>
                  <div className="date-section">10/08</div>
                </div>
                <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                  <div className="row-icon">
                    <i
                      class="fa fa-envelope"
                      aria-hidden="true"
                    ></i>
                    <a href="#">
                      {" "}
                      Respond to DDNews-Alnylam and Regeneron
                    </a>
                  </div>
                  <div className="date-section">10/08</div>
                </div>
                <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                  <div className="row-icon">
                    <i
                      class="fa fa-envelope"
                      aria-hidden="true"
                    ></i>
                    <a href="#">
                      {" "}
                      Givosiran An Investigational
                    </a>
                  </div>
                  <div className="date-section">10/08</div>
                </div>
                <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                  <div className="row-icon">
                    <i
                      class="fa fa-envelope"
                      aria-hidden="true"
                    ></i>
                    <a href="#">
                      {" "}
                      Respond to DDNews-Alnylam and Regeneron
                    </a>
                  </div>
                  <div className="date-section">10/08</div>
                </div>
                 <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                  <div className="row-icon">
                    <i
                      class="fa fa-envelope"
                      aria-hidden="true"
                    ></i>
                    <a href="#">
                      {" "}
                      Respond to RNAi-Factsheet
                    </a>
                  </div>
                  <div className="date-section">10/07</div>
                </div>
              </div>
            </div>
          </div>
          <div
            className="col-md-12 col-lg-12 col-xs-12 col-sm-12 pl-0 pr-0 row2dashb"
            style={{ marginTop: "30px" }}
          >
            <div className="col-md-6 col-lg-6 col-xs-6 col-sm-6 pl-0">
              <div className="dashbcards">
                <div className="dashbcards-heading">
                  <span className="dashbcards-heading-text"><i class="fa fa-eye" aria-hidden="true"></i>
                  &nbsp;&nbsp;Recently Viewed</span>
                </div>
                <div className="dashbcards-cardcontent">
                  <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                    <div className="row-icon">
                      <i
                        className="fa fa-file-word-o"
                        aria-hidden="true"
                      ></i>
                      &nbsp;
                      <a href="#">
                        Alnylam Pharmaceuticals, Inc. (ALNY) Investor Meeting 
                      </a>
                      <br />
                      <span class="file-path">
                        Mark Taylor's OneDrive for
                        Business>>...>>WIP
                      </span>
                    </div>
                    <div className="date-section">10/05</div>
                  </div>
                  <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                    <div className="row-icon">
                      <i
                        className="fa fa-file-pdf-o"
                        aria-hidden="true"
                      ></i>
                      &nbsp;
                      <a href="#">Treatment of Transthyretin-Mediated Amyloidosis</a>
                      <br />
                      <span class="file-path">
                        Mark Taylor's OneDrive for
                        Business>>...>>WIP
                      </span>
                    </div>
                    <div className="date-section">10/03</div>
                  </div>
                  <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                    <div className="row-icon">
                      <i
                        className="fa fa-file-word-o"
                        aria-hidden="true"
                      ></i>
                      &nbsp;
                      <a href="#">
                        Givosiran An Investigational RNAi Therapeutic
                      </a>
                      <br />
                      <span class="file-path">
                        Mark Taylor's OneDrive for
                        Business>>...>>WIP
                      </span>
                    </div>
                    <div className="date-section">10/01</div>
                  </div>
                  <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                    <div className="row-icon">
                      <i
                        className="fa fa-file-pdf-o"
                        aria-hidden="true"
                      ></i>
                      &nbsp;
                      <a href="#">
                        Alnylam prepares to land first RNAi drug approval
                      </a>
                      <br />
                      <span class="file-path">
                        Mark Taylor's OneDrive for
                        Business>>...>>WIP
                      </span>
                    </div>
                    <div className="date-section">10/01</div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-6 col-lg-6 col-xs-6 col-sm-6 pl-0">
              <div className="dashbcards">
                <div className="dashbcards-heading">
                  <span className="dashbcards-heading-text"><i class="fa fa-pencil" aria-hidden="true"></i>
                  &nbsp;&nbsp;Recently Edited</span>
                </div>
                <div className="dashbcards-cardcontent">
                  <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                    <div className="row-icon">
                      <i
                        className="fa fa-file-word-o"
                        aria-hidden="true"
                      ></i>
                      &nbsp;
                      <a href="#">
                        Alnylam Pharmaceuticals, Inc. (ALNY) Investor Meeting 
                      </a>
                      <br />
                      <span class="file-path">
                        Mark Taylor's OneDrive for
                        Business>>...>>WIP
                      </span>
                    </div>
                    <div className="date-section">10/05</div>
                  </div>
                  <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                    <div className="row-icon">
                      <i
                        className="fa fa-file-pdf-o"
                        aria-hidden="true"
                      ></i>
                      &nbsp;
                      <a href="#">Treatment of Transthyretin-Mediated Amyloidosis</a>
                      <br />
                      <span class="file-path">
                        Mark Taylor's OneDrive for
                        Business>>...>>WIP
                      </span>
                    </div>
                    <div className="date-section">10/03</div>
                  </div>
                  <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                    <div className="row-icon">
                      <i
                        className="fa fa-file-word-o"
                        aria-hidden="true"
                      ></i>
                      &nbsp;
                      <a href="#">
                        Givosiran An Investigational RNAi Therapeutic
                      </a>
                      <br />
                      <span class="file-path">
                        Mark Taylor's OneDrive for
                        Business>>...>>WIP
                      </span>
                    </div>
                    <div className="date-section">10/01</div>
                  </div>
                  <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                    <div className="row-icon">
                      <i
                        className="fa fa-file-pdf-o"
                        aria-hidden="true"
                      ></i>
                      &nbsp;
                      <a href="#">
                        Alnylam prepares to land first RNAi drug approval
                      </a>
                      <br />
                      <span class="file-path">
                        Mark Taylor's OneDrive for
                        Business>>...>>WIP
                      </span>
                    </div>
                    <div className="date-section">10/01</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div
            className="col-md-12 col-lg-12 col-xs-12 col-sm-12 pl-0 pr-0 row2dashb"
            style={{ marginTop: "30px", marginBottom: "30px" }}
          >
            <div className="col-md-6 col-lg-6 col-xs-6 col-sm-6 pl-0">
              <div className="dashbcards">
                <div className="dashbcards-heading">
                  <span className="dashbcards-heading-text"><i
                    class="fa fa-share-alt"
                    aria-hidden="true"
                  ></i>
                  &nbsp;&nbsp;Shared With Me</span>
                </div>
                <div className="dashbcards-cardcontent">
                  <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                    <div className="row-icon">
                      <i
                        className="fa fa-file-word-o"
                        aria-hidden="true"
                      ></i>
                      &nbsp;
                      <a href="#">
                        Alnylam Pharmaceuticals, Inc. (ALNY) Investor Meeting 
                      </a>
                      <br />
                      <span class="file-path">
                        Mark Taylor's OneDrive for
                        Business>>...>>WIP
                      </span>
                    </div>
                    <div className="date-section">10/05</div>
                  </div>
                  <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                    <div className="row-icon">
                      <i
                        className="fa fa-file-pdf-o"
                        aria-hidden="true"
                      ></i>
                      &nbsp;
                      <a href="#">Treatment of Transthyretin-Mediated Amyloidosis</a>
                      <br />
                      <span class="file-path">
                        Mark Taylor's OneDrive for
                        Business>>...>>WIP
                      </span>
                    </div>
                    <div className="date-section">10/03</div>
                  </div>
                  <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                    <div className="row-icon">
                      <i
                        className="fa fa-file-word-o"
                        aria-hidden="true"
                      ></i>
                      &nbsp;
                      <a href="#">
                        Givosiran An Investigational RNAi Therapeutic
                      </a>
                      <br />
                      <span class="file-path">
                        Mark Taylor's OneDrive for
                        Business>>...>>WIP
                      </span>
                    </div>
                    <div className="date-section">10/01</div>
                  </div>
                  <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                    <div className="row-icon">
                      <i
                        className="fa fa-file-pdf-o"
                        aria-hidden="true"
                      ></i>
                      &nbsp;
                      <a href="#">
                        Alnylam prepares to land first RNAi drug approval
                      </a>
                      <br />
                      <span class="file-path">
                        Mark Taylor's OneDrive for
                        Business>>...>>WIP
                      </span>
                    </div>
                    <div className="date-section">10/01</div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-6 col-lg-6 col-xs-6 col-sm-6 pl-0">
              <div className="dashbcards">
                <div className="dashbcards-heading">
                  <span className="dashbcards-heading-text"><i
                    class="fa fa-share-alt"
                    aria-hidden="true"
                  ></i>
                  &nbsp;&nbsp;Shared By Me</span>
                </div>
                <div className="dashbcards-cardcontent">
                  <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                    <div className="row-icon">
                      <i
                        className="fa fa-file-word-o"
                        aria-hidden="true"
                      ></i>
                      &nbsp;
                      <a href="#">
                        Alnylam Pharmaceuticals, Inc. (ALNY) Investor Meeting 
                      </a>
                      <br />
                      <span class="file-path">
                        Mark Taylor's OneDrive for
                        Business>>...>>WIP
                      </span>
                    </div>
                    <div className="date-section">10/05</div>
                  </div>
                  <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                    <div className="row-icon">
                      <i
                        className="fa fa-file-pdf-o"
                        aria-hidden="true"
                      ></i>
                      &nbsp;
                      <a href="#">Treatment of Transthyretin-Mediated Amyloidosis</a>
                      <br />
                      <span class="file-path">
                        Mark Taylor's OneDrive for
                        Business>>...>>WIP
                      </span>
                    </div>
                    <div className="date-section">10/03</div>
                  </div>
                  <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                    <div className="row-icon">
                      <i
                        className="fa fa-file-word-o"
                        aria-hidden="true"
                      ></i>
                      &nbsp;
                      <a href="#">
                        Givosiran An Investigational RNAi Therapeutic
                      </a>
                      <br />
                      <span class="file-path">
                        Mark Taylor's OneDrive for
                        Business>>...>>WIP
                      </span>
                    </div>
                    <div className="date-section">10/01</div>
                  </div>
                  <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                    <div className="row-icon">
                      <i
                        className="fa fa-file-pdf-o"
                        aria-hidden="true"
                      ></i>
                      &nbsp;
                      <a href="#">
                        Alnylam prepares to land first RNAi drug approval
                      </a>
                      <br />
                      <span class="file-path">
                        Mark Taylor's OneDrive for
                        Business>>...>>WIP
                      </span>
                    </div>
                    <div className="date-section">10/01</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      }
      {
        this.props.client=='nba' &&
        <div className="col-md-12 col-lg-12 col-xs-12 col-sm-12 pl-0 pr-0">
        <div className="col-md-6 col-lg-6 col-xs-6 col-sm-6 pl-0">
          <div className="dashbcards">
            <div className="dashbcards-heading">
              <span className="dashbcards-heading-text"><i className="fa fa-bell" aria-hidden="true"></i>
              &nbsp;&nbsp;My Alerts</span>
            </div>
            <div className="dashbcards-cardcontent">
              <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                <div className="row-icon">
                  <i className="fa fa-file-word-o" aria-hidden="true"></i>
                  &nbsp;
                  <a href="#">Review Houston Rockets Contract </a>
                </div>
                <div className="date-section">01/25</div>
              </div>
              <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                <div className="row-icon">
                  <i className="fa fa-file-pdf-o" aria-hidden="true"></i>
                  &nbsp;
                  <a href="#">LA Lakers Contract Expiry</a>
                </div>
                <div className="date-section">06/25</div>
              </div>
              <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                <div className="row-icon">
                  <i className="fa fa-file-word-o" aria-hidden="true"></i>
                  &nbsp;
                  <a href="#">Boston Celtics Contract Renewal</a>
                </div>
                <div className="date-section">06/26</div>
              </div>
              <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                <div className="row-icon">
                  <i className="fa fa-file-pdf-o" aria-hidden="true"></i>
                  &nbsp;
                  <a href="#">Chicago Bulls Contract Expiry</a>
                </div>
                <div className="date-section">06/27</div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-md-6 col-lg-6 col-xs-6 col-sm-6 pl-0">
          <div className="dashbcards">
            <div className="dashbcards-heading">
              <span className="dashbcards-heading-text"><i className="fa fa-clock-o" aria-hidden="true"></i>
              &nbsp;&nbsp;My Reminders</span>
            </div>
            <div className="dashbcards-cardcontent">
              <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                <div className="row-icon">
                  <i class="fa fa-envelope" aria-hidden="true"></i>
                  <a href="#">
                    {" "}
                    Respond to Skadden &amp; Arps on GLeague Compliance Filings
                  </a>
                </div>
                <div className="date-section">06/25</div>
              </div>
              <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                <div className="row-icon">
                  <i class="fa fa-envelope" aria-hidden="true"></i>
                  <a href="#"> Respond to Chicago Bulls Contract Filings</a>
                </div>
                <div className="date-section">06/26</div>
              </div>
              <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                <div className="row-icon">
                  <i class="fa fa-envelope" aria-hidden="true"></i>
                  <a href="#"> Respond to Boston Celtics Annual Filings</a>
                </div>
                <div className="date-section">06/27</div>
              </div>
              <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                <div className="row-icon">
                  <i class="fa fa-envelope" aria-hidden="true"></i>
                  <a href="#"> Houston Rockets Contract Renewal</a>
                </div>
                <div className="date-section">06/28</div>
              </div>
            </div>
          </div>
        </div>
        <div
          className="col-md-12 col-lg-12 col-xs-12 col-sm-12 pl-0 pr-0 row2dashb"
          style={{ marginTop: "30px" }}
        >
          <div className="col-md-6 col-lg-6 col-xs-6 col-sm-6 pl-0">
            <div className="dashbcards">
              <div className="dashbcards-heading">
                <span className="dashbcards-heading-text"><i class="fa fa-eye" aria-hidden="true"></i>
                &nbsp;&nbsp;Recently Viewed</span>
              </div>
              <div className="dashbcards-cardcontent">
                {this.state.viewedDocumentList.map((item, index) => {
                  let fileExtension = this.getFileExtension(item.url);
                  let itemClass = "fa fa-bookmark";
                  if (fileExtension == "pdf") {
                    itemClass = "fa fa-file-pdf-o";
                  } else if (
                    fileExtension == "docx" ||
                    fileExtension == "doc"
                  ) {
                    itemClass = "fa fa-file-word-o";
                  } else if (fileExtension == "msg") {
                    itemClass = "fa fa-envelope";
                  } else if (
                    fileExtension == "xls" ||
                    fileExtension == "csv" ||
                    fileExtension == "xlsx"
                  ) {
                    itemClass = "fa fa-file-excel-o";
                  } else if (
                    fileExtension == "ppt" ||
                    fileExtension == "pptx"
                  ) {
                    itemClass = "fa fa-file-powerpoint-o";
                  }

                  return (
                    <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                      <div className="row-icon">
                      <ReactToolTip  title={item.file_name} theme="dark">
                        <i className={itemClass} aria-hidden="true"></i>
                        &nbsp;
                   
                           <a href={item.url}> {item.file_name.substring(0, 40)}</a>
                        </ReactToolTip>
                      
                        <br />
                      </div>
                      <div className="date-section">
                        {item.ViewedOn.substring(0, 5)}
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
          <div className="col-md-6 col-lg-6 col-xs-6 col-sm-6 pl-0">
            <div className="dashbcards">
              <div className="dashbcards-heading">
                <span className="dashbcards-heading-text"><i class="fa fa-pencil" aria-hidden="true"></i>
                &nbsp;&nbsp;Recently Edited</span>
              </div>
              <div className="dashbcards-cardcontent">
                {this.state.modifiedDocumentList.map((item, index) => {
                  let fileExtension = this.getFileExtension(item.RelativeURL);
                  let itemClass = "fa fa-bookmark";
                  if (fileExtension == "pdf") {
                    itemClass = "fa fa-file-pdf-o";
                  } else if (
                    fileExtension == "docx" ||
                    fileExtension == "doc"
                  ) {
                    itemClass = "fa fa-file-word-o";
                  } else if (fileExtension == "msg") {
                    itemClass = "fa fa-envelope";
                  } else if (
                    fileExtension == "xls" ||
                    fileExtension == "csv" ||
                    fileExtension == "xlsx"
                  ) {
                    itemClass = "fa fa-file-excel-o";
                  } else if (
                    fileExtension == "ppt" ||
                    fileExtension == "pptx"
                  ) {
                    itemClass = "fa fa-file-powerpoint-o";
                  }

                  return (
                    <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                      <div className="row-icon">
                      <ReactToolTip  title={item.Name} theme="dark">
                        <i className={itemClass} aria-hidden="true"></i>
                        &nbsp;
                        <a href={item.RelativeURL}> {item.Name.substring(0, 40)}</a>
                        </ReactToolTip>
                        <br />
                      </div>
                      <div className="date-section">
                        {item.ModifiedOn.substring(0, 4)}
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
        <div
          className="col-md-12 col-lg-12 col-xs-12 col-sm-12 pl-0 pr-0 row2dashb"
          style={{ marginTop: "30px", marginBottom: "30px" }}
        >
          <div className="col-md-6 col-lg-6 col-xs-6 col-sm-6 pl-0">
            <div className="dashbcards">
              <div className="dashbcards-heading">
                <span className="dashbcards-heading-text"><i class="fa fa-share-alt" aria-hidden="true"></i>
                &nbsp;&nbsp;Shared With Me</span>
              </div>
              <div className="dashbcards-cardcontent">
                {this.state.sharedWithmeDocList.map((item, index) => {
                  let fileExtension = this.getFileExtension(item.FilePath);
                  let itemClass = "fa fa-bookmark";
                  if (fileExtension == "pdf") {
                    itemClass = "fa fa-file-pdf-o";
                  } else if (
                    fileExtension == "docx" ||
                    fileExtension == "doc"
                  ) {
                    itemClass = "fa fa-file-word-o";
                  } else if (fileExtension == "msg") {
                    itemClass = "fa fa-envelope";
                  } else if (
                    fileExtension == "xls" ||
                    fileExtension == "csv" ||
                    fileExtension == "xlsx"
                  ) {
                    itemClass = "fa fa-file-excel-o";
                  } else if (
                    fileExtension == "ppt" ||
                    fileExtension == "pptx"
                  ) {
                    itemClass = "fa fa-file-powerpoint-o";
                  }
                  return (
                    <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                      <div className="row-icon">
                      <ReactToolTip  title={item.FileName} theme="dark">
                        <i className={itemClass} aria-hidden="true"></i>
                        &nbsp;
                        <a href={item.FilePath}> {item.FileName.substring(0, 40)}</a>
                        </ReactToolTip>
                        <br />
                      </div>
                      <div className="date-section">
                        {item.SharedOn.substring(0, 5)}
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
          <div className="col-md-6 col-lg-6 col-xs-6 col-sm-6 pl-0">
            <div className="dashbcards">
              <div className="dashbcards-heading">
                <span className="dashbcards-heading-text"><i class="fa fa-share-alt" aria-hidden="true"></i>
                &nbsp;&nbsp;Shared By Me</span>
              </div>
              <div className="dashbcards-cardcontent">
                {this.state.sharedBymeDocList.map((item, index) => {
                  let fileExtension = this.getFileExtension(item.FilePath);
                  let itemClass = "fa fa-bookmark";
                  if (fileExtension == "pdf") {
                    itemClass = "fa fa-file-pdf-o";
                  } else if (
                    fileExtension == "docx" ||
                    fileExtension == "doc"
                  ) {
                    itemClass = "fa fa-file-word-o";
                  } else if (fileExtension == "msg") {
                    itemClass = "fa fa-envelope";
                  } else if (
                    fileExtension == "xls" ||
                    fileExtension == "csv" ||
                    fileExtension == "xlsx"
                  ) {
                    itemClass = "fa fa-file-excel-o";
                  } else if (
                    fileExtension == "ppt" ||
                    fileExtension == "pptx"
                  ) {
                    itemClass = "fa fa-file-powerpoint-o";
                  }
                  return (
                    <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12 pl-0 pr-0">
                      <div className="row-icon">
                      <ReactToolTip theme="dark" title={item.FileName} >
                        <i className={itemClass} aria-hidden="true"></i>
                        &nbsp;
                        <a href={item.FilePath}>{item.FileName.substring(0, 35)}</a>
                        </ReactToolTip>
                        <br />
                      </div>
                      <div className="date-section">
                        {item.SharedOn.substring(0, 5)}
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </div>
 
      }
      </div>
    );
  }
}

// export default Search;
export default AppHome;
