import React, { Component } from 'react';
import "../app.sharedWith/app.sharedWith.css";
import "./app.documentProperties.css";
import TreeDataHelper from '../helpers/TreeDataHelper';

class DocumentProperties extends Component {
    constructor(props) {
      super(props);
      this.state = {
        sharedWithUsers: [],
        showShareModal: false,
        userList : []
        }
    }


    componentWillMount() {
        // this.getUsersList();
    }

    render() {
    return  (
        <div>
            <div className="panel-group">
                <div className="panel panel-default">
                    {/* <div className="panel-heading">
                        <h4 className="panel-title">
                            <a data-toggle="collapse" href="#collapse1">Document Properties</a>
                        </h4>
                    </div> */}
                    {(this.props.selectedDoc !== undefined && Object.keys(this.props.selectedDoc).length > 0) && <div id="collapse1" class="panel-collapse collapse in">
                        <div className="panel-body less-padding">
                            <div className="row">
                                <div className="col-md-4 doc-props-label">Name </div>
                                <div className="col-md-8 doc-props-desc">{this.props.selectedDoc.file_name[0]}</div>
                            </div>
                            <hr className="less-margin" />
                            <div className="row">
                                <div className="col-md-4 doc-props-label">Title </div>
                                <div className="col-md-8 doc-props-desc">{this.props.selectedDoc.file_name[0]}</div>
                            </div>
                            <hr className="less-margin"/>
                            {this.props.selectedDoc.doc_size !== undefined && <div className="row">
                                <div className="col-md-4 doc-props-label">Size </div>
                                <div className="col-md-8 doc-props-desc">{TreeDataHelper.getFileSizeFormatted(this.props.selectedDoc.doc_size)}</div>
                            </div>}
                            {this.props.selectedDoc.doc_size !== undefined && <hr className="less-margin"/>}
                            {this.props.selectedDoc.tags !== undefined && <div className="row">
                                <div className="col-md-4 doc-props-label">Tags </div>
                                <div className="col-md-8 doc-props-desc">{TreeDataHelper.getAllTags(this.props.selectedDoc.tags)}</div>
                            </div>}
                            {this.props.selectedDoc.tags !== undefined && <hr className="less-margin"/>}
                            {this.props.selectedDoc.anatomies !== undefined && <div className="row">
                                <div className="col-md-4 doc-props-label">Anatomies </div>
                                <div className="col-md-8 doc-props-desc">{TreeDataHelper.getAllTags(this.props.selectedDoc.anatomies)}</div>
                            </div>}
                            {this.props.selectedDoc.anatomies !== undefined && <hr className="less-margin"/>}
                            {this.props.selectedDoc.health_conditions !== undefined && <div className="row">
                                <div className="col-md-4 doc-props-label">Health Conditions </div>
                                <div className="col-md-8 doc-props-desc">{TreeDataHelper.getAllTags(this.props.selectedDoc.health_conditions)}</div>
                            </div>}
                            {this.props.selectedDoc.health_conditions !== undefined && <hr className="less-margin"/>}
                            {this.props.selectedDoc.organizations !== undefined && <div className="row">
                                <div className="col-md-4 doc-props-label">Organizations </div>
                                <div className="col-md-8 doc-props-desc">{TreeDataHelper.getAllTags(this.props.selectedDoc.organizations)}</div>
                            </div>}
                            {this.props.selectedDoc.organizations !== undefined && <hr className="less-margin"/>}
                            {this.props.selectedDoc.modified !== undefined && <div className="row">
                                <div className="col-md-4 doc-props-label">Modified Date </div>
                                <div className="col-md-8 doc-props-desc">{TreeDataHelper.dateToString(this.props.selectedDoc.modified)}</div>
                            </div>}
                            {this.props.selectedDoc.modified !== undefined && <hr className="less-margin"/>}
                            {this.props.selectedDoc.modifier_email !== undefined && <div className="row">
                                <div className="col-md-4 doc-props-label">Modified By </div>
                                <div className="col-md-8 doc-props-desc">{this.props.selectedDoc.last_author}</div>
                            </div>}
                            {this.props.selectedDoc.modifier_email !== undefined && <hr className="less-margin"/>}
                            {this.props.selectedDoc.author !== undefined && <div className="row">
                                <div className="col-md-4 doc-props-label">Created By </div>
                                <div className="col-md-8 doc-props-desc">{this.props.selectedDoc.author}</div>
                            </div>}
                            {this.props.selectedDoc.author !== undefined && <hr className="less-margin"/>}
                            {this.props.selectedDoc.creation_date !== undefined && <div className="row">
                                <div className="col-md-4 doc-props-label">Created On </div>
                                <div className="col-md-8 doc-props-desc">{TreeDataHelper.dateToString(this.props.selectedDoc.creation_date)}</div>
                            </div>}
                        </div>
                    </div>}
                </div>
            </div>
    </div>
    )
  }
};



export default DocumentProperties;