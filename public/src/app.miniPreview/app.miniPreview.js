import React from "react";
// import './app.documentPreview.css';
import Axios from 'axios';


class MiniDocPreview extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pdfPath: "",
      errorInPdf: false,
      currentFileType: "",
      currentItem: {},
      loader: true,
    };
  }

  componentDidMount() {
    var list = document.getElementById("dvContainer");
    if (list && list.childNodes && list.childNodes[0]) {
      list.removeChild(list.childNodes[0]);
    }
    var list = document.getElementById("miniContainer");
    if (list && list.childNodes && list.childNodes[0]) {
      list.removeChild(list.childNodes[0]);
    }
    if (this.props.fileType !== "msg") {
      this.setState({
        currentFileType: this.props.fileType,
        currentItem: this.props.item,
      });

      this.documentPreview();
     
    } else if (this.props.fileType == "msg" || this.props.fileType == "doc") {

        this.documentPreview();
    }
    this.sendDocumentClickMiniPreviewEvent();
  }

  documentPreview()
  {
    let request;
    let requestUrl = "";
    let docID = this.props.documentId == null ? 0 : this.props.documentId;

    request = {
      source_type:this.props.source_type,
      title: this.removeHTML(this.props.item.file_name[0]),
      documentId: docID,
      ViewedBy: this.props.userEmail,
      AccessToken: this.props.accessToken,
      RefreshToken: this.props.refreshToken,
      FileID: docID,
    };
    request["searchText"] = this.props.searchQuery == "" ? "":this.props.searchQuery;
    request["ClientName"] = this.props.client;
    Axios.post("/preview/docPreview", request)
      .then((response) => {
        this.setState({
          loader: false,
        });
        if(response.data["HtmlContent"]) {
          const fragment = document
          .createRange()
          .createContextualFragment(response.data["HtmlContent"]);
          document.getElementById("miniContainer").appendChild(fragment);
        }

      })
      .catch((error) => {
        this.setState({
          loader: false,
        });
      });
  }
  sendDocumentClickMiniPreviewEvent = () => {
    var params = {};
    var multiParams = {
      source_type_ss: this.props.source,
    };
    params.recommandation_b = false;
    params.ContentType_s = "";
    params.SourceType_s = "";
    params.docurl_s = this.props.item.url;
    params.pageno_i = this.props.pageNo;
    this.searcheventRequest = {};
    this.searcheventRequest.event_status = 1;
    this.searcheventRequest.sessionid = this.props.sessionId;
    params.UserSubCategory_s = "";
    params.UserCategory_s = "";
    params.numofresults = this.props.totalCount;
    params.search_responsetime = this.props.timeCaptureTotal * 1000;
    params.dataset_s = this.props.solrCollection;
    var docPosition;
    if (this.props.pageNo == 1) {
      docPosition = this.props.item.displayCount;
    } else {
      docPosition =
        this.props.pageSize * this.props.pageNo -
        this.props.pageSize +
        this.props.item.displayCount;
    }
    var clickEventData = {
      documentStartTime: new Date().toISOString(),
      documentEndTime: new Date().toISOString(),
      query: this.props.searchQuery ? this.props.searchQuery : this.props.item.title,
      documentPosition: docPosition,
      docId: this.props.documentId ? this.props.documentId : this.props.document_ID,
      userIP: this.props.userIP,
      eventStatus: this.searcheventRequest.event_status,
      eventTimestamp: new Date().toISOString(),
      appId: this.props.eventAppId,
      user: this.props.userEmail,
      application: this.props.eventAppId,
      sessionId: this.searcheventRequest.sessionid,
      eventType: "Document Click",
      params: params,
      multiParams: multiParams,
    };
    Axios.post("/search/saveSearchEventData", clickEventData);
  };

  checkIframeLoaded = () => {
    var iframe = document.getElementById("pdfMiniIframe");
    if (!iframe) {
      setTimeout(this.checkIframeLoaded, 100);
    } else {
      this.setState({
        loader: false,
      });
    }
  };

  removeHTML = (sourceString) => {
    var elem = document.createElement("textarea");
    var stringItem = "";
    if(sourceString) {
      stringItem = sourceString.replace(/<(.|\n)*?>/g, "");
      elem.innerHTML = stringItem;
      stringItem = elem.value;
    }
    return stringItem;
  };

  render() {
    let columnClass = "col-md-8 no-padding-both-sides center-align-content";
    return (
        <div>
            { this.state.currentFileType !=='msg' ?
                <div style={{'textAlign':'center'}}>
                    {this.state.loader==true &&
                        (<img src={this.props.loaderIcon} className="loader"></img>)
                    }
                    {this.state.loader==false && <div id="miniContainer" style={{ height: "100vh", width: "100%" }}></div>}
                </div>
                :
                <div style={{'textAlign':'center'}}>
                    {/* {this.state.loader==true && 
                     this.props.client=='alnylam' ?
                     <img src={loadingImg} className="loader"></img> :
                     <img src={loadingBallImg} className="loader" width="60px"></img>
                        // <img src={loadingImg} className="loader" width="60px"></img>
                    } */}
                    
                    <iframe src={this.state.pdfPath} height="600px" width="100%" id="pdfMiniIframe"/>
                </div>
            }
        </div>
    )
}
}

export default MiniDocPreview;
