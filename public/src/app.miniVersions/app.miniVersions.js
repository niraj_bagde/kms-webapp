import React, { Component } from "react";
import {editDocument} from "../helpers/openDocForEdit";
import {editVersionDocument} from "../helpers/openVersionDocForEdit";
import "./app.miniVersions.css";
import axios from "axios";

class MiniVersions extends Component {
  constructor(props) {
    super(props);
    // this.version = Array.isArray(this.props.versions) ? this.props.versions : [];
  }

  editDocument = (item) => {
    var editNode = item;
    if (!editNode.VersionHistoryUrl) {
      editNode.VersionHistoryUrl = editNode.EncodedAbsUrl;
    }
    editDocument(item, this.props.source, this.props.client,this.props.edocsBackendAPI);
  };

//   getVersionDocumentURL(docid, versionid, filepath) {
     
//     var searchRequest = {
//       DocumentId: docid,
//       VersionDocumentId: versionid,
//     };

//     axios
//       .post(
//         "https://sp16-nba.thedigitalgroup.com:9091/api/v1/downloadVersionHistorydoc",
//         searchRequest
//       )
//       .then((response) => {
//         if (response && response.data && response.data.FilePath) {
         
//           var url = "";
//           let fileNExt = filepath.split(".");

//           if (fileNExt.length > 1) {
//             let fileExt = fileNExt[fileNExt.length - 1];
//             if (fileExt == "doc" || fileExt == "docx") {
//               url = "ms-word:ofe|u|" + response.data.FilePath;
//             } else if (fileExt == "xls" || fileExt == "xlsx") {
//               url = "ms-excel:ofe|u|" + response.data.FilePath;
//             } else if (fileExt == "ppt" || fileExt == "pptx") {
//               url = "ms-powerpoint:ofe|u|" + response.data.FilePath;
//             } else {
//               url = "ms-word:ofe|u|" + response.data.FilePath;
//             }
//             var mylink = document.getElementById("MyLink");
//             mylink.setAttribute("href", url);
//             mylink.click();
//           }
//         }
//       });
//   }
getFileExtension(url) {
  var matches = url.match(/\/([^\/?#]+)[^\/]*$/);
  if (matches && matches.length > 1) {
    let fileName = matches[1].split(".");
    if (fileName !== undefined && fileName.length > 0) {
      let fileExtension = fileName[fileName.length - 1];
      return fileExtension;
    } else {
      return matches[1];
    }
  }
  return null;
}
  render() {
    return (
      <div style={{ marginTop: "10px" }}>
        <a id="MyLink" style={{ display: "none" }}></a>
        {this.props.versions.length==0 && 
        <div style={{"margin-top": "30px", "text-align": "center"}}>
          <span className="coming_soon_text">MVP 1.0 Release...</span>
          </div>
        }
        {this.props.versions.map((item, index) => {
          let fileExtension = this.getFileExtension(item.EncodedAbsUrl);
          let itemClass = "fa fa-bookmark";
          if (fileExtension == "pdf" || fileExtension == "msg" ) {
            itemClass = "displaynone";
          }
          else{
            itemClass="editicon"
          }
          return (
          <div className="infobx-wrapper" key={index}>
            {/* { this.props.fileType!=='pdf' && this.props.fileType!=='msg' &&
                    <i class="fa fa-pencil-square-o" 
                        aria-hidden="true" style={{'float':'right', "marginRight":"30px", "marginTop": "5px"}}
                        onClick={()=>this.editDocument(item)}
                        title="Edit this version"
                    ></i>
                } */}
            <div className="miniversion-top-content">
              <span
                className={
                  item.IsCurrentVersion == "true"
                    ? "currentVersion-hilight"
                    : "version-hilight"
                }
              >
                Version {item.VersionLabel}
              </span>{" "}
              by{" "}
              <span className="txt-underline">
                {item.CreatedBy == "System Account" ||
                item.CreatedBy == "SHAREPOINT\\system"
                  ? "Richa Sinha"
                  : item.CreatedBy}
              </span>
              <span className={itemClass}
              
                onClick={(e) =>
                  editVersionDocument(
                    this.props.documentId,
                    item.DocumentId,
                    item.EncodedAbsUrl,
                    this.props.source,
                    this.props.edocsBackendAPI
                  )
                }
              >
                <a>
                  <i
                    class="fa fa-pencil-square-o"
                    aria-hidden="true"
                    style={{ fontSize: "20px" }}
                  ></i>
                </a>
              </span>
            </div>
            <div className="miniversion-bottom-content">
              <label>
                <i
                  class="fa fa-ticket"
                  aria-hidden="true"
                  title="Title"
                  style={{ marginRight: "5px", fontSize: "15px" }}
                ></i>
                &nbsp;
              </label>
              {item.Title}
            </div>
            <div
              className="version-bottom-content"
              style={{ marginLeft: "5px" }}
            >
              <span>
                <i
                  class="fa fa-calendar"
                  aria-hidden="true"
                  title="Modified"
                  style={{ marginRight: "5px" }}
                ></i>
              </span>
              <label>&nbsp;</label>
              {item.Created}
            </div>
            {item.Comments && (
              <div
                className="version-comments-box"
                style={{ marginLeft: "5px!important", marginRight: "5px" }}
              >
                <label>
                  <i
                    class="fa fa-comment"
                    title="Comments"
                    aria-hidden="true"
                    style={{ marginRight: "5px" }}
                  ></i>
                  &nbsp;
                </label>
                {item.Comments}
              </div>
            )}
          </div>
         ) })}
      </div>
    );
  }
}

export default MiniVersions;
