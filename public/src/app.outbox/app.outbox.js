import React from 'react';
import "./app.outbox.css";

class Outbox extends React.Component {
    render() {
        return(
            
            <div className="col-md-12 col-sm-12 col-xs-12 col-lg-12 pl-0 pr-0">
                <div className="outlook-strip">&nbsp;</div>
                <div className="tabbed-area">
                    <div className="box-wrap">
                    <div id="box-one">
            <span className="inbx-info-wrapper">
              <span className="inbx-left-section">
                <h5>Alex Curry</h5>
                <span className="email-sub">Digital Customer Confirmation</span><br />
                <span>Please make sure that you have one of the following cards with you when we deliver your ord...</span>
              </span>
              <sopan className="inbx-right-section">
                <span>13.16</span>
                <span><i className="fa fa-trash-o" aria-hidden="true" /></span>
              </sopan></span>
            <span className="inbx-info-wrapper">
              <span className="inbx-left-section">
                <h5>Sean Paul</h5>
                <span className="email-sub">Your iBuy.com grocery shopping confirmation</span><br />
                <span>Please make sure that you have one of the following cards with you when we deliver your ord...</span>
              </span>
              <span className="inbx-right-section">
                <span>13.10</span>
                <span><i className="fa fa-trash-o" aria-hidden="true" /></span>
              </span></span>
            <span className="inbx-info-wrapper">
              <span className="inbx-left-section">
                <h5>Enrico Fermi</h5>
                <span className="email-sub">Your Order #224820998666029 has been Confirmed</span><br />
                <span>Your Order #224820998666029 has been placed on Saturday, 29 June...</span>
              </span>
              <span className="inbx-right-section">
                <span>13.00</span>
                <span><i className="fa fa-trash-o" aria-hidden="true" /></span>
              </span></span>
            <span className="inbx-info-wrapper">
              <span className="inbx-left-section">
                <h5>Jane Goodall</h5>
                <span className="email-sub">Payment Notification DLOP2329KD</span><br />
                <span>Your payment of 4500USD to AirCar has been authorized and confirmed, thank you your account. This...</span>
              </span>
              <span className="inbx-right-section">
                <span>12.45</span>
                <span><i className="fa fa-trash-o" aria-hidden="true" /></span>
              </span></span>
            <span className="inbx-info-wrapper">
              <span className="inbx-left-section">
                <h5>Max O'Brien Planck</h5>
                <span className="email-sub">Congratulations on your iRun Coach subscription</span><br />
                <span>Congratulations on your iRun Coach subscription. You made no space for excuses and you...</span>
              </span>
              <span className="inbx-right-section">
                <span>12.30</span>
                <span><i className="fa fa-trash-o" aria-hidden="true" /></span>
              </span></span>
            <span className="inbx-info-wrapper">
              <span className="inbx-left-section">
                <h5>Rita Levi-Montalcini</h5>
                <span className="email-sub">Pay bills &amp; win up to 600$ Cashback!</span><br />
                <span>Congratulations on your iRun Coach subscription. You made no space for excuses and you decided on a healthier and happier life...</span>
              </span>
              <span className="inbx-right-section">
                <span>12.10</span>
                <span><i className="fa fa-trash-o" aria-hidden="true" /></span>
              </span></span>
            <span className="inbx-info-wrapper">
              <span className="inbx-left-section">
                <h5>Stephen Hawking</h5>
                <span className="email-sub">Activate your LIPO Account today</span><br />
                <span>Thank you for creating a LIPO Account. Please click the link below to activate your account...</span>
              </span>
              <span className="inbx-right-section">
                <span>12.05</span>
                <span><i className="fa fa-trash-o" aria-hidden="true" /></span>
              </span></span>
          </div>
          </div>
                </div>
                <div className="tabbed-area-middle">
                    <h4>Your iBuy.com grocery shopping...</h4>
        <span className="inbx-left-section">
          <span className="intials">JG</span>
          <span className="mailtodesc pull-left">John Grey<br/>To&nbsp;&nbsp;Sean Paul</span>
          <span className="emailbody">
            Hi Sean,<br/><br/>
With resrpect, i must disagree with Mr.Zinsser. We all know the most part of important part of any article is the title.Without a compelleing title, your reader won't even get to the first sentence.After the title, however, the first few sentences of your article are certainly the most important part.<br/><br/>
Jornalists call this critical, introductory section the "Lede," and when bridge properly executed, it's the that carries your reader from an headine try at attention-grabbing to the body of your blog post, if you want to get it right on of these 10 clever ways to omen your next blog posr with a bang<br/><br/>
Jornalists call this critical, introductory section the "Lede," and when bridge properly executed, it's the that carries your reader from an headine try at attention-grabbing to the body of your blog post, if you want to get it right on of these 10 clever ways to omen your next blog posr with a bang<br/><br/>

Best regards,<br/><br/>
John Grey
          </span>
        </span>
        <span className="inbx-right-section icons-wrapper">
            <span><i className="fa fa-reply" aria-hidden="true"></i></span>
            <span><i className="fa fa-reply-all" aria-hidden="true"></i></span>
            <span><i className="fa fa-arrow-right" aria-hidden="true" /></span>
        </span>
                </div>
                <div className="tabbed-area-last">Document Preview</div>
            </div>
        )
    }
}

export default Outbox