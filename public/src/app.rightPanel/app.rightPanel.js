import React from "react";
import DocumentPreview from "../app.documentPreview/app.documentPreview";
import DocTree from "../app.docTree/app.docTree";
import Annotation from "../app.annotation/app.annotation";
import Attachment from "../app.attachment/app.attachment";
import DocumentProperties from "../app.documentProperties/app.documentProperties";

class RightPanel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: props.activeRightPanelTab,
      showAttachmentTab: false
    };
    this.setActiveTab = this.setActiveTab.bind(this);
  }

  componentDidMount() {
    let name = this.props.item.file_name ? this.props.item.file_name : (this.props.item.name?this.props.item.name[0]:[])
    if(Array.isArray(name)) {
      name = name[0];
    }
    if(name) {
      let fileNameNExt = name.split(".");
      let ext = fileNameNExt[(fileNameNExt.length - 1)];
      if (ext === "msg") {
        this.setState({showAttachmentTab: true});
      }
    }

  }

  setActiveTab(event) {
    event.preventDefault();
    this.setState({ activeTab: event.currentTarget.id });
  }

  render() {
    let tabToDisplay = "",
    classForProperties="",
    classForDocPre = "",
    classForDocTree = "",
    classForAnotation = "",
    classForAttachment = "",
    classForShared="",
    classForVersions="";
    if (this.state.activeTab === "docPreview") {
     
      tabToDisplay = <DocumentPreview docUrl={this.props.docUrl}
                                      loaderIcon={this.props.loaderIcon}
                                      userEmail={this.props.userEmail} 
                                      fileType={this.props.item.fileExtension} 
                                      documentId={this.props.item.sp_doc_id} 
                                      item={this.props.item} 
                                      dgiUserName={this.props.dgiUserName}
                                      mainTreeData={this.props.mainTreeData}
                                      versionTreeData={this.props.versionTreeData}
                                      source={this.props.source ? this.props.source : ["SharePoint"]}
                                      client={this.props.client}
                                      accessToken={this.props.accessToken}
                                      refreshToken={this.props.refreshToken}
                                      source_type={this.props.source_type}
                                      fileid={this.props.file_id}
                                      selectedDocInfo={this.props.selectedDocInfo}
                                      SPUserName={this.props.SPUserName}
                                      boxUserGroupIds={this.props.boxUserGroupIds}
                                      boxUserId={this.props.boxUserId}
                                      accessToken={this.props.accessToken}
                                      refreshToken={this.props.refreshToken}
                                      userEmail={this.props.userEmail}
                                      dgiUserName={this.props.dgiUserName}
                                      edocsBackendAPI={this.props.edocsBackendAPI}
                                      searchQuery={this.props.searchQuery}
                                      eventAppId={this.props.eventAppId}
                                      userIP={this.props.userIP}
                                      sessionId={this.props.sessionId}
                                      solrCollection = {this.props.solrCollection}
                                      />;
      classForDocPre = "active";
    } 
    else if (this.state.activeTab === "docProperties") {
      // tabToDisplay = 
      // <AppDocProperties docProperties={this.state.docProperties} 
      // source={this.props.source} 
      // client={this.props.client}/>;
      tabToDisplay = <DocumentProperties selectedDoc={this.props.selectedDocInfo} />
      classForProperties = "active";
    } 
    else if (this.state.activeTab === "docShare") { 
    {/* <SharedWith
    selectedDoc={this.state.currentItem}
    fileType={this.state.currentFileType}
    documentId={this.state.currentItem.DocumentId}
    userEmail={this.props.userEmail}
    source={this.props.source}
    client={this.props.client}
    edocsBackendAPI={this.props.edocsBackendAPI}
  />; */}
  tabToDisplay = <div>Sharing Tab</div>
  classForShared = "active";
    }
    else if (this.state.activeTab === "docVersions") { 
      {/* <AppDocVersions
      versions={this.state.versionArray}
      documentId={this.props.documentId}
      source={this.props.source}
      client={this.props.client}
      edocsBackendAPI={this.props.edocsBackendAPI}
    />; */}
    tabToDisplay = <div>Versions Tab</div>
    classForVersions= "active";
  }
    else if (this.state.activeTab === "docTree") {
      tabToDisplay = <DocTree loaderIcon={this.props.loaderIcon} docUrl={this.props.docUrl} source={this.props.source} client={this.props.client}
      SPUserName={this.props.SPUserName}
      boxUserGroupIds={this.props.boxUserGroupIds}
      boxUserId={this.props.boxUserId}
      accessToken={this.props.accessToken}
      refreshToken={this.props.refreshToken}
      userEmail={this.props.userEmail}
      dgiUserName={this.props.dgiUserName}
      selectedDocInfo={this.props.selectedDocInfo}
      />;
      classForDocTree = "active";
    } else if (this.state.activeTab === "docAnnotation") {
      // tabToDisplay = <Annotation loaderIcon={this.props.loaderIcon} source_id={this.props.item.sp_doc_id} source={this.props.source} client={this.props.client}  edocsBackendAPI={this.props.edocsBackendAPI}/>;
      tabToDisplay = <div>Collections </div>
      classForAnotation = "active";
    } else if (this.state.activeTab === "docAttachment") {
      // tabToDisplay = <Attachment item={this.props.item} resultSource={this.props.resultSource}/>;
      tabToDisplay = <div>Versions Tab</div>
      classForAttachment = "active";
    }
    
    return (
      <div className="sliding-panel-wrapper">
          {/* <div className="float-to-right right-panel-close-btn"><button className="btn btn-link" onClick={this.props.closeRightPanel}><i className="fa fa-window-close" aria-hidden="true"></i></button> </div>  */}
          <span className="close-filter-icon" onClick={this.props.closeRightPanel}>X</span>
          <ul className="nav nav-tabs">
          <li className={classForProperties}>
              <a
                data-toggle="tab"
                id="docProperties"
                onClick={this.setActiveTab}
                href="/"
              >
               <i class="fa fa-tasks" aria-hidden="true"></i>  Properties  
              </a>
            </li>
            <li className={classForDocPre} >
              <a
                data-toggle="tab"
                id="docPreview"
                onClick={this.setActiveTab}
                href="/"
              >
                <i class="fa fa-eye" aria-hidden="true"></i> Preview 
              </a>
            </li>

            <li className={classForAnotation}>
              <a
                data-toggle="tab"
                id="docAnnotation"
                onClick={this.setActiveTab}
                href="/"
              >
               <i class="fa fa-bars" aria-hidden="true"></i> Collections  
              </a>
            </li>
            <li className={classForShared}>
              <a
                data-toggle="tab"
                id="docShare"
                onClick={this.setActiveTab}
                href="/"
              >
               <i class="fa fa-share-alt" aria-hidden="true"></i>  Sharing 
              </a>
            </li>
            
            <li className={classForVersions}>
              <a
                data-toggle="tab"
                id="docVersions"
                onClick={this.setActiveTab}
                href="/"
              >
               <i class="fa fa-code-fork" aria-hidden="true"></i>  Versions
              </a>
            </li>
            {/* <li className={classForDocTree}>
              <a
                data-toggle="tab"
                id="docTree"
                onClick={this.setActiveTab}
                href="/"
              >
                Document Tree
              </a>
            </li> */}
            {/* <li className={classForAnotation}>
              <a
                data-toggle="tab"
                id="docAnnotation"
                onClick={this.setActiveTab}
                href="/"
              >
                Tags
              </a>
            </li> */}
            {(this.props.item != undefined && this.state.showAttachmentTab) && (
              <li className={classForAttachment}>
                <a
                  data-toggle="tab"
                  id="docAttachment"
                  onClick={this.setActiveTab}
                  href="/"
                >
                  Attachments
                </a>
              </li>
            )}
          </ul>
        <div className="tab-content tree-border">
          <div id="home" className="tab-pane fade in active">
            {tabToDisplay}
          </div>
        </div>
      </div>
    );
  }
}

export default RightPanel;
