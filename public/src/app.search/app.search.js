import React, { Component } from "react";
import ReactDOM from 'react-dom';
//import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import axios from "axios";
import { Badge, Card, Button } from "react-bootstrap";
import AppHeader from "../app.header/app.header";
import AppHome from "../app.home/app.home";
import "./app.search.css";
import AppFacet from "../app.facet/app.facet";
import getHighLightedResult_NBA from "../helpers/getHighLightedResult_NBA";
import getPipelineTokenMap from "../helpers/getPipelineTokenMap";
import getFormattedResultforSolr from "../helpers/getFormattedResultforSolr";
import SlidingPane from "react-sliding-pane";
import "react-sliding-pane/dist/react-sliding-pane.css";
import DocumentPreview from "../app.documentPreview/app.documentPreview";
import RightPanel from "../app.rightPanel/app.rightPanel";
import DocumentLibrary from "../app.documentLibrary/app.documentLibrary";
import contract1 from "../images/contract-1.png";
import contract2 from "../images/contract-2.png";
import contract3 from "../images/contract-3.png";
import People from "../app.people/app.people";
import Teams from "../app.Teams/app.Teams";
import Matters from "../app.matters/app.matters";
import WorkSpaces from "../app.workSpaces/app.workSpaces";
import Outbox from "../app.outbox/app.outbox";
import { withRouter } from "react-router-dom";
import AppToaster from '../app.toasters/app.toasters'
import AppNotes from '../app.reactStickies/app.reactStickies'
import AppFileInfoPanel from '../app.fileInfoPanel/app.fileInfoPanel'
import PipelineTokens from '../app.pipelineTokens/app.pipelineTokens'
import AppExpansions from '../app.semanticExpansions/app.semanticExpansions'
import Suggestions from '../app.suggestions/app.suggestions'

import NoteSlider from "../app.slickNotes/app.noteSlider"; 
import noteImage from "../images/note-icon-new.png"; 
// import spImage from "../images/SP.png"; 
import AppSettings from "../app.settings/app.settings.js";

var utilsJs = require('../helpers/utils');
var utils = new utilsJs();

import DocumentLibrarySolr from "../app.documentLibrary/app.documentLibrarySolr";

class Search extends Component {
  constructor(props) {
    super(props);
    this.userName = props.user;
    this.userEmail = props.userEmail;
    if(props.location.search.indexOf('code') == -1 && props.location.search.indexOf('error=access_denied') == -1) {
        localStorage.setItem("user", this.userName);
        localStorage.setItem("userEmail", this.userEmail);
    }
    this.client = this.getClient(props.userEmail);
    this.storage = [];
    this.getClientConfigs();
    this.getEnvConfigs();
    this.state = {
      isSuggestionBoxVisible : false,
      queryId: "",
      clickedtoken: "",
      tokenMap: {},
      pinnedConcepts: [],
      edocsBackendAPI: "",
      showExpansions: false,
      pipelineTokens: [],
      showCategories: true,
      modalTitle: "",
      modalSize: "",
      modalContent: "",
      showToaster: false,
      loaderIcon: "",
      sources: [],
      tabs: [],
      boxAuthorized: false,
      spAuthorized: false,
      autosuggestCategory: "",
      resultSource: 'searchService',
      boxUserId: "",
      boxUserGroupIds: [],
      SPUserName: "",//"richas@dgi",
      SPpassword: "",//"GY@ca49nst",
      showSettings: false,
      dgiUserName: 'richas@dgi',
      isNotesOpen: false, 
      accessToken: "",
      userList: [],
      userNotes: {},
      viewedfacets: [],
      dgiUserlist:[],
      mainTreeList:null,
      versionTreeList:null,
      showFileInfo: false,
      landingPage: false,
      showAppNotes: false,
      errorInResponse: true,
      showSWS:false,
      searchWithin: false,
      searchWithinQuery: "",
      selectedDocId:"",
      selectedDoc: {},
      intervalFacetQuery:["{!key=day}[NOW-1DAY,NOW]","{!key=week}[NOW-7DAY,NOW]","{!key=month}[NOW-30DAY,NOW]","{!key=older}[1900-01-01T00:00:00Z,NOW]"],
      showLogin : true,
      searchQuery: "",
      modalShow: false,
      loading: false,
      showFilters: false,
      totalCount: 0,
      filterPayload: null,
      page: 1,
      pageOffset: 0,
      sizePerPage: 50,
      facets: [],
      intervalFacets: [],
      rows: [],
      columnDefs: [
        {
          headerName: "dataset_key",
          field: "dataset_key",
        },
        {
          headerName: "item_type",
          field: "item_type",
        },
        {
          headerName: "Title",
          field: "title",
        },
      ],
      rowData: [],
      exampleItems: [],
      pageOfItems: [],
      displayCount: 0,
      itemsPerPage: 10,
      pageNo: 1,
      pager: {},
      username: "",
      domain: "",
      itemCount: 25,
      selectedFacets: [],
      filterObj: {},
      isPaneOpen: false,
      selectedDocumentUrl: "",
      activeRightPanelTab: "",
      pageSize: 10,
      showDash: false,
      showInbox: false,
      showSearch: true,
      showLib: false,
      showOutbox: false,
      showRecents: false,
      showUrl: false,
      item: [],
      isLoading: false,
      showPeople: false,
      showTeams: false,
      showMatters: false,
      showWorkSpaces: false,
      timeCaptureTotal : '',
      sessionId : '',
      userIP : '',
      showLibSolr: false,
      boxAuthUser: "",
      sort:"score desc",
      itemToAddToCollection: {},
      showCollection: false
    };


    if(props.location.search.indexOf('code') == -1 && 
        props.location.search.indexOf('error=access_denied') == -1 && 
        props.user == "") {
      localStorage.setItem("user", "");
      localStorage.setItem("userEmail", "");
      this.props.history.push("/login");
    } 
    else {
      this.getUsersList();
      if(props.location.search.indexOf('code') > -1) {
        this.userName = localStorage.getItem("user");
        this.userEmail = localStorage.getItem("userEmail");
        let searchString = props.location.search;
        let codeIndex = searchString.indexOf('code');
        
        let code = searchString.substr(codeIndex+5, searchString.length);
        this.getAuthTokenByCode(code);
        this.props.history.push("/search");
      } else if (props.location.search.indexOf('error=access_denied') > -1) {
        this.userName = localStorage.getItem("user");
        this.userEmail = localStorage.getItem("userEmail");
         this.props.history.push("/search");
         setTimeout(()=>{
          this.setState({
            "modalTitle": "Box Access Denied",
            "modalContent": "You have denied access to Box. Please authorize for Box in Integrations.",
            "modalSize": "md",
            "showToaster": true,
            "accessToken": "",
            "refreshToken": "",
            "boxAuthorized": false,
            isLoading: false
           })
           this.saveRefreshTokenToSolr("");
         }, 700)

      } else {
        setTimeout(()=>{
          var refreshToken;
          if(this.userEmail && this.state.userDBData) {
            var ind = this.state.userDBData
              .map(function (d) {
                return d["digital_email"].toLowerCase();
              })
              .indexOf(this.userEmail.toLowerCase());
              if (ind > -1) {
                refreshToken = this.state.userDBData[ind].box_token;
                if(refreshToken) {
                  this.getAuthCodeByRefreshToken(refreshToken);
                } else {
                  if(this.state.spAuthorized==false) {
                    this.setState({'showSettings': true})
                  } 
                  console.log("Please authorize for Box")
                }
              } 
            }
        },1000);

      }

      // this.showLandingResults();
      this.getUserIP();
      
     // this.getTreeData();
     
      //this.getShareByMeDocument();
    }

    // if(this.state.boxAuthorized==false && this.state.spAuthorized==false) {
    //   this.setState({'showSettings': true})
    // }
    this.onChangePage = this.onChangePage.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.clearFilter = this.clearFilter.bind(this);
    this.performSearch = this.performSearch.bind(this);

    this.openRightPane = this.openRightPane.bind(this);
    this.loadPage = this.loadPage.bind(this);
    this.getFileExtension = this.getFileExtension.bind(this);
    this.getBreadCrum = this.getBreadCrum.bind(this);
    this.state.sessionId = sessionStorage.SessionID = this.accessKey() + this.accessKey() + this.accessKey();
    this.isFiltersSelected = false;
    this.isPaginationDone = false;
  }

  getEnvConfigs = () => {
    axios.get("/getEnvConfigs").then((response, error) => {
        if(response) {
            this.setState({
                "edocsBackendAPI": response.data.edocsBackendAPI
              });
        }

      })
  }


  getClient = (userEmail) => {
    let email = userEmail.toLowerCase();
    let patterns = ['alnylam', 'nba'];
    let selectedPattern;
    for(var i = 0; i< patterns.length; i++) {
      selectedPattern = patterns[i];
      if(email.indexOf(selectedPattern) > -1 ) {
        return selectedPattern;
      }
    }

    //return "alnylam"
    return "nba"
  }


  getUserIP = () => {
    if($) {
      $.getJSON('https://jsonip.com/?callback=?',(data) => {
        this.state.userIP = data.ip;
      });
    }

  }

  getClientConfigs = () => {
    axios.get("/clientConfig/"+this.client).then((response, error) => {
      this.setState({"ExplorerFacets": response.data.ExplorerFacets,
                      "sources": response.data.Sources,
                      "tabs":response.data.Tabs,
                      "loaderIcon": response.data.Loader,
                      "background": response.data.Background,
                      "logo": response.data.Logo,
                      "edocsLogo": response.data.edocsLogo,
                      "solrCollection": response.data.solrCollection,
                      "serviceType": response.data.serviceType,
                      "legacyDataset": response.data.legacyDataset,
                      "eventAppId": response.data.eventAppId,
                      "datasetBox": response.data.datasetBox,
                      "datasetSP": response.data.datasetSP,
                      "spDataset": response.data.spDataset,
                      "showPilotSticker": response.data.showPilotSticker
                    });
    })
  }

  AuthorizeBoxUser = () => {
    window.open("https://account.box.com/api/oauth2/authorize?response_type=code&client_id=94u5zs5ed1mwc4jl6jdvo1xk51f0it27", "_self")

  }

  getAuthTokenByCode = (code) => {
    var request = {
      "code": code,
      "user": this.userEmail
    }
    axios.post("/Box/getAuthTokenByCode", request).then((response, error) => {
      if(response && response.data && response.data.accessToken) {
       this.setState({"accessToken": response.data.accessToken, "refreshToken": response.data.refreshToken,
                        "client": response.data.client, "boxAuthorized": true})
       this.saveRefreshTokenToSolr(response.data.refreshToken);
       this.getUserBoxId();
        this.setState({
          "modalTitle": "Box Authentication",
          "modalContent": "Your Box authentication is <strong>successful</strong>. You can perform search now.",
          "modalSize": "md",
          /* "showToaster": true, */
          "showSettings": true
        })
      } else {
          this.setState({
            "modalTitle": "Box Authentication",
            "modalContent": "Your <strong>Box authentication</strong> has failed. Please authenticate again in the <strong>Integrations</strong>.",
            "modalSize": "md",
            "showToaster": true,
            "accessToken": "",
            "refreshToken": "",
            "boxAuthorized": false
           })
      }
    });
  }


  getAuthCodeByRefreshToken = (refreshToken) => {
    var request = {
      "refreshToken": refreshToken,
      "user": this.userEmail
    }
    axios.post("/Box/getAuthTokenByRefreshToken", request).then((response, error) => {
      if(response && response.data && response.data.accessToken) {
        this.setState({"accessToken": response.data.accessToken,
                       "refreshToken": response.data.refreshToken,
                       "boxAuthorized": true,
                      })
       
        this.saveRefreshTokenToSolr(response.data.refreshToken);
        this.getUserBoxId();
      } else {
        if(this.state.spAuthorized==false) {
          this.setState({'showSettings': true})
        } else {
          this.setState({
            "modalTitle": "Box authorization",
            "modalContent": "Your <strong>Box</strong> access has expired. Please authorize again in the <strong>Integrations</strong>.",
            "modalSize": "md",
            "showToaster": true,
            "accessToken": "",
            "refreshToken": "",
            "boxAuthorized": false,
            "boxAuthUser": ""
           })
        }

      }
    });
  };

  getUserBoxId = () => {
    // axios.get("/Box/getCurrentUserId").then((response, error) => {
    axios.get("/Box/getCurrentUserId/"+ this.userEmail).then((response, error) => {
      if(response.data.userid) {
        this.getUserBoxGroups(response.data.userid);
        this.setState({"boxUserId": response.data.userid, boxAuthUser: response.data.boxUser})
        this.performSearch();
      }
    });
  }


  getUserBoxGroups = (userid) => {
    // axios.get("/Box/getUserGroups/"+userid).then((response, error) => {
      var request = {
        "userid": userid,
        "userEmail": this.userEmail
      }
      axios.post("/Box/getUserGroups", request).then((response, error) => {
      if(response && response.data && response.data.memberships) {
        let mems = response.data.memberships;
        var groupIds=[];
        if(mems && mems.entries && mems.entries.length>0) {
          let entries = mems.entries;
          for(let i=0; i<entries.length; i++) {
            groupIds.push('G_'+entries[i].group.id)
          }
        }
      }
      this.setState({"boxUserGroupIds": groupIds})
    })
  }


  saveRefreshTokenToSolr = (refreshToken) => {
    setTimeout(()=>{
      if(this.userEmail && this.state.userDBData) {
        var ind = this.state.userDBData
          .map(function (d) {
            return d["digital_email"].toLowerCase();
          })
          .indexOf(this.userEmail.toLowerCase());
          if (ind > -1) {
            let userData = this.state.userDBData;
            let userRow = this.state.userDBData[ind];
            if(userRow._version_) {
              delete userRow._version_;
            }
            if(userRow.indexed_date) {
              delete userRow.indexed_date;
            }
            userRow.box_token = refreshToken;
            var request = {"data" : userRow}
            axios.post("/Solr/setUserData", request);
            this.setState({userDBData: userData})
          } else {
              var dataRow = {
                "digital_email": this.userEmail
              }
              dataRow.box_token = refreshToken;
              var request = {"data" : dataRow}
              axios.post("/Solr/setUserData", request);
              this.setState({userData: dataRow})
            }
        }
    }, 1000)

  }


  getUsersList = () => {
    var searchRequest = {
        'prop': 'digital_email',
         'value': this.props.userEmail
    }
    // var searchRequest = {
    //   'prop': '*',
    //    'value': '*'
    // }
    var postgreusernotes;
    
    // axios.post("/Postgres/getUserNotesFromPostgres", searchRequest).then((res, error) => {
    
      // postgreusernotes = res['data'][0].usernotes;
      // console.log(458);
      axios.post("/Solr/getUserData", searchRequest).then((response, error) => {
      if(response && response.data && response.data.docs && response.data.docs.length>0) {
        var userDBData = response.data.docs;
        this.setState({
          userDBData: response.data.docs
        });
        if(this.userEmail) {
          var ind = userDBData
            .map(function (d) {
              return d["digital_email"].toLowerCase();
            })
            .indexOf(this.userEmail.toLowerCase());
            if (ind > -1) {
              // userDBData[ind].user_notes = postgreusernotes;
              this.setState({
                // userNotes: postgreusernotes ? postgreusernotes : "",
                userData: userDBData[ind]
              });
            } else {
              var row = {
                "digital_email": this.userEmail,
                "name": this.userName ? this.userName : "",
                "user_notes": "",
                "box_token": ""
              }
              var request = {"data" : row}
              axios.post("/Solr/setUserData", request);
            }
            // console.log('userNotes...'+state.userNotes);
          }
        // searchRequest.data = {'digital_email': 'jaquline.charles@thedigitalgroup.com', 'name': 'Jaquline Charles'}
        //   axios.post("/Solr/removeUser").then((response, error) => {
        // })
      } else {
        this.setState({
          userDBData: {}
        });
      }

    })
  // });
    // var userList = [];
    // let request = {};
    // let requestUrl = "";
    // if(this.state.sources.indexOf('Sharepoint') >-1) {
    //   request = {
    //     "ConnectorSourceName":"SharePoint"
    //   }
    //   requestUrl = "https://sp16-nba.thedigitalgroup.com:9091/api/v1/sharepointusers";
    // }
    // request["ClientName"]= this.client;
    // axios.post(requestUrl, request).then(response => {

    // if(response.data) {
    //     userList = response.data
    //   }

    //   this.setState({
    //       dgiUserlist: userList
    //   }, () => {this.getUserDgiName()});
    // })
  }


  getUserDgiName = () => {
    var userList = this.state.dgiUserlist;
    var userEmail = this.userEmail;
    if(userEmail) {
      var ind = userList
      .map(function (d) {
        return d["DigitalEmail"].toLowerCase();
      })
      .indexOf(userEmail.toLowerCase());
      if (ind > -1) {
        let dgiUserName = userList[ind].Email;
        let dgiIndex = dgiUserName.indexOf('.com');
        dgiUserName = dgiUserName.substr(0, dgiIndex);
       
        this.setState({dgiUserName: dgiUserName},() => {
          this.getTreeData();
      });
       // this.getShareWithMeDocument(dgiUserName);
       //this.getModifiedDocList(dgiUserName);

      }
    }

  }

 

  // getDownloadFileURL = (refreshToken) => {
  // 
  //   var req={
  //     "name":"Alnylam-Corporate-Presentation.pdf",
  //     "description":"Alnylam-Corporate-Presentation",
  //     "FileID":"732466922515",
  //     "FolderID":"0",
  //     "refreshToken":refreshToken
  //   }
  //   axios.post("/Box/getAllFiles",req)
      
  //     .then((response) => {
  //      
  //       // let demo = response ;
  //       // console.log("Demo",demo)
       
  //   });
  // };

  getTreeData() {
    setTimeout(()=>{
      let tRequest;
      let tRequestUrl="";
      let dgiUsername =this.state.dgiUserName.split("@")[0];
      if(this.state.sources.indexOf('Sharepoint') >-1) {
        tRequest = {
              "ConnectorSourceName":"SharePoint",
              "DGIUserName": dgiUsername,
              "SPUserName": this.state.SPUserName,
              "SPPassWord": this.state.SPpassword
          }
          tRequestUrl = this.state.edocsBackendAPI+"api/v1/document/getsharepointdata";
      }
      tRequest["ClientName"]= this.client;
      axios.post(tRequestUrl, tRequest).then(docTreeDetails => {
      // axios.get("https://sp16-nba.thedigitalgroup.com:9091/api/v1/document/getsharepointdata/"+dgiUsername).then((docTreeDetails) => {
        this.setState({mainTreeList: docTreeDetails.data})
        setTimeout(()=>{
            let request;
            let requestUrl="";
            if(this.state.sources.indexOf('Sharepoint') >-1) {
                request = {
                    "ConnectorSourceName":"SharePoint",
                    "DGIUserName": dgiUsername,
                    "SPUserName": this.state.SPUserName,
                    "SPPassWord": this.state.SPpassword
                }
                requestUrl = this.state.edocsBackendAPI+"api/v1/documentversions";
            }
            request["ClientName"]= this.client;
            // axios.get("https://sp16-nba.thedigitalgroup.com:9091//api/v1/documentversions/sharepoint/"+dgiUsername).then((docTreeVersions) => {
            axios.post(requestUrl, request).then(docTreeVersions => {
            this.setState({versionTreeList: docTreeVersions.data}) 
            }); }, 500)
    });
  }, 500)
  }
 
  showLandingResults = () => {
    this.setState(
      {
        isLoading: true,
      });

      // setTimeout(()=>{
      //   this.setState(
      //     {
      //       isLoading: false,
      //     });
      // }, 8000)

      const promises = [];
      if(this.state.sources.indexOf('Sharepoint') >-1) {
        let searchRequest = {
          "ViewedBy": this.userEmail ? this.userEmail : "richa.sinha@thedigitalgroup.com",
          "ConnectorSourceName":"SharePoint"
        }
        searchRequest["ClientName"]= this.client;
        const solrsearch = () => {
          return new Promise((resolve, reject) => {
            axios.post(this.state.edocsBackendAPI+"api/v1/vieweditems", searchRequest).then((response) => {
              try{
                resolve(response);
              } 
              catch(err) {
                this.setState(
                  {
                    isLoading: false,
                  });
              }
            
            });
          })
        }
        promises.push(solrsearch());
        Promise.all(promises)
      .then(response => {
          var finalResponse={"data": []};
          for(var j=0; j<response.length; j++) {
            finalResponse.data = finalResponse.data.concat(response[j].data);
          }
          this.processResponse(finalResponse);
        })
      }
  }


  processResponse = (response) => {
    this.setState(
      {
        pageOffset: 0,
        isLoading: false,
      },
      () => {
        let pageOffset = this.state.pageOffset - 1;
        if (response.data) {
          let responses = {};

          response.data.map((item, index) => {
            response.data[index]['title'] = [response.data[index]['title']];
            response.data[index]['author'] = [response.data[index]['author']];
            response.data[index]['keywords'] = response.data[index]['keywords']==""? [] : [response.data[index]['keywords']];
          });
          let resp = {"data" :{"searchResponse": {"response": {"docs": response.data}}}};

          let dataStore = {
            displayCount: 0,
            itemsPerPage: 50,
            pageNo: 1,
          };
          let res = getHighLightedResult_NBA(resp.data, dataStore);
          this.setState({
              resultDataset: res,
              selectedDoc: resp.data.searchResponse.response.docs.length>0 ? resp.data.searchResponse.response.docs[0]: {},
            }, ()=> this.formatReponses())
          this.setState({
            rowData: resp.data.searchResponse.response.docs,
            totalCount: resp.data.searchResponse.response.docs.length,
            landingPage: true
          });
        }
    })
  }


  formatReponses = () => {

    let pageOffset = this.state.pageOffset - 1;
    let res = this.state.resultDataset;
    let responses = res.map((item, index) => {
      let fileExtension = this.getFileExtension(item.url);
      let fileName = unescape(this.getFileName(item.url));
      item.showUrl = unescape(item.url);
      let breadkCrum = this.getBreadCrum(item.url);
      let itemClass = "fa fa-bookmark";
      if (fileExtension == "pdf") {
        itemClass = "fa fa-file-pdf-o";
      } else if (fileExtension == "docx" || fileExtension == "doc") {
        itemClass = "fa fa-file-word-o";
      } else if (fileExtension == "msg") {
        itemClass = "fa fa-envelope";
      } else if (fileExtension == "xls" || fileExtension == "csv" || fileExtension == "xlsx") {
        itemClass = "fa fa-file-excel-o";
      } else if (fileExtension == "ppt" || fileExtension == "pptx") {
        itemClass = "fa fa-file-powerpoint-o";
      }
      item.fileExtension = fileExtension;
      let today = new Date(item.creation_date);
      let sdate = new Date(item.signed_date);
      let edate = new Date(item.expiry_date);
      var options = { year: 'numeric', month: 'long', day: 'numeric' };
      let date = today.toLocaleDateString("en-US", options);
      let signed_date = sdate.toLocaleDateString("en-US", options);
      let expiry_date = edate.toLocaleDateString("en-US", options);
      pageOffset += 1;
      return (
        <div 
        className={"result-child1 "+ (this.state.selectedDoc.sp_doc_id==item.sp_doc_id ? "selected-document":"")} 
        id={item.id} key={index + 1}
          id={item.id} key={index + 1}>
          <div
            style={{
              float: "right",
            }}
          >
            {item.authorsStr &&
            <span><i className="fa fa-book" title="Author" style={{fontSize:"15px", 'marginRight':"5px"}} aria-hidden="true"></i>
            <p
              title="Author"
              style={{ display:"inline-block", fontSize:"12px", textTransform:"capitalize" }}
              dangerouslySetInnerHTML={{
                __html: `${item.authorsStr}`,
              }}
            /> 
            </span>
            }
          </div>
          <div
            style={{
              fontSize: "14px",
              color: "#1d4289",
              margin: "0",
              fontWeight: "700",
            }}
            className="resultarea"
          >
            <span className="" aria-hidden="true" style={{marginRight: "5px"}}>
              {pageOffset + 1 + ". "}
            </span>

            {item.title &&
            <span
              href="/"
              title={item.showUrl}
              style={{
                cursor: "pointer",
              }}
              onClick={(e) =>
                this.changeSelectedDoc(item)
              }
              dangerouslySetInnerHTML={{
                __html: `${item.title}`,
              }}
            ></span>
            }
            {!item.title && fileName &&
            <a
              href="/"
              title={item.url}
              onClick={(e) =>
                this.openRightPane(e, item.url, "docPreview", item)
              }
              dangerouslySetInnerHTML={{
                __html: `${fileName}`,
              }}
            ></a>
            }
            

          </div>

          <div className="result-child2">
            <a
              className={itemClass}
              href={item.showUrl} target="_blank"
              style={{ color: "#1d4289", margin: "0px 6px 0px 0px", "cursor":"pointer!important","fontWeight": "bold", float: "left" }}
              aria-hidden="true"
            ></a> {breadkCrum} 
            <a
              href="/"
              className="doc-tree-icon"
              onClick={(e) =>
                this.openRightPane(e, item.url, "docPreview", item)
              }
            >
              <i className="fa fa-sitemap" aria-hidden="true"></i>
            </a>
            {item.embd_doc_num !== undefined && this.isAttach(item) &&(
              <a
                href="/"
                className={
                  this.isEmbd(Object.keys(item.highlight))
                    ? "doc-attachment-true"
                    : "doc-attachment"
                }
                onClick={(e) =>
                  this.openRightPane(e, item.url, "docAttachment", item)
                }
              >
                <i className="fa fa-paperclip" aria-hidden="true"></i>
              </a>
            )}
          </div>
          <ul
              style={{
                display: "flex",
                justifyContent: "flex-start",
                // marginTop: "1rem",
                marginBottom: "1rem",
                fontSize: "1.25rem",
              }}
              className="list-parent"
            >
              {item.ViewedOn && (
                <li className="list-item">
                  <span><i className="fa fa-calendar" style={{fontSize:"13px", 'marginRight':"10px"}} aria-hidden="true"></i></span>
                  <span><b>Viewed: </b></span>
                  <span>
                    {item.ViewedOn}
                    </span>
                </li>
              )}
              {item.creation_date && (
                <li className="list-item">
                  <b>Created: </b>
                  <span>
                    {item.creation_date}
                    </span>
                </li>
              )}
              {item.signed_date && (
                <li className="list-item">
                  <b>Signed: </b>
                  <span>
                    {signed_date}
                    </span>
                </li>
              )}
              {item.expiry_date && (
                <li className="list-item">
                  <b>Expiry: </b>
                  <span>
                    {expiry_date}
                    </span>
                </li>
              )}
              </ul>
          {item.desc &&
          <div dangerouslySetInnerHTML={{ __html: `${item.desc}` }} />
          }

          <ul
            style={{
              display: "flex",
              justifyContent: "flex-start",
              marginTop: "0rem",
              marginBottom: "2rem",
              fontSize: "1.25rem",
            }}
            className="list-parent"
          >
            {item.displayKeywords.length > 0 && (
              <li className="list-item">
                <b><i className="fa fa-tags" aria-hidden="true" title="Tags"></i></b>
                <span
                  dangerouslySetInnerHTML={{
                    __html: ` ${item.displayKeywords}`,
                  }}
                />
              </li>
            )}
            {item.displayTeam.length > 0 && (
              <li className="list-item">
                <b>Teams: </b>
                <span
                  dangerouslySetInnerHTML={{
                    __html: `${item.displayTeam}`,
                  }}
                />
              </li>
            )}
            {item.displayPlayers.length > 0 && (
              <li className="list-item">
                <b>Players: </b>
                <span
                  dangerouslySetInnerHTML={{
                    __html: `${item.displayPlayers}`,
                  }}
                />
              </li>
            )}
            {item.displayGames.length > 0 && (
              <li className="list-item">
                <b>Games:</b>{" "}
                <span
                  dangerouslySetInnerHTML={{
                    __html: `${item.displayGames}`,
                  }}
                />
              </li>
            )}
            {item.displayOfficials.length > 0 && (
              <li className="list-item">
                <b>Officials:</b>{" "}
                <span
                  dangerouslySetInnerHTML={{
                    __html: `${item.displayOfficials}`,
                  }}
                />
              </li>
            )}
            {item.document_state && (
              <li className="list-item">
                <b>Document Category:</b>{" "}
                <span
                  dangerouslySetInnerHTML={{
                    __html: `${item.document_state}`,
                  }}
                />
              </li>
            )}
            {item.amount && (
              <li className="list-item">
                <b>Amount: </b>$
                <span
                  dangerouslySetInnerHTML={{
                    __html: `${item.amount}`,
                  }}
                />
              </li>
            )}
            {item.emailToStr && (
              <li className="list-item">
                <b>Email To:</b>{" "}
                <span
                  dangerouslySetInnerHTML={{
                    __html: `${item.emailToStr}`,
                  }}
                />
              </li>
            )}
            {item.emailFromStr && (
              <li className="list-item">
                <b>Email From:</b>{" "}
                <span
                  dangerouslySetInnerHTML={{
                    __html: `${item.emailFromStr}`,
                  }}
                />
              </li>
            )}
          </ul>
        </div>
      );
    });
    if (responses.length == 0) {
  
    } else {
      this.setState({errorInResponse: false})
    }
    this.setState({
      exampleItems: responses,
      showFileInfo: true
    });
  }


  changeSelectedDoc = (item) => {
    this.setState(
      {'selectedDoc': item,
      'showFileInfo': false},
      () => {
        this.formatReponses()
      }
    );
  }

  changeSelectedDocument = (item) => {
    this.setState(
      {'selectedDoc': item,
      'showFileInfo': false, showCollection: false, itemToAddToCollection: {}},
      () => {
        if(this.state.resultSource=='solr') {
          this.formatsolrsearchresponse();
        } else {
          this.formatSearchResponse()
        }
      }
    );

  }


  loadPage(param1, param2, param3) {
    this.isPaginationDone = true;
    this.setState(
      { pageOffset: param1, pageSize: param2, pageNo: param3 },
      () => {
        this.performSearch();
      }
    );
  }
  clearFilter(item) {
    let { selectedFacets } = this.state;
    selectedFacets = selectedFacets.filter((elem) => {
      return elem.name.toLowerCase() != item.name.toLowerCase();
    });
    this.onFilterSelection(selectedFacets);
  }
  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  onChangePage(pageOfItems, pager) {
    // update state with new page of items
    this.setState({ pager: pager, pageOfItems: pageOfItems });
  }

  openRightPane(e, urlToOpen, activeTab, item) {
    e.preventDefault();
    // let selectedDocumentInfo = this.state.selectedDoc
    this.setState({
      sharepoint_id : item.sp_doc_id,
      selectedDocumentUrl: urlToOpen,
      activeRightPanelTab: activeTab,
      isPaneOpen: true,
      item: item
    });
  
  }


  getFileExtension(url) {
    var matches = url.match(/\/([^\/?#]+)[^\/]*$/);
    if (matches && matches.length > 1) {
      let fileName = matches[1].split(".");
      if (fileName !== undefined && fileName.length > 0) {
        let fileExtension = fileName[fileName.length - 1];
        return fileExtension;
      } else {
        return matches[1];
      }
    }
    return null;
  }

  getFileName(url) {
    var matches = url.match(/\/([^\/?#]+)[^\/]*$/);
    if (matches && matches.length > 1) {
      let fileName = matches[1].split(".");
      if (fileName !== undefined && fileName.length > 0) {
        fileName = fileName[0];
        return fileName;
      } 
    }
    return null;
  }

  getBreadCrum = (titleUrl, source_type) => {
    let urlParts = unescape(titleUrl).split("/");
    if(this.state.resultSource=='searchService') {
      urlParts.splice(0, 5);
    }
    if(source_type && source_type.indexOf('sharepoint')>-1) {
      urlParts.splice(0, 5);
    }
    // urlParts.splice(-1, 1);
    let breadkCrum = <div className="bread-crum-wrapper pull-left">{urlParts.map((folder, index) => {
        let crum = <span key={index} className="bread-crum-span">{folder} <i className="fa fa-caret-right" aria-hidden="true"></i> </span>
        if (index === (urlParts.length -1)) {
          crum = <span key={index} className="bread-crum-span">{folder}</span>
        }
        return crum
    })}
    </div>

    return breadkCrum;
  }

  searchNotesQuery = (data) => {
    this.setState({
      searchQuery: data,
      showAppNotes: false
    }, () => {
      this.performSearch('reset');
    });
  }

  updateUserNotes = (notes) => {
    this.focusDiv();
    var userEmail = this.userEmail;
    // if(userEmail) {
    //   var userDBData = this.state.userDBData;
    //   var ind = userDBData
    //     .map(function (d) {
    //       return d["digital_email"].toLowerCase();
    //     })
    //     .indexOf(userEmail.toLowerCase());
    //     var noteRow;
    //     if (ind > -1) {
    //       noteRow = userDBData[ind];
    //     } else {
    //       //create a row in the db
    //       var noteRow = {
    //         "digital_email": userEmail
    //       }
    //     }
    //     noteRow.user_notes = notes;
    //     noteRow.name = this.userName ? this.userName : "";
    //     if(noteRow._version_) {
    //       delete noteRow._version_;
    //     }
    //     if(noteRow.indexed_date) {
    //       delete noteRow.indexed_date;
    //     }
    //     var request = {"data" : noteRow}
    //     axios.post("/Solr/setUserData", request);
    // }
    this.setState({showAppNotes: false})
  }

  gotoMainSearch = () => {
    this.setState({
      queryTokens: [],
      showCategories:true,
      page: 1,
      pageOffset: 0,
      pageNo: 1,
      searchWithinQuery:'',
      searchWithin: false,
      showSWS: false,
      tokenMap: {},
      pinnedConcepts: [],
      pipelineTokens: [],
      clickedtoken: ""
    },
    () => {
      this.performSearch();
    });
  }


  resetProps = () => {
    this.setState({
      queryTokens: [],
      showCategories:true,
      page: 1,
      pageOffset: 0,
      pageNo: 1,
      searchWithinQuery:'',
      autosuggestCategory: "",
      searchWithin: false,
      selectedFacets: [],
      filterObj: {},
      tokenMap: {},
      pinnedConcepts: [],
      pipelineTokens: [],
      clickedtoken: "",
      queryId: ""

    },
    () => {
      this.onFilterSelection([]);
    });
  }

  performSearchWithinSearch = () => {
    this.setState({
      searchWithin: true
    });
    setTimeout(()=> {
      this.performSearch();
    },200)
  }

  setSearchQuery = (value) => {
    this.setState({
      searchQuery: value
    })
  }


  onSuggestionClick = (value) => {
    if(value && value.term) {
      let category;
      if(value.payload) {
        category = value.payload.toLowerCase();
        category = category=='tags'? 'keywords' : category;
        category = category.replace(" ","_");
      }

      this.setState({
        searchQuery: value.term,
        queryTokens: [],
        page: 1,
        pageOffset: 0,
        pageNo: 1,
        searchWithinQuery:'',
        selectedFacets: [],
        filterObj: {},
        tokenMap: {},
        pinnedConcepts: [],
        pipelineTokens: [],
        clickedtoken: "",
        autosuggestCategory: category ? category : ""
      },  () => {
        this.performSearch();
      });
    } 
  }

  accessKey = () => {
    return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
}

  performSearch(source) {
    this.setState({
      queryId: "",
      showFileInfo: false,
      showDash: false,
      showInbox: false,
      showSearch: true,
      showLib: false,
      showOutbox: false,
      showRecents: false,
      showPeople: false,
      showTeams: false,
      showMatters: false,
      showWorkSpaces: false,
      errorInResponse: true,
      landingPage: false
    });
    if(source=='reset') {
      this.resetProps();
      return
    }
    this.setState({ isLoading: true, showFilters: false });

    setTimeout(() => {
      this.setState({ isLoading: false });
    }, 10000);
    this.timeStart = performance.now();
    var searchQuery;
    searchQuery = this.state.searchQuery == "" ? "*:*" : this.state.searchQuery;
    var filterObj = this.state.filterObj;
      switch(this.client) {
        case 'nba':
        this.setState({'resultSource': 'searchService'}, () => {
          this.doLegacySearch(searchQuery, filterObj);
        })
          break;
        case 'alnylam':
          //search service flow
          // this.setState({'resultSource': 'searchService'}, () => {
          //   this.doLegacySearch(searchQuery, filterObj, 'alnylam');
          // })
          // solr flow
          this.setState({'resultSource': 'solr'}, () => {
            this.doSolrSearch(searchQuery, filterObj);
          })
          break;
      }
  }

  doSolrSearch = (searchQuery, filterObj) => {

    let AT = this.state.accessToken.toString();
    let acl = [];
    if(this.state.boxUserId!='') {
      acl.push("U_"+this.state.boxUserId);
    }

    if(this.state.boxUserGroupIds.length!==0) {
      acl = acl.concat(this.state.boxUserGroupIds);
    }
    if(acl.length==0 && !this.state.SPUserName) {
      this.setState({showSettings:true})
      return;
    }
    var solrRequest = {
        "searchQuery": searchQuery,
        "pageOffset": this.state.pageOffset,
        "pageSize": this.state.pageSize,
        "serviceType": this.state.serviceType,
        "accessToken": [AT],
        "dataset": this.state.solrCollection,
        "client": this.client,
        "acl" : acl,
        "autosuggestCategory": this.state.autosuggestCategory,
        "tokensFound": acl.length>0 ? true : false,
        "filterObj": this.constructFilterString(this.state.selectedFacets),
         "sort":this.state.sort
    }
    if(this.state.SPUserName!=="") {
      solrRequest["manifoldUsername"] = this.state.SPUserName;
    }
    if(this.state.searchWithin == true && this.state.searchWithinQuery!=='') {
      solrRequest.searchWithinQuery = this.state.searchWithinQuery;
    }

    try{
    axios.post("/SearchSolr/getResults", solrRequest).then((response) => {
        this.timeEnd = performance.now();
        this.state.timeCaptureTotal = ((this.timeEnd - this.timeStart) / 1000).toFixed(3)
        if(response.data) {
          let res = {"data": {"searchResponse": response.data }}
          if(response.data.pipelineResponse && response.data.pipelineResponse.tokens) {
            //create expansion tree for all tokens
            // this.createTokenMap(response.data.pipelineResponse.tokens);
            this.setState({"pipelineTokens": response.data.pipelineResponse.tokens, pinnedConcepts: response.data.pipelineResponse.pinnedConcepts})
            let tokenMap = getPipelineTokenMap(response.data.pipelineResponse.tokens)
            this.setState({"tokenMap": tokenMap})
          } else {
            this.setState({"pipelineTokens": [], pinnedConcepts: []})
          }
          if(response.data.tokens) {
            this.setState({"queryTokens": response.data.tokens})
          }
          if(response.data.runtime_highlight_param && response.data.runtime_highlight_param.queryId) {
            this.setState({"queryId": response.data.runtime_highlight_param.queryId}, ()=> this.getHLData(response.data.runtime_highlight_param.queryId))
          }
          this.processResponseFromSolr(res);
          if ( this.isFiltersSelected == true || this.isPaginationDone == true ) {
            if ( this.isPaginationDone == true ) {
              this.isPaginationDone = false;
              this.sendSearchPaginationEvent();
            }
            if ( this.isFiltersSelected == true ) {
              this.isFiltersSelected = false;
              this.saveFilterApplyEvent();
            }
          }else {
            this.sendSearchEventData();
          }
        } else {
          this.setState({
            resultDataset: []
          });
          this.formatSearchResponse();
        }
      // }

    })
  }
    catch(err) {
    let responses = [];
    let fallback = (
      <div
        style={{
          color: "rgb(116 115 115)",
          fontSize: "1.75rem",
          fontWeight: "600",
          "textAlign": "center",
          "marginTop": "100px"
        }}
      >
        No results found for your search..
      </div>
    );
    responses.push(fallback);
    this.setState({exampleItems: responses, errorInResponse: true, isLoading: false, showFileInfo: true})
      }
  }

  getHLData = (queryId) => {
    var request = {
                    "queryId": queryId
                  }

    axios.post("/search/getHLData", request).then((response) => {
      if(response && response.data && response.data.queryId && 
                    response.data.highlighting && Object.keys(response.data.highlighting).length>0) {
        if(this.state.queryId == response.data.queryId) {
          let dataStore = {
            displayCount: 0,
            itemsPerPage: this.state.pageSize,
            pageNo: this.state.pageNo,
          };
          let rawResponse = this.state.rawResponse;
          if(rawResponse.searchResponse) {
            var newHighlighting = response.data.highlighting;
            var oldHighlighting = rawResponse.searchResponse.highlighting;
            this.updateHighlightingObject(oldHighlighting, newHighlighting);
            let res = getFormattedResultforSolr(rawResponse, dataStore);
            this.setState({
              resultDataset: res
            }, ()=> this.formatsolrsearchresponse())
          }

        }
      }
    });
  }

  updateHighlightingObject = (oldHighlighting, newHighlighting) => {
    Object.keys(newHighlighting).forEach((key, keyIndex) => {
      if(oldHighlighting[key]) {
        oldHighlighting[key].content = newHighlighting[key].content;
      }
    });
  }


  sendSearchEventData = () => {
    var params={};
    let sources = [];
    let datasets = [];
    if(this.state.boxAuthorized == true) {
      sources.push("Box");
      datasets.push(this.state.solrCollection)
    }
    if(this.state.spAuthorized == true) {
      sources.push("Sharepoint");
      datasets.push(this.state.spDataset)
    }

    var multiParams = {
      source_type_ss: sources,
      dataset_ss: datasets
   };
    var tokenCount = this.state.pipelineTokens.length;
    params.conceptcount_i = tokenCount;
    if(tokenCount>1)
      params.querytype_s = "Combo";
    else
      params.querytype_s = "Singular";

    params.SemanticExpansionsCount_i = "";
    params.UserSubCategory_s = "";
    params.UserCategory_s = "";
    this.searcheventRequest = {};
    this.searcheventRequest.event_status = 1;
    this.searcheventRequest.sessionid = this.state.sessionId;
    params.numofresults = this.state.totalCount;
    params.search_responsetime = this.state.timeCaptureTotal * 1000;
    // params.dataset_s = this.state.solrCollection;

    var searchEventData={
        "eventTimestamp" : new Date().toISOString(),
        "eventStatus" : this.searcheventRequest.event_status,
        "appId" : this.state.eventAppId,
        "user" : this.userEmail,
        "application" : this.state.eventAppId,
        "sessionId" : this.searcheventRequest.sessionid,
        "eventType" : 'search',
        "userIP": this.state.userIP,
        "query": this.state.searchQuery,
        "params": params,
        "multiParams": multiParams,
    }
    axios.post("/search/saveSearchEventData", searchEventData);
  }

  sendDocClickEventData(e, item) {

    var params = {};
    var multiParams = {
      source_type_ss: this.state.sources
    };
    params.recommandation_b = false;
    params.ContentType_s = "";
    params.SourceType_s = item.source_type;;
    params.docurl_s = item.url;
    params.pageno_i = this.state.pageNo;
    this.searcheventRequest = {};
    this.searcheventRequest.event_status = 1;
    this.searcheventRequest.sessionid = this.state.sessionId
    params.UserSubCategory_s = "";
    params.UserCategory_s = "";
    params.numofresults = this.state.totalCount;
    params.search_responsetime = this.state.timeCaptureTotal * 1000;
    params.dataset_s = this.state.solrCollection;
    var docPosition;
    if( this.state.pageNo == 1 ) {
      docPosition = item.displayCount;
    }else {
      docPosition = (((this.state.pageSize*this.state.pageNo) - this.state.pageSize ) + item.displayCount);
    }
    var clickEventData = {
        //"documentStartTime": new Date().toISOString(),
        //"documentEndTime": new Date().toISOString(),
        "query": this.state.searchQuery ? this.state.searchQuery : item.title,
        "documentPosition": docPosition,
        "docId": item.sp_doc_id ? item.sp_doc_id : item.file_id,
        "userIP": this.state.userIP,
        "eventStatus": this.searcheventRequest.event_status,
        "eventTimestamp": new Date().toISOString(),
        "appId": this.state.eventAppId,
        "user" : this.props.userEmail,
        "application": this.state.eventAppId,
        "sessionId": this.searcheventRequest.sessionid,
        "eventType": "Document Click",
        "params": params,
        "multiParams": multiParams
    };
    axios.post("/search/saveSearchEventData", clickEventData);
  }

  sendSearchPaginationEvent = () => {
    var params={};
    let sources = [];
    if(this.state.boxAuthorized == true) {
      sources.push("Box");
    }
    if(this.state.spAuthorized == true) {
      sources.push("Sharepoint");
    }

    var multiParams = {
      source_type_ss: sources
    };
    params.pageno_i = this.state.pageNo;
    params.TotalPage_i  = Math.ceil(this.state.totalCount / this.state.pageSize);
    params.UserSubCategory_s = "";
    params.UserCategory_s = "";
    this.searcheventRequest = {};
    this.searcheventRequest.event_status = 1;
    this.searcheventRequest.sessionid = this.state.sessionId;
    params.numofresults = this.state.totalCount;
    params.search_responsetime = this.state.timeCaptureTotal * 1000;
    var paginationEventData={
      "eventTimestamp" : new Date().toISOString(),
      "eventStatus" : this.searcheventRequest.event_status,
      "appId" : this.state.eventAppId,
      "user" : this.userEmail,
      "userIP": this.state.userIP,
      "application" : this.state.eventAppId,
      "sessionId" : this.searcheventRequest.sessionid,
      "eventType" : 'Page Navigation',
      "query": this.state.searchQuery,
      "params": params,
      "multiParams": multiParams
      }
      axios.post("/search/saveSearchEventData", paginationEventData);
  }

  processResponseFromSolr = (response) => {
    let dataStore = {
      displayCount: 0,
      itemsPerPage: 50,
      pageNo: 1,
    };
    this.setState(
      {
        pageOffset: response.data.searchResponse.response.start,
        rawResponse: JSON.parse(JSON.stringify(response.data))
        //isLoading: false,
      },
      () => {
        let pageOffset = this.state.pageOffset - 1;
        if (response.data.searchResponse && response.data.searchResponse.response && response.data.searchResponse.response.docs) {
          let responses = {};
          let res = getFormattedResultforSolr(response.data, dataStore);
          this.setState({
            resultDataset: res,
            selectedDoc: response.data.searchResponse.response.docs && response.data.searchResponse.response.docs.length>0 ? response.data.searchResponse.response.docs[0]: {},
          }, ()=> this.formatsolrsearchresponse())

          this.setState({
            rowData: response.data.searchResponse.response.docs,
            facets: response.data.searchResponse.facet_counts 
              ? response.data.searchResponse.facet_counts
              : [],
            intervalFacets: response.data.searchResponse.facet_counts && response.data.searchResponse.facet_counts.facet_intervals
            ? response.data.searchResponse.facet_counts.facet_intervals
            : [],
            showFilters: true,
            totalCount: response.data.searchResponse.response.numFound,
          });
        } else {
          this.setState({
            resultDataset: []
          });
          this.formatsolrsearchresponse();
        }
      }
    );

  }
  
  openToEditDocument = (url) => {
    var newWin = window.open(url , 'Edit', 'width=1250,height=580,left=235,top=150');
  }

  toggleContent = (item, index) => {
    let res = this.state.resultDataset;
    res[index].showMore = !res[index].showMore;
    this.setState({resultDataset: res}, ()=>{this.formatsolrsearchresponse()})
  }

  formatsolrsearchresponse = () => {
  
    let pageOffset = this.state.pageOffset - 1;
    let res = this.state.resultDataset;
    let responses = [];
    if(res) {
      this.focusDiv();

      responses = res.map((item, index) => {
        let fileExtension = "";
        let fileName = "";
        if(item.file_name && item.file_name.length>0) {
          fileExtension = utils.getFileExt(item.file_name[0]);
          fileName = unescape(utils.getFileName(item.file_name[0]));
        }
        if(fileName == "" && item.name) {
          fileExtension = utils.getFileExt(item.name);
          fileName = unescape(utils.getFileName(item.name));
        }

        item.showUrl = unescape(item.url);
        let breadkCrum = this.getBreadCrum(item.doc_path, item.source_type);
        let itemClass = "fa fa-bookmark";
        if (fileExtension == "pdf") {
          itemClass = "fa fa-file-pdf-o";
        } else if (fileExtension == "docx" || fileExtension == "doc") {
          itemClass = "fa fa-file-word-o";
        } else if (fileExtension == "msg") {
          itemClass = "fa fa-envelope";
        } else if (fileExtension == "xls" || fileExtension == "csv" || fileExtension == "xlsx") {
          itemClass = "fa fa-file-excel-o";
        } else if (fileExtension == "ppt" || fileExtension == "pptx") {
          itemClass = "fa fa-file-powerpoint-o";
        }
        item.fileExtension = fileExtension;
        // let date = new Date(item.creation_date).toDateString();
        let today = new Date(item.modified);
        let sdate = new Date(item.signed_date);
        let edate = new Date(item.expiry_date);
        var options = { year: 'numeric', month: 'long', day: 'numeric' };
        let date = today.toLocaleDateString("en-US", options);
        let signed_date = sdate.toLocaleDateString("en-US", options);
        let expiry_date = edate.toLocaleDateString("en-US", options);
        pageOffset += 1;

        return (
          <div className={"result-child1 "+ (this.state.selectedDoc.id==item.id ? "selected-document":"")} 
            id={item.id} key={index + 1}>
            <div className="resultarea">
              <span className="" aria-hidden="true" style={{marginRight: "1px"}} >
                <button className="btn btn-link add-to-collection-btn" title="Add to Collections" onClick={(event) => this.addToCollection(event, item)} style={{"marginTop": "-1px"}}><i className="fa fa-bars"></i></button>
              </span> &nbsp;
              <span className="" aria-hidden="true" style={{marginRight: "5px"}}
               onClick={(e) =>
               this.openToEditDocument(item.showUrl) }  style={{"cursor": "pointer",}} >
                  {pageOffset + 1 + ". "}
              </span>
  
              {item.title &&
              <span
                 href="/"
                title={"Click here to see more information on the right"}
                style={{
                  "cursor": "pointer",
                }}
                onClick={(e) =>{
                    this.sendDocClickEventData(e,item);
                    this.changeSelectedDocument(item);}
                }
                dangerouslySetInnerHTML={{
                  __html: `${item.title}`,
                }}
              ></span>
              }
              {/* {!item.title && fileName &&
              <a
                href="/"
                title={item.url}
                // onClick={(e) =>
                //   this.openRightPane(e, item.url, "docPreview", item)
                // }
                dangerouslySetInnerHTML={{
                  __html: `${fileName}`,
                }}
              ></a>
              } */}
            </div>
            <div className="resultarearightblock">
            {item.source_type &&
              <p
                title="Source"
                className={item.authorsStr} style={{display: "inline-block",marginBottom:"0",marginLeft:"5px"}}
                // style={{ display:"inline-block", fontSize:"12px", textTransform:"capitalize" }}
                // dangerouslySetInnerHTML={{
                //   __html: `${item.source_type}`,
                // }}
                >
                { item.source_type.toLowerCase()=='box' && <span className="box-logo"></span>}
                { item.source_type.toLowerCase()=='sharepoint' &&  <span className="sharepoint-logo"></span>}
                </p>
              
              }
              {item.authorsStr &&
              <p
                title="Author"
                className={(item.modified && (!item.signed_date && !item.expiry_date)) && "pipe"}
                style={{ marginLeft: "3px", display:"inline-block", fontSize:"12px", textTransform:"capitalize", marginBottom:"0",verticalAlign:"super"}}
                dangerouslySetInnerHTML={{
                  __html: `${item.authorsStr}`,
                }}
              /> 
              }
  
              {(item.modified && (!item.signed_date && !item.expiry_date)) && 
              <p style={{ marginLeft: "5px", display:"inline-block", fontSize:"12px", textTransform:"capitalize", marginBottom:"0",verticalAlign:"super"}}
                title="Updated date">
                {date}
              </p>
              }
            </div>
  
            <div className="result-child2">
              {/*  href={item.showUrl} target="_blank" <a href={item.url}>{item.url}</a> */}

              <a
                title={item.showUrl}
                className={itemClass}
                onClick={(e) =>{
                  this.sendDocClickEventData(e, item);this.openToEditDocument(item.showUrl);}
                }
                style={{ color: "#1d4289", margin: "0px 6px 0px 0px", "cursor":"pointer!important","fontWeight": "bold", float: "left" }}
                aria-hidden="true"
              ></a>
              <span style={{"cursor": "pointer",}} onClick={(e) =>this.openToEditDocument(item.showUrl)
                } >{breadkCrum} </span> 
              
              {/* <a
                href="/"
                className="doc-tree-icon"
                onClick={(e) =>
                  this.openRightPane(e, item.url, "docPreview", item)
                }
              >
                <i className="fa fa-sitemap" aria-hidden="true"></i>
              </a> */}
              {item.embd_doc_num !== undefined && this.isAttach(item) &&(
                <a
                  href="/"
                  className={
                    this.isEmbd(Object.keys(item.highlight))
                      ? "doc-attachment-true"
                      : "doc-attachment"
                  }
                  onClick={(e) =>
                    this.openRightPane(e, item.url, "docAttachment", item)
                  }
                >
                  <i className="fa fa-paperclip" aria-hidden="true"></i>
                </a>
              )}
                <div
              style={{
                float: "right",
                width:"20%",
                textAlign:"right",
                overflow:"hidden",
                whiteSpace:"noWrap",
                textOveflow:"ellipsis"
              }}
            >
              <p
                title="Score"
                 style={{display: "inline-block",marginBottom:"0",marginLeft:"5px"}}>
                  Score: {item.score}
                  {/* <button className="btn btn-link" title="Add to Collections" onClick={(event) => this.addToCollection(event, item)} style={{"marginTop": "-1px"}}><i className="fa fa-bars"></i></button> */}
                </p>
                
            </div>
            </div>
  
          
            {(item.modified && (item.signed_date || item.expiry_date)) && 
                <ul
                style={{
                  display: "flex",
                  justifyContent: "flex-start",
                  // marginBottom: "2rem",
                  fontSize: "1.25rem",
                }}
                className="list-parent"
              >
              <li><i className="fa fa-calendar" style={{fontSize:"14px",  marginRight: "10px"}} aria-hidden="true"></i> </li>
                {item.modified && (
                  <li className="list-item">
                    <b>Updated: </b>
                    <span>
                      {date}
                      </span>
                  </li>
                )}
                {item.signed_date && (
                  <li className="list-item">
                    <b>Signed: </b>
                    <span>
                      {signed_date}
                      </span>
                  </li>
                )}
                {item.expiry_date && (
                  <li className="list-item">
                    <b>Expiry: </b>
                    <span>
                      {expiry_date}
                      </span>
                  </li>
                )}
                </ul>
              }

            {item.shortDesc && !item.showMore && 
            <div style={{"marginTop": "20px"}}>
            <div dangerouslySetInnerHTML={{ __html: `${item.shortDesc}` }} />
              {item.showExpandIcon && !item.showMore && 
              <i className="fa fa-arrow-circle-down" aria-hidden="true" title="Click to see detailed content"
                onClick={(e) => this.toggleContent(item, index)}
                style={{"float": "right", "cursor": "pointer", "marginTop": "-15px"}}></i>
              }
            </div>
            }
            {item.desc && item.showMore && 
            <div style={{"marginTop": "20px"}}>
            <div dangerouslySetInnerHTML={{ __html: `${item.desc}` }} />
            {item.showExpandIcon && item.showMore && 
              <i className="fa fa-arrow-circle-up" aria-hidden="true" title="Click to see shorter content"
                onClick={(e) => this.toggleContent(item, index)}
                style={{"float": "right", "cursor": "pointer", "marginTop": "-15px"}}></i>
              }
            </div>
            }
  
            <ul
              style={{
                display: "flex",
                justifyContent: "flex-start",
                marginTop: "2rem",
                marginBottom: "2rem",
                fontSize: "1.25rem",
              }}
              className="list-parent"
            >
              {item.displayKeywords.length > 0 && (
                <li className="list-item">
                  <b>Tags:</b>
                  <span
                    dangerouslySetInnerHTML={{
                      __html: ` ${item.displayKeywords}`,
                    }}
                  />
                </li>
              )}
              {item.displayTeam.length > 0 && (
                <li className="list-item">
                  <b>Teams: </b>
                  <span
                    dangerouslySetInnerHTML={{
                      __html: `${item.displayTeam}`,
                    }}
                  />
                </li>
              )}
              {item.displayPlayers.length > 0 && (
                <li className="list-item">
                  <b>Players: </b>
                  <span
                    dangerouslySetInnerHTML={{
                      __html: `${item.displayPlayers}`,
                    }}
                  />
                </li>
              )}
              {item.displayGames.length > 0 && (
                <li className="list-item">
                  <b>Games:</b>{" "}
                  <span
                    dangerouslySetInnerHTML={{
                      __html: `${item.displayGames}`,
                    }}
                  />
                </li>
              )}
              {item.displayOfficials.length > 0 && (
                <li className="list-item">
                  <b>Officials:</b>{" "}
                  <span
                    dangerouslySetInnerHTML={{
                      __html: `${item.displayOfficials}`,
                    }}
                  />
                </li>
              )}
              {item.document_state && (
                <li className="list-item">
                  <b>Document Category:</b>{" "}
                  <span
                    dangerouslySetInnerHTML={{
                      __html: `${item.document_state}`,
                    }}
                  />
                </li>
              )}
              {item.amount && (
                <li className="list-item">
                  <b>Amount: </b>$
                  <span
                    dangerouslySetInnerHTML={{
                      __html: `${item.amount}`,
                    }}
                  />
                </li>
              )}
              {item.displayAnatomies && item.displayAnatomies.length > 0 && (
                <li className="list-item">
                  <b>Anatomies: </b>
                  <span
                    dangerouslySetInnerHTML={{
                      __html: `${item.displayAnatomies}`,
                    }}
                  />
                </li>
              )}
              {item.displayHealthConditions && item.displayHealthConditions.length > 0 && (
                <li className="list-item">
                  <b>Health Conditions: </b>
                  <span
                    dangerouslySetInnerHTML={{
                      __html: `${item.displayHealthConditions}`,
                    }}
                  />
                </li>
              )}
              {item.displayDrugs && item.displayDrugs.length > 0 && (
                <li className="list-item">
                  <b>Drugs:</b>{" "}
                  <span
                    dangerouslySetInnerHTML={{
                      __html: `${item.displayDrugs}`,
                    }}formatsolrsearchresponse
                  />
                </li>
              )}
              {item.displayOrganizations && item.displayOrganizations.length > 0 && (
                <li className="list-item">
                  <b>Organizations:</b>{" "}
                  <span
                    dangerouslySetInnerHTML={{
                      __html: `${item.displayOrganizations}`,
                    }}
                  />
                </li>
              )}
  
              {item.emailToStr && (
                <li className="list-item">
                  <b>Email To:</b>{" "}
                  <span
                    dangerouslySetInnerHTML={{
                      __html: `${item.emailToStr}`,
                    }}
                  />
                </li>
              )}
              {item.emailFromStr && (
                <li className="list-item">
                  <b>Email From:</b>{" "}
                  <span
                    dangerouslySetInnerHTML={{
                      __html: `${item.emailFromStr}`,
                    }}
                  />
                </li>
              )}
            </ul>
          </div>

          
        );
      });
    }

    if (responses.length == 0) {
      let fallback = (
        <div
          style={{
            color: "rgb(116 115 115)",
            fontSize: "1.75rem",
            fontWeight: "600",
            "textAlign": "center",
            "marginTop": "100px"
          }}
        >
          No results found for your search..
        </div>
      );
      responses.push(fallback);
      this.setState({errorInResponse: true})
    } else {
      this.setState({errorInResponse: false})
    }
    this.setState({
      exampleItems: responses,
      showFileInfo: true,
      isLoading: false
    });
  }

  addToCollection = (event, itemToAddToCollection) => {
    this.setState({showCollection: true, itemToAddToCollection: itemToAddToCollection})
  }

  constructFilterString = (selectedFacets) => {
    var finalFilterArr = [];
    for(var i =0;i<selectedFacets.length;i++){
          var filteredObj = this.getFilteredQueryObj(selectedFacets,selectedFacets[i],finalFilterArr);

          if(filteredObj && filteredObj.field != "" && filteredObj.value != ""){
              finalFilterArr.push(filteredObj);
          }
     }
     return finalFilterArr;
  }


  getFilteredQueryObj = (selectedFilterArr,selectedFilter,finalFilters) => {
    var filterObj = {};
    filterObj.field = "";
    filterObj.value = "";

    if(finalFilters){
        var obj = finalFilters.filter(function(filter){
            return filter.field == selectedFilter.facetName;
        });

        if(obj && obj.length == 0){
            var objFilter = selectedFilterArr.filter(function(obj){
                return selectedFilter.facetName === obj.facetName;
            });

            if(objFilter && objFilter.length > 0){
                filterObj.field = objFilter[0].facetName;
                if(filterObj.field === "modified"){
                    filterObj.value = objFilter[0].filterStr;
                    for(var i = 1; i < objFilter.length; i++){
                        // filterObj.value += ' OR '+selectedFilter.facetName+':'+objFilter[i].filterStr;  
                        filterObj.value += ' OR ' + '"' + objFilter[i].filterStr + '"';
                    }
                } else {
                    filterObj.value = '"'+ objFilter[0].filterStr +'"';
                    for(var i = 1; i < objFilter.length; i++){
                        // filterObj.value += ' OR '+selectedFilter.facetName+':/"'+objFilter[i].filterStr+'/"';
                        filterObj.value += ' OR ' + '"' + objFilter[i].filterStr + '"';
                    }
                    let preValue = '('+filterObj.value+')';
                    filterObj.value = preValue;
                }
            }
        }
    }

     return filterObj;
  }
 
 doLegacySearch = (searchQuery, filterObj) => {
    var searchRequest = {};
    var pname = this.state.dgiUserName;
    searchRequest = {
      userPermissionInfo: {
        userName: pname,
      },
      searchRequest: {
        basicQuery: searchQuery,
        datasets: this.state.legacyDataset,
        searchType: "basic",
        advancedQuery: {
          queryFields: ["title", "body"],
        },
        "dateCriteria": {
          "field": "",
          "startDate": "",
          "endDate": "",
          "excludeStartDate": false,
          "excludeEndDate": false
        },
    
        facet: {
          count: 0,
          facetFields: [],
        },
        pageInfo: {
          pageOffset: this.state.pageOffset,
          pageSize: this.state.pageSize,
        },
        filterQueries: filterObj,
        filteredQueries: filterObj,
        sortInfo: { score: "desc" },
        query: searchQuery,
        "additionalParameters": {
          "intent": ["true"],
          "facet.interval": ["{!key=modified}modified"],
          "f.modified.facet.interval.set": this.state.intervalFacetQuery
          }
          
      },
      intent: true,
      userInfo: {
        // userName: "sagarg@thedigitalgroup",
        userName: pname,
      },
    };
    if(this.state.searchWithin == true && this.state.searchWithinQuery!=='') {
      searchRequest.searchRequest.searchWithinQuery = this.state.searchWithinQuery;
    }

    searchRequest.client = this.client;
    axios.post("/search/getSearchResults", searchRequest).then((response) => {
    try{
      // this.timeEnd = performance.now();
      // this.state.timeCaptureTotal = ((this.timeEnd - this.timeStart) / 1000).toFixed(3)
      this.processSearchResponse(response);
      // this.sendSearchEventData();
    } 
    catch(err) {
      this.setState(
        {
          isLoading: false,
        });
    }
  
  });
  }


  processSearchResponse = (response) => {
    let dataStore = {
      displayCount: 0,
      itemsPerPage: 50,
      pageNo: 1,
    };
    this.setState(
      {
        pageOffset: response.data.searchResponse.response.start,
        isLoading: false,
      },
      () => {
        let pageOffset = this.state.pageOffset - 1;
        if (response.data.searchResponse && response.data.searchResponse.response && response.data.searchResponse.response.docs) {
          let responses = {};
          let res = getHighLightedResult_NBA(response.data, dataStore);
          this.setState({
            resultDataset: res,
            selectedDoc: response.data.searchResponse.response.docs && response.data.searchResponse.response.docs.length>0 ? response.data.searchResponse.response.docs[0]: {},
          }, ()=> this.formatSearchResponse())

          this.setState({
            rowData: response.data.searchResponse.response.docs,
            facets: response.data.searchResponse.facetResponse && response.data.searchResponse.facetResponse.facets
              ? response.data.searchResponse.facetResponse.facets
              : [],
            intervalFacets: response.data.searchResponse.facetResponse && response.data.searchResponse.facetResponse.intervalFacets
            ? response.data.searchResponse.facetResponse.intervalFacets
            : [],
            showFilters: true,
            totalCount: response.data.searchResponse.response.numFound,
          });
        } else {
          this.setState({
            resultDataset: []
          });
          this.formatSearchResponse();
        }
      }
    );
  }

  formatSearchResponse = () => {
    let pageOffset = this.state.pageOffset - 1;
    let res = this.state.resultDataset;
    let responses = res.map((item, index) => {
      let fileExtension = this.getFileExtension(item.url);
      let fileName = unescape(this.getFileName(item.url));
      item.showUrl = unescape(item.url);
      let breadkCrum = this.getBreadCrum(item.url);
      let itemClass = "fa fa-bookmark";
      if (fileExtension == "pdf") {
        itemClass = "fa fa-file-pdf-o";
      } else if (fileExtension == "docx" || fileExtension == "doc") {
        itemClass = "fa fa-file-word-o";
      } else if (fileExtension == "msg") {
        itemClass = "fa fa-envelope";
      } else if (fileExtension == "xls" || fileExtension == "csv" || fileExtension == "xlsx") {
        itemClass = "fa fa-file-excel-o";
      } else if (fileExtension == "ppt" || fileExtension == "pptx") {
        itemClass = "fa fa-file-powerpoint-o";
      }
      item.fileExtension = fileExtension;
      // let date = new Date(item.creation_date).toDateString();
      let today = new Date(item.creation_date);
      let sdate = new Date(item.signed_date);
      let edate = new Date(item.expiry_date);
      var options = { year: 'numeric', month: 'long', day: 'numeric' };
      let date = today.toLocaleDateString("en-US", options);
      let signed_date = sdate.toLocaleDateString("en-US", options);
      let expiry_date = edate.toLocaleDateString("en-US", options);
      pageOffset += 1;
      return (
        <div className={"result-child1 "+ (this.state.selectedDoc.sp_doc_id==item.sp_doc_id ? "selected-document":"")} id={item.id} key={index + 1}>
          <div
            style={{
              float: "right",
            }}
          >
            {item.authorsStr &&
            <p
              title="Author"
              className={(item.creation_date && (!item.signed_date && !item.expiry_date)) && "pipe"}
              style={{ display:"inline-block", fontSize:"12px", textTransform:"capitalize" }}
              dangerouslySetInnerHTML={{
                __html: `${item.authorsStr}`,
              }}
            /> 
            }

            {(item.creation_date && (!item.signed_date && !item.expiry_date)) && 
            <p style={{ marginLeft: "5px" , display:"inline-block", fontSize:"12px" }}
              title="Created date">
              {date}
            </p>
            }
          </div>
          <div
            style={{
              fontSize: "14px",
              color: "#1d4289",
              margin: "0",
              fontWeight: "700",
            }}
            className="resultarea"
          >
            <span className="" aria-hidden="true" style={{marginRight: "5px"}}>
              {pageOffset + 1 + "."}
            </span>

            {item.title &&
            <span
              href="/"
              title={item.showUrl}
              style={{
                "cursor": "pointer",
              }}
              onClick={(e) =>
                this.changeSelectedDocument(item)
              }
              dangerouslySetInnerHTML={{
                __html: `${item.title}`,
              }}
            ></span>
            }
            {!item.title && fileName &&
            <a
              href="/"
              title={item.url}
              // onClick={(e) =>
              //   this.openRightPane(e, item.url, "docPreview", item)
              // }
              dangerouslySetInnerHTML={{
                __html: `${fileName}`,
              }}
            ></a>
            }
            

          </div>

          <div className="result-child2">
            {/* <a href={item.url}>{item.url}</a> */}
            <a
              className={itemClass}
              href={item.showUrl} target="_blank"
              style={{ color: "#1d4289", margin: "0px 6px 0px 0px", "cursor":"pointer!important","fontWeight": "bold", float: "left" }}
              aria-hidden="true"
            ></a> {breadkCrum} <a
              href="/"
              className="doc-tree-icon"
              onClick={(e) =>
                this.openRightPane(e, item.url, "docPreview", item)
              }
            >
              <i className="fa fa-sitemap" aria-hidden="true"></i>
            </a>
            {item.embd_doc_num !== undefined && this.isAttach(item) &&(
              <a
                href="/"
                className={
                  this.isEmbd(Object.keys(item.highlight))
                    ? "doc-attachment-true"
                    : "doc-attachment"
                }
                onClick={(e) =>
                  this.openRightPane(e, item.url, "docAttachment", item)
                }
              >
                <i className="fa fa-paperclip" aria-hidden="true"></i>
              </a>
            )}
          </div>

          {(item.creation_date && (item.signed_date || item.expiry_date)) && 
              <ul
              style={{
                display: "flex",
                justifyContent: "flex-start",
                // marginBottom: "2rem",
                fontSize: "1.25rem",
              }}
              className="list-parent"
            >
            <li><i className="fa fa-calendar" style={{fontSize:"14px",  marginRight: "10px"}} aria-hidden="true"></i> </li>
              {item.creation_date && (
                <li className="list-item">
                  <b>Created: </b>
                  <span>
                    {date}
                    </span>
                </li>
              )}
              {item.signed_date && (
                <li className="list-item">
                  <b>Signed: </b>
                  <span>
                    {signed_date}
                    </span>
                </li>
              )}
              {item.expiry_date && (
                <li className="list-item">
                  <b>Expiry: </b>
                  <span>
                    {expiry_date}
                    </span>
                </li>
              )}
              </ul>
            }
          {item.desc &&
          <div dangerouslySetInnerHTML={{ __html: `${item.desc}` }} />
          }

          <ul
            style={{
              display: "flex",
              justifyContent: "flex-start",
              marginTop: "2rem",
              marginBottom: "2rem",
              fontSize: "1.25rem",
            }}
            className="list-parent"
          >
            {item.displayKeywords.length > 0 && (
              <li className="list-item">
                <b>Tags:</b>
                <span
                  dangerouslySetInnerHTML={{
                    __html: ` ${item.displayKeywords}`,
                  }}
                />
              </li>
            )}
            {item.displayTeam.length > 0 && (
              <li className="list-item">
                <b>Teams: </b>
                <span
                  dangerouslySetInnerHTML={{
                    __html: `${item.displayTeam}`,
                  }}
                />
              </li>
            )}
            {item.displayPlayers.length > 0 && (
              <li className="list-item">
                <b>Players: </b>
                <span
                  dangerouslySetInnerHTML={{
                    __html: `${item.displayPlayers}`,
                  }}
                />
              </li>
            )}
            {item.displayGames.length > 0 && (
              <li className="list-item">
                <b>Games:</b>{" "}
                <span
                  dangerouslySetInnerHTML={{
                    __html: `${item.displayGames}`,
                  }}
                />
              </li>
            )}
            {item.displayOfficials.length > 0 && (
              <li className="list-item">
                <b>Officials:</b>{" "}
                <span
                  dangerouslySetInnerHTML={{
                    __html: `${item.displayOfficials}`,
                  }}
                />
              </li>
            )}
            {item.document_state && (
              <li className="list-item">
                <b>Document Category:</b>{" "}
                <span
                  dangerouslySetInnerHTML={{
                    __html: `${item.document_state}`,
                  }}
                />
              </li>
            )}
            {item.amount && (
              <li className="list-item">
                <b>Amount: </b>$
                <span
                  dangerouslySetInnerHTML={{
                    __html: `${item.amount}`,
                  }}
                />
              </li>
            )}
            {item.displayAnatomies && item.displayAnatomies.length > 0 && (
              <li className="list-item">
                <b>Anatomies: </b>
                <span
                  dangerouslySetInnerHTML={{
                    __html: `${item.displayAnatomies}`,
                  }}
                />
              </li>
            )}
            {item.displayHealthConditions && item.displayHealthConditions.length > 0 && (
              <li className="list-item">
                <b>Health Conditions: </b>
                <span
                  dangerouslySetInnerHTML={{
                    __html: `${item.displayHealthConditions}`,
                  }}
                />
              </li>
            )}
            {item.displayDrugs && item.displayDrugs.length > 0 && (
              <li className="list-item">
                <b>Drugs:</b>{" "}
                <span
                  dangerouslySetInnerHTML={{
                    __html: `${item.displayDrugs}`,
                  }}
                />
              </li>
            )}
            {item.displayOrganizations && item.displayOrganizations.length > 0 && (
              <li className="list-item">
                <b>Organizations:</b>{" "}
                <span
                  dangerouslySetInnerHTML={{
                    __html: `${item.displayOrganizations}`,
                  }}
                />
              </li>
            )}
            {item.emailToStr && (
              <li className="list-item">
                <b>Email To:</b>{" "}
                <span
                  dangerouslySetInnerHTML={{
                    __html: `${item.emailToStr}`,
                  }}
                />
              </li>
            )}
            {item.emailFromStr && (
              <li className="list-item">
                <b>Email From:</b>{" "}
                <span
                  dangerouslySetInnerHTML={{
                    __html: `${item.emailFromStr}`,
                  }}
                />
              </li>
            )}
          </ul>
        </div>
      );
    });
    if (responses.length == 0) {
      let fallback = (
        <div
          style={{
            color: "rgb(116 115 115)",
            fontSize: "1.75rem",
            fontWeight: "600",
            "textAlign": "center",
            "marginTop": "100px"
          }}
        >
          No results found for your search..
        </div>
      );
      responses.push(fallback);
      this.setState({errorInResponse: true})
    } else {
      this.setState({errorInResponse: false})
    }
    this.setState({
      exampleItems: responses,
      showFileInfo: true
    });
  }

  onFilterSelection = (selFilters, newFacetSelected) =>
    new Promise((resolve, reject) => {
      this.setState({"pageOffset": 0, "page": 1, "pageNo": 1})
      if(selFilters.length>0)
        this.isFiltersSelected = true;
      this.newFacetSelecteObj = newFacetSelected;
      let filterObj = {};
      const result = [];
      // const map = new Map();
      for (const item of selFilters) {
        // if (!map.has(item.name)) {
          // map.set({"name":item.name, "parent": item.parent.name}, true); // set any value to Map
          result.push({
            count: item.count,
            displayName: item.displayName,
            isSelected: item.isSelected,
            nodeLength: item.nodeLength,
            nodes: item.nodes,
            name: item.name,
            filterStr: item.name,
            fieldName: item.name,
            parent: item.parent,
            facetName: item.parent.name
          });
        // }
      }
      selFilters = result;
      selFilters.forEach((item) => {
        if(item.parent.name !=="modified") {
          let filterArr = [];
          if (filterObj[item.parent.name] !== undefined) {
            filterArr = filterObj[item.parent.name];
            if(item.parent.name.indexOf('_facet') > -1) {
              filterArr.push('"/'+item.name+'/"');
            } else {
              filterArr.push(item.name);
            }
            
            filterArr = [...new Set(filterArr)];
            filterObj[item.parent.name] = filterArr;
          } else {
            if(item.parent.name.indexOf('_facet') > -1) {
              filterArr.push('/"'+item.name+'"/');
            } else {
              filterArr.push(item.name);
            }
            filterObj[item.parent.name] = [...new Set(filterArr)];
          }
        } else {
          let intervalArr= [];
          switch(item.name) {
            case "[1900-01-01T00:00:00Z TO NOW]":
              break;
            case "[NOW-30DAY TO NOW]":
              break;
            case "[NOW-7DAY TO NOW]":
               break;
            case "[NOW-1DAY TO NOW]":
              break;
          }
          intervalArr.push(item.name)
          filterObj['modified'] = intervalArr;
        }
      });
      this.setState(
        { selectedFacets: selFilters, filterObj: filterObj, },
        () => {
          this.performSearch();
          resolve(this.state.selectedFacets);
        }
      );
    });

    saveFilterApplyEvent = () => {
      if(this.state.selectedFacets.length!==0){
        this.state.selectedFacets.forEach((item)=>{
          var params={};
          var multiParams = {
            source_type_ss: this.state.sources
          };
          var selectedFilters = [];
          selectedFilters.push(item.fieldName);
          params.UserSubCategory_s = "";
          params.UserCategory_s = "";
          this.searcheventRequest = {};
          this.searcheventRequest.event_status = 1;
          this.searcheventRequest.sessionid = this.state.sessionId;
          params.numofresults = this.state.totalCount;
          params.search_responsetime = this.state.timeCaptureTotal * 1000;
          params.dataset_s = this.state.solrCollection;
          var facetData= {
            "query": this.state.searchQuery,
            "eventStatus": this.searcheventRequest.event_status,
            "eventTimestamp": new Date().toISOString(),
            "appId": this.state.eventAppId,
            "user" : this.props.userEmail,
            "application": this.state.eventAppId,
            "sessionId": this.searcheventRequest.sessionid,
            "eventType":"Filter Action",
            "userIP": this.state.userIP,
            "facetFieldName":item.parent.displayName,
            "selectedFacet": selectedFilters,
            "searchQuery": this.state.searchQuery,
            "params":params,
            "multiParams": multiParams
          }
          axios.post("/search/saveSearchEventData", facetData);
        });
       }
    }



  isEmbd = (itemArray) => {
    let myBool = false;
    for (let i = 0; i < itemArray.length; i++) {
      if (itemArray[i].match(/_title/g)) {
        myBool = true;
        break;
      } else if (itemArray[i].match(/_content/g)) {
        myBool = true;
        break;
      } else if (itemArray[i].match(/_creator/g)) {
        myBool = true;
        break;
      } else {
        myBool = false;
      }
    }
    if (myBool == true) {
      return true;
    } else {
      return false;
    }
  };


  isAttach = (item) => {
    let isValidAttach = false;
    let embd_length = parseInt(item.embd_doc_num);
    for(let i=0; i<embd_length; i++) {
      var property = `embd_${i+1}_content_type`;
      if(item[property] &&  item[property][0].indexOf('application')>-1) {
        isValidAttach = true;
        break;
      }
    }
    return isValidAttach;

  }

  onLogin = () => {
    this.setState({ showLogin: false})
  };


  showNotes = () => {
    this.setState({ showAppNotes: true})
  };

  displayExpansions = (phrase) => {
    this.setState({clickedtoken:phrase}, ()=> {this.setState({showExpansions:true})})

  }

  removeBoxAuth = () => {
    this.saveRefreshTokenToSolr("");
    this.setState({
      boxAuthorized: false,
      boxAuthUserL: "",
      boxUserGroupIds: "",
      boxUserId: "",
      accessToken: ""
    });
    this.performSearch();

  }

  removeSharePointAuth = () => {
    // removeSharePointAuth
    // SPUserName: SPUserName, SPpassword:SPpassword, spAuthorized: true
    this.setState({
      SPUserName: "",
      SPpassword: "",
      spAuthorized: false
    });
    this.performSearch();
  }

  collectionNodeSelected = (collectionNode) => {
    collectionNode.isFromCollection = true;
    collectionNode.keyIntree = collectionNode.key;
    collectionNode.title = collectionNode.file_name[0];
    this.setState({
      sharepoint_id : collectionNode.sp_doc_id,
      selectedDocumentUrl: collectionNode.doc_path,
      activeRightPanelTab: "docPreview",
      isPaneOpen: true,
      item: collectionNode,
      selectedDoc: collectionNode
    });
  }

  
  focusDiv = () => {
    setTimeout(()=>{
      if(this.refs.theDiv)
        ReactDOM.findDOMNode(this.refs.theDiv).focus();
    }, 1500)
  }

  scrollToTop = () => {
    $( "#result_card").animate({ scrollTop: 0 }, "slow");
  }


  handleKeyDown = (e) => {
    let selectedDoc = this.state.selectedDoc;
    let resultList = this.state.resultDataset ? this.state.resultDataset : [];
    if (e.keyCode === 38 && !this.state.isSuggestionBoxVisible) {
      if(selectedDoc && resultList.length>1) {
        if(selectedDoc.count) {
          let count = selectedDoc.count - 2;
          let item = resultList[count] ? resultList[count]:null;
          if(item) {
            this.changeSelectedDocument(item);
            let scrollPixel = count*200;
            $( "#result_card").animate({ scrollTop: scrollPixel }, "slow");
          }
        }
      }
    } else if (e.keyCode === 40 && !this.state.isSuggestionBoxVisible) {
      if(selectedDoc && resultList.length>1) {
        if(selectedDoc.count) {
          let count = selectedDoc.count;
          let item = resultList[count] ? resultList[count]:null;
          if(item) {
            this.changeSelectedDocument(item)
            let scrollPixel = count*200;
            $( "#result_card").animate({ scrollTop: scrollPixel }, "slow");
          }
        }
      }
    }
  }

  sendCollectionAddEvent = (selectedDocument,collection) => {
    var params = {};
    params.SourceType_s = selectedDocument.source_type;
    params.docurl_s = selectedDocument.url;
    params.UserSubCategory_s = "";
    params.UserCategory_s = "";
    params.dataset_s = selectedDocument.dataset_key;
    params.collection_id_s = collection.id;
    params.collection_title_s = collection.title;

    this.searcheventRequest = {};
    this.searcheventRequest.event_status = 1;
    this.searcheventRequest.sessionid = this.state.sessionId;

    var multiParams = {};
    multiParams.file_type_ss = selectedDocument.file_type;

    var clickEventData = {
        "eventType": "Document Add",
        "eventStatus": this.searcheventRequest.event_status,
        "eventTimestamp": new Date().toISOString(),
        "appId": this.state.eventAppId,
        "user" : this.userEmail,
        "application": this.state.eventAppId,
        "sessionId": this.searcheventRequest.sessionid,
        "userIP": this.state.userIP,
        "query": this.state.searchQuery,
        "docId": selectedDocument.source_type==="sharepoint"? selectedDocument.sp_doc_id : selectedDocument.file_id,
        "params": params,
        "multiParams": multiParams
    };
    axios.post("/search/saveSearchEventData", clickEventData);
  }

  setSuggestionBoxVisibility = (visibility) =>{
    this.setState({
      isSuggestionBoxVisible : visibility
    });
  }

  render() {
    let {
      showSettings,
      exampleItems,
      pageOfItems,
      pager,
      selectedFacets,
      totalCount,
      pageOffset,
      pageSize,
      pageNo,
      showDash,
      showInbox,
      showSearch,
      showLib,
      showOutbox,
      showRecents,
      isLoading,
      showPeople,
      showTeams,
      showMatters,
      showWorkSpaces,
      errorInResponse,
      showAppNotes,
      landingPage,
      showLibSolr
    } = this.state;

    let classForSearchColList = "navbar-static-side search_col_list";
    let classForRightHandPanel = "col-md-2 col-lg-2 col-sm-2 col-xs-2 pl-0 file-info-section pr-0"
    if (!isLoading) {
      classForSearchColList = classForSearchColList + " trial-class-for-floating-left-panel";
      classForRightHandPanel = classForRightHandPanel + " trial-for-floating-right-panel"
    }


    return (
       <div style={{"background": "url("+this.state.background+") left bottom repeat #010819"}}>
        <div>
        <AppHeader
          user={this.userName}
          userEmail={this.userEmail}
          client={this.client}
          logo={this.state.logo}
          edocsLogo={this.state.edocsLogo}
          showPilotSticker={this.state.showPilotSticker}
        />
        <div className={"row content-section " }>
          <div className="filter_section col-md-2 col-lg-2 col-sm-2 col-xs-2 pl-0 pr-0 left-sec">
            <div className="filter-section-inner">
              {/* <div className="collapse-leftmenu">
                <i className="fa fa-caret-left" aria-hidden="true"></i>
              </div> */}
              <div
                /* className="navbar-static-side search_col_list" */
                className={classForSearchColList}
                role="navigation"
              >
                <div className="sidebar-collapse">
                  <ul className="nav left_nav">
                    {/*<li className="section_header">
                      <a style={{ padding: "10px 0px" }}>Menu</a>
                    </li>*/}
                    {this.state.tabs.indexOf('home')>-1 &&
                    <li
                      className={
                        this.state.showDash
                          ? "open_list selected_link"
                          : "open_list"
                      }
                    >
                      <div
                        onClick={() => {
                          this.setState({
                            showDash: true,
                            showInbox: false,
                            showSearch: false,
                            showLib: false,
                            showLibSolr: false,
                            showOutbox: false,
                            showRecents: false,
                            showPeople: false,
                            showTeams: false,
                            showMatters: false,
                            showWorkSpaces: false,
                          });
                          this.scrollToTop();
                        }}
                      >
                        <span>
                          <i className="fa fa-home" aria-hidden="true"></i>
                          <span className="collapse-txt">&nbsp;&nbsp;Home</span>
                        </span>
                      </div>
                    </li>
                    }
                    {this.state.tabs.indexOf('library')>-1 &&
                    <li
                      className={
                        this.state.showLib
                          ? "open_list selected_link"
                          : "open_list"
                      }
                    >
                      <div
                        onClick={() => {
                          this.setState({
                            showDash: false,
                            showInbox: false,
                            showSearch: false,
                            showLib: true,
                            showOutbox: false,
                            showRecents: false,
                            showPeople: false,
                            showTeams: false,
                            showMatters: false,
                            showWorkSpaces: false,
                          });
                          this.scrollToTop();
                        }}
                      >
                        <span>
                          <i class="fa fa-book" aria-hidden="true"></i>
                          <span className="collapse-txt">
                            &nbsp;&nbsp;Library
                          </span>
                        </span>
                      </div>
                    </li>
                    }
                    {/* {this.state.tabs.indexOf('library')>-1 &&
                    <li
                      className={
                        this.state.showLib
                          ? "open_list selected_link"
                          : "open_list"
                      }
                    >
                      <div
                        onClick={() => {
                          this.setState({
                            showDash: false,
                            showInbox: false,
                            showSearch: false,
                            showLib: false,
                            showLibSolr: true,
                            showOutbox: false,
                            showRecents: false,
                            showPeople: false,
                            showTeams: false,
                            showMatters: false,
                            showWorkSpaces: false,
                          });
                        }}
                      >
                        <span>
                          <i className="fa fa-book" aria-hidden="true"></i>
                          <span className="collapse-txt">
                            &nbsp;&nbsp;Library Solr
                          </span>
                        </span>
                      </div>
                    </li>
                    } */}
                    {this.state.tabs.indexOf('search')>-1 &&
                    <li
                      className={
                        this.state.showSearch
                          ? "open_list selected_link"
                          : "open_list"
                      }
                    >
                      <div
                        onClick={() => {
                          this.setState({
                            showDash: false,
                            showInbox: false,
                            showSearch: true,
                            showLib: false,
                            showLibSolr: false,
                            showOutbox: false,
                            showRecents: false,
                            showPeople: false,
                            showTeams: false,
                            showMatters: false,
                            showWorkSpaces: false,
                          });
                          this.scrollToTop();
                          this.focusDiv();
                        }}
                      >
                        <span>
                          <i className="fa fa-search" aria-hidden="true"></i>
                          <span className="collapse-txt">
                            &nbsp;&nbsp;Search
                          </span>
                        </span>
                      </div>
                      {this.state.showFilters && this.state.showSearch && (
                        <div>
                          <li className="open_list">
                            <div><span><i className="filter-icon" aria-hidden="true"></i>
                            <span className="collapse-txt" style={{fontSize: "12px"}}>&nbsp;&nbsp;CATEGORIES</span></span></div>
                          </li>
                          {this.state.showCategories &&
                            <div
                              style={{
                                height: this.state.filterheight,
                                overflowY: "auto",
                                paddingTop:"0px",
                                paddingLeft:"0px",
                                textIndent:"10px"
                              }}
                            >

                              <AppFacet
                                facets={this.state.facets}
                                ExplorerFacets={this.state.ExplorerFacets}
                                intervalFacets={this.state.intervalFacets}
                                onFilterSelection={this.onFilterSelection}
                                expanded={this.state.expandedFacets}
                                selected={this.state.selectedFacets}
                                resultSource={this.state.resultSource}
                              />
                            </div>
                          }
                        </div>

                    )}
                    </li>
                    }
                    {this.state.tabs.indexOf('inbox')>-1 &&
                    <li
                      className={
                        this.state.showInbox
                          ? "open_list selected_link"
                          : "open_list"
                      }
                    >
                      <div
                        onClick={() => {
                          this.setState({
                            showDash: false,
                            showInbox: true,
                            showSearch: false,
                            showLib: false,
                            showLibSolr: false,
                            showOutbox: false,
                            showRecents: false,
                            showPeople: false,
                            showTeams: false,
                            showMatters: false,
                            showWorkSpaces: false,
                          });
                          this.scrollToTop();
                        }}
                      >
                        <span>
                          <i className="fa fa-inbox" aria-hidden="true"></i>
                          <span className="collapse-txt">
                            &nbsp;&nbsp;Inbox
                          </span>
                        </span>
                      </div>
                    </li>
                    }
                    {this.state.tabs.indexOf('outbox')>-1 &&
                    <li
                        className={
                            this.state.showOutbox
                              ? "open_list selected_link"
                              : "open_list"
                        }
                      >
                      <div
                          onClick={() => {
                            this.setState({
                              showDash: false,
                              showInbox: false,
                              showSearch: false,
                              showLib: false,
                              showLibSolr: false,
                              showOutbox: true,
                              showRecents: false,
                              showPeople: false,
                              showTeams: false,
                              showMatters: false,
                              showWorkSpaces: false,
                            });
                            this.scrollToTop();
                          }}
                    >

                        <span>
                          <i
                            className="fa fa-share-square-o"
                            aria-hidden="true"
                          ></i>
                          <span className="collapse-txt">&nbsp;&nbsp;Outbox</span>
                        </span>
                      </div>
                    </li>
                    }
                    {this.state.tabs.indexOf('teams')>-1 &&
                    <li
                      className={
                        this.state.showTeams
                          ? "open_list selected_link"
                          : "open_list"
                      }
                    >
                      <div
                        onClick={() => {
                          this.setState({
                            showDash: false,
                            showInbox: false,
                            showSearch: false,
                            showLib: false,
                            showLibSolr: false,
                            showOutbox: false,
                            showRecents: false,
                            showPeople: false,
                            showTeams: true,
                            showMatters: false,
                            showWorkSpaces: false,
                          });
                          this.scrollToTop();
                        }}
                      >
                        <span>
                          <i className="fa fa-users" aria-hidden="true"></i>
                          <span className="collapse-txt">&nbsp;&nbsp;Teams</span>
                        </span>
                      </div>
                    </li>
                    }
                    {this.state.tabs.indexOf('people')>-1 &&
                    <li
                      className={
                        this.state.showPeople
                          ? "open_list selected_link"
                          : "open_list"
                      }
                    >
                      <div
                        onClick={() => {
                          this.setState({
                            showDash: false,
                            showInbox: false,
                            showSearch: false,
                            showLib: false,
                            showLibSolr: false,
                            showOutbox: false,
                            showRecents: false,
                            showPeople: true,
                            showTeams: false,
                            showMatters: false,
                            showWorkSpaces: false,
                          });
                          this.scrollToTop();
                        }}
                      >
                        <span>
                          <i className="fa fa-user" aria-hidden="true"></i>
                          <span className="collapse-txt">&nbsp;&nbsp;People</span>
                        </span>
                      </div>
                    </li>
                    }
                    {this.state.tabs.indexOf('matters')>-1 &&
                    <li
                      className={
                        this.state.showMatters
                          ? "open_list selected_link"
                          : "open_list"
                      }
                    >
                      <div
                        onClick={() => {
                          this.setState({
                            showDash: false,
                            showInbox: false,
                            showSearch: false,
                            showLib: false,
                            showLibSolr: false,
                            showOutbox: false,
                            showRecents: false,
                            showPeople: false,
                            showTeams: false,
                            showMatters: true,
                            showWorkSpaces: false,
                          });
                          this.scrollToTop();
                        }}
                      >
                        <span>
                          <i className="fa fa-users" aria-hidden="true"></i>
                          <span className="collapse-txt">&nbsp;&nbsp;Matters</span>
                        </span>
                      </div>
                    </li>
                    }
                    {this.state.tabs.indexOf('workspaces')>-1 &&
                    <li
                      className={
                        this.state.showWorkSpaces
                          ? "open_list selected_link"
                          : "open_list"
                      }
                    >
                      <div
                        onClick={() => {
                          this.setState({
                            showDash: false,
                            showInbox: false,
                            showSearch: false,
                            showLib: false,
                            showLibSolr: false,
                            showOutbox: false,
                            showRecents: false,
                            showPeople: false,
                            showTeams: false,
                            showMatters: false,
                            showWorkSpaces: true,
                          });
                          this.scrollToTop();
                        }}
                      >
                        <span>
                          <i className="fa fa-building" aria-hidden="true"></i>
                          <span className="collapse-txt">
                            &nbsp;&nbsp;WorkSpaces
                          </span>
                        </span>
                      </div>
                    </li>
                    }
                 </ul>
                </div>
              </div>
            </div>
          </div>
          <div className="main-section col-md-6 col-lg-6 col-sm-6 col-xs-6 middle-sec">
            <Card style={{"outline": "none"}} id="result_card"
                ref = "theDiv" onKeyDown={ (e) => {this.handleKeyDown(e)} } tabIndex="0">
              {showDash && (
                <Card.Body>
                  <div style={{
                      fontSize: "1.75rem",
                      fontWeight: "600",
                    }} >
                  
                    <div className="col-md-12 col-lg-12 col-sm-12 col-xs-12" style={{'marginTop':'15px'}}>
                    <AppHome  user={this.userName} userEmail={this.userEmail} dgiUserName={this.state.dgiUserName} source={this.state.sources} client={this.client} edocsBackendAPI={this.state.edocsBackendAPI}   />
                    </div>
                   </div>
                </Card.Body>
              )}
              
                <Card.Body>
                  {isLoading ? (
                    <div style={{ textAlign: "center" }}>
                    <img src={this.state.loaderIcon} className="loader"></img>

                    </div>
                  ) : (
                  <div>
                    <div className="row" id="searchQuerydiv">
                      <div
                        className="col-lg-12 pl-0 pr-0"
                        style={{ marginBottom: "15px" }}
                      >
                        <div className="col-lg-6 customtooltip">

                          <div className="custom-search-input">
                            {/* <div
                              className="input-group"
                              style={{ marginTop: "0px" }}
                            >
                              <input
                                type="text"
                                className="search-query"
                                placeholder="Search here"
                                id="Search"
                                name="searchQuery"
                                value={this.state.searchQuery}
                                onChange={this.handleChange}
                                autoComplete="off"
                                required
                                autoFocus
                                onKeyPress={(e) => {
                                  e.which === 13 ? this.performSearch('reset') : null;
                                }}
                              />
                              <span
                                className="input-group-btn input-align"
                                onClick={(e) => this.performSearch('reset')}
                              >
                                <button
                                  className="btn"
                                  type="submit"
                                  aria-label="click here to Search"
                                  style={{ outline: "none" }}
                                >
                                  <span className="glyphicon glyphicon-search"></span>
                                </button>
                              </span>

                              
                            </div> */}
                            <Suggestions  onSuggestionClick={this.onSuggestionClick} setSearchQuery={this.setSearchQuery}
                                          onSearchIconClick={()=> this.performSearch('reset')}
                                          eventAppId={this.state.eventAppId}
                                          searchQuery={this.state.searchQuery}
                                          client={this.client}
                                          userIP={this.state.userIP}
                                          sessionId={this.state.sessionId}
                                          userEmail={this.userEmail} 
                                          source={this.state.sources}
                                          legacyDataset={this.state.legacyDataset}
                                          boxAuthorized={this.state.boxAuthorized}
                                          spAuthorized={this.state.spAuthorized}
                                          datasetBox={this.state.datasetBox}
                                          datasetSP={this.state.datasetSP}
                                          setSuggestionBoxVisibility = {this.setSuggestionBoxVisibility}
                                          />
                            {this.state.showSWS==false &&
                              <i className="fa fa-search-plus srch-whtn-search-icon" 
                                title="Search within search" 
                                onClick={()=>this.setState({showSWS:true})}
                                aria-hidden="true"></i>
                            }
                            {this.state.showSWS==true &&
                              <i className="fa fa-search-minus srch-whtn-search-icon" title="Hide search within search input" 
                                aria-hidden="true"
                                onClick={()=>this.setState({showSWS:false})}
                              ></i>
                            }
                            {/* <i class="fa fa-ticket sp_image" style={{"marginRight": "150px"}} aria-hidden="true" onClick={(e) => this.setState({showExpansions:true})}></i> */}
                            <i class="fa fa-cogs sp_image" aria-hidden="true" onClick={(e) => this.setState({showSettings:true})}></i>
                            <img src={noteImage} className="note_image" onClick={() => this.showNotes()} title="Open Note"/>
                          </div>
                          {/* <div className="customtooltiptext customtooltip-bottom">
                          <div>term1 <strong>OR</strong> term2</div>
                          <div>term1 <strong>AND</strong> term2</div>
                          <div><strong>"</strong>some query<strong>"</strong></div>
                          <div><strong>field:</strong>term</div>
                          <div><strong>field:"</strong>term<strong>"</strong> (term in double quotes)</div>
                          <div><strong>field1:</strong>term1 AND <strong>field2:</strong> term2</div>
                          <div><strong>field1:</strong>term1 OR <strong>field2:</strong> term2</div>
                        </div> */}
                        </div>

                        {/* <div className="col-lg-6">
                        <div className="pull-right cog_div">
                          <button
                            type="button"
                            className="btn-cog collapsed"
                            aria-expanded="false"
                            title="Application Selection"
                          >
                            <i className="fa fa-cog" aria-hidden="true"></i>
                          </button>
                        </div>
                      </div> */}
                      </div>
                    </div>
                    {showSearch && landingPage && exampleItems.length > 0 && (
                      <div className="result-header" >
                        <div
                          style={{
                            color: "#B00E29",
                            fontSize: "1.6rem",
                            fontWeight: "600",textAlign:"center!important"
                          }}
                        >
                        <span style={{textAlign:"center!important"
                          }}><i className="fa fa-clock-o recent-style" aria-hidden="true"></i>Recently Viewed Documents
                          {/* {this.state.selectedDoc.sp_doc_id} */}
                          </span>
                        </div>
                      </div>
                    )}
                    {!errorInResponse && showSearch && !landingPage && (
                      <div style={{"height": "40px"}}>
                      <div className="col-md-9 pl-0 pr-0 block-left">
                      <PipelineTokens tokens={this.state.queryTokens} pipelinetokens={this.state.pipelineTokens}
                                      displayExpansions={this.displayExpansions}
                                      pinnedconcepts={this.state.pinnedConcepts}
                                      tokenMap={this.state.tokenMap}
                                      />
                      </div>
                      <div className="col-md-3 pl-0 pr-0 block-right">
                          <ul class="legend_list">
                          <li class="red_legend">
                            <div class="red legend_tooltip">&nbsp;</div>
                            <span class="red_tt tooltiptext">Exact Term Matches</span></li>
                          <li class="green_legend">
                            <div class="green legend_tooltip">&nbsp;</div>
                            <span class="green_tt tooltiptext">Term Synonyms</span></li>
                          <li class="blue_legend">
                            <div class="blue legend_tooltip">&nbsp;</div>
                            <span class="blue_tt tooltiptext">Sub Concepts</span></li>
                          <li class="yellow_legend">
                            <div class="yellow legend_tooltip">&nbsp;</div>
                            <span class="yellow_tt tooltiptext">Related</span></li>
                          </ul>
                      </div>
                        </div>
                    )}
                    {showSearch && !landingPage && exampleItems.length> 0 &&(
                      <div className="result-header">
                          <div
                            style={{
                              color: "#B00E29",
                              fontSize: "1.6rem",
                              fontWeight: "600",
                            }}
                          >
                          <div className={errorInResponse? "hide_div": ""}>
                            Showing Results {`${pageOffset +1}`} -{" "}
                            {pageOffset + pageSize > totalCount
                              ? `${totalCount}`
                              : `${pageOffset + pageSize}`}{" "}
                            (of {`${totalCount}`})
                            </div>
                          </div>
                        
                        { !errorInResponse && this.state.showSWS==false &&
                      <div className="pagination" style={{  "paddingTop": "7px"}}> 
                            <button
                              className="pagination-button"
                              onClick={() => this.loadPage(0, pageSize, 1)}
                              disabled={pageNo == 1 ? true : false}
                            >
                              <i className="fa fa-fast-backward"></i>
                            </button>
                            <button
                              className="pagination-button"
                              onClick={() => {
                                if (pageOffset - pageSize > 0) {
                                  this.loadPage(
                                    pageOffset - pageSize,
                                    pageSize,
                                    pageNo - 1
                                  );
                                } else {
                                  this.loadPage(0, pageSize, 1);
                                }
                              }}
                              disabled={pageNo == 1 ? true : false}
                            >
                              <i className="fa fa-step-backward"></i>
                            </button>
                            <input
                              className="pagination-input"
                              value={this.state.pageNo}
                              // onChange={(e) => {
                                // this.setState(
                                //   { pageNo: Number(e.target.value) }
                                //   () => {
                                    // this.loadPage(
                                    //   e.target.value * pageSize,
                                    //   pageSize,
                                    //   this.state.pageNo
           
                                //   }
                                // );
                              // }}
                            ></input>
                            <span className="pagination-totalpage">
                              / {Math.ceil(totalCount / pageSize)}
                            </span>
                            <button
                              className="pagination-button"
                              onClick={() => {
                                if (pageOffset + pageSize > totalCount) {
                                  this.loadPage(
                                    totalCount - pageOffset,
                                    pageSize,
                                    pageNo + 1
                                  );
                                } else {
                                  this.loadPage(
                                    pageOffset + pageSize,
                                    pageSize,
                                    pageNo + 1
                                  );
                                }
                              }}
                              disabled={
                                pageNo <= Math.floor(totalCount / pageSize)
                                  ? false
                                  : true
                              }
                            >
                              <i className="fa fa-step-forward"></i>
                            </button>
                            <button
                              className="pagination-button"
                              onClick={() => {
                                if (pageOffset + pageSize > totalCount) {
                                  this.loadPage(
                                    totalCount - pageOffset,
                                    pageSize,
                                    pageNo + 1
                                  );
                                } else {
                                  this.loadPage(
                                    totalCount - pageSize,
                                    pageSize,
                                    Math.ceil(Number(totalCount / pageSize))
                                  );
                                }
                              }}
                              disabled={
                                pageNo <= Math.floor(totalCount / pageSize)
                                  ? false
                                  : true
                              }
                            >
                              <i className="fa fa-fast-forward"></i>
                            </button>
                          </div>
                    }
                        {this.state.showSWS==true &&
                        <div style={{
                            position: "relative",
                            right: "0",
                          }} className="customtooltip">
                          <div>
                            <div className="srch-whtn-search srch-whtn-search-focus">
                            <div
                              className="input-group"
                              style={{ marginTop: "0px" }}
                            >
                              <input
                                type="text"
                                className="search-query"
                                placeholder="Search within search"
                                id="SearchWithin"
                                name="searchWithinQuery"
                                value={this.state.searchWithinQuery}
                                onChange={this.handleChange}
                                autoComplete="off"
                                onKeyPress={(e) => {
                                  e.which === 13 ? this.performSearchWithinSearch() : null;
                                }}
                              />
                              <span
                                className="input-group-btn input-align"
                                onClick={(e) => this.performSearchWithinSearch()}
                              >
                                <button
                                  className="btn"
                                  type="submit"
                                  aria-label="click here to Search"
                                  style={{ outline: "none" }}
                                >
                                  <span className="glyphicon glyphicon-search"></span>
                                </button>
                              </span>
                            </div>
                          </div>
                          </div>
                          {/* <Pagination
                          pageSize={this.state.itemCount}
                          items={exampleItems}
                          onChangePage={this.onChangePage}
                          className="pagination-bar"
                        /> */}
                        {/* <div className="customtooltiptext customtooltip-bottom">
                          <div>term1 <strong>OR</strong> term2</div>
                          <div>term1 <strong>AND</strong> term2</div>
                          <div><strong>"</strong>some query<strong>"</strong></div>
                          <div><strong>field:</strong> term</div>
                          <div><strong>field: "</strong>term<strong>"</strong> </div>
                          <div><strong>field1:</strong> term1 <strong>AND field2:</strong> term2</div>
                          <div><strong>field1:</strong> term1 <strong>OR  field2:</strong> term2</div>
                        </div> */}
                        </div>
                        }
                        <div className={errorInResponse? "hide_div": ""}>
                           <select title="Result per page"
                              id="item-count"
                              name="pageSize"
                              value={this.state.pageSize}
                              type="number"
                              onChange={(e) => {
                                this.setState(
                                  { pageSize: Number(e.target.value) },
                                  () => {
                                    this.performSearch();
                                  }
                                );
                              }}
                              style={{
                                color: "#000000",
                                fontSize: "1.5rem",
                                fontWeight: "400",
                                border: "1px solid black",
                                borderRadius: "1.5rem",
                                padding: "5px",
                                marginLeft: "5px",
                                boxShadow: "inset -1px 0px 10px 0px #9E9E9E",
                              }}
                            >
                              <option value={10}>10</option>
                              <option value={25}>25</option>
                              <option value={50}>50</option>
                              <option value={100}>100</option>
                            </select>
                            <select title="Sort by"
                              id="item-count"
                              name="sort"
                              value={this.state.sort}
                              type="number"
                              onChange={(e) => {
                                this.setState(
                                  { sort: String(e.target.value) },
                                  () => {
                                    this.performSearch();
                                  }
                                );
                              }}
                              style={{
                                color: "#000000",
                                fontSize: "1.5rem",
                                fontWeight: "400",
                                border: "1px solid black",
                                borderRadius: "1.5rem",
                                padding: "5px",
                                marginLeft: "5px",
                                boxShadow: "inset -1px 0px 10px 0px #9E9E9E",
                              }}
                            >
                              <option value={"score desc"}>Relevancy</option>
                              <option value={"modified desc"}>Modified Date</option>
                              <option value={"doc_source_info asc"}>Title</option>
                            </select>
                        </div>
                      </div>
                    )}
                    {showSearch &&
                    <div>
                      {selectedFacets.map((item) => {
                        return (
                          <span className="filter-token"
                            onClick={() => this.clearFilter(item)}
                          >
                            <span
                              className="fa fa-close"
                              style={{ marginRight: "4px"}}
                            ></span>
                            {item.displayName}
                          </span>
                        );
                      })}
                      {selectedFacets.length > 0 && (
                        <span className="filter-token"
                        onClick={() =>
                          this.setState(
                            { filterObj: {}, selectedFacets: [] },
                            () => {
                              this.onFilterSelection([]);
                            }
                          )
                        }
                        >
                          <span
                            className="fa fa-close"
                            style={{ marginRight: "4px" }}

                          ></span>
                          Reset all
                        </span>
                      )}
                    </div>
                    }
                    {showSearch && showAppNotes && 
                      <NoteSlider isNotesOpen={this.state.showAppNotes}  
                                  client={this.client}
                                  userNotes={this.state.userNotes}
                                  onCloseNotesSlider={this.updateUserNotes}
                                  userEmail={this.props.userEmail}
                                  onPerformSearch={(data)=> this.searchNotesQuery(data)}/>
                    }
                    {showSearch &&
                    <div className="result"  >
                    { !errorInResponse && this.state.showSWS==true &&
                    <div>
                      <div className="pagination" style={{"marginBottom": "15px!important"}}>
                            <button
                              className="pagination-button"
                              onClick={() => this.loadPage(0, pageSize, 1)}
                              disabled={pageNo == 1 ? true : false}
                            >
                              <i className="fa fa-fast-backward"></i>
                            </button>
                            <button
                              className="pagination-button"
                              onClick={() => {
                                if (pageOffset - pageSize > 0) {
                                  this.loadPage(
                                    pageOffset - pageSize,
                                    pageSize,
                                    pageNo - 1
                                  );
                                } else {
                                  this.loadPage(0, pageSize, 1);
                                }
                              }}
                              disabled={pageNo == 1 ? true : false}
                            >
                              <i className="fa fa-step-backward"></i>
                            </button>
                            <input
                              className="pagination-input"
                              value={this.state.pageNo}
                              onChange={(e) => {
                                this.setState(
                                  { pageNo: Number(e.target.value) },
                                  () => {
                                    this.loadPage(
                                      this.state.pageNo * pageSize,
                                      pageSize,
                                      this.state.pageNo
                                    );
                                  }
                                );
                              }}
                            ></input>
                            <span className="pagination-totalpage">
                              / {Math.ceil(totalCount / pageSize)}
                            </span>
                            <button
                              className="pagination-button"
                              onClick={() => {
                                if (pageOffset + pageSize > totalCount) {
                                  this.loadPage(
                                    totalCount - pageOffset,
                                    pageSize,
                                    pageNo + 1
                                  );
                                } else {
                                  this.loadPage(
                                    pageOffset + pageSize,
                                    pageSize,
                                    pageNo + 1
                                  );
                                }
                              }}
                              disabled={
                                pageNo <= Math.floor(totalCount / pageSize)
                                  ? false
                                  : true
                              }
                            >
                              <i className="fa fa-step-forward"></i>
                            </button>
                            <button
                              className="pagination-button"
                              onClick={() => {
                                if (pageOffset + pageSize > totalCount) {
                                  this.loadPage(
                                    totalCount - pageOffset,
                                    pageSize,
                                    pageNo + 1
                                  );
                                } else {
                                  this.loadPage(
                                    totalCount - pageSize,
                                    pageSize,
                                    Math.ceil(Number(totalCount / pageSize))
                                  );
                                }
                              }}
                              disabled={
                                pageNo <= Math.floor(totalCount / pageSize)
                                  ? false
                                  : true
                              }
                            >
                              <i className="fa fa-fast-forward"></i>
                            </button>
                          </div>
                          {this.state.searchWithinQuery &&
                            <span className="filter-token"
                              style={{"float": "right", "marginTop": "-45px"}}
                              onClick={() => this.gotoMainSearch()}
                            >
                            Go to main search
                          </span>
                          }
                          </div>
                    }
                    {exampleItems.map((item) => item)}
                    { !errorInResponse &&
                    <div style={{"marginBottom": "15px", "marginTop": "15px"}} className="pagination" >
                            <button
                              className="pagination-button"
                              onClick={() => this.loadPage(0, pageSize, 1)}
                              disabled={pageNo == 1 ? true : false}
                            >
                              <i className="fa fa-fast-backward"></i>
                            </button>
                            <button
                              className="pagination-button"
                              onClick={() => {
                                if (pageOffset - pageSize > 0) {
                                  this.loadPage(
                                    pageOffset - pageSize,
                                    pageSize,
                                    pageNo - 1
                                  );
                                } else {
                                  this.loadPage(0, pageSize, 1);
                                }
                              }}
                              disabled={pageNo == 1 ? true : false}
                            >
                              <i className="fa fa-step-backward"></i>
                            </button>
                            <input
                              className="pagination-input"
                              value={this.state.pageNo}
                              onChange={(e) => {
                                this.setState(
                                  { pageNo: Number(e.target.value) },
                                  () => {
                                    this.loadPage(
                                      this.state.pageNo * pageSize,
                                      pageSize,
                                      this.state.pageNo
                                    );
                                  }
                                );
                              }}
                            ></input>
                            <span className="pagination-totalpage">
                              / {Math.ceil(totalCount / pageSize)}
                            </span>
                            <button
                              className="pagination-button"
                              onClick={() => {
                                if (pageOffset + pageSize > totalCount) {
                                  this.loadPage(
                                    totalCount - pageOffset,
                                    pageSize,
                                    pageNo + 1
                                  );
                                } else {
                                  this.loadPage(
                                    pageOffset + pageSize,
                                    pageSize,
                                    pageNo + 1
                                  );
                                }
                              }}
                              disabled={
                                pageNo <= Math.floor(totalCount / pageSize)
                                  ? false
                                  : true
                              }
                            >
                              <i className="fa fa-step-forward"></i>
                            </button>
                            <button
                              className="pagination-button"
                              onClick={() => {
                                if (pageOffset + pageSize > totalCount) {
                                  this.loadPage(
                                    totalCount - pageOffset,
                                    pageSize,
                                    pageNo + 1
                                  );
                                } else {
                                  this.loadPage(
                                    totalCount - pageSize,
                                    pageSize,
                                    Math.ceil(Number(totalCount / pageSize))
                                  );
                                }
                              }}
                              disabled={
                                pageNo <= Math.floor(totalCount / pageSize)
                                  ? false
                                  : true
                              }
                            >
                              <i className="fa fa-fast-forward"></i>
                            </button>
                          </div>
                      }
                    </div>
                    }
                  </div>)}
                </Card.Body>
              
              {showInbox && (
                <Card.Body>
                  <div className="outlook-strip">&nbsp;</div>
                  <div
                    style={{
                      color: "#B00E29",
                      fontSize: "1.75rem",
                      fontWeight: "600",
                    }}
                  >
                    <div className="tabbed-area">
        {/*<ul className="tabs group">
          <li><a href="#box-one" className="tabbed-area-active">Inbox</a></li>
          <li><a href="#box-two">Outbox</a></li>
        </ul>*/}
        <div className="box-wrap">
          <div id="box-two">
            <span className="inbx-info-wrapper">
              <span className="inbx-left-section">
                <h5>Glen Maxwell</h5>
                <span className="email-sub">Digital Customer Confirmation Survey</span><br />
                <span>Please make sure that you have one of the following cards with you when we deliver your ord...</span>
              </span>
              <span className="inbx-right-section">
                <span>11.16</span>
                <span><i className="fa fa-trash-o" aria-hidden="true" /></span>
              </span></span>
            <span className="inbx-info-wrapper">
              <span className="inbx-left-section">
                <h5>Stephen Hawking</h5>
                <span className="email-sub">Activate your LIPO Account today</span><br />
                <span>Thank you for creating a LIPO Account. Please click the link below to activate your account...</span>
              </span>
              <span className="inbx-right-section">
                <span>12.05</span>
                <span><i className="fa fa-trash-o" aria-hidden="true" /></span>
              </span></span>
            <span className="inbx-info-wrapper">
              <span className="inbx-left-section">
                <h5>Rita Levi-Montalcini</h5>
                <span className="email-sub">Pay bills &amp; win up to 600$ Cashback!</span><br />
                <span>Congratulations on your iRun Coach subscription. You made no space for excuses and you decided on a healthier and happier life...</span>
              </span>
              <span className="inbx-right-section">
                <span>12.10</span>
                <span><i className="fa fa-trash-o" aria-hidden="true" /></span>
              </span></span>
            <span className="inbx-info-wrapper">
              <span className="inbx-left-section">
                <h5>Max O'Brien Planck</h5>
                <span className="email-sub">Congratulations on your iRun Coach subscription</span><br />
                <span>Congratulations on your iRun Coach subscription. You made no space for excuses and you...</span>
              </span>
              <span className="inbx-right-section">
                <span>12.30</span>
                <span><i className="fa fa-trash-o" aria-hidden="true" /></span>
              </span></span>
            <span className="inbx-info-wrapper">
              <span className="inbx-left-section">
                <h5>Jane Goodall</h5>
                <span className="email-sub">Payment Notification DLOP2329KD</span><br />
                <span>Your payment of 4500USD to AirCar has been authorized and confirmed, thank you your account. This...</span>
              </span>
              <span className="inbx-right-section">
                <span>12.45</span>
                <span><i className="fa fa-trash-o" aria-hidden="true" /></span>
              </span></span>
            <span className="inbx-info-wrapper">
              <span className="inbx-left-section">
                <h5>Enrico Fermi</h5>
                <span className="email-sub">Your Order #224820998666029 has been Confirmed</span><br />
                <span>Your Order #224820998666029 has been placed on Saturday, 29 June...</span>
              </span>
              <span className="inbx-right-section">
                <span>13.00</span>
                <span><i className="fa fa-trash-o" aria-hidden="true" /></span>
              </span></span>
            <span className="inbx-info-wrapper">
              <span className="inbx-left-section">
                <h5>Sean Paul</h5>
                <span className="email-sub">Your iBuy.com grocery shopping confirmation</span><br />
                <span>Please make sure that you have one of the following cards with you when we deliver your ord...</span>
              </span>
              <span className="inbx-right-section">
                <span>13.10</span>
                <span><i className="fa fa-trash-o" aria-hidden="true" /></span>
              </span></span>
           
          </div>
        </div>
      </div>
      <div className="tabbed-area-middle">
        <h4>Digital Customer Confirmation Survey</h4>
        <span className="inbx-left-section">
          <span className="intials">GM</span>
          <span className="mailtodesc pull-left">Allen Morgon<br/>To&nbsp;&nbsp;Glen Maxwell</span>
          <span className="emailbody">
            Hi Glen,<br/><br/>
With resrpect, i must disagree with Mr.Zinsser. We all know the most part of important part of any article is the title.Without a compelleing title, your reader won't even get to the first sentence.After the title, however, the first few sentences of your article are certainly the most important part.<br/><br/>
Jornalists call this critical, introductory section the "Lede," and when bridge properly executed, it's the that carries your reader from an headine try at attention-grabbing to the body of your blog post, if you want to get it right on of these 10 clever ways to omen your next blog posr with a bang<br/><br/>
Jornalists call this critical, introductory section the "Lede," and when bridge properly executed, it's the that carries your reader from an headine try at attention-grabbing to the body of your blog post, if you want to get it right on of these 10 clever ways to omen your next blog posr with a bang<br/><br/>

Best regards,<br/><br/>
          </span>
        </span>
        <span className="inbx-right-section icons-wrapper">
            <span><i className="fa fa-reply" aria-hidden="true"></i></span>
            <span><i className="fa fa-reply-all" aria-hidden="true"></i></span>
            <span><i className="fa fa-arrow-right" aria-hidden="true" /></span>
        </span>
      </div>
      <div className="tabbed-area-last">Document Preview</div>
                  </div>
                </Card.Body>
              )}
              {showRecents && (
                <Card.Body>
                  {" "}
                  <div
                    style={{
                      color: "#B00E29",
                      fontSize: "1.75rem",
                      fontWeight: "600",
                    }}
                  >
                    Welcome to Recents
                  </div>
                </Card.Body>
              )}
              {showLib && (
                <Card.Body>
                  <DocumentLibrarySolr  
                  eventAppId={this.state.eventAppId}
                  loaderIcon={this.state.loaderIcon}
                  serviceType={this.state.serviceType}
                  solrCollection={this.state.solrCollection}
                  userEmail={this.userEmail}
                  dgiUserName={this.state.dgiUserName} 
                  mainTreeData={this.state.mainTreeList}
                  source={this.state.sources}
                  client={this.client}
                  versionTreeData={this.state.versionTreeList}
                  pageNo={this.state.pageNo}
                  searchQuery={this.state.searchQuery}
                  userIP={this.state.userIP}
                  sessionId={this.state.sessionId}
                  documentID={this.state.selectedDoc.file_id}
                  SPUserName={this.state.SPUserName}
                  boxUserGroupIds={this.state.boxUserGroupIds}
                  boxUserId={this.state.boxUserId}
                  accessToken={this.state.accessToken}
                  refreshToken={this.state.refreshToken}
                  edocsBackendAPI={this.state.edocsBackendAPI}
                  isFromSettings={false}
                 />
                  
                </Card.Body>
              )}
              {showLibSolr && (
                <Card.Body>
                  <DocumentLibrary  
                  loaderIcon={this.state.loaderIcon}
                  edocsBackendAPI={this.state.edocsBackendAPI}
                  eventAppId={this.state.eventAppId}
                  userEmail={this.userEmail}
                  dgiUserName={this.state.dgiUserName} 
                  mainTreeData={this.state.mainTreeList}
                  source={this.state.sources}
                  client={this.client}
                  versionTreeData={this.state.versionTreeList}
                  pageNo={this.state.pageNo}
                  searchQuery={this.state.searchQuery}
                  userIP={this.state.userIP}
                  sessionId={this.state.sessionId}
                  documentID={this.state.selectedDoc.file_id} />
                </Card.Body>
              )}
              {showOutbox && (
                <Card.Body>
                  <Outbox />
                </Card.Body>
              )}

              {showPeople && (
                <Card.Body>
                  <People />
                </Card.Body>
              )}
              {showTeams && (
                <Card.Body>
                  <Teams />
                </Card.Body>
              )}
              {showMatters && (
                <Card.Body>
                  <Matters />
                </Card.Body>
              )}
              {showWorkSpaces && (
                <Card.Body>
                  <WorkSpaces />
                </Card.Body>
              )}
            </Card>
          </div>
          { showSearch && this.state.showFileInfo &&
            <div /* className="col-md-2 col-lg-2 col-sm-2 col-xs-2 pl-0 file-info-section pr-0" */ className={classForRightHandPanel} >
              {this.state.showFileInfo &&
                <AppFileInfoPanel selectedDoc={this.state.selectedDoc} 
                                  loaderIcon={this.state.loaderIcon}
                                  eventAppId={this.state.eventAppId}
                                  solrCollection={this.state.solrCollection}
                                  fileType={this.state.selectedDoc.fileExtension} 
                                  source={this.state.sources}
                                  client={this.client}
                                  userEmail={this.userEmail} 
                                  openRightPane={this.openRightPane}
                                  accessToken={this.state.accessToken}
                                  refreshToken={this.state.refreshToken}
                                  pageNo={this.state.pageNo}
                                  timeCaptureTotal={this.state.timeCaptureTotal}
                                  searchQuery={this.state.searchQuery}
                                  totalCount={this.state.totalCount}
                                  docUrl={this.state.selectedDocumentUrl}
                                  userIP={this.state.userIP}
                                  sessionId={this.state.sessionId}
                                  document_ID={this.state.selectedDoc.file_id}
                                  pageSize={this.state.pageSize}
                                  source_type={this.state.selectedDoc.source_type==null?"sharepoint":this.state.selectedDoc.source_type}
                                  edocsBackendAPI={this.state.edocsBackendAPI}
                                  showCollection={this.state.showCollection}
                                  itemToAddToCollection={this.state.itemToAddToCollection}
                                  collectionNodeSelected={this.collectionNodeSelected}
                                  sendCollectionAddEvent={this.sendCollectionAddEvent}
                                  />
              }
            </div>
          }
         {showSettings &&
          <AppSettings
            boxAuthorized={this.state.boxAuthorized}
            boxAuthUser={this.state.boxAuthUser}
            spAuthorized={this.state.spAuthorized}
            SPUserName={this.state.SPUserName}
            show={showSettings}
            storage={this.state.sources}
            onHide={() => { this.setState({ showSettings: false }); }}
            onSuccess={(SPUserName,SPpassword) => {
              this.setState({ /* showSettings: false,  */SPUserName: SPUserName, SPpassword:SPpassword, spAuthorized: true })
            }}
            dgiUserName={this.state.dgiUserName}
            accessToken={this.state.accessToken}
            refreshToken={this.state.refreshToken}
            SPUserName={this.state.SPUserName}
            boxUserGroupIds={this.state.boxUserGroupIds}
            boxUserId={this.state.boxUserId}
            removeBoxAuth={this.removeBoxAuth}
            removeSharePointAuth={this.removeSharePointAuth}
            eventAppId={this.state.eventAppId}
            loaderIcon={this.state.loaderIcon}
            serviceType={this.state.serviceType}
            solrCollection={this.state.solrCollection}
            userEmail={this.userEmail}
            mainTreeData={this.state.mainTreeList}
            source={this.state.sources}
            client={this.client}
            versionTreeData={this.state.versionTreeList}
            pageNo={this.state.pageNo}
            searchQuery={this.state.searchQuery}
            userIP={this.state.userIP}
            sessionId={this.state.sessionId}
            documentID={this.state.selectedDoc.file_id}
            edocsBackendAPI={this.state.edocsBackendAPI}
          />
        }
        {this.state.showExpansions &&
          <AppExpansions
            pipelineTokens={this.state.pipelineTokens}
            clickedtoken={this.state.clickedtoken}
            show={this.state.showExpansions}
            storage={this.state.sources}
            tokenMap={this.state.tokenMap}
            onHide={() => { this.setState({ showExpansions: false }); }}
            onSuccess={() => {
              this.setState({ showExpansions: false})
            }}
          />
        }
        {this.state.showToaster &&
            <AppToaster
              show={this.state.showToaster}
              title={this.state.modalTitle}
              size={this.state.modalSize}
              modalContent={this.state.modalContent}
              onHide={() => { this.setState({ showToaster: false }); }}
            />
        }

          {/* <div className="col-md-2 col-lg-2 col-sm-2 col-xs-2 pl-0 filter_section pr-0">
            <div
              id="filterDiv"
              className="filter-section-inner zp search_col angular-ui-tree-handle"
            >
              <div
                className="navbar-static-side search_col_list"
                role="navigation"
              >
                <div className="sidebar-collapse">
                  <ul className="nav left_nav" id="side-menu">
                    <li className="section_header">
                      <span
                        className="filter-icon"
                      >
                        &nbsp;
                      </span>
                    </li>
                    <ol className="angular-ui-tree-nodes">
                      <li className="angular-ui-tree-node open_list ">
                        <div
                          id="filterUITree"
                          className="tree-node tree-node-content angular-ui-tree-handle"
                        >
                        { landingPage ? 
                          <span
                          style={{ display: "block", cursor: "pointer" }}
                        >
                          RECENTLY VIEWED
                          <a id="toggle_content_type_facet">
                              <i className="pull-right tree_arrow fa fa-angle-right"></i>
                          </a>
                        </span>
                        :
                        <span
                          style={{ display: "block", cursor: "pointer" }}
                        >
                          RECENTS
                          <a id="toggle_content_type_facet">
                              <i className="pull-right tree_arrow fa fa-angle-right"></i>
                          </a>
                        </span>
                        }
                        </div>
                      </li>
                    </ol>
                    
                  </ul>
                </div>
                <div
                  id="scroll_msg"
                  className="scroll_msg"
                  style={{ display: "none" }}
                >
                  {" "}
                  <i className="fa fa-chevron-down"></i> <br />
                  Scroll for more facets
                </div>
                </div>
            </div>
          </div> */}
        </div> 
        <SlidingPane
          className="right-info-panel"
          overlayClassName="some-custom-overlay-class"
          isOpen={this.state.isPaneOpen}
          width="85%"
          // title="Hey, it is optional pane title.  I can be React component too."
          // subtitle="Optional subtitle."
          onRequestClose={() => {
            this.setState({ isPaneOpen: false, showFileInfo: false });
            setTimeout(()=>{
              this.setState({ showFileInfo: true });
            },500)
          }}
        >
          {/* <DocumentPreview docUrl={this.state.selectedDocumentUrl} /> */}
          <RightPanel
            edocsBackendAPI={this.state.edocsBackendAPI}
            loaderIcon={this.state.loaderIcon}
            accessToken={this.state.accessToken}
            refreshToken={this.state.refreshToken}
            resultSource={this.state.resultSource}
            docUrl={this.state.selectedDocumentUrl}
            activeRightPanelTab={this.state.activeRightPanelTab}
            item={this.state.item}
            userEmail={this.userEmail}
            dgiUserName={this.state.dgiUserName}
            mainTreeData={this.state.mainTreeList}
            versionTreeData={this.state.versionTreeList}
            source={this.state.sources}
            client={this.client}
            source_type={this.state.selectedDoc.source_type==null?"sharepoint":this.state.selectedDoc.source_type}
            fileid={this.state.selectedDoc.file_id}
            closeRightPanel={() => {
              this.setState({ isPaneOpen: false, showFileInfo: false });
              setTimeout(()=>{
                this.setState({ showFileInfo: true });
              },500)
            }}
            SPUserName={this.state.SPUserName}
            boxUserGroupIds={this.state.boxUserGroupIds}
            boxUserId={this.state.boxUserId}
            file_id={this.state.selectedDoc.file_id}
            selectedDocInfo={this.state.selectedDoc}
            searchQuery={this.state.searchQuery}
            eventAppId={this.state.eventAppId}
            userIP={this.state.userIP}
            sessionId={this.state.sessionId}
            solrCollection={this.state.solrCollection}
          />
        </SlidingPane>
        </div>
      {/* // } */}
      </div>
    );
  }
}

// export default Search;
export default withRouter(Search);
