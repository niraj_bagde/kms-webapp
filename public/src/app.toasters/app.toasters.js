import React from 'react';
import { Modal, Row, Container, Col, Button, Form } from "react-bootstrap";

const AppToaster = (props) => {
    return (

        <Modal
        {...props}
        aria-labelledby="contained-modal-title-vcenter"
        size={props.size}
        centered
      >
        <Modal.Header closeButton className="Settingspopup">
          <Modal.Title id="contained-modal-title-vcenter">{props.title}</Modal.Title>
        </Modal.Header>
        <Modal.Body className="settings">

        <span 
                      dangerouslySetInnerHTML={{
                        __html: `${props.modalContent}`,
                      }}></span>

        </Modal.Body>
        <Modal.Footer>
          <Form.Group as={Row}>
            <Col md={12} right>
              <Button variant="primary" onClick={props.onHide}>
                OK
              </Button>
              <Button variant="secondary" onClick={props.onHide}>
                Cancel
              </Button>
            </Col>
          </Form.Group>
        </Modal.Footer>
      </Modal>
    );
  }
  export default AppToaster;