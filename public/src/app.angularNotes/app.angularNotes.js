import angular from "angular";
import React, { useState, useEffect } from "react";
import "./app.angularNotes.css"

angular
  .module('ngApp', [])//, ['ui.bootstrap.datetimepicker'])
  .controller('ngController', [
    '$scope',
    function($scope) {
      $scope.value = 'World';
    },
  ])
const angularTemplate = `<div ng-controller="ngController" class="angular-note">Hello {{ value }}! 
                            Angular component plugged in!!!</div>`;
class AngularWidget extends React.Component {
  componentDidMount () {
    angular.bootstrap(this.container, ['ngApp']);
  }
  
  html = angularTemplate;

  render = () => (
    <div
      ref={c => this.container = c}
      dangerouslySetInnerHTML={{__html: this.html}}
      />
  );
}

export default AngularWidget;

