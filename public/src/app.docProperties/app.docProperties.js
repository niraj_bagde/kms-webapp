import React, { Component } from 'react';
import "./app.docProperties.css"
import DatePicker from "react-datepicker";
import "../node_modules/react-datepicker/dist/react-datepicker.css";
import Axios from 'axios';

class AppDocProperties extends Component {

    constructor(props) {
      super(props);
      this.state = {
        formData: {
                  'ExpiryDate': {
                      value: null,
                      validationMessage: "",
                      validationState: null
                    },
                    'SignedOn': {
                      value: null,
                      validationMessage: "",
                      validationState: null
                    },
      },
        oldFormData:{},
        disableSave: true,
        disableCancel: true,
        showToaster: false
    }
    }
    
    componentWillMount() {
      // if(this.props.docProperties.Title) {
        let formData = this.prepareFormData();
        this.setState({ formData: formData, oldFormData: JSON.parse(JSON.stringify(formData))});
      // }

    }


    prepareFormData = () => {
      let docp = this.props.docProperties;
      let formData = {
        'DocumentId': {
          value: docp.DocumentId ? docp.DocumentId : "",
          validationMessage: "",
          validationState: null
        },
        'Title': {
          value: docp.Title ? docp.Title : "",
          validationMessage: "",
          validationState: null
        },
        'From': {
          value: docp.From ? docp.From : "",
          validationMessage: "",
          validationState: null
        },
        'Amount': {
          value: docp.Amount ? docp.Amount : "",
          validationMessage: "",
          validationState: null
        },
        'ContentType': {
          value: docp.ContentType ? docp.ContentType :"",
          validationMessage: "",
          validationState: null
        },
        'Date': {
          value: docp.Date ? docp.Date : "",
          validationMessage: "",
          validationState: null
        },
        'ExpiryDate': {
          value: docp.ExpiryDate=='' ? null : new Date(docp.ExpiryDate),
          validationMessage: "",
          validationState: null
        },
        'From': {
          value: docp.From ? docp.From :"",
          validationMessage: "",
          validationState: null
        },
        'Player': {
          value: docp.Player ? docp.Player : "" ,
          validationMessage: "",
          validationState: null
        },
        'SignedOn':{
          value: docp.SignedOn=='' ? null : new Date(docp.SignedOn),
          validationMessage: "",
          validationState: null
        },
        'State': {
          value: docp.State  ? docp.State : "",
          validationMessage: "",
          validationState: null
        },
        'Subject': {
          value: docp.Subject ? docp.Subject : "",
          validationMessage: "",
          validationState: null
        },
        'Tags': {
          value: docp.Tags ? docp.Tags : '',
          validationMessage: "",
          validationState: null
        },
        'Team': {
          value: docp.Team ? docp.Team: "",
          validationMessage: "",
          validationState: null
        },
        'To': {
          value: docp.To ? docp.To : "",
          validationMessage: "",
          validationState: null
        },
        'Name': {
          value: docp.Name ? docp.Name : "",
          validationMessage: "",
          validationState: null
        },
        'Anatomies': {
          value: docp.Anatomies ? docp.Anatomies : "",
          validationMessage: "",
          validationState: null
        },
        'HealthConditions': {
          value: docp.HealthConditions ? docp.HealthConditions : "",
          validationMessage: "",
          validationState: null
        },
        'Organizations': {
          value: docp.Organizations ? docp.Organizations : "",
          validationMessage: "",
          validationState: null
        },
        'Drugs': {
          value: docp.Drugs ? docp.Drugs : "",
          validationMessage: "",
          validationState: null
        }
      };
  
      return formData;
    };

    handleInputChange = (value, field) => {
      if(this.props.docProperties.Title) {
        let formData = this.state.formData;
        formData[field].value = value
  
        this.setState({ disableSave: false, disableCancel: false, formData: formData});
      }

    }

    saveFormChanges = () => {
      let formData = this.state.formData;
      let saveObj = this.createSaveObject();
      if(this.props.source.indexOf('Sharepoint') >-1) {
        saveObj.ConnectorSourceName = 'SharePoint';
      }
      saveObj["ClientName"]= this.props.client;
      Axios.post("/docTree/saveDocProperties", saveObj).then(response => {
        this.setState({ oldFormData: JSON.parse(JSON.stringify(formData)), disableSave: true, disableCancel: true, showToaster: true});
      })
    }


    cancelFormChanges = () => {
      let oldFormData = this.state.oldFormData;
      if(oldFormData.SignedOn.value !== null) {
        oldFormData.SignedOn.value = new Date(oldFormData.SignedOn.value);
      }
      if(oldFormData.ExpiryDate.value !== null) {
        oldFormData.ExpiryDate.value = new Date(oldFormData.ExpiryDate.value);
      }
     this.setState({ formData: JSON.parse(JSON.stringify(oldFormData)), disableSave: true, disableCancel: true});
    }


    createSaveObject = () => {
      let formData = this.state.formData;
      let saveObj = {
        DocumentId: formData.DocumentId.value,
        Amount: formData.Amount.value,
        ContentType: formData.ContentType.value,
        Date: formData.Date.value,
        ExpiryDate: formData.ExpiryDate.value==null ? "" : formData.ExpiryDate.value.toLocaleString().replace(',', ''),
        From: formData.From.value,
        Name: formData.Name.value,
        Player: formData.Player.value,
        SignedOn: formData.SignedOn.value==null ? "" : formData.SignedOn.value.toLocaleString().replace(',', ''),
        State: formData.State.value,
        Subject: formData.Subject.value,
        Tags: formData.Tags.value,
        Team: formData.Team.value,
        Title: formData.Title.value,
        To: formData.To.value,
        Anatomies: formData.Anatomies.value,
        HealthConditions: formData.HealthConditions.value,
        Organizations: formData.Organizations.value,
        Drugs: formData.Drugs.value,
      };
      return saveObj;
    }


    render() {

    return  (
      <div>
        {/* {this.state.showToaster && <AppToaster />} */}
      <div style={{'marginBottom': '10px', marginLeft:'15px'}}><h4 className="version-heading">Document Properties</h4></div>
        {/* {this.props.docProperties.Title && */}
        <div className="property-form-wrapper">
              <div className="col-md-12">
                <div className="col-md-2"><label>Content Type: </label></div>
                <div className="col-md-10">
                  <select class="form-control" value={this.state.formData.ContentType.value}
                          onChange={e =>
                            this.handleInputChange(e.target.value, 'ContentType')
                          }
                    >
                    <option value="Document">Document</option>
                    <option value="Email Messages">Email Messages</option>
                  </select>
                </div>
              </div>
              <div className="col-md-12">
                <div className="col-md-2"><label>Title: </label></div>
                <div className="col-md-10">
                  <input type="text" class="form-control" value={this.state.formData.Title.value} 
                          onChange={e =>
                            this.handleInputChange(e.target.value, 'Title')
                          }/>
                </div>
              </div>
              <div className="col-md-12">
                <div className="col-md-2"><label>Name: </label></div>
                <div className="col-md-10">
                  <input type="text" class="form-control" value={this.state.formData.Name.value} disabled/>
                </div>
              </div>

              <div className="col-md-12">
                <div className="col-md-2"><label>Tags: </label></div>
                <div className="col-md-10">
                  <input class="form-control" value={this.state.formData.Tags.value}
                            onChange={e => this.handleInputChange(e.target.value, 'Tags')}
                  />
                </div>
              </div>
              { this.props.client=='nba' &&
              <div>
              <div className="col-md-12">
                <div className="col-md-2"><label>Signed Date: </label></div>
                <div className="col-md-10">
                <div className="signeddate-box3"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                  <div className="signeddate-box1">
                  <DatePicker
                      selected={this.state.formData.SignedOn.value}
                      showTimeSelect
                      dateFormat="MM/dd/yyyy h:mm aa"
                      className="date-control"
                      placeholderText="Click to select a date"
                      onChange={e =>
                        this.handleInputChange(e, "SignedOn")
                      }
                  />
                  </div>
                </div>
              </div>
              <div className="col-md-12">
                <div className="col-md-2"><label>Expiry Date: </label></div>
                <div className="col-md-10">
                  <div className="signeddate-box3"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                  <div className="signeddate-box1">
                    <DatePicker
                      selected={this.state.formData.ExpiryDate.value}
                      showTimeSelect
                      dateFormat="MM/dd/yyyy h:mm aa"
                      className="date-control"
                      placeholderText="Click to select a date"
                      onChange={e =>
                        this.handleInputChange(e, "ExpiryDate")
                      }
                  />
                  </div>
                </div>
              </div>
              <div className="col-md-12">
                <div className="col-md-2"><label>Category: </label></div>
                <div className="col-md-10">
                  <input type="text" class="form-control" value={this.state.formData.State.value}
                          onChange={e => this.handleInputChange(e.target.value, 'State')}
                  />
                </div>
              </div>
              <div className="col-md-12">
                <div className="col-md-2"><label>Amount: </label></div>
                <div className="col-md-10">
                  <input type="number" class="form-control" value={this.state.formData.Amount.value}
                          onChange={e => this.handleInputChange(e.target.value, 'Amount')}
                  />
                </div>
              </div>
              <div className="col-md-12">
                <div className="col-md-2"><label>Team: </label></div>
                <div className="col-md-10">
                  <input type="text" class="form-control" value={this.state.formData.Team.value}
                          onChange={e => this.handleInputChange(e.target.value, 'Team')}
                  />
                </div>
              </div>
              <div className="col-md-12">
                <div className="col-md-2"><label>Player: </label></div>
                <div className="col-md-10">
                  <input type="text" class="form-control" value={this.state.formData.Player.value}
                        onChange={e => this.handleInputChange(e.target.value, 'Player')}
                  />
                </div>
              </div>
              </div>
              }
              { this.props.client=='alnylam' &&
              <div>
                        <div className="col-md-12">
                        <div className="col-md-2"><label>Anatomies: </label></div>
                        <div className="col-md-10">
                          <input type="text" class="form-control" value={this.state.formData.Anatomies.value}
                                onChange={e => this.handleInputChange(e.target.value, 'Anatomies')}
                          />
                        </div>
                      </div>
                      <div className="col-md-12">
                        <div className="col-md-2"><label>HealthConditions: </label></div>
                        <div className="col-md-10">
                          <input type="text" class="form-control" value={this.state.formData.HealthConditions.value}
                                onChange={e => this.handleInputChange(e.target.value, 'HealthConditions')}
                          />
                        </div>
                      </div>
                      <div className="col-md-12">
                        <div className="col-md-2"><label>Organizations: </label></div>
                        <div className="col-md-10">
                          <input type="text" class="form-control" value={this.state.formData.Organizations.value}
                                onChange={e => this.handleInputChange(e.target.value, 'Organizations')}
                          />
                        </div>
                      </div>
                      <div className="col-md-12">
                        <div className="col-md-2"><label>Drugs: </label></div>
                        <div className="col-md-10">
                          <input type="text" class="form-control" value={this.state.formData.Drugs.value}
                                onChange={e => this.handleInputChange(e.target.value, 'Drugs')}
                          />
                        </div>
                      </div>
                      </div>
              }
              <div className="col-md-12 form-btn-wrapper text-right">
                  <button type="submit" class="btn btn-primary" disabled={this.state.disableSave}
                          onClick={()=>{this.saveFormChanges(); }}>Save</button>
                  <button type="submit" class="btn btn-primary" disabled={this.state.disableCancel}
                          onClick={()=>this.cancelFormChanges()}>Cancel</button>
              </div>
        </div>

        </div>
    )
  }
};


export default AppDocProperties;