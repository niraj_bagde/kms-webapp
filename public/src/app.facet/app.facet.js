import React, { Component } from "react";
import Tree, { TreeNode } from 'rc-tree';
var utilsJs = require('../helpers/utils');
var utils = new utilsJs();

const STYLE = `
.rc-tree-child-tree {
  display: block;
}

.node-motion {
  transition: all .3s;
  overflow-y: hidden;
}
`;


const motion = {
  motionName: 'node-motion',
  motionAppear: false,
  onAppearStart: () => ({ height: 0 }),
  onAppearActive: node => ({ height: node.scrollHeight }),
  onLeaveStart: node => ({ height: node.offsetHeight }),
  onLeaveActive: () => ({ height: 0 }),
};

class AppFacet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      facets: [],
      rawIntervalFacets: this.props.intervalFacets,
      selectedFilters: [],
      expanded: this.props.expanded,
      selected: JSON.parse(JSON.stringify(this.props.selected)),
      aggregations: [],
      prevfacets: [],
      selectedTags: [],
      facetTree: [],
      checkedKeys:[],
      expandedKeys:[],
      newFacetSelected: {}
    };
  }

  componentWillMount() {
    var rawfacets = this.props.facets;
    var facets;
    var facetTree = []
    if(this.props.resultSource == 'searchService') {
      facets = this.formatFacets(rawfacets);
    }
    if(this.props.resultSource == 'solr') {
      facets = this.formatFacetsForsolr(rawfacets);
    }
    if(this.props.ExplorerFacets && this.props.ExplorerFacets==true) {
      facetTree = this.getTreeData(facets);
    }
    this.setState({
      facets: facets,
      facetTree: facetTree,
      prevfacets: this.props.facets,
      selectedFilters: JSON.parse(JSON.stringify(this.props.selected)),
    });
  }

  formatFacets = (rawfacets) => {
    var facetStructure = [];
    this.getFilteredFacetData(rawfacets, this.props.selected, facetStructure);
    let intervalfacets = this.formatIntervalFacets(this.props.intervalFacets, this.props.selected)
    this.getFilteredData(facetStructure);
    this.orderFiltersByDisplayName(facetStructure);
    facetStructure = facetStructure.filter(function (filter) {
      return filter.nodes && filter.nodes.length > 0;
    });
    facetStructure.push(intervalfacets);
    return facetStructure;
  };

  formatFacetsForsolr = (rawfacets) => {
    var facetStructure = [];
    var facetRawData = rawfacets.facet_fields;
    this.getFilteredFacetDataForSolr(facetRawData, this.props.selected, facetStructure);
    let intervalfacets = this.formatIntervalFacetsForSolr(this.props.intervalFacets, this.props.selected)
    // let filterFacets = this.getFilteredData(facetStructure);
    this.getFilteredData(facetStructure);
    this.orderFiltersByDisplayName(facetStructure);
    // filterFacets = filterFacets.filter(function (filter) {
    //   return filter.nodes && filter.nodes.length > 0;
    // });
    facetStructure.push(intervalfacets);
    return facetStructure;
  };

  formatIntervalFacets = (intFacets, selected) => {
    let filterRow = {};
    filterRow.displayName = "Recently Modified";
    filterRow.count = "";
    filterRow.isTopFilter = true;
    filterRow.name = "modified";
    filterRow.solrName = "modified";
    filterRow.nodes = [];
    filterRow.expanded = false;
    if(intFacets.length > 0) {
      intFacets[0].intervals.forEach((key, keyIndex) => {
        let displayName = "";
        let name="";
        switch(key.key) {
          case 'older':
            displayName = "Older than a month";
            name="[1900-01-01T00:00:00Z TO NOW]";
            break;
          case 'month':
            displayName = "Less than a month";
            name="[NOW-30DAY TO NOW]";
            break;
          case 'week':
            displayName = "Less than a week";
            name="[NOW-7DAY TO NOW]";
            break;
          case 'day':
            displayName = "Yesterday";
            name="[NOW-1DAY TO NOW]";
            break;
          default:
        }
  
        let isSelected = false;
        for(let k=0; k< selected.length; k++) {
          if(selected[k].displayName == displayName && selected[k].parent.name == 'modified') {
            isSelected = true;
            filterRow.expanded = true;
          }
        }
  
  
        filterRow.nodes.push({
          count: key.count,
          displayName: displayName,
          isSelected: isSelected,
          name: name
        })
      });
    }
     return filterRow;
  };

  formatIntervalFacetsForSolr = (intFacets, selected) => {
    let filterRow = {};
    filterRow.displayName = "Recently Modified";
    filterRow.count = "";
    filterRow.isTopFilter = true;
    filterRow.name = "modified";
    filterRow.solrName = "modified";
    filterRow.nodes = [];
    filterRow.expanded = false;
    if(intFacets.modified) {
      Object.keys(intFacets.modified).forEach((key, keyIndex) => {
        let displayName = "";
        let name="";
        switch(key) {
          case 'older':
            displayName = "Older than a month";
            name="[1900-01-01T00:00:00Z TO NOW]";
            break;
          case 'month':
            displayName = "Less than a month";
            name="[NOW-30DAY TO NOW]";
            break;
          case 'week':
            displayName = "Less than a week";
            name="[NOW-7DAY TO NOW]";
            break;
          case 'day':
            displayName = "Yesterday";
            name="[NOW-1DAY TO NOW]";
            break;
          default:
        }
  
        let isSelected = false;
        for(let k=0; k< selected.length; k++) {
          if(selected[k].displayName == displayName && selected[k].parent.name == 'modified') {
            isSelected = true;
            filterRow.expanded = true;
          }
        }

        filterRow.nodes.push({
          count: intFacets.modified[key],
          displayName: displayName,
          isSelected: isSelected,
          name: name
        })
      });
    }
     return filterRow;
  };


  orderFiltersByDisplayName = (allFacetsData, isRemove) => {
    allFacetsData.forEach((key, keyIndex) => {
      if (key.nodes.length > 1) {
        this.arrangeNodes(key, keyIndex, isRemove);
      }
    });
    return allFacetsData;
  };

  arrangeNodes = (item, index, isRemove) => {
    if (!isRemove) {
      item.nodes = item.nodes.filter(function (node) {
        return node.count != 0 || node.nodes.length>0
      });
    }
    item.nodes.forEach((key, keyIndex) => {
      if (key.nodes.length > 1) {
        this.arrangeNodes(key, keyIndex);
      }
    });
  };

  getFilteredData = (allFacetsData) => {
    for (var key in allFacetsData) {
      if(!allFacetsData[key].displayName) {
        if (allFacetsData[key].name == "content_type_facet") {
          allFacetsData[key].displayName = "Content Type";
          allFacetsData[key].isTopFilter = true;
          allFacetsData[key].solrName = "content_type_facet";
        } else if (allFacetsData[key].name == "source") {
          allFacetsData[key].displayName = "Source";
          allFacetsData[key].isTopFilter = true;
          allFacetsData[key].solrName = "source";
        } else if (allFacetsData[key].name == "topics_taxonomy_facet") {
          allFacetsData[key].displayName = "Topics";
          allFacetsData[key].isTopFilter = true;
          allFacetsData[key].solrName = "topics_taxonomy_facet";
        } else if (allFacetsData[key].name == "hbs_source") {
          allFacetsData[key].displayName = "HBS Source";
          allFacetsData[key].isTopFilter = true;
          allFacetsData[key].solrName = "hbs_source";
        } else if (allFacetsData[key].name == "industry_taxonomy_facet") {
          allFacetsData[key].displayName = "Industry Taxonomy Facet";
          allFacetsData[key].isTopFilter = true;
          allFacetsData[key].solrName = "industry_taxonomy_facet";
        } else if (allFacetsData[key].name == "geoarea_taxonomy_facet") {
          allFacetsData[key].displayName = "Geographic Area";
          allFacetsData[key].isTopFilter = true;
          allFacetsData[key].solrName = "geoarea_taxonomy_facet";
        } else if (allFacetsData[key].name == "industry_facet") {
          allFacetsData[key].displayName = "Industry";
          allFacetsData[key].isTopFilter = true;
          allFacetsData[key].solrName = "industry_facet";
        } else if (allFacetsData[key].name == "published_year_facet") {
          allFacetsData[key].displayName = "Date";
          allFacetsData[key].isTopFilter = true;
          allFacetsData[key].solrName = "published_year_facet";
        } else if (allFacetsData[key].name == "item_type_facet") {
          allFacetsData[key].displayName = "Item Type";
          allFacetsData[key].isTopFilter = true;
          allFacetsData[key].solrName = "item_type_facet";
        } else if (allFacetsData[key].name == "creator_facet") {
          allFacetsData[key].displayName = "Author";
          allFacetsData[key].isTopFilter = true;
          allFacetsData[key].solrName = "creator_facet";
        } else if (allFacetsData[key].name == "contributor_facet") {
          allFacetsData[key].displayName = "Contributor";
          allFacetsData[key].isTopFilter = true;
          allFacetsData[key].solrName = "contributor_facet";
        } else if (allFacetsData[key].name == "players_facet") {
          allFacetsData[key].displayName = "Players";
          allFacetsData[key].isTopFilter = true;
          allFacetsData[key].solrName = "players_facet";
        } else if (allFacetsData[key].name == "teams_facet") {
          allFacetsData[key].displayName = "Teams";
          allFacetsData[key].isTopFilter = true;
          allFacetsData[key].solrName = "teams_facet";
        } else if (allFacetsData[key].name == "games_facet") {
          allFacetsData[key].displayName = "Games";
          allFacetsData[key].isTopFilter = true;
          allFacetsData[key].solrName = "games_facet";
        } else if (allFacetsData[key].name == "officials_facet") {
          allFacetsData[key].displayName = "Officials";
          allFacetsData[key].isTopFilter = true;
          allFacetsData[key].solrName = "officials_facet";
        } else if (allFacetsData[key].name == "playerposition_facet") {
          allFacetsData[key].displayName = "Player Position";
          allFacetsData[key].isTopFilter = true;
          allFacetsData[key].solrName = "playerposition_facet";
        } else if (allFacetsData[key].name == "callornocall_facet") {
          allFacetsData[key].displayName = "Call or No Call";
          allFacetsData[key].isTopFilter = true;
          allFacetsData[key].solrName = "callornocall_facet";
        } else if (allFacetsData[key].name == "calltypegroupname_facet") {
          allFacetsData[key].displayName = "Call Type Group";
          allFacetsData[key].isTopFilter = true;
          allFacetsData[key].solrName = "calltypegroupname_facet";
        } else if (allFacetsData[key].name == "eventtypename_facet") {
          allFacetsData[key].displayName = "Event Type";
          allFacetsData[key].isTopFilter = true;
          allFacetsData[key].solrName = "eventtypename_facet";
        } else if (allFacetsData[key].name == "infractionratingname_facet") {
          allFacetsData[key].displayName = "Infraction Rating";
          allFacetsData[key].isTopFilter = true;
          allFacetsData[key].solrName = "infractionratingname_facet";
        } else if (allFacetsData[key].name == "infractiontypename_facet") {
          allFacetsData[key].displayName = "Infraction Type";
          allFacetsData[key].isTopFilter = true;
          allFacetsData[key].solrName = "infractiontypename_facet";
        } else if (allFacetsData[key].name == "playeraction_facet") {
          allFacetsData[key].displayName = "Player Action";
          allFacetsData[key].isTopFilter = true;
          allFacetsData[key].solrName = "playeraction_facet";
        } else if (allFacetsData[key].name == "playtypename_facet") {
          allFacetsData[key].displayName = "Player Type";
          allFacetsData[key].isTopFilter = true;
          allFacetsData[key].solrName = "playtypename_facet";
        } else if (allFacetsData[key].name == "subplaytypename_facet") {
          allFacetsData[key].displayName = "Sub Play Type";
          allFacetsData[key].isTopFilter = true;
          allFacetsData[key].solrName = "subplaytypename_facet";
        } else if (allFacetsData[key].name == "periodname_facet") {
          allFacetsData[key].displayName = "Period Name";
          allFacetsData[key].isTopFilter = true;
          allFacetsData[key].solrName = "periodname_facet";
        } else if (allFacetsData[key].name == "keywords_facet") {
          allFacetsData[key].displayName = "Tags";
          allFacetsData[key].isTopFilter = true;
          allFacetsData[key].solrName = "keywords_facet";
        } else if (allFacetsData[key].name == "file_type") {
          allFacetsData[key].displayName = "Doc Type";
          allFacetsData[key].isTopFilter = true;
          allFacetsData[key].solrName = "file_type";
        } else if (allFacetsData[key].name == "dataset_key_facet") {
          allFacetsData[key].displayName = "Datasets";
          allFacetsData[key].isTopFilter = true;
          allFacetsData[key].solrName = "dataset_key_facet";
        } else {
          allFacetsData[key].displayName = allFacetsData[key].name.replace(
            /_facet/g,
            ""
          );
          allFacetsData[key].displayName = allFacetsData[key].displayName.replace(
            /tdg_ner/g,
            ""
          );
          allFacetsData[key].displayName = allFacetsData[key].displayName.replace(
            /_/g,
            " "
          );
          allFacetsData[key].isTopFilter = true;
          allFacetsData[key].solrName = allFacetsData[key].name;
        }
      }

      //support for n level
      if (allFacetsData[key].nodes.length > 1) {
        this.getFilteredData(allFacetsData[key].nodes);
      }
    }

    // return allFacetsData;
  };

  getFilteredFacetData = (facetRawData, selected, finalFacetData) => {
    // let prevfacets = selected;
    // var facetRawData = result;
    // var finalFacetData = [];
    for (var key in facetRawData) {
      var jsonObj = {};

      jsonObj.name = key;
      jsonObj.count = "";
      jsonObj.nodes = [];

      var childNodeData = facetRawData[key];
      var childJsonObj = this.createJsonObjectArr(childNodeData);

      var allKeys = [];
      for (var i = 0; i < childJsonObj.length; i++) {
        var nodeKey = childJsonObj[i].name;
        if (nodeKey.toLowerCase() !== "n/a") {
          if (allKeys.indexOf(nodeKey) == -1) {
            var nodeValue = childJsonObj[i].count;
            allKeys.push(nodeKey);
            var displayName = nodeKey;
            if (
              jsonObj.name == "dataset_key_facet"
               ||
              jsonObj.name == "item_type_facet"
            ) {
              continue;
            } else {
              let dName = displayName.split(" ");
              for (var j = 0; j < dName.length; j++) {
                if (dName[j])
                  dName[j] =
                    dName[j][0].toUpperCase() + dName[j].slice(1).toLowerCase();
              }
              displayName = dName.join(" ");
            }
            var json = {
              name: nodeKey,
              count: nodeValue,
              nodes: [],
              isSelected: false,
              displayName: displayName,
              nodeLength: childJsonObj.length,
            };
            for(let k=0; k< selected.length; k++) {
              if(selected[k].name == nodeKey && selected[k].parent.name == jsonObj.name) {
                json.isSelected = true;
                jsonObj.expanded = true;
              }
            }
            jsonObj.nodes.push(json);
          }
        }
      }
      if (jsonObj.nodes && jsonObj.nodes.length > 0) {
        if(jsonObj.name=="file_type") {
          finalFacetData.unshift(jsonObj);
        } else {
          finalFacetData.push(jsonObj);
        }
      }
    }

    // return finalFacetData;
  }

  getFilteredFacetDataForSolr = (facetRawData, selected, finalFacetData) => {
      // var finalFacetData = [];

      for(var key in facetRawData){
          var jsonObj = {};
          jsonObj.name = key;
          jsonObj.count = "";
          jsonObj.nodes = [];

          var childNodeData = facetRawData[key];
          var childJsonObj = this.createJsonObjectArrForSolr(childNodeData);
          var allKeys = [];
          var isParent = false;
          for(var i = 0 ; i < childJsonObj.length; i++){
              var nodeKey = childJsonObj[i].name;
              // childNodeKeys = [];
              if (nodeKey.toLowerCase() !== "n/a") {
                if(allKeys.indexOf(nodeKey) == -1){
                    var nodeValue = childJsonObj[i].count;
                    // var nodes = iterateChildNodeObj(childJsonObj,childJsonObj[i].name);
                    allKeys.push(nodeKey);
                    if (
                      // jsonObj.name == "dataset_key_facet"
                      //  ||
                      jsonObj.name == "item_type_facet"
                    ) {
                      continue;
                    } else {
                    var displayName = nodeKey;
                    // displayName = displayName.replace(/[^a-zA-Z0-9 ]/g, "");

                    //if(nodeValue != 0){
                        var json = {
                            "name":nodeKey,
                            "count":nodeValue,
                            "nodes":[],
                            "isSelected":false,
                            "displayName":displayName,
                            "nodeLength": childJsonObj.length
                          };
                          for(let k=0; k< selected.length; k++) {
                            if(selected[k].name == nodeKey && selected[k].parent.name == jsonObj.name) {
                              json.isSelected = true;
                              jsonObj.expanded = true;
                            }
                          }
                        jsonObj.nodes.push(json);
                    }
                    //}
                } 
            }

//             if(jsonObj.nodes && jsonObj.nodes.length>0) {
//               this.getFilteredFacetDataForSolr(jsonObj.nodes, selected, jsonObj.nodes)
//             }
          }
          // if(jsonObj.nodes && jsonObj.nodes.length > 0){
          //    finalFacetData.push(jsonObj);
          // }

          if (jsonObj.nodes && jsonObj.nodes.length > 0) {
            if(jsonObj.name=="file_type") {
              finalFacetData.unshift(jsonObj);
            } else {
               var entryObject = this.checkIfCombinationNeeded(key);
              let specialFilter = false;
              let filterRow= {}
              switch(entryObject.fieldName) {
                case 'anatomies':
                case 'drugs':
                case 'health_conditions':
                case 'organizations':
                    specialFilter = true;
                    filterRow.name = entryObject.fieldName;
                    filterRow.fieldName = entryObject.fieldName;
                    filterRow.nodes = [];
                    filterRow.isTopFilter = true;
                    filterRow.isSelected = false;
                    let isSpecialExpanded = false;
                    for(let k=0; k< selected.length; k++) {
                      if(selected[k].parent.name == entryObject.name) {
                        isSpecialExpanded = true;
                      }
                    }
                    if(isSpecialExpanded==true) {
                      filterRow.expanded = true;
                    }
                    filterRow.nodes.push({  
                                          "name": entryObject.name, 
                                          "parent": entryObject.name,
                                          "topFilter": entryObject.fieldName,
                                          "count": "",
                                          "fieldName": entryObject.fieldName,
                                          "specialFilter": true,
                                          "expanded": isSpecialExpanded,
                                          "nodes":jsonObj.nodes,
                                          "displayName":entryObject.type,
                                          "nodeLength": jsonObj.nodes.length,
                                          "isTopFilter": true,
                                          "isSelected": false
                                        });
                  break;
              }
      
              var fIndex = finalFacetData.map(function (d) { return d['fieldName']; }).indexOf(entryObject.fieldName);
              if(specialFilter==true) {
                if(fIndex == -1) {
                  finalFacetData.push(filterRow);
                } else {
                  finalFacetData[fIndex].nodes.push(filterRow.nodes[0])
                }
              } else {
                finalFacetData.push(jsonObj);
              }
            }
          }
      }

      var dIndex = finalFacetData.map(function (d) { return d['name']; }).indexOf("dataset_key_facet");
      if(dIndex > -1) {
        let datasetFacet = finalFacetData[dIndex];
        finalFacetData.splice(dIndex, 1)
        finalFacetData.unshift(datasetFacet);
      }
  };


  checkIfCombinationNeeded = (key) => {
    let entryObject = {};
    entryObject.name = key;
    if(key.indexOf('anatom')> -1) {
      entryObject.fieldName= 'anatomies';
      if(key.indexOf('ner')> -1) {
        entryObject.type= 'auto detected';
      } else {
        entryObject.type= 'user added';
      }
      return entryObject;
    }
    if(key.indexOf('drug')> -1) {
      entryObject.fieldName= 'drugs';
      if(key.indexOf('ner')> -1) {
        entryObject.type= 'auto detected';
      } else {
        entryObject.type= 'user added';
      }
      return entryObject;
    }
    if(key.indexOf('health')> -1) {
      entryObject.fieldName= 'health_conditions';
      if(key.indexOf('ner')> -1) {
        entryObject.type= 'auto detected';
      } else {
        entryObject.type= 'user added';
      }
      return entryObject;
    }
    if(key.indexOf('organiz')> -1) {
      entryObject.fieldName= 'organizations';
      if(key.indexOf('ner')> -1) {
        entryObject.type= 'auto detected';
      } else {
        entryObject.type= 'user added';
      }
      return entryObject;
    }
    return entryObject;
 }  
 
 createJsonObjectArrForSolr = (strArr) => {
      var arr = [];
      for(var i = 0 ;i < strArr.length; i+=2){
        var nodeKey = strArr[i];
        var nodeValue = strArr[i+1];
        var obj = {"name":nodeKey,"count":nodeValue};
        arr.push(obj);
    }
    return arr;
  }


  createJsonObjectArr = (strArr) => {
    var arr = [];
    for (var key in strArr) {
      var obj = { name: key, count: strArr[key] };
      arr.push(obj);
    }
    return arr;
  };

  showHideKeys = (item) => {
    var facets = this.state.facets;
    var ind = facets
      .map(function (d) {
        return d["name"];
      })
      .indexOf(item.name);
    if (ind > -1) {
      facets[ind].expanded = !facets[ind].expanded;
    }
    this.setState({
      facets: facets,
    });
  };

  showHideSpecialKeys = (item) => {
    var facets = this.state.facets;
    var pInd = facets.map(function (d) { return d["name"];}).indexOf(item.topFilter);
    if (pInd > -1) {
      var parentRow = facets[pInd];
      var cInd = parentRow.nodes.map(function (d) { return d["name"];}).indexOf(item.name);
      if(cInd>-1) {
        parentRow.nodes[cInd].expanded = !parentRow.nodes[cInd].expanded
      }
    }
    this.setState({
      facets: facets,
    });
  };

  // handleTopSelection = (target, itemList) => {
  //   var newFacetSelected = {};
  //   var isSelected = !item.isSelected;

  //   var selFilters = this.state.selectedFilters;
  //   if (isSelected == true) {
  //     var filterItem = item;
  //     filterItem.parent = target;
  //     filterItem.isSelected = !filterItem.isSelected;
  //     let notFound = true;
  //     let index;
  //     for (let i of selFilters) {
  //       if (i.name.toLowerCase() == filterItem.name.toLowerCase() && i.facetName==filterItem.parent.name) {
  //         notFound = false;
  //         index = i;
  //       }
  //     }
  //     if (notFound) {
  //       selFilters.push(filterItem);
  //     } else {
  //       selFilters = selFilters.filter((item) => {
  //         return item.name.toLowerCase() != index.name.toLowerCase();
  //       });
  //     }
  //     // if ( selFilters.length > 0 ) {
  //     //   newFacetSelected.facet_DisplayName = target.displayName;
  //     //   newFacetSelected.facetFieldName = item.name;
  //     // }
  //   } else {
  //     var rIndex = selFilters
  //       .map(function (d) {
  //         return d["name"];
  //       })
  //       .indexOf(item.name);
  //     if (rIndex > -1) {
  //       selFilters.splice(rIndex, 1);
  //     }
  //   }

  //   this.setState(
  //     {
  //       selectedFilters: selFilters,
  //     },
  //     () => {
  //       this.props.onFilterSelection(selFilters, newFacetSelected)
  //      }
  //   );

  // }

  handleInputChange = (target, item) => {
    var newFacetSelected = {};
    var isSelected = !item.isSelected;

    var selFilters = this.state.selectedFilters;
    if (isSelected == true) {
      var filterItem = item;
      filterItem.parent = target;
      filterItem.isSelected = !filterItem.isSelected;
      let notFound = true;
      let index;
      for (let i of selFilters) {
        if (i.name.toLowerCase() == filterItem.name.toLowerCase() && i.facetName==filterItem.parent.name) {
          notFound = false;
          index = i;
        }
      }
      if (notFound) {
        selFilters.push(filterItem);
      } else {
        selFilters = selFilters.filter((item) => {
          return item.name.toLowerCase() != index.name.toLowerCase();
        });
      }
      if ( selFilters.length > 0 ) {
        newFacetSelected.facet_DisplayName = target.displayName;
        newFacetSelected.facetFieldName = item.name;
      }
    } else {
      var rIndex = selFilters
        .map(function (d) {
          return d["name"];
        })
        .indexOf(item.name);
      if (rIndex > -1) {
        selFilters.splice(rIndex, 1);
      }
    }

    this.setState(
      {
        selectedFilters1: selFilters, newFacetSelected1: newFacetSelected
      }
      // ,
      // () => {
      //   this.props.onFilterSelection(selFilters, newFacetSelected)
      //  }
    );
  };

  applyFilter = () => {
    this.props.onFilterSelection(this.state.selectedFilters,  this.state.newFacetSelected)
  }

  renderFacets = () => {
    if(this.props.ExplorerFacets && this.props.ExplorerFacets==true) {
      return this.renderExplorerFacets();
    } else {
      return this.renderLegacyFacets();
    }
  }


  renderLegacyFacets = () => {
    return this.state.facets.map((listitem, index) => (
      <ol key={"ol_" + index} className="angular-ui-tree-nodes">
        <li className="angular-ui-tree-node open_list ">
          <div
            id="filterUITree"
            className="tree-node tree-node-content angular-ui-tree-handle"
          >
            <span
              style={{ display: "block", cursor: "pointer", 
              textTransform:"uppercase" ,
              "fontSize": "12px"
            }}
              onClick={(e) => this.showHideKeys(listitem)}
            >
              {listitem.displayName}
              <a id="toggle_content_type_facet">
                {!listitem.expanded && (
                  <i className="pull-right tree_arrow fa fa-angle-right"></i>
                )}
                {listitem.expanded && (
                  <i className="pull-right tree_arrow fa fa-angle-down"></i>
                )}
              </a>
            </span>
          </div>
          {listitem.expanded && (
            <ol
              className="test angular-ui-tree-nodes"
              style={{ marginTop: "0" }}
            >
              {listitem.nodes.map((item, ind) => (
                <li
                  className="angular-ui-tree-node"
                  key={"li_" + ind}
                  style={{
                    display: "block",
                    borderBottom: "1px solid #6e6c96",
                  }}
                >
                  <div className="tree-node tree-node-content angular-ui-tree-handle">

                    {!item.specialFilter 
                        &&
                        <span
                          style={{
                            display: "block",
                            float: "left",
                            marginRight: "3px",
                          }}
                          key={item.name}
                        >
                      <input
                        type="checkbox"
                        value={item.name}
                        key={item.name}
                        checked={item.isSelected}
                        onChange={(e) => {
                          this.handleInputChange(listitem, item);
                        }}
                      />
                      </span>
                    }
 
                    <span
                      style={{ fontSize: "12px",  textTransform:"uppercase"}}
                    >

                      
                      {item.specialFilter 
                        ?
                        <span
                        style={{ display: "block", cursor: "pointer", 
                        textTransform:"uppercase" ,
                        "fontSize": "12px", "marginLeft": "7px"
                      }}
                        onClick={(e) => this.showHideSpecialKeys(item)}
                      >
                      {item.displayName}
                        <a id="toggle_content_type_facet">
                        {!item.expanded && (
                          <i className="pull-right tree_arrow fa fa-angle-right"></i>
                        )}
                        {item.expanded && (
                          <i className="pull-right tree_arrow fa fa-angle-down"></i>
                        )}
                        </a>
                        </span>
                        :
                        <span>
                        {item.displayName}
                        <span
                          className="facet_count"
                          style={{ marginLeft: "7px" }}
                        >
                          ({item.count})
                        </span>
                      </span>
                      }
                    </span>
                 </div>
                  {item.expanded && (
                    <ol
                      className="test angular-ui-tree-nodes"
                      style={{ marginTop: "0" }}
                    >
                      {item.nodes.map((child, ind) => (
                        <li
                          className="angular-ui-tree-node"
                          key={"li__child_" + ind}
                          style={{
                            display: "block",
                            borderBottom: "1px solid #6e6c96",
                          }}
                        >
                          <div className="tree-node tree-node-content angular-ui-tree-handle">
                            <span
                              style={{
                                display: "block",
                                float: "left",
                                marginRight: "3px",
                              }}
                              key={child.name}
                            >
                              <input
                                type="checkbox"
                                value={child.name}
                                key={child.name}
                                checked={child.isSelected}
                                onChange={(e) => {
                                  this.handleInputChange(item, child);
                                }}
                              />
                            </span>
                            <span
                              style={{ fontSize: "12px",  textTransform:"uppercase"}}
                            >
                            
                              {child.displayName}
                              <span
                                className="facet_count"
                                style={{ marginLeft: "7px" }}
                              >
                                ({child.count})
                              </span>
                            </span>
                          </div>
                        </li>
                      ))}
                    </ol>
                  )}
                </li>
              ))}
            </ol>
          )}
        </li>
      </ol>
    ));
  }

  renderExplorerFacets = () => {
    const loop = data => {
      return data.map((item) => {
        if (item.children && item.children.length) {
          return <TreeNode key={item.key} 
                          checkable={false}
                           title={item.title} 
                           displayName={item.displayName}
                           count={item.count}
                           name={item.name}
                           nodes={item.nodes}
                           fieldName={item.fieldName}
                           isSelected={item.isSelected}
                           isTopFilter={item.isTopFilter}
                  >{loop(item.children)}</TreeNode>;
        }
        if(item.isleaf== true){
          return <TreeNode key={item.key} 
                    icon={<span />}
                    title={item.title} 
                    isleaf={item.isleaf} 
                    displayName={item.displayName}
                    count={item.count}
                    name={item.name}
                    nodes={item.nodes}
                    fieldName={item.fieldName}
                    isSelected={item.isSelected}
                  />;
        } else {
          return <TreeNode key={item.key} 
                           checkable={false}
                           title={item.title} 
                           isleaf={item.isleaf} 
                           displayName={item.displayName}
                           count={item.count}
                           name={item.name}
                           nodes={item.nodes}
                           fieldName={item.fieldName}
                           isSelected={item.isSelected}
                  />;
        }

      });
    };
    return (
      <div className="tree_div">
        <style dangerouslySetInnerHTML={{ __html: STYLE }} />
        <button type="submit" className="btn btn-link apply_btn"
                style={{"marginTop":"60px", "marginLeft":"170px", "marginBottom":"-60px"}}
                // disabled={this.state.selectedFilters.length==0}
                onClick={()=>{this.applyFilter(); }}><i className="fa fa-play" />
          </button>

        <div>
          <div className="custom_tree">
            <Tree
              checkable={true}
              onCheck={this.onCheck}
              onExpand={this.onExpand}
              defaultExpandAll={false}
              motion={motion}
              selectable={false}
              checkedKeys={this.state.checkedKeys}
              expandedKeys={this.state.expandedKeys}
              // switcherIcon={switcherIcon}
              // treeData={this.state.facetTree}
              >
                {loop(this.state.facetTree)}
              </Tree>
          </div>
        </div>
      </div>
    );
  }

  getTreeData = (facets) => {
    let treeList = [];
    var expandedKeys = [], checkedKeys = [];
    facets.forEach((facet,key) => {
      let item = facet;
      item['key'] = '0-'+key;
      item['title'] = utils.titleCase(item.displayName);
      var ind = treeList.map(function (d) {return d["key"]}).indexOf(item['key']);
      if(ind==-1) {
        if(item['count'] && item['count']!=="") {
          item['title'] = item['title'] + ' (' + item['count'] + ')'
        }
        if(!item.nodes || item.nodes.length==0){
          item['isleaf'] = true
        }
        treeList.push(item);
        if(item.expanded==true) {
          expandedKeys.push(item.key)
        }
        if(item.isSelected==true) {
          checkedKeys.push(item.key)
        }
        if(facet.nodes && facet.nodes.length>0) {
          this.loopTree(facet.nodes, treeList[treeList.length-1], checkedKeys, expandedKeys)
        }
      }
    });
    setTimeout(()=>{
      this.setState({expandedKeys: expandedKeys,checkedKeys: checkedKeys})
    },500)
    return treeList;
  }

  loopTree = (childList, listNode, checkedKeys, expandedKeys) => {
    childList.forEach((facet,key) => {
      let item = facet;
      item['key'] = listNode.key + '-' + key;
      item['title'] = utils.titleCase(item.displayName);
      if(!listNode.children) {
        listNode.children = []
      }
      var ind = listNode.children.map(function (d) {return d["key"]}).indexOf(item['key']);
      if(ind==-1) {
        if(item['count']!=="") {
          item['title'] = item['title'] + ' (' + item['count'] + ')'
        }
        if(!item.nodes || item.nodes.length==0){
          item['isleaf'] = true
        }
        listNode.children.push(item);
        if(item.expanded==true) {
          expandedKeys.push(item.key)
        }
        if(item.isSelected==true) {
          checkedKeys.push(item.key)
        }
        if(facet.nodes && facet.nodes.length>0) {
          this.loopTree(facet.nodes, listNode.children[listNode.children.length-1], checkedKeys, expandedKeys)
        } 
      }
    });
  }

  onCheck = (checkedKeys, e) => {
    // this.handleTopSelection(checkedKeys, e);
    this.setState({checkedKeys: checkedKeys});
    var node = e.node;
    if(checkedKeys.indexOf(node.key)>-1) {
      node.isSelected = false;
    } else {
      node.isSelected = true;
    }
    this.fetchParentNode(node);
  }

  onExpand = (expandedKeys, e) => {
    this.setState({expandedKeys: expandedKeys});
  }

  fetchParentNode = (node) => {
    const loop = (data, key, callback) => {
      data.forEach((item, index, arr) => {
        if (item.key === key) {
          callback(item, index, arr);
          return;
        }
        if (item.children) {
          loop(item.children, key, callback);
        }
      });
    };
    const data = [...this.state.facetTree];

    let parentKey = this.findParentKey(node.key)
    // Find parent
    var parent;
    loop(data, parentKey, (item, index, arr) => {
      parent = item;
      this.handleInputChange(parent, node)
    });
  }

  findParentKey = (key) => {
    let parentKey = key;
    parentKey = parentKey.split('-');
    parentKey.pop();
    parentKey = parentKey.join('-');
    return parentKey;
  }

  render() {
    return this.renderFacets()
  }
}
export default AppFacet;
