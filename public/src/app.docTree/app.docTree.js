import React from "react";
import ReactDOM from "react-dom";
import axios from "axios";
import editDocument from "../helpers/openDocForEdit";

import Tree, { TreeNode } from "rc-tree";
import TreeDataHelper from "../helpers/TreeDataHelper";
import CollectionHelper from "../helpers/CollectionHelper";
import "rc-tree/assets/index.css";
import "./app.docTree.css";
import Tooltip from "rc-tooltip";
import { bool } from "prop-types";

class DocTree extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      treeData: {},
      expandedKeys: [],
      currentFileName: "",
      popupVisible: false,
      flag: false,
      solrTreeData: [],
      defaultSelectedKeys: []
    };

    this.formatDocListForRcTree = this.formatDocListForRcTree.bind(this);
  }

  componentDidMount() {
    this.getCollectionsData();
  }
  componentDidUpdate() {
    let versionTreeData = this.props.versionTreeData;
    if (versionTreeData != null && this.state.flag === false) {
      this.setState({ flag: true });
      this.setState({ treeData: [], expandedKeys: [] });
      this.createDocTree(versionTreeData, "versions");
    }
  }
  componentWillUnmount() {
    if (this.cmContainer) {
      ReactDOM.unmountComponentAtNode(this.cmContainer);
      document.body.removeChild(this.cmContainer);
      this.cmContainer = null;
    }
  }

  getCollectionsData = () => {
    let requestObj = {userEmail: this.props.userEmail}
    axios.post("/collections/getCollectionsTree", requestObj).then(collectionsData => {
      if (collectionsData !== undefined) {
        let collectionTreeData = CollectionHelper.collectionTreeAssembly(collectionsData.data, this.addRootCollection, this.addChildCollection, false);
        let collectionTree = {
          "key": CollectionHelper.randomStringGenerator(),
          "title": "Collections",
          "isLeaf": false,
          "icon": <i className="fa fa-bars collection-bars" />,
          "children": collectionTreeData
        }
        this.getSolrTreeData(collectionTree);
      }
    })
  }

  getSolrTreeData = (collectionTree) => {
    let AT = this.props.accessToken.toString();
    // let AT = "AaEdJPq8gc8heKyEDZLRB38KkdyADpP6"
    let acl = [];
    if (this.props.boxUserId != '') {
        acl.push("U_" + this.props.boxUserId);
    }

    if (this.props.boxUserGroupIds.length !== 0) {
        acl = acl.concat(this.props.boxUserGroupIds);
    }
    let spUserName; // = "richas@dgi";
    if (this.props.SPUserName !== undefined && this.props.SPUserName !== "" && this.props.SPUserName !== null) {
        spUserName = this.props.SPUserName;
    }
    var solrRequest = {
        "searchQuery": "*:*",
        "accessToken": [AT],
        "dataset": this.props.solrCollection,
        "client": this.props.client,
        "acl": acl, //["U_13589537292"],
        "tokensFound": true,
        "docPath": "*",
        "manifoldUsername": spUserName
    }
    var compThis = this;

    axios.post("/solrTree/getTreeStructFromSolr", solrRequest).then((response) => {
        try {
            let treeRoots = response.data.facet_counts.facet_fields.doc_path_hierarchy;
            let treeDocuments = response.data.response.docs;
            let toBeExpanded = [];
            let formattedTreeNodes = TreeDataHelper.formatSolrDataForTree(treeRoots, toBeExpanded, this.props.serviceTypes, treeDocuments, true);
            let allNodesKeys = [],  defaultSelectedKeys;
            if (!this.props.selectedDocInfo.isFromCollection) {
              let documentPath = this.props.selectedDocInfo.doc_path;
              if (documentPath !== undefined) {
                let allPartsOfPaths = documentPath.split("/");
                allPartsOfPaths.forEach(eachPath => {
                  let currentNode = TreeDataHelper.searchTree(formattedTreeNodes.docTreeRoots[0], eachPath);
                  if (currentNode !== undefined && currentNode !== null) {
                    allNodesKeys.push(currentNode.key);
                  }
                })
              }
              defaultSelectedKeys = this.props.selectedDocInfo.file_id
            } else {
              let collectionSelectedNode = CollectionHelper.findAllParents(collectionTree, this.props.selectedDocInfo.keyIntree);
              allNodesKeys = collectionSelectedNode;
              defaultSelectedKeys = this.props.selectedDocInfo.keyIntree
            } 
            
            let treeDataWithCollections = [collectionTree, ...formattedTreeNodes.docTreeRoots];
            compThis.setState({
                isLoading: false,
                solrTreeData: treeDataWithCollections,
                defaultExpanded: allNodesKeys,
                defaultSelectedKeys: [defaultSelectedKeys],
                checkedKeys: ['0-0'],
            }, function () {
                // console.log("I am called")
            });
        }
        catch (err) {
            console.log(err);
            this.setState(
                {
                    isLoading: false,
                });
        }
    })
  }


  createDocTree = (docTreeData) => {
    let rcTreeDocObj = this.formatDocListForRcTree(docTreeData);
    let exKeys = this.state.expandedKeys;
    this.setState({ treeData: rcTreeDocObj, expandedKeys: exKeys });
    setTimeout(() => {
      let element = $(".file-node-from-tree");
      $(".file-node-from-tree").get(0).scrollIntoView();
    }, 500);
  };

  formatDocListForRcTree(docTreeData, parentNode) {
    docTreeData.forEach((node, index) => {
      node.key = node["$id"];
      node.title = node["Name"];
      if (parentNode) {
        node.parentId = parentNode["$id"];
      }

      let fileNExt = node.Name.split(".");
      if (fileNExt.length > 1) {
        let fileExt = fileNExt[fileNExt.length - 1];
        if (fileExt === "msg") {
          node.icon = (
            <i className="fa fa-envelope blue-icons" aria-hidden="true"></i>
          );
        } else if (fileExt === "doc" || fileExt === "docx") {
          node.icon = (
            <i className="fa fa-file-word-o blue-icons" aria-hidden="true"></i>
          );
        } else if (fileExt === "xls" || fileExt === "xlsx") {
          node.icon = (
            <i className="fa fa-file-excel-o blue-icons" aria-hidden="true"></i>
          );
        } else if (fileExt === "pdf") {
          node.icon = (
            <i className="fa fa-file-pdf-o blue-icons" aria-hidden="true"></i>
          );
        } else if (fileExt == "ppt" || fileExt == "pptx") {
          node.icon = (
            <i
              className="fa fa-file-powerpoint-o blue-icons"
              aria-hidden="true"
            ></i>
          );
        }
      }
      node.children = node.Members;
      this.formatDocListForRcTree(node.children, node);

      if (node.Name === this.state.currentFileName) {
        node.className = "file-node-from-tree";
        this.setState({ expandedKeys: [node.parentId] });
      } else {
        node.className = "";
      }
    });
    return docTreeData;
  }

  pushInLastNode(treeData, nodeToPush) {
    treeData.forEach((node) => {
      if (node.children.length > 0) {
        this.pushInLastNode(node.children, nodeToPush);
      } else {
        node.children.push(nodeToPush);
      }
    });

    return treeData;
  }

  onRightClick = (info) => {
    if (this.toolTip) {
      ReactDOM.unmountComponentAtNode(this.cmContainer);
      this.toolTip = null;
    }
    let fileNameNExt;
    if (info.node.props.VersionHistoryUrl) {
      var str = info.node.VersionHistoryUrl;

      if (str.includes(".pdf") == true || str.includes(".msg") == true) {
        return;
      } else {
        this.renderCm(info);
      }
    } else {
      fileNameNExt = info.node.Name.split(".");
    }
    let fileExt = fileNameNExt[fileNameNExt.length - 1];
    if (fileExt == "pdf" || fileExt == "msg") {
      return;
    }

    if (!info.node.props.FileSize) {
      return;
    }

    if (info.node.DoucumentId == 0) {
      return;
    }
    this.setState({
      selectedKeys: [info.node.props.eventKey],
      editNode: info.node,
    });
    this.renderCm(info);
  };

  //   onMouseEnter = info => {
  //     this.renderCm(info);
  //   };

  //   onMouseLeave = info => {
  //   };

  getContainer() {
    if (!this.cmContainer) {
      this.cmContainer = document.createElement("div");
      document.body.appendChild(this.cmContainer);
    }
    return this.cmContainer;
  }

  renderCm(info) {
    if (this.toolTip) {
      ReactDOM.unmountComponentAtNode(this.cmContainer);
      this.toolTip = null;
    }
    if (!this.state.popupVisible) {
      // attach/remove event handler
      document.addEventListener("click", this.handleOutsideClick, false);
    } else {
      document.removeEventListener("click", this.handleOutsideClick, false);
    }

    this.setState((prevState) => ({
      popupVisible: !prevState.popupVisible,
    }));
    if (info.node.Name.split(":")[0] == "Version") {
      this.toolTip = (
        <Tooltip
          trigger="click"
          placement="bottomRight"
          className="rc-tree-contextmenu"
          defaultVisible
          overlay={
            <div
              className="custom_menu"
              onClick={() => this.editVersionDocument(info)}
            >
              <div className="doctree-editopt">Edit</div>
            </div>
          }
        >
          <span />
        </Tooltip>
      );
    } else {
      this.toolTip = (
        <Tooltip
          trigger="click"
          placement="bottomRight"
          className="rc-tree-contextmenu"
          defaultVisible
          overlay={
            <div className="custom_menu" onClick={() => this.editDocument()}>
              <div className="doctree-editopt">Edit</div>
            </div>
          }
        >
          <span />
        </Tooltip>
      );
    }
    const container = this.getContainer();
    Object.assign(this.cmContainer.style, {
      position: "absolute",
      left: `${info.event.pageX + 150}px`,
      top: `${info.event.pageY - 15}px`,
    });

    ReactDOM.render(this.toolTip, container);
  }

  handleOutsideClick = (e) => {
    var elem = e.target;
    if (elem.className !== "doctree-editopt") {
      this.unrenderCm();
    }
    this.handleClick();
  };

  handleClick() {
    if (!this.state.popupVisible) {
      // attach/remove event handler
      document.addEventListener("click", this.handleOutsideClick, false);
    } else {
      document.removeEventListener("click", this.handleOutsideClick, false);
    }

    this.setState((prevState) => ({
      popupVisible: !prevState.popupVisible,
    }));
  }

  unrenderCm() {
    if (this.toolTip) {
      ReactDOM.unmountComponentAtNode(this.cmContainer);
      this.toolTip = null;
    }
  }

  editDocument = () => {
    editDocument(this.state.editNode, this.props.source, this.props.client,this.props.edocsBackendAPI);
  };

  editVersionDocument = (info) => {
    let request;
    let requestUrl="";
    if(this.props.source.indexOf('Sharepoint') >-1) {
        request = {
        "ConnectorSourceName":"SharePoint",
        "DocumentId": info.node.DocumentId,
        "VersionDocumentId": info.node.VersionHistoryUrl.split("/")[1]
        };

        requestUrl = this.props.edocsBackendAPI+"api/v1/downloadVersionHistorydoc";
    }
    request["ClientName"]= this.props.client;
    axios.post(requestUrl, request).then(response => {
    // var searchRequest = {
    //   DocumentId: info.node.DocumentId,
    //   VersionDocumentId: info.node.VersionHistoryUrl.split("/")[1],
    // };

    // axios
    //   .post(
    //     "https://sp16-nba.thedigitalgroup.com:9091/api/v1/downloadVersionHistorydoc",
    //     searchRequest
    //   )
    //   .then((response) => {
        if (response && response.data && response.data.FilePath) {
          var url = "";
          let fileNExt = info.node.VersionHistoryUrl.split(".");

          if (fileNExt.length > 1) {
            let fileExt = fileNExt[fileNExt.length - 1];
            if (fileExt == "doc" || fileExt == "docx") {
              url = "ms-word:ofe|u|" + response.data.FilePath;
            } else if (fileExt == "xls" || fileExt == "xlsx") {
              url = "ms-excel:ofe|u|" + response.data.FilePath;
            } else if (fileExt == "ppt" || fileExt == "pptx") {
              url = "ms-powerpoint:ofe|u|" + response.data.FilePath;
            } else {
              url = "ms-word:ofe|u|" + response.data.FilePath;
            }
            var mylink = document.getElementById("MyLink");
            mylink.setAttribute("href", url);
            mylink.click();
          }
        }
      });
  };

  getVersionDocumentURL(selectedNode) {
    var searchRequest = {
      DocumentId: selectedNode.node.DocumentId,
      VersionDocumentId: selectedNode.node.VersionHistoryUrl.split("/")[1],
    };

    axios
      .post(
        this.props.edocsBackendAPI+"api/v1/downloadVersionHistorydoc",
        searchRequest
      )
      .then((response) => {
        if (response && response.data && response.data.FilePath) {
          var url = "";
          let fileNExt = selectedNode.node.VersionHistoryUrl.split(".");

          if (fileNExt.length > 1) {
            let fileExt = fileNExt[fileNExt.length - 1];
            if (fileExt == "doc" || fileExt == "docx") {
              url = "ms-word:ofe|u|" + response.data.FilePath;
            } else if (fileExt == "xls" || fileExt == "xlsx") {
              url = "ms-excel:ofe|u|" + response.data.FilePath;
            } else if (fileExt == "ppt" || fileExt == "pptx") {
              url = "ms-powerpoint:ofe|u|" + response.data.FilePath;
            } else {
              url = "ms-word:ofe|u|" + response.data.FilePath;
            }
            var mylink = document.getElementById("MyLink");
            mylink.setAttribute("href", url);
            mylink.click();
          }
        }
      });
  }

  treeNodeSelected = (selectedKeys, selectedNode) => {
    if (selectedNode.node.isLeaf) {
      let fileType = "doc";
      if (selectedNode.node.VersionHistoryUrl) {
        fileType = "version";
        this.getVersionDocumentURL(selectedNode);
      }
      /* let documentId = selectedNode.node.DoucumentId
        ? selectedNode.node.DoucumentId
        : selectedNode.node.DocumentId; */
      /* let fileNameNExt =
        fileType == "doc"
          ? selectedNode.node.Name.split(".")
          : selectedNode.node.VersionHistoryUrl.split("."); */
      let currentFileName = selectedNode.node.title;
      this.setState({ currentFileName: currentFileName });
      let fileExt = selectedNode.node.fileExtType //fileNameNExt[fileNameNExt.length - 1];
      this.props.refreshDocumentPreview(selectedNode.node.key, fileExt, selectedNode.node);
    } else {

      let subTree = [];
      this.setState({
          pdfPath: "",
          docLoading: false,
          errorInPdf: false,
          showSecondCol: true
          /* defaultExpanded:  */
      });

      // this.loadTreeData(selectedNode.node).then(() => {
          //serviceType, selectedKey, nodeTitle, folderPath, resolve
          if (selectedNode.node.children !== undefined && selectedNode.node.children.length > 0) {
              let secondColFld = [], secondColFiles = [];
              selectedNode.node.children.forEach(childNode => {

                  // if (childNode.Source === "folder.png") {
                  if (!childNode.isLeaf) {
                      // secondColFld.author = childNode.Author? childNode.Author : "";
                      // secondColFld.fileSize = childNode.FileSize ? childNode.FileSize : "";
                      // secondColFld.modifiedOn = childNode.ModifiedOn;
                      secondColFld.push(childNode);
                  } else {
                      // secondColFiles.author = childNode.Author? childNode.Author : "";
                      // secondColFiles.fileSize = childNode.FileSize ? childNode.FileSize : "";
                      // secondColFiles.modifiedOn = childNode.ModifiedOn;
                      secondColFiles.push(childNode);
                  }
              });
              this.setState({
                  secondColumnFolders: secondColFld,
                  secondColumnFiles: secondColFiles,
                  showSecondCol: true
              });
              this.props.showExplorerView(secondColFld, secondColFiles)

          } /* else {  defaultExpanded: formattedTreeNodes.toBeExpanded
              let documentId = selectedNode.node.DoucumentId ? selectedNode.node.DoucumentId : selectedNode.node.DocumentId;
              let fileNameNExt = selectedNode.node.Name.split(".");
              let fileExt = fileNameNExt[(fileNameNExt.length - 1)];
              this.documentPreview(documentId, fileExt, selectedNode.node.Name)
          } */
      // });
    }
    
  };

  onExpand = (keys, more) => {
    let expandedKeys = keys;
    this.setState({ expandedKeys: expandedKeys });
  };

  render() {
    let styleClass = "align-loader";
    let treeStructure = (
      <img src={this.props.loaderIcon} className="loader"></img>
      
    );
    if (this.state.solrTreeData.length > 0) {
      // const { solrTreeData } = this.state;
      styleClass = ""
      treeStructure = (
        <Tree
          className="myCls"
          showLine
          autoExpandParent={true}
          // onMouseEnter={this.onMouseEnter}
          // onMouseLeave={this.onMouseLeave}
          // expandedKeys={this.state.expandedKeys}
          defaultExpandedKeys={this.state.defaultExpanded}
          defaultSelectedKeys={this.state.defaultSelectedKeys}
          onRightClick={this.onRightClick}
          // onExpand={this.onExpand}
          // treeData={this.state.treeData}
          treeData={this.state.solrTreeData}
          onSelect={this.treeNodeSelected}
        />
      );
    }
    return (
      <div
        ref={(node) => {
          this.node = node;
        }}
        className={styleClass}
      >
        {treeStructure}
      </div>
    );
  }
}

export default DocTree;
