import Autosuggest from 'react-autosuggest';
import axios from "axios";
import React, { Component, useDebugValue } from "react";
import "./app.suggestions.css";
import { keysIn } from 'lodash';

class Suggestions extends Component {
  constructor(props) {
    super(props);

    // Autosuggest is a controlled component.
    // This means that you need to provide an input value
    // and an onChange handler that updates this value (see below).
    // Suggestions also need to be provided to the Autosuggest,
    // and they are initially empty because the Autosuggest is closed.
    this.state = {
      value: props.searchQuery,
      suggestions: []
    };
  }

  getSuggestionList = (suggestionData) => {
    var suggestionList = [];
    var lstsuggestion;
    var order =  Object.keys(suggestionData);

    for(var i = 0; i < order.length; i++){
        var suggest = suggestionData[order[i]]//[$scope.dataStore.originalQuery];

        var orderSubkey=Object.keys(suggest);
      
        for(var j = 0; j < orderSubkey.length; j++){
          lstsuggestion=suggest[orderSubkey[j]];
        }
        if(lstsuggestion["suggestions"].length >0)
        {
        for(var k = 0; k < lstsuggestion["suggestions"].length; k++){
       
          if(order[i] == "Keywords")
          {
            lstsuggestion["suggestions"][k].payload = "Tags";
            }else{
              lstsuggestion["suggestions"][k].payload = order[i];
            }
            suggestionList.push(lstsuggestion["suggestions"][k]); 
        }
      }
    }
    if(suggestionList && suggestionList.length === 0){
         return [];
    }else{
      this.suggestionListLength = suggestionList.length;
         return suggestionList;
    }
}

  // When suggestion is clicked, Autosuggest needs to populate the input
  // based on the clicked suggestion. Teach Autosuggest how to calculate the
  // input value for every given suggestion.
  getSuggestionValue = suggestion => suggestion.term;
  
  // Use your imagination to render suggestions.
  renderSuggestion = suggestion => (
    <div>
        <span>{suggestion.term} {suggestion.payload && 'in'} <span className='categoryHighlight'>{suggestion.payload}</span></span>
    </div>
  );

  onChange = (event, { newValue }) => {
    this.setState({
      value: newValue
    });
    this.props.setSearchQuery(newValue);
  };

  onKeyPress = (e) => {
    e.which === 13 ? this.props.onSearchIconClick() : null;
  };

  // Autosuggest will call this function every time you need to update suggestions.
  // You already implemented this logic above, so just use it.
  onSuggestionsFetchRequested = ({ value }) => {
    if(!value) {
        return 
    }
    if(value.length < 3) {
        return 
    }

    this.TypedText_s = value;

    var dataset;
    if( this.props.boxAuthorized == true)
    {
      dataset=this.props.datasetBox;

    }else if(this.props.spAuthorized == true){

      dataset=this.props.datasetSP;
    }

    if(this.props.boxAuthorized == true && this.props.spAuthorized == true ){

      dataset=this.props.datasetBox+" OR "+this.props.datasetSP;
    }

    var searchRequest = {
        'query': value,
        'count' : 5,
        'datasets': dataset,
        // 'client': this.props.client
    }
    axios.post("/AutoSuggestSolr/getAutoSuggestResults", searchRequest).then((response) => {
        var suggestionData = response.data.suggest;
        var suggestionList = this.getSuggestionList(suggestionData);
        this.setState({
            suggestions: suggestionList
          });

        if(suggestionList.length>0)
          this.props.setSuggestionBoxVisibility(true);
    })

  };

  onSuggestionSelected = (event, { suggestion, suggestionValue, suggestionIndex, sectionIndex, method }) => {
      this.props.onSuggestionClick(suggestion)
      this.onSuggestionsClearRequested();
      this.saveAutoSuggestDetail(suggestion)
  }

  saveAutoSuggestDetail = (suggestion) => {
    var params={};
    var multiParams = {
      source_type_ss: this.props.source
    };
    params.TypedText_s = this.TypedText_s;
    params.ClickedSuggationText_s = suggestion.term;
    params.ClickedSuggationCategory_s = suggestion.payload ? suggestion.payload : "";
    params.TotalAutoSuggations_i  = this.suggestionListLength;
    params.UserSubCategory_s = "";
    params.UserCategory_s = "";
    this.searcheventRequest = {};
    this.searcheventRequest.event_status = 1;
    this.searcheventRequest.sessionid = this.props.sessionId;
    var siteVisitData = {
    "query": suggestion.term,
    "eventStatus":this.searcheventRequest.event_status,
    "eventTimestamp": new Date().toISOString(),
    "appId": this.props.eventAppId,
    "user" : this.props.userEmail,
    "application": this.props.eventAppId,
    "sessionId": this.searcheventRequest.sessionid,
    "eventType": "Autosuggestion Click",
    "params":params,
    "userIP": this.props.userIP,
    "multiParams": multiParams
  };
  axios.post("/search/saveSearchEventData", siteVisitData);
  }


  searchIconClicked = () => {
    this.props.onSearchIconClick();
    this.onSuggestionsClearRequested();
 }


  // Autosuggest will call this function every time you need to clear suggestions.
  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: []
    });
    this.props.setSuggestionBoxVisibility(false);
  };

  renderInputComponent = inputProps => {
    return <div>
                <input {...inputProps} />
                <i aria-hidden="true"   
                    className="fa fa-search search-icon"
                    onClick={()=>this.searchIconClicked()}></i>
            </div>
  };


  render() {
    const { value, suggestions } = this.state;

    // Autosuggest will pass through all these props to the input.
    const inputProps = {
      placeholder: 'Search',
      value,
      onChange: this.onChange,
      onKeyPress: this.onKeyPress
    };

    // Finally, render it!
    return (
      <Autosuggest
        suggestions={suggestions}
        onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
        onSuggestionsClearRequested={this.onSuggestionsClearRequested}
        getSuggestionValue={this.getSuggestionValue}
        onSuggestionSelected={this.onSuggestionSelected}
        renderSuggestion={this.renderSuggestion}
        inputProps={inputProps}
        renderInputComponent={this.renderInputComponent}
      />
    );
  }
}

export default Suggestions;