import React from 'react';
import axios from 'axios';

import Tree, { TreeNode } from 'rc-tree';
import TreeDataHelper from '../helpers/TreeDataHelper';
import 'rc-tree/assets/index.css';
import './app.documentLibrary.css';
// import FileViewer from 'react-file-viewer';

class DocumentLibrary extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            mainTreeData: [],
            showSecondCol: true,
            selectedDocument: "",
            documentStream: "",
            secondColumnFolders: [],
            secondColumnFiles: [],
            defaultExpanded: [],
            docLoading: false,
            errorInPdf: false,
            currentFileType:"",
            versionArray: []
        }

        this.formatDocListForRcTree = this.formatDocListForRcTree.bind(this);
        this.documentPreview = this.documentPreview.bind(this);
    }

    componentDidMount() {

       let dgiUsername =this.props.dgiUserName.split("@")[0];
       setTimeout(()=>{

          let rcTreeDocObj = this.formatDocListForRcTree(this.props.mainTreeData, "");
          this.setState({ mainTreeData: rcTreeDocObj });  }, 500)
      
   
        // axios.get("https://sp16-nba.thedigitalgroup.com:9091/api/v1/document/sharepoint/"+dgiUsername).then((docTreeDetails) => {
        //     let docTreeData = docTreeDetails.data;
        //     let rcTreeDocObj = this.formatDocListForRcTree(docTreeData, "");
        //     this.setState({ mainTreeData: rcTreeDocObj });
        // })
    }

    formatDocListForRcTree(docTreeData, currentFileName) {

        docTreeData.forEach((node, index) => {
            node.key = node["$id"];
            node.title = node["Name"];
            node.className = "all-node-wrap";
            if (this.state.defaultExpanded.length < 2) {
                let toBeExpanded = this.state.defaultExpanded;
                toBeExpanded.push(node["$id"]);
                this.setState({defaultExpanded: toBeExpanded});
            }
            if (node.Members.length > 0) {
                node.children = node.Members
                this.formatDocListForRcTree(node.children, currentFileName);
            } else {
                let fileNExt = node.Name.split(".");
                if (fileNExt.length > 1) {
                    let fileExt = fileNExt[(fileNExt.length - 1)];
                    if (fileExt === "msg") {
                        node.icon = <i className="fa fa-envelope blue-icons" aria-hidden="true"></i>
                    } else if (fileExt === "doc" || fileExt === "docx") {
                        node.icon = <i className="fa fa-file-word-o blue-icons" aria-hidden="true"></i>
                    } else if (fileExt === "xls" || fileExt === "xlsx") {
                        node.icon = <i className="fa fa-file-excel-o blue-icons" aria-hidden="true"></i>
                    } else {
                        node.icon = <i className="fa fa-file-pdf-o blue-icons" aria-hidden="true"></i>
                    }
                }

                if (node.Name === currentFileName) {
                    node.className = "all-node-wrap file-node-from-tree"
                }

                node.isLeaf = true;
            }
        });

        return docTreeData;
    }

    treeNodeSelected = (selectedKeys, selectedNode) => {
        let subTree = [];
        this.setState({
            pdfPath: "",
            docLoading: false,
            errorInPdf: false,
            showSecondCol: true
        })
        this.docPosition = selectedNode.node.key;
        if (selectedNode.node.children !== undefined && selectedNode.node.children.length > 0) {
            let secondColFld = [], secondColFiles = [];
            selectedNode.node.children.forEach(childNode => {

                if (childNode.Source === "folder.png") {
                    // secondColFld.author = childNode.Author? childNode.Author : "";
                    // secondColFld.fileSize = childNode.FileSize ? childNode.FileSize : "";
                    // secondColFld.modifiedOn = childNode.ModifiedOn;
                    secondColFld.push(childNode);
                } else {
                    // secondColFiles.author = childNode.Author? childNode.Author : "";
                    // secondColFiles.fileSize = childNode.FileSize ? childNode.FileSize : "";
                    // secondColFiles.modifiedOn = childNode.ModifiedOn;
                    secondColFiles.push(childNode);
                }
            });
            this.setState({
                secondColumnFolders: secondColFld,
                secondColumnFiles: secondColFiles,
                showSecondCol: true
            })
            
        } else {
            let documentId = selectedNode.node.DoucumentId ? selectedNode.node.DoucumentId : selectedNode.node.DocumentId;
            let fileNameNExt = selectedNode.node.Name.split(".");
            let fileExt = fileNameNExt[(fileNameNExt.length - 1)];
            this.documentPreview(documentId, fileExt, selectedNode.node.Name)
        }
    };

    folderDblClick = (selectedFld) => {
        let secondColFld = [], secondColFiles = [];
        if (selectedFld.children !== undefined) {
            selectedFld.children.forEach(subFld => {

                if (subFld.Source === "folder.png") {
                    // subFld.author = childNode.Author? childNode.Author : "";
                    // subFld.fileSize = childNode.FileSize ? childNode.FileSize : "";
                    // subFld.modifiedOn = childNode.ModifiedOn;
                    secondColFld.push(subFld);
                } else {
                    // secondColFiles.author = childNode.Author? childNode.Author : "";
                    // secondColFiles.fileSize = childNode.FileSize ? childNode.FileSize : "";
                    // secondColFiles.modifiedOn = childNode.ModifiedOn;
                    secondColFiles.push(subFld);
                }
            });
            this.setState({
                secondColumnFolders: secondColFld,
                secondColumnFiles: secondColFiles
            })
        }
    }
   
    documentPreview(documentId, fileExt, fileName) {
        this.docName = fileName
        this.docId = documentId
        if ( this.state.secondColumnFiles.length > 0 ) {
            var index = this.state.secondColumnFiles.map(function (d) { return d['DocumentId']; }).indexOf(documentId);
            this.docPosition = this.state.secondColumnFiles[index].key;
        }
        this.setState({showSecondCol: false, currentFileType: fileExt})
        // this.setState({pdfPath: "", docLoading: true, errorInPdf: false}, function () {
        //         var url = "https://" + window.location.hostname + "/getDocPreview/" + documentId;
        //         this.setState({
        //            pdfPath: url
        //        })
        // })
        var list = document.getElementById("dvContainer1");
        if(list && list.childNodes && list.childNodes[0]) {
            list.removeChild(list.childNodes[0]); 
        }
        if(fileExt!=='msg') {
            let request;
            let requestUrl="";
            if(this.props.source.indexOf('Sharepoint') >-1) {
                request = {
                    "ConnectorSourceName":"SharePoint",
                    "DocumentId": documentId,
                    ViewedBy: this.props.userEmail,
                    
                }
                //requestUrl = "https://sp16-nba.thedigitalgroup.com:9091/api/v1/getdoccontent/";
            }
            else{
                request = {
                  "ConnectorSourceName":"Box",
                  title: this.removeHTML(this.props.item.title),
                  documentId: 0,
                  ViewedBy: this.props.userEmail,
                  AccessToken:this.props.accessToken,
                  RefreshToken:this.props.refreshToken,
                  FileID:708941880000
                };
              }
              request["ClientName"]= this.props.client;
              //Axios.post("/preview/docPreview", request).then(response => {
                axios.post("/preview/docPreview", request).then(response => {
            
                const fragment = document
                .createRange()
                .createContextualFragment(response.data["HtmlContent"]);
                document.getElementById('dvContainer1').appendChild(fragment);

                let request;
                let requestUrl="";
                if(this.props.source.indexOf('Sharepoint') >-1) {
                    request = {
                      "ConnectorSourceName":"SharePoint",
                      "DocumentId": documentId
                    }
                    requestUrl = this.props.edocsBackendAPI+"api/v1/docversionhistory/";
                  }
                  request["ClientName"]= this.props.client;
                  //get doc versions
                  axios.post(requestUrl, request).then(response => {
                //   axios.get("/docTree/getDocVersions/" + this.props.documentId).then(response => {
                    let versionArray = response && response.data ? response.data : [];
                    this.setState({
                        versionArray: versionArray
                    })
                })
            })
        } else {
            this.setState({pdfPath: "", docLoading: true, errorInPdf: false}, function () {
            // var url = "https://" + window.location.hostname + "/getDocPreview/" + documentId;
            // if(this.props.source.indexOf('Sharepoint') >-1) {
            //     url += '/'+'SharePoint';
            //   }
            //   url += '/'+'this.props.client';
            //     this.setState({
            //         pdfPath: url
            //     })
            let request;
            let requestUrl="";
            let docID = documentId == null ? 0 : documentId;
                request = {
                  Title: fileName ? fileName : "",
                  documentId:docID,
                  ViewedBy: this.props.userEmail,
                  ViewedBy: this.props.userEmail,
                  AccessToken:this.props.accessToken,
                  RefreshToken:this.props.refreshToken,
                  FileID:750515727382
                };
              request["ClientName"]= this.props.client;
              axios.post("/preview/emailPreview", request).then(response => {
               
              this.setState({
                loader: false,
              });
              this.setState({
                pdfPath: response.data.FilePath,
                currentFileType: fileExt,
                // currentItem: this.props.item,
              });
            })

                let request1;
                let requestUrl1="";
                if(this.props.source.indexOf('Sharepoint') >-1) {
                    request1 = {
                        "ConnectorSourceName":"SharePoint",
                        "DocumentId": documentId
                    }
                    requestUrl1 =this.props.edocsBackendAPI+"api/v1/docversionhistory/";
                }
                request1["ClientName"]= this.props.client;
                //get doc versions
                axios.post(requestUrl1, request1).then(response => {
                // axios.get("/docTree/getDocVersions/" + this.props.documentId).then(response => {
                    let versionArray = response && response.data ? response.data : [];
                    this.setState({
                        versionArray: versionArray
                    })
                })
            })
        }
        this.sendDocumentClickLibraryEvent();
    }

    sendDocumentClickLibraryEvent = () => {
        var params = {};
        var multiParams = {
            source_type_ss: this.props.source
          };
        params.recommandation_b = false;
        params.ContentType_s = "";
        params.SourceType_s = "";
        params.docurl_s = "";
        params.pageno_i = "";
        this.searcheventRequest = {};
        this.searcheventRequest.event_status = 1;
        this.searcheventRequest.sessionid = this.props.sessionId;
        params.UserSubCategory_s = "";
        params.UserCategory_s = "";
        params.numofresults = "";
        params.search_responsetime = 0;
        var clickEventData = {
            "documentStartTime": new Date().toISOString(),
            "documentEndTime": new Date().toISOString(),
            "query": this.docName,
            "documentPosition": this.docPosition,
            "docId": this.docId,
            "userIP": this.props.userIP,
            "eventStatus": this.searcheventRequest.event_status,
            "eventTimestamp": new Date().toISOString(),
            "appId": this.props.eventAppId,
            "user" : this.props.userEmail,
            "application": this.props.eventAppId,
            "sessionId": this.searcheventRequest.sessionid,
            "eventType": "Document Click",
            "params": params,
            "multiParams": multiParams
        };
        axios.post("/search/saveSearchEventData", clickEventData);
      }

    render() {
        let mainTreeStructure = <img src={this.props.loaderIcon} className="loader"></img>;
        let secondTreeList = "";
        let firstColumnClass = "col-md-3 doc-lib-col doc-lib-first-col center-align-content";

        let selectedDoc = <div className="select-file-for-preview"> Select file for preview. </div>
        if (this.state.docLoading) {
            selectedDoc = <img src={this.props.loaderIcon} className="loader"></img>;
        }
        if (this.state.pdfPath !== "" ) {
            selectedDoc = <iframe src={this.state.pdfPath} referrerPolicy="unsafe-url" height="630px" width="100%"/>
        }
        if (this.state.errorInPdf) {
            selectedDoc = <h4>Unable to load preview of document.</h4>
        }

        if (this.state.mainTreeData.length > 0) {
            firstColumnClass = "col-md-3 doc-lib-col doc-lib-first-col";
            mainTreeStructure = <Tree
                className="myCls"
                showLine
                autoExpandParent={true}
                onExpand={this.onExpand}
                treeData={this.state.mainTreeData}
                onSelect={this.treeNodeSelected}
                defaultExpandedKeys={this.state.defaultExpanded}
            />
        }

        let today = new Date();
        let dd = String(today.getDate()).padStart(2, '0');
        let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        let yyyy = today.getFullYear();

        today = mm + '/' + dd + '/' + yyyy;
        let folderList = "", fileList = "", modifiedBy = "Amit D";
        if (this.state.secondColumnFolders.length > 0) {
            folderList = this.state.secondColumnFolders.map(folder => {
                return <div key={folder.key} className="row doc-list-row" onDoubleClick={() => this.folderDblClick(folder)}>
                    <div className="col-md-5 mouse-pointer">
                        <i className="fa fa-folder folder-icon" aria-hidden="true"></i> &nbsp;{folder.Name}
                    </div>
                    <div className="col-md-2 mouse-pointer">

                    </div>
                    <div className="col-md-3 mouse-pointer">
                        {/* {today} */}
                        {folder.ModifiedOn}
                    </div>
                    <div className="col-md-2 mouse-pointer">
                        {/* {modifiedBy} */}
                        {folder.Author}
                    </div>
                </div>
            })
        }

        if (this.state.secondColumnFiles.length > 0) {
            fileList = this.state.secondColumnFiles.map(file => {
                let fileNameNExt = file.Name.split(".");
                let fileExt = fileNameNExt[(fileNameNExt.length - 1)];
                let fileIcon = <i className="fa fa-file-word-o no-left-margin word-doc-icon" aria-hidden="true"></i>
                if (fileExt === "msg") {
                    fileIcon = <i className="fa fa-envelope no-left-margin email-doc-icon" aria-hidden="true"></i>
                } else if (fileExt === "doc" || fileExt === "docx") {
                    fileIcon = <i className="fa fa-file-word-o no-left-margin word-doc-icon" aria-hidden="true"></i>
                } else if (fileExt === "xls" || fileExt === "xlsx") {
                    fileIcon = <i className="fa fa-file-excel-o no-left-margin word-doc-icon" aria-hidden="true"></i>
                } else {
                    fileIcon = <i className="fa fa-file-pdf-o no-left-margin pdf-doc-icon" aria-hidden="true"></i>
                }
                return <div key={file.key} id={file.key} onClick={() => this.documentPreview(file.DoucumentId ? file.DoucumentId : file.DocumentId, fileExt, file.Name )} className="row doc-list-row">
                    <div className="col-md-5 mouse-pointer">
                        {fileIcon} &nbsp;{fileNameNExt[0]}
                    </div>
                    <div className="col-md-2 mouse-pointer">
                        {/* {modifiedBy} */}
                        {file.FileSize}
                    </div>
                    <div className="col-md-3 mouse-pointer">
                        {/* {today} */}
                        {file.ModifiedOn}
                    </div>
                    <div className="col-md-2 mouse-pointer">
                        {/* {modifiedBy} */}
                        {file.Author}
                    </div>
                </div>
            })
        }
        
        let classForLastCol = "col-md-9 doc-lib-col doc-lib-third-col"
       
        return (
            <div className="container">
                <div className="row">
                    <div className={firstColumnClass}>
                        {mainTreeStructure}
                    </div>
                    {this.state.showSecondCol && <div className="col-md-9 doc-lib-col">
                        <div className="row">
                            <div className="col-md-5 header-row">Name</div>
                            <div className="col-md-2 header-row">File Size</div>
                            <div className="col-md-3 header-row">Modified date</div>
                            <div className="col-md-2 header-row">Modified by</div>
                        </div>
                        {folderList}
                        {fileList}
                    </div>}
                    {!this.state.showSecondCol && <div id="document-preview-container" className={classForLastCol}>
                        {/* {selectedDoc} */}
                        {/* <div id="dvContainer1" style={{ height: "100vh", width: "100%" }}></div> */}
                        { this.state.currentFileType=='msg' &&
                            selectedDoc
                        }
                        { this.state.currentFileType!=='msg' &&
                            <div id="dvContainer1" style={{ height: "100vh", width: "100%" }}></div>
                        }
                    </div>}
                </div>
            </div>
        )
    }
}

export default DocumentLibrary