import React from 'react';
import axios from 'axios';

import Tree, { TreeNode } from 'rc-tree';
import TreeDataHelper from '../helpers/TreeDataHelper';
import CollectionHelper from '../helpers/CollectionHelper';
import 'rc-tree/assets/index.css';
import './app.documentLibrary.css';
// import FileViewer from 'react-file-viewer';
import loadingImg from "../images/loader-1.gif";
import loadingBallImg from "../images/loader-2.png";



function generateTreeNodes(treeNode) {
    const arr = [];
    const key = treeNode.props.eventKey;
    for (let i = 0; i < 3; i += 1) {
        arr.push({ title: `leaf ${key}-${i}`, key: `${key}-${i}` });
    }
    return arr;
}

function setLeaf(treeData, curKey, level) {
    const loopLeaf = (data, lev) => {
        const l = lev - 1;
        data.forEach(item => {
            if (
                item.key.length > curKey.length
                    ? item.key.indexOf(curKey) !== 0
                    : curKey.indexOf(item.key) !== 0
            ) {
                return;
            }
            if (item.children) {
                loopLeaf(item.children, l);
            } else if (l < 1) {
                // eslint-disable-next-line no-param-reassign
                item.isLeaf = true;
            }
        });
    };
    loopLeaf(treeData, level + 1);
}

function getNewTreeData(treeData, curKey, child, level) {
    const loop = data => {
        if (level < 1 || curKey.length - 3 > level * 2) return;
        data.forEach(item => {
            if (curKey.indexOf(item.key) === 0) {
                if (item.children) {
                    loop(item.children);
                } else {
                    // eslint-disable-next-line no-param-reassign
                    item.children = child;
                }
            }
        });
    };
    loop(treeData);
    setLeaf(treeData, curKey, level);
}


class DocumentLibrarySolr extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            mainTreeData: [],
            showSecondCol: true,
            selectedDocument: "",
            documentStream: "",
            secondColumnFolders: [],
            secondColumnFiles: [],
            defaultExpanded: [],
            docLoading: false,
            errorInPdf: false,
            currentFileType: "",
            versionArray: [],
            solrTreeData: [],
            treeData: [

            ],
            checkedKeys: [],
            newCollectionName: "",
            collectionNodeSelected: false
        }

        this.formatDocListForRcTree = this.formatDocListForRcTree.bind(this);
        this.documentPreview = this.documentPreview.bind(this);
    }

    componentDidMount() {
        // this.getSolrTreeData();
        this.getCollectionsData();
        // axios.get("https://sp16-nba.thedigitalgroup.com:9091/api/v1/document/sharepoint/"+dgiUsername).then((docTreeDetails) => {
        //     let docTreeData = docTreeDetails.data;
        //     let rcTreeDocObj = this.formatDocListForRcTree(docTreeData, "");  
        //     this.setState({ mainTreeData: rcTreeDocObj });
        // })
    }

    componentDidUpdate (prevProps, prevState, snapshot) {
        if (prevProps.SPUserName !== this.props.SPUserName) {
            this.getCollectionsData();
        }
    }

    getCollectionsData = () => {
        let requestObj = {userEmail: this.props.userEmail}
        axios.post("/collections/getCollectionsTree", requestObj).then(collectionsData => {
          if (collectionsData !== undefined) {
            let collectionTreeData = CollectionHelper.collectionTreeAssembly(collectionsData.data, this.addRootCollection, this.addChildCollection, true);
            let collectionTree = {
              "key": CollectionHelper.randomStringGenerator(),
              "title": "Collections",
              "isLeaf": false,
              "isCollectionNode": true,
              "icon": <i className="fa fa-bars collection-bars" />,
              "children": collectionTreeData
            }
            this.getSolrTreeData(collectionTree);
          }
        })
    }

    getSolrTreeData = (collectionTree) => {
        this.setState({docLoading: true})
        let dgiUsername = this.props.dgiUserName.split("@")[0];
        let AT = this.props.accessToken.toString();
        let acl = [];
        if (this.props.boxUserId != '') {
            acl.push("U_" + this.props.boxUserId);
        }

        if (this.props.boxUserGroupIds.length !== 0) {
            acl = acl.concat(this.props.boxUserGroupIds);
        }
        let spUserName; // = "richas@dgi";
        if (this.props.SPUserName !== undefined && this.props.SPUserName !== "" && this.props.SPUserName !== null) {
            spUserName = this.props.SPUserName;
        }
        var solrRequest = {
            "searchQuery": "*:*",
            "accessToken": [AT],
            // "accessToken": ["nioA5tgsn6TPD8gT8wAyRoj63cGzoN6f"],
            "dataset": this.props.solrCollection,
            "client": this.props.client,
            "acl": acl,
            // "acl": ["U_13589537292"],
            "tokensFound": true,
            "docPath": "*",
            "manifoldUsername": spUserName
            // "manifoldUsername": "richas@dgi"
        }
        var compThis = this;
        axios.post("/solrTree/getTreeStructFromGroupedSolr", solrRequest).then((response) => {
            try {
                // return this.processResponseFromSolr(response, selectedKey, nodeTitle, serviceType, resolve);
                // let treeRoots = response.data.facet_counts.facet_fields.doc_path_hierarchy;
                let treeRoots = response.data.grouped.doc_path_hierarchy.groups;
                let treeLeafNodes = []; //response.data.response.docs;
                let toBeExpanded = [];
                let formattedTreeNodes = TreeDataHelper.formatSolrDataForTree(treeRoots, toBeExpanded, this.props.serviceTypes, treeLeafNodes, false);
                let nodeToBeselected = {
                    node: formattedTreeNodes.docTreeRoots[0]["children"][0]
                } 
                this.treeNodeSelected("", nodeToBeselected);
                compThis.setState({
                    isLoading: false,
                    solrTreeData: [collectionTree, ...formattedTreeNodes.docTreeRoots],
                    defaultExpanded: [collectionTree["key"], formattedTreeNodes.docTreeRoots[0]["children"][0]["key"]],
                    checkedKeys: ['0-0'],
                }, function () {
                    console.log("I am called")
                });
            }
            catch (err) {
                console.log(err);
                this.setState(
                    {
                        isLoading: false,
                    });
            }
        })
    }

    formatDocListForRcTree(docTreeData, currentFileName) {

        docTreeData.forEach((node, index) => {
            node.key = node["$id"];
            node.title = node["Name"];
            node.className = "all-node-wrap";
            if (this.state.defaultExpanded.length < 2) {
                let toBeExpanded = this.state.defaultExpanded;
                toBeExpanded.push(node["$id"]);
                this.setState({ defaultExpanded: toBeExpanded });
            }
            if (node.Members.length > 0) {
                node.children = node.Members
                this.formatDocListForRcTree(node.children, currentFileName);
            } else {
                let fileNExt = node.Name.split(".");
                if (fileNExt.length > 1) {
                    let fileExt = fileNExt[(fileNExt.length - 1)];
                    if (fileExt === "msg") {
                        node.icon = <i className="fa fa-envelope blue-icons" aria-hidden="true"></i>
                    } else if (fileExt === "doc" || fileExt === "docx") {
                        node.icon = <i className="fa fa-file-word-o blue-icons" aria-hidden="true"></i>
                    } else if (fileExt === "xls" || fileExt === "xlsx") {
                        node.icon = <i className="fa fa-file-excel-o blue-icons" aria-hidden="true"></i>
                    } else {
                        node.icon = <i className="fa fa-file-pdf-o blue-icons" aria-hidden="true"></i>
                    }
                }

                if (node.Name === currentFileName) {
                    node.className = "all-node-wrap file-node-from-tree"
                }

                node.isLeaf = true;
            }
        });

        return docTreeData;
    }

    treeNodeSelected = (selectedKeys, selectedNode) => {
        
        if (selectedNode.node.isLeaf) {
            if (document.getElementById("documentPreviewContainer") !== null) {
                document.getElementById("documentPreviewContainer").innerHTML = "";
            }
            this.setState({ showSecondCol: false, docLoading: true, selectedDocument: selectedNode.node });
            this.showDocPreview(selectedNode);
        } else {
            let subTree = [];
            if (selectedNode.node.isCollectionNode) {
                this.setState({collectionNodeSelected: true });
            } else {
                this.setState({collectionNodeSelected: false });
            }
            this.setState({
                pdfPath: "",
                docLoading: false,
                errorInPdf: false,
                showSecondCol: true
                /* defaultExpanded:  */
            });

            // this.loadTreeData(selectedNode.node).then(() => {
                //serviceType, selectedKey, nodeTitle, folderPath, resolve
                if (selectedNode.node.children !== undefined && selectedNode.node.children.length > 0) {
                    let secondColFld = [], secondColFiles = [];
                    selectedNode.node.children.forEach(childNode => {

                        // if (childNode.Source === "folder.png") {
                        if (!childNode.isLeaf) {
                            // secondColFld.author = childNode.Author? childNode.Author : "";
                            // secondColFld.fileSize = childNode.FileSize ? childNode.FileSize : "";
                            // secondColFld.modifiedOn = childNode.ModifiedOn;
                            secondColFld.push(childNode);
                        } else {
                            // secondColFiles.author = childNode.Author? childNode.Author : "";
                            // secondColFiles.fileSize = childNode.FileSize ? childNode.FileSize : "";
                            // secondColFiles.modifiedOn = childNode.ModifiedOn;
                            secondColFiles.push(childNode);
                        }
                    });
                    this.setState({
                        secondColumnFolders: secondColFld,
                        secondColumnFiles: secondColFiles,
                        showSecondCol: true
                    })

                } else { 
                    this.setState({
                        secondColumnFolders: [],
                        secondColumnFiles: [],
                        showSecondCol: true
                    })
                }
        }
    };

    getCollectionExplorer = (selectedCollection) => {
        /* console.log("selectedCollection");
        console.log(selectedCollection); */
        let allChildren = selectedCollection.children;
        allChildren.forEach(collection => {
            if (collection.isLeaf) {

            }
        })
    } 

    sendDocumentClickLibraryEvent = (id, selectedDoc) => {
        var params = {};
        var multiParams = {
            source_type_ss: this.props.source
          };
        params.recommandation_b = false;
        params.ContentType_s = "";
        params.SourceType_s = selectedDoc.serviceType;
        params.docurl_s = selectedDoc.url? selectedDoc.url : selectedDoc.solrId;
        this.searcheventRequest = {};
        this.searcheventRequest.event_status = 1;
        this.searcheventRequest.sessionid = this.props.sessionId;
        params.UserSubCategory_s = "";
        params.UserCategory_s = "";
        var clickEventData = {
            "query": selectedDoc.title,
            // "documentPosition": this.docPosition,
            "docId": id,
            "userIP": this.props.userIP,
            "eventStatus": this.searcheventRequest.event_status,
            "eventTimestamp": new Date().toISOString(),
            "appId": this.props.eventAppId,
            "user" : this.props.userEmail,
            "application": this.props.eventAppId,
            "sessionId": this.searcheventRequest.sessionid,
            "eventType": "Document Click",
            "params": params,
            "multiParams": multiParams
        };
        axios.post("/search/saveSearchEventData", clickEventData);
      }

    showDocPreview = (selectedNode) => {
        this.setState({ showSecondCol: false, docLoading: true });
        let nodeDetails = selectedNode.node;
        let fileId = nodeDetails.key;
        let docID = (fileId === null || fileId === undefined) ? 0 : parseInt(fileId);
        let sourceType = "SharePoint";
        if (nodeDetails.serviceType === "box") {
            sourceType = "Box"
            docID = (this.props.documentID === null || this.props.documentID === undefined)  ? 0 : this.props.documentID;
        }

        let fileName = ""
        if (selectedNode.node.isCollectionNode) {
            fileName = selectedNode.node.file_name[0]; 
            fileId = selectedNode.node.file_id;
        } else {
            fileName = selectedNode.node.title;
            fileId = selectedNode.node.key;
        }
        this.sendDocumentClickLibraryEvent(docID,  nodeDetails);

        let request = {
            source_type: sourceType,
            title: fileName,
            documentId: docID,
            ViewedBy: this.props.userEmail,
            AccessToken: this.props.accessToken.toString(),
            RefreshToken: this.props.refreshToken,
            FileID: fileId,
            ClientName: this.props.client
        };
        request["searchText"] = this.props.searchQuery == "" ? "":this.props.searchQuery;
        axios.post("/preview/docPreview", request)
            .then((response) => {
                this.setState({
                    loader: false,
                    docLoading: false
                });
                if (response.data["HtmlContent"]) {
                    const fragment = document
                        .createRange()
                        .createContextualFragment(response.data["HtmlContent"]);
                    document.getElementById("documentPreviewContainer").appendChild(fragment);
                }

                if (response.data["Message"]) {
                    // document.getElementById("documentPreviewContainer").appendChild(response.data["Message"]);
                    document.getElementById("documentPreviewContainer").innerHTML = "<span>" + response.data["Message"] + "</span>"
                }

            })
            .catch((error) => {
                this.setState({
                    loader: false,
                });
            });
    }

    folderDblClick = (selectedFld) => {
        let secondColFld = [], secondColFiles = [];
        if (selectedFld.children !== undefined) {
            selectedFld.children.forEach(subFld => {

                if (subFld.Source === "folder.png") {
                    // subFld.author = childNode.Author? childNode.Author : "";
                    // subFld.fileSize = childNode.FileSize ? childNode.FileSize : "";
                    // subFld.modifiedOn = childNode.ModifiedOn;
                    secondColFld.push(subFld);
                } else {
                    // secondColFiles.author = childNode.Author? childNode.Author : "";
                    // secondColFiles.fileSize = childNode.FileSize ? childNode.FileSize : "";
                    // secondColFiles.modifiedOn = childNode.ModifiedOn;
                    secondColFiles.push(subFld);
                }
            });
            this.setState({
                secondColumnFolders: secondColFld,
                secondColumnFiles: secondColFiles
            })
        }
    }

    documentPreview(documentId, fileExt, fileName) {
        this.setState({ showSecondCol: false, currentFileType: fileExt })
        // this.setState({pdfPath: "", docLoading: true, errorInPdf: false}, function () {
        //         var url = "https://" + window.location.hostname + "/getDocPreview/" + documentId;
        //         this.setState({
        //            pdfPath: url
        //        })
        // })
        var list = document.getElementById("dvContainer1");
        if (list && list.childNodes && list.childNodes[0]) {
            list.removeChild(list.childNodes[0]);
        }
        if (fileExt !== 'msg') {
            let request;
            let requestUrl = "";
            if (this.props.source.indexOf('Sharepoint') > -1) {
                request = {
                    "ConnectorSourceName": "SharePoint",
                    "DocumentId": documentId,
                    ViewedBy: this.props.userEmail,

                }
                //requestUrl = "https://sp16-nba.thedigitalgroup.com:9091/api/v1/getdoccontent/";
            }
            else {
                request = {
                    "ConnectorSourceName": "Box",
                    title: this.removeHTML(this.props.item.title),
                    documentId: 0,
                    ViewedBy: this.props.userEmail,
                    AccessToken: this.props.accessToken,
                    RefreshToken: this.props.refreshToken,
                    FileID: 708941880000
                };
            }
            request["ClientName"] = this.props.client;
            //Axios.post("/preview/docPreview", request).then(response => {
            axios.post("/preview/docPreview", request).then(response => {

                const fragment = document
                    .createRange()
                    .createContextualFragment(response.data["HtmlContent"]);
                document.getElementById('dvContainer1').appendChild(fragment);

                let request;
                let requestUrl = "";
                if (this.props.source.indexOf('Sharepoint') > -1) {
                    request = {
                        "ConnectorSourceName": "SharePoint",
                        "DocumentId": documentId
                    }
                    requestUrl = this.props.edocsBackendAPI+"api/v1/docversionhistory/";
                }
                request["ClientName"] = this.props.client;
                //get doc versions
                axios.post(requestUrl, request).then(response => {
                    //   axios.get("/docTree/getDocVersions/" + this.props.documentId).then(response => {
                    let versionArray = response && response.data ? response.data : [];
                    this.setState({
                        versionArray: versionArray
                    })
                })
            })
        } else {
            this.setState({ pdfPath: "", docLoading: true, errorInPdf: false }, function () {
                // var url = "https://" + window.location.hostname + "/getDocPreview/" + documentId;
                // if(this.props.source.indexOf('Sharepoint') >-1) {
                //     url += '/'+'SharePoint';
                //   }
                //   url += '/'+'this.props.client';
                //     this.setState({
                //         pdfPath: url
                //     })
                let request;
                let requestUrl = "";
                let docID = documentId == null ? 0 : documentId;
                request = {
                    Title: fileName ? fileName : "",
                    documentId: docID,
                    ViewedBy: this.props.userEmail,
                    ViewedBy: this.props.userEmail,
                    AccessToken: this.props.accessToken,
                    RefreshToken: this.props.refreshToken,
                    FileID: 750515727382
                };

                request["ClientName"] = this.props.client;
                axios.post("/preview/emailPreview", request).then(response => {

                    this.setState({
                        loader: false,
                    });
                    this.setState({
                        pdfPath: response.data.FilePath,
                        currentFileType: fileExt,
                        // currentItem: this.props.item,
                    });
                })

                let request1;
                let requestUrl1 = "";
                if (this.props.source.indexOf('Sharepoint') > -1) {
                    request1 = {
                        "ConnectorSourceName": "SharePoint",
                        "DocumentId": documentId
                    }
                    requestUrl1 = this.props.edocsBackendAPI+"api/v1/docversionhistory/";
                }
                request1["ClientName"] = this.props.client;
                //get doc versions
                axios.post(requestUrl1, request1).then(response => {
                    // axios.get("/docTree/getDocVersions/" + this.props.documentId).then(response => {
                    let versionArray = response && response.data ? response.data : [];
                    this.setState({
                        versionArray: versionArray
                    })
                })
            })
        }
    }

    getTreeFromSolr = (serviceType, selectedKey, nodeTitle, folderPath, resolve) => {
        let AT = this.props.accessToken.toString();
        let acl = [];
        if (this.props.boxUserId != '') {
            acl.push("U_" + this.props.boxUserId);
        }

        if (this.props.boxUserGroupIds.length !== 0) {
            acl = acl.concat(this.props.boxUserGroupIds);
        }
        let spUserName; // = "richas@dgi";
        if (this.props.SPUserName !== undefined && this.props.SPUserName !== "" && this.props.SPUserName !== null) {
            spUserName = this.props.SPUserName;
        }

        var solrRequest = {
            "searchQuery": "*:*",
            // "serviceType": this.props.serviceType,
            "serviceType": serviceType,
            "accessToken": [AT],
            "dataset": this.props.solrCollection,
            "client": this.props.client,
            "acl": acl,
            "tokensFound": acl.length > 0 ? true : false,
            "docPath": folderPath,
            "manifoldUsername": spUserName,
            // "source_type": this.props.serviceType
            "source_type": serviceType
        }

        return axios.post("/solrTree/getTreeStructFromSolr", solrRequest).then((response) => {
            try {
                return this.processResponseFromSolr(response, selectedKey, nodeTitle, serviceType, resolve);
            }
            catch (err) {
                console.log(err);
                this.setState(
                    {
                        isLoading: false,
                    });
            }
        })
    }

    processResponseFromSolr = (solrData, selectedKey, nodeTitle, serviceType, resolve) => {

        if (selectedKey !== "sptMainRoot" && selectedKey !== "bctMainRoot") {

            let solrDocs = solrData.data.response.docs;
            var currentTreeData = [...this.state.solrTreeData];
            let elementFromTree;
            currentTreeData.forEach(treeNode => {
                let elementFromTreeCheck = TreeDataHelper.searchTree(treeNode, nodeTitle);
                if (elementFromTreeCheck !== null) {
                    elementFromTree = elementFromTreeCheck;
                }
            });

            solrDocs.forEach((eachDocs, index) => {
                let nodeTitleShow = "";
                // if (nodeTitleShow == undefined)
                if (eachDocs.title !== undefined) {
                    nodeTitleShow = eachDocs.title[0];
                } else if (eachDocs.title_substitute !== undefined) {
                    nodeTitleShow = eachDocs.title_substitute[0];
                }
                let fileUrl = eachDocs.doc_path;
                let filePathArray = fileUrl.split("/");
                let fileName = filePathArray[filePathArray.length - 1];
                let nodeIcon = <i className="fa fa-file-pdf-o blue-icons" aria-hidden="true"></i>
                let fileNExt = fileName.split(".");

                if (fileNExt.length > 1) {
                    let fileExt = fileNExt[1];
                    nodeIcon = TreeDataHelper.getFileExtIcon(fileExt);
                    
                }/*  else {

                } */
                // dateToString
                let dateToShow = ""
                if (eachDocs.doc_modified !== undefined) {
                    dateToShow = TreeDataHelper.dateToString(eachDocs.doc_modified);
                } else {
                    if (eachDocs.last_modified !== undefined) {
                        dateToShow = TreeDataHelper.dateToString(eachDocs.last_modified);
                    }
                }

                let fileSize = "";
                if (eachDocs.doc_size !== undefined) {
                    fileSize = (eachDocs.doc_size / 1024).toFixed(0);
                }
                nodeTitleShow = fileName;
                let fileIdKey = index + nodeTitle
                if (eachDocs.file_id !== undefined) {
                    fileIdKey = eachDocs.file_id
                }
                let nodeToFld = {
                    key: fileIdKey,
                    title: decodeURI(nodeTitleShow),
                    url: eachDocs.url,
                    isLeaf: true,
                    fileId: eachDocs.id,
                    dateMod: dateToShow,
                    file_type: eachDocs.file_type,
                    last_author: eachDocs.last_author,
                    fileSize: fileSize + " KB",
                    Source: "file",
                    serviceType: eachDocs.source_type,
                    // doc_modified
                    icon: nodeIcon
                }
                elementFromTree.children.push(nodeToFld);
            });
            this.setState({
                solrTreeData: currentTreeData
            });
            resolve();
        } else {
            // let treeRoots = solrData.data.facet_counts.facet_fields.doc_path_hierarchy;
            let treeRoots = solrData.data.grouped;
            let toBeExpanded = this.state.defaultExpanded;
            let formattedTreeNodes = TreeDataHelper.formatSolrDataForTree(treeRoots, toBeExpanded, serviceType);
            var currentTreeData = [...this.state.solrTreeData];

            currentTreeData.forEach(treeNode => {
                let elementFromTree = TreeDataHelper.searchTree(treeNode, nodeTitle);
                if (elementFromTree !== null) {
                    elementFromTree.children.push(...formattedTreeNodes.docTreeRoots)
                }
            })

            this.setState({
                solrTreeData: currentTreeData,
                defaultExpanded: formattedTreeNodes.toBeExpanded
            }, function () {
                this.setState({
                    solrTreeData: currentTreeData,
                    defaultExpanded: formattedTreeNodes.toBeExpanded
                })
            })
            resolve();
        }

    }

    loadTreeData = (treeNode) => {
        let strFirstThree = treeNode.key.substring(0, 3);
        //sptMainRoot bctMainRoot
        let serviceType = "";
        if (strFirstThree === "spt") {
            serviceType = "sharepoint"
        } else if (strFirstThree === "bct") {
            serviceType = "box";
        } else {
            serviceType = treeNode.serviceType;
        }
        const solrTreeData = [...this.state.solrTreeData];

        return new Promise(resolve => {
            if (treeNode.isCollectionNode) {
                resolve();
            } else {
                if (treeNode.title === "Box.com" || treeNode.title === "SharePoint 2016") {
                    resolve();
                } else {
                    if (treeNode.key === "sptMainRoot" || treeNode.key === "bctMainRoot") {
                        let folderPath;
                        return this.getTreeFromSolr(serviceType, treeNode.key, treeNode.title, folderPath, resolve);
                        // this.setState({ solrTreeData });
                        // resolve();
                    } else {
                        let folderPath = treeNode.allFldPath;
                        this.getTreeFromSolr(serviceType, treeNode.key, treeNode.title, folderPath, resolve);
                    }
                }
            }
        });
    }

    getNewTreeData = (solrTreeData, nodeKey, nodeTitle) => {
        let strFirstThree = nodeKey.substring(0, 3);
        //sptMainRoot bctMainRoot
        let serviceType = "";
        if (strFirstThree === "spt") {
            serviceType = "sharePoint"
        } else if (strFirstThree === "bct") {
            serviceType = "box";
        }
        return new Promise(resolve => {
            setTimeout(() => {
                const treeData = [...this.state.treeData];
                this.getTreeFromSolr("sharePoint", nodeKey, nodeTitle, solrTreeData);
                this.setState({ treeData });
                resolve();
            }, 500);
        });
    }

    getParent = (root, id, allNodes) => {
        var i, node;
        for (var i = 0; i < root.length; i++) {
            node = root[i];
            if (node.key === key || node.children && (node = this.getParent(node.children, id))) {
                // return node; sptMainRoot bctMainRoot
                allNodes.push(node);
                if (node.key !== "sptMainRoot" && node.key !== "bctMainRoot") {
                    this.getParent(this.state.solrTreeData, node.key, allNodes)
                } else {
                    return allNodes;
                }
            }
        }
        return null;
    }

    goUpFolder = () => {
        console.log("Going up the folders")
    }

    collectionNameOnChange = (event) => {
        this.setState({
            newCollectionName: event.currentTarget.value
        })
    }

    checkIfEnterHit = (event, parentId) => {
        if(event.keyCode == 13) {
            this.setState({isLoading: true});
            let collectionObject = {
                name: event.target.value,
                parent_id: parentId,
                document_info: "[]",
                owner: this.props.userEmail,
                created_date: new Date(),
                modified_by: this.props.userEmail,
                modified_date: new Date()
            }
            axios.post("/collections/addNewCollection", collectionObject).then(addResult => {
                this.setState({
                    newCollectionName: ""
                })
                this.getCollectionsData();
            })
        } else if (event.keyCode == 27) {
            this.getCollectionsData();
        }
    }

    addRootCollection = () => {
        let collectionTreeData = [...this.state.solrTreeData];
        let newRootCollection = <span><input 
                            type="text"
                            name="collectionName"
                            placeholder="Collection Name"
                            className="collection-input-boxes"
                            /* value={this.state.newCollectionName} */
                            onChange={this.collectionNameOnChange}
                            autoFocus
                            onKeyDown={(event) => this.checkIfEnterHit(event, 0)} /> 
            </span>
        let newKey = CollectionHelper.randomStringGenerator();
        
        collectionTreeData[0]["children"][0] = {
            "id": "0",
            "key": newKey,
            "title": newRootCollection, //collection.name,
            "parentId": "",
            "isLeaf": true,
            "icon": <i className="fa fa-bars collection-bars" />
        }
        
        this.setState({
            selectedKeys: [String(newKey)],
            solrTreeData: collectionTreeData,
        })
    }

    addChildCollection = (parentCollection) => {
        let collectionTreeData = [...this.state.solrTreeData];
        let currentParentNode = {}; 
        collectionTreeData.forEach(collection => {
            currentParentNode = CollectionHelper.searchTreeWithId(collection, parentCollection.id)
            if (currentParentNode !== null) {
                if (currentParentNode !== null) {
                    let newCollection = <span><input 
                                    type="text"
                                    name="collectionName"
                                    placeholder="Collection Name"
                                    className="collection-input-boxes"
                                    onChange={this.collectionNameOnChange}
                                    autoFocus
                                    onKeyDown={(event) => this.checkIfEnterHit(event, parentCollection.id)} /> 
                    </span>
                    let newKey = CollectionHelper.randomStringGenerator();
                    let treeNodeForAdd = {
                        "id": "0",
                        "key": newKey,
                        "title": newCollection,
                        "parentId": "",
                        "isLeaf": true,
                        "icon": <i className="fa fa-bars collection-bars" />
                    }
                    currentParentNode.children[0] = treeNodeForAdd;
        
                    this.setState({selectedKeys: [String(newKey)], solrTreeData: collectionTreeData});
                }
            }
        });
    }

    render() {
        let loaderClass = "loader";
        if (this.props.isFromSettings) {
            loaderClass = loaderClass + " adjust-height"
        }
        let mainTreeStructure = <img src={this.props.loaderIcon} className={loaderClass}></img>
        let secondTreeList = "";
        let firstColumnClass = "col-md-3 doc-lib-col doc-lib-first-col center-align-content";

        let selectedDoc = <div className="select-file-for-preview"> Select file for preview. </div>
        if (this.state.docLoading) {
            selectedDoc = <img src={this.props.loaderIcon} className="loader"></img>;
        }

        if (this.state.solrTreeData.length > 0) {
            firstColumnClass = "col-md-3 doc-lib-col doc-lib-first-col";
            const { solrTreeData } = this.state;
            mainTreeStructure = <Tree
                className="myCls"
                showLine
                autoExpandParent={true}
                // onExpand={this.onExpand}
                treeData={solrTreeData}
                onSelect={this.treeNodeSelected}
                defaultExpandedKeys={this.state.defaultExpanded}
                loadData={this.loadTreeData}
            />
        }

        let today = new Date();
        let dd = String(today.getDate()).padStart(2, '0');
        let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
        let yyyy = today.getFullYear();

        today = mm + '/' + dd + '/' + yyyy;
        let folderList = "", fileList = "", modifiedBy = "Amit D";
        if (this.state.secondColumnFolders.length > 0) {
            folderList = this.state.secondColumnFolders.map((folder, index) => {
                if (index === 0) {
                    return false;
                }
                return <div key={folder.key} className="row doc-list-row" onDoubleClick={() => this.folderDblClick(folder)}>
                    <div className="col-md-5 mouse-pointer">
                        {!this.state.collectionNodeSelected && <span><i className="fa fa-folder folder-icon" aria-hidden="true"></i> &nbsp;{shortStringWithTitle(folder.title, 20)}</span>}
                        {this.state.collectionNodeSelected && <span><i className="fa fa-bars folder-icon" aria-hidden="true"></i> &nbsp; {shortStringWithTitle(folder.title, 20)}</span>}
                    </div>
                    <div className="col-md-2 mouse-pointer">
                        {folder.fileSize} files
                    </div>
                    <div className="col-md-3 mouse-pointer">
                        {/* {today} */}
                        {shortStringWithTitle(folder.dateMod, 10)}
                    </div>
                    <div className="col-md-2 mouse-pointer">
                        {/* {modifiedBy} */}
                        {shortStringWithTitle(folder.Author, 5)}
                    </div>
                </div>
            })
        }

        if (this.state.secondColumnFiles.length > 0) {

            fileList = this.state.secondColumnFiles.map(file => {
                let fileNode = {};
                console.log("file")
                console.log(file)
                fileNode.node = file;
                let fileNameNExt = [];
                if (typeof file.title === "object") {
                    fileNameNExt = file.title[0].split(".");
                } else {
                    fileNameNExt = file.title.split(".");
                }
                let fileExt = fileNameNExt[(fileNameNExt.length - 1)];
                let fileIcon = TreeDataHelper.getFileExtIcon(fileExt);

                let fileSize = file.fileSize
                if (file.fileSize === undefined) {
                    fileSize = TreeDataHelper.getFileSizeFormatted(file.doc_size)
                }
                
                let modifiedDate = file.modifiedDate;
                if (file.modifiedDate === undefined) {
                    modifiedDate = file.modified;
                }

                return <div key={file.key} id={file.key} onClick={() => this.showDocPreview(fileNode)} className="row doc-list-row">
                    <div className="col-md-5 mouse-pointer">
                        {fileIcon} &nbsp;{shortStringWithTitle(fileNameNExt[0], 20)}
                    </div>
                    <div className="col-md-2 mouse-pointer">
                        {/* {modifiedBy} */}
                        {fileSize}
                    </div>
                    <div className="col-md-3 mouse-pointer">
                        {/* {today} */}
                        {TreeDataHelper.dateToString(modifiedDate)}
                    </div>
                    <div className="col-md-2 mouse-pointer">
                        {shortStringWithTitle(file.last_author[0], 10)}
                    </div>
                </div>
            })
        }

        if (!this.state.docLoading && this.state.secondColumnFolders.length === 0 && this.state.secondColumnFiles.length === 0) {
            folderList = <div  className="row doc-list-row" >
                    <div className="col-md-5 mouse-pointer">
                        This folder is empty
                    </div>
                </div>
        }

        let classForLastCol = "col-md-6 doc-lib-col doc-lib-third-col"
        return (
            <div className="container">
                <div className="row">
                    <div className={firstColumnClass}>
                        {mainTreeStructure}
                    </div>
                    {this.state.showSecondCol && <div className="col-md-6 doc-lib-col">
                        <div className="row">
                            <div className="col-md-5 header-row">Name</div>
                            <div className="col-md-2 header-row">File Size</div>
                            <div className="col-md-3 header-row">Modified Date</div>
                            <div className="col-md-2 header-row">Modified By</div>
                        </div>
                        {folderList}
                        {fileList}
                    </div>}
                    {!this.state.showSecondCol && <div id="document-preview-container" className={classForLastCol}>
                        <ul className="nav nav-tabs">
                            <li className="active"><a data-toggle="tab" href="#properties">Properties</a></li>
                            {!this.props.isFromSettings && <li><a data-toggle="tab" href="#home">Preview</a></li>}
                            {!this.props.isFromSettings && <li><a data-toggle="tab" href="#menu1">Versions</a></li>}
                            {!this.props.isFromSettings && <li><a data-toggle="tab" href="#menu2">Share</a></li>}
                        </ul>
                        <div class="tab-content">
                            <div id="properties" class="tab-pane fade in active">
                                <div className="row"></div>
                                <div className="row">
                                    <div className="col-md-4 doc-prop-label">Name </div>
                                    <div className="col-md-8 doc-prop-desc">{this.state.selectedDocument.title}</div>
                                </div>
                                <hr className="less-margin" />
                                <div className="row">
                                    <div className="col-md-4 doc-prop-label">Title </div>
                                    <div className="col-md-8 doc-prop-desc">{this.state.selectedDocument.title}</div>
                                </div>
                                <hr className="less-margin"/>
                                {this.state.selectedDocument.tags !== undefined && <div className="row">
                                    <div className="col-md-4 doc-prop-label">Tags </div>
                                    <div className="col-md-8 doc-prop-desc">{TreeDataHelper.getAllTags(this.state.selectedDocument.tags)}</div>
                                </div>}
                                {this.state.selectedDocument.tags !== undefined && <hr className="less-margin"/>}
                                {this.state.selectedDocument.fileSize !== undefined && <div className="row">
                                    <div className="col-md-4 doc-prop-label">Size </div>
                                    <div className="col-md-8 doc-prop-desc">{this.state.selectedDocument.fileSize}</div>
                                </div>}
                                {this.state.selectedDocument.fileSize !== undefined && <hr className="less-margin"/>}
                                {this.state.selectedDocument.modifiedDate !== undefined && <div className="row">
                                    <div className="col-md-4 doc-prop-label">Modified Date </div>
                                    <div className="col-md-8 doc-prop-desc">{TreeDataHelper.dateToString(this.state.selectedDocument.modifiedDate)}</div>
                                </div>}
                                {this.state.selectedDocument.modifiedDate !== undefined && <hr className="less-margin"/>}
                                {this.state.selectedDocument.last_author !== undefined && <div className="row">
                                    <div className="col-md-4 doc-prop-label">Modified By </div>
                                    <div className="col-md-8 doc-prop-desc">{this.state.selectedDocument.last_author}</div>
                                </div>}
                                {this.state.selectedDocument.last_author !== undefined && <hr className="less-margin"/>}
                                {this.state.selectedDocument.last_author !== undefined && <div className="row">
                                    <div className="col-md-4 doc-prop-label">Created By </div>
                                    <div className="col-md-8 doc-prop-desc">{this.state.selectedDocument.last_author}</div>
                                </div>}
                                {this.state.selectedDocument.last_author !== undefined && <hr className="less-margin"/>}
                                {this.state.selectedDocument.creation_date !== undefined && <div className="row">
                                    <div className="col-md-4 doc-prop-label">Created On </div>
                                    <div className="col-md-8 doc-prop-desc">{TreeDataHelper.dateToString(this.state.selectedDocument.creation_date)}</div>
                                </div>}
                            </div>
                            <div id="home" class="tab-pane fade">
                                {this.state.currentFileType == 'msg' &&
                                selectedDoc
                                }
                                {this.state.currentFileType !== 'msg' &&
                                    <div id="documentPreviewContainer" style={{ height: "95vh"}} className="col-md-12 doc-lib-col lib-preview-panel">
                                        {this.state.docLoading && selectedDoc}
                                    </div>
                                }
                            </div>
                            <div id="menu1" class="tab-pane fade">
                                <p>Versions</p>
                            </div>
                            <div id="menu2" class="tab-pane fade">
                                <p>Share</p>
                            </div>
                        </div>
                        
                    </div>}
                </div>
            </div>
        )
    }
}

function dateToString(dateString) {
    let dateModified = "", dateToShow = "";
    if (dateString !== undefined) {
        dateModified = new Date(dateString);
        let month = '' + (dateModified.getMonth() + 1);
        let day = '' + dateModified.getDate();
        let year = dateModified.getFullYear();
        let hour = dateModified.getHours();
        let minutes = dateModified.getMinutes();
        dateToShow = day + "-" + month + "-" + year + " " + hour + ':' + minutes
    }

    return dateToShow;
}

function shortStringWithTitle(stringToShow, charCount) {
    /* if (charCount === undefined) {
        charCount = 20
    } */
    if (stringToShow !== undefined) {
        let stringDisplay = (stringToShow.length > charCount) ? stringToShow.substr(0, charCount - 1) + '...' : stringToShow;
        return <span title={stringToShow}>{stringDisplay}</span>
    } else {
        return ""
    }

}

export default DocumentLibrarySolr