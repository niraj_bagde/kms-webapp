import React, { useState } from "react";
import "./app.loginForm.css";
import { useHistory } from "react-router-dom";
import MicrosoftLogin from "react-microsoft-login";
import nbalogo from '../images/NBA.png';
import alnylamLogo from '../images/alnylam-logo-icon.png';
import eDocsLogo from '../images/edocs-logo.png';
import eDocsAlnylamLogo from '../images/Logo_Alnylam_5.png';


const LoginForm = (props) => {
    const history = useHistory();
    const loginClick = () => {
        props.onLogin("Richa Sinha", "richa.sinha@thedigitalgroup.com", props.client);
        history.push("/search");
    }
    const authHandler = (err, data) => {
      // console.log(err, data);
      var name, username;

      if(data && data.authResponseWithAccessToken && data.authResponseWithAccessToken.account) {
        name = data.authResponseWithAccessToken.account.name;
        username = data.authResponseWithAccessToken.account.userName;
      }
      props.onLogin(name, username, props.client);
      setTimeout( ()=>{
        if(!err)
        history.push("/search");
      }, 100)

    };
    return (
    <div className="login-bg">
      <div className="container-fluid login">
        <div className="row login-page">
          <div className="col-md-12 login-header-alnylam">
            <div className="col-lg-6 col-md-6 col-sm-6">
              <h1 className="logo" style={{"marginLeft": "0"}}>
              {/* { props.client=='alnylam' &&
                <span>
                  <span style={{"marginRight": "0px"}}><img src={alnylamLogo} alt="Alnylam Logo" style={{"height":"60px"}} /></span>
                  <img src={eDocsAlnylamLogo} alt="eDocs Logo" />
                </span>
              } */}
              {/* { props.client!=='alnylam' && */}
                <span>
                  {/* <span style={{"marginRight": "10px"}}><img src={nbalogo} alt="NBA Logo" style={{"height":"90px"}} /></span> */}
                  <img src={eDocsAlnylamLogo} alt="eDocs Logo" />
                </span>

              {/* } */}
              
              </h1>
            </div>
            { <button className="btn btn-lg" type="submit"
                  onClick={(e) => loginClick()}
                >Login</button> }
            <div className="col-lg-6 col-md-6 col-sm-6 text-right">
              <div style={{"paddingTop": "3px"}}>
                  {/* <MicrosoftLogin clientId="ace32814-5422-4d03-8a7a-855d3b621bf7" 
                                  authCallback={authHandler} 
                                  prompt="login"
                                  redirectUri="http://localhost:8985/search"
                                  buttonTheme="light_short"
                                  /> */}
                  <MicrosoftLogin clientId={props.azureClientId} 
                    authCallback={authHandler} 
                    prompt="login"
                    redirectUri={props.redirectUri}
                    buttonTheme="dark"
                    className="MS-login"
                    />
                </div>
            </div>
          </div>
          {/*<div className="col-lg-8 col-md-6 col-sm-6 hidden-xs login-info">
              <h1 className="logo" style={{"marginLeft": "30px"}}><img src={"/public/src/images/edocs-logo.png"} alt="eDocs Logo" /></h1>
              <h1 className="tagline">Document &amp; Email Management </h1>
          </div>
          <div className="col-lg-4 col-md-6 col-sm-6 login-form">
            <p className="t1"><span className="blue">NBA eDocs</span></p>
              <form className="form-signin">
              <h2 className="form-signin-heading">Login</h2>
              <div>
                  <label className="">Username</label>
                  <input type="email" id="Username" className="form-control" placeholder="Username" autoFocus />
                  <i className="fa fa-envelope ico"></i>
              </div>
                <br />
                <div>
                    <label className="">Password</label>
                    <input type="password" id="inputPassword" className="form-control" placeholder="Password" />
                    <i className="fa fa-key ico"></i>
                </div>
                <div style={{"margin": "15px 0"}}>
                    <input type="checkbox" /> Remember me
                    <a href="#" className="pull-right">Forgot Password</a>
                </div>
                <button className="btn btn-lg" type="submit"
                  onClick={(e) => loginClick()}
                >Login</button> 
                <MicrosoftLogin clientId="ace32814-5422-4d03-8a7a-855d3b621bf7" 
                                  authCallback={authHandler} 
                                  prompt="login"
                                  redirectUri="https://edocs-nba.thedigitalgroup.com/search"
                                  buttonTheme="light_short"
                                  > 
                
                <button className="btn btn-lg"
                onClick={(e) => loginClick()}
                ><span className="microsoft-icon"></span>&nbsp;Sign in</button>
                </MicrosoftLogin> 
                <div style={{"marginTop": "10px"}}>
                  <MicrosoftLogin clientId="ace32814-5422-4d03-8a7a-855d3b621bf7" 
                                  authCallback={authHandler} 
                                  prompt="login"
                                  redirectUri="http://localhost:8985/search"
                                  buttonTheme="light_short"
                                  />
                  <MicrosoftLogin clientId="ace32814-5422-4d03-8a7a-855d3b621bf7" 
                    authCallback={authHandler} 
                    prompt="login"
                    redirectUri="https://edocs-nba.thedigitalgroup.com/search"
                    buttonTheme="dark"
                    className="MS-login"
                    />
                </div>

            </form>
          </div>*/}
        </div>
      </div>

      </div>
)
}
export default LoginForm;