import React, { Component } from "react";
import "./app.version.css";
import {editVersionDocument} from "../helpers/openVersionDocForEdit";
class AppDocVersions extends Component {
  constructor(props) {
    super(props);
  }

//   getVersionDocumentURL(docid, versionid, filepath) {
//     var searchRequest = {
//       DocumentId: docid,
//       VersionDocumentId: versionid,
//     };

//     axios
//       .post(
//         "https://sp16-nba.thedigitalgroup.com:9091/api/v1/downloadVersionHistorydoc",
//         searchRequest
//       )
//       .then((response) => {
//         if (response && response.data && response.data.FilePath) {
      
//           var url = "";
//           let fileNExt = filepath.split(".");

//           if (fileNExt.length > 1) {
//             let fileExt = fileNExt[fileNExt.length - 1];
//             if (fileExt == "doc" || fileExt == "docx") {
//               url = "ms-word:ofe|u|" + response.data.FilePath;
//             } else if (fileExt == "xls" || fileExt == "xlsx") {
//               url = "ms-excel:ofe|u|" + response.data.FilePath;
//             } else if (fileExt == "ppt" || fileExt == "pptx") {
//               url = "ms-powerpoint:ofe|u|" + response.data.FilePath;
//             } else {
//               url = "ms-word:ofe|u|" + response.data.FilePath;
//             }
//             var mylink = document.getElementById("MyLink");
//             mylink.setAttribute("href", url);
//             mylink.click();
//           }
//         }
//       });
//   }
getFileExtension(url) {
  var matches = url.match(/\/([^\/?#]+)[^\/]*$/);
  if (matches && matches.length > 1) {
    let fileName = matches[1].split(".");
    if (fileName !== undefined && fileName.length > 0) {
      let fileExtension = fileName[fileName.length - 1];
      return fileExtension;
    } else {
      return matches[1];
    }
  }
  return null;
}
  render() {
    return (
      <div>
        <a id="MyLink" style={{ display: "none" }}></a>
        <div style={{ marginBottom: "10px", marginLeft: "15px" }}>
          <h4 className="version-heading">Version History</h4>
        </div>
        {this.props.versions.length==0 && 
        <div style={{"margin-top": "30px", "text-align": "center"}}>
          <span className="coming_soon_text">MVP 1.0 Release...</span>
          </div>
        }
        {this.props.versions.map((item, index) => {
         let fileExtension = this.getFileExtension(item.EncodedAbsUrl);
         let itemClass = "fa fa-bookmark";
         if (fileExtension == "pdf" || fileExtension == "msg" ) {
           itemClass = "displaynone";
         }
         else{
           itemClass="editdocument"
         }
         return(
         <div className="infobx-wrapper">
            <div
              className={
                item.IsCurrentVersion == "true"
                  ? "currentVersion"
                  : "versionbox"
              }
              title={item.IsCurrentVersion == "true" && "Current Version"}
            >
              V{item.VersionLabel}
            </div>
            <div className="version-top-content">
              <span
                className={
                  item.IsCurrentVersion == "true"
                    ? "currentVersion-hilight"
                    : "version-hilight"
                }
              >
                Version {item.VersionLabel}
              </span>{" "}
              by
              <span className="txt-underline">
                {item.CreatedBy == "System Account" ||
                item.CreatedBy == "SHAREPOINT\\system"
                  ? "Richa Sinha"
                  : item.CreatedBy}
              </span>
            
            </div>
            <div className="version-bottom-content" style={{ float: "right" }}>
              <span>
                <i
                  class="fa fa-calendar"
                  aria-hidden="true"
                  style={{ marginRight: "5px" }}
                ></i>
              </span>
              <label>Modified:&nbsp;</label>
              {item.Created}
            </div>


            <div className={itemClass}>
            <span 
                onClick={(e) =>
                    editVersionDocument(
                    this.props.documentId,
                    item.DocumentId,
                    item.EncodedAbsUrl,
                    this.props.source,
                    this.props.client,
                    this.props.edocsBackendAPI
                  )
                }
              >
                <a>
                  <i
                    class="fa fa-pencil-square-o"
                    aria-hidden="true"
                    style={{ fontSize: "20px" }}
                  ></i>
                </a>
              </span>
            </div>

            <div
              className="version-top-content"
              title="Title"
              style={{ marginBottom: "0px" }}
            >
              <label>Title:&nbsp;</label>
              {item.Title}
            </div>
            <div className="version-comments-box">
              <label>Comments:&nbsp;</label>
              {item.Comments}
            </div>
          </div>
    )})}
      </div>
    );
  }
}

export default AppDocVersions;
