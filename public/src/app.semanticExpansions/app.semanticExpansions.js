import React, { Component } from "react";
import { Modal, Row, Container, Col, Button, Form } from "react-bootstrap";
import "./app.semanticExpansions.css";

var utilsJs = require('../helpers/utils');
var utils = new utilsJs();

class AppExpansions extends Component {
    constructor(props) {
      super(props);
      this.state = {
        tokenMap: props.tokenMap ? props.tokenMap : {}
        // totalExpansionCount: 0,
      };
    }

    componentWillMount() {
        if(this.props.pipelineTokens && 
                this.props.pipelineTokens.length>0 && 
                    Object.keys(this.props.tokenMap).length==0) {
                        this.createTokenMap(this.props.pipelineTokens)
                    }
        this.showTokenRow(this.props.clickedtoken)
    }

    createTokenMap = (tokenArray) => {
        var totalExpansionCount = this.state.totalExpansionCount;
        for( var itemIndex=0; itemIndex < tokenArray.length; itemIndex++){
            if(tokenArray[itemIndex].concept && tokenArray[itemIndex].concept.graph)
                this.generateExpansions(tokenArray[itemIndex].concept.graph, tokenArray[itemIndex].token);

            if(tokenArray[itemIndex].entity && tokenArray[itemIndex].entity.extra)
                this.generateExpansions(tokenArray[itemIndex].entity.extra, tokenArray[itemIndex].token)

            if(tokenArray[itemIndex].relations && tokenArray[itemIndex].relations.length > 0) {
                for ( var i=0; i < tokenArray[itemIndex].relations.length; i++ ) {
                    this.currentSourceName = tokenArray[itemIndex].relations[i].name
                    this.generateExpansions(tokenArray[itemIndex].relations[i].graph, tokenArray[itemIndex].token)
                }
            }
            //extracting smart expansions
            if(tokenArray[itemIndex].iris) {
                var smartExpansions = [];
                    if(tokenArray[itemIndex].iris['variants'].length>0)
                        smartExpansions = this.createSourceExpansions(tokenArray[itemIndex].iris['variants'], smartExpansions, 'variants', tokenArray[itemIndex].token);

                    if(tokenArray[itemIndex].iris['acronyms'].length>0)
                        smartExpansions = this.createSourceExpansions(tokenArray[itemIndex].iris['acronyms'], smartExpansions, 'acronyms', tokenArray[itemIndex].token);

                    if(tokenArray[itemIndex].iris['related'].length>0)
                        smartExpansions = this.createSourceExpansions(tokenArray[itemIndex].iris['related'], smartExpansions, 'related', tokenArray[itemIndex].token);
                if(smartExpansions.length>0 ){
                    this.pushInTokenmap(smartExpansions, 'Machine Learning Enrichment', tokenArray[itemIndex].token);
                    totalExpansionCount += smartExpansions.length;
                }
            }
        }
        this.setState({"totalExpansionCount": totalExpansionCount})
    }

    generateExpansions = (tokenGraphObject, tokenName) => {
        for ( var i=0; i < tokenGraphObject.length; i++ ) {
            this.extractExpansions(tokenGraphObject[i], tokenName);
        }
    }

    extractExpansions = (graphObject, tokenName) => {
        var totalExpansionCount = this.state.totalExpansionCount;
        var sourceExpansions = [];
        var expansionObject = {};
        if(graphObject.expansions) {
            expansionObject = graphObject.expansions
        } else if (graphObject.expansion){
            expansionObject = graphObject.expansion
        }
        for(var key in expansionObject){
              if(key.match('standardname$') !== null){
                  var standardObject = expansionObject[key] ? expansionObject[key] : null;
                  if(standardObject) {
                      sourceExpansions = this.createSourceExpansions(standardObject, sourceExpansions, 'standardname', tokenName);
                  }
              }else if(key.match('synonyms$') !== null){
                  sourceExpansions = this.createSourceExpansions(expansionObject[key], sourceExpansions, 'synonyms', tokenName);
              }else if(key.match('related$') !== null){
                  sourceExpansions = this.createSourceExpansions(expansionObject[key], sourceExpansions, 'related', tokenName);
              }else if(key.match('narrower$') !== null){
                  sourceExpansions = this.createSourceExpansions(expansionObject[key], sourceExpansions, 'narrower', tokenName)
              }else{
                  sourceExpansions = this.createSourceExpansions(expansionObject[key], sourceExpansions, 'related', tokenName);
              }
        }
        if(sourceExpansions.length>0) {
            this.pushInTokenmap(sourceExpansions, graphObject.sourceTitle, tokenName);
            totalExpansionCount += sourceExpansions.length;
        }
        this.setState({"totalExpansionCount": totalExpansionCount})

    }

    createSourceExpansions =  (expansionObject, sourceExpansions, type, tokenName) => {
        var index = -1;
        for( var i=0; i < expansionObject.length; i++) {
            index = sourceExpansions.map(function (d) { return d['label']; }).indexOf(expansionObject[i]);
            if(index == -1) {
                if(tokenName.toLowerCase() !== expansionObject[i].toLowerCase())
                    sourceExpansions.push({'label': expansionObject[i], 'type': type})
            }
        }
        return sourceExpansions;
    }

    pushInTokenmap = (sourceExpansions, sourceTitle, tokenName) => {
        var tokenMap = this.state.tokenMap;
        if(tokenMap && tokenMap[tokenName]) {
            if(this.containsKey(tokenMap[tokenName], sourceTitle)){
                for ( var i=0; i < sourceExpansions.length; i++ ) {
                    var index = tokenMap[tokenName][sourceTitle].indexOf(sourceExpansions[i])
                    if(index == -1) {
                        tokenMap[tokenName][sourceTitle].push(sourceExpansions[i]);
                        tokenMap[tokenName]['show'] = tokenName==this.props.clickedtoken ? true : false
                        if(sourceTitle=='Machine Learning Enrichment') {
                            tokenMap[tokenName]['showRelated'] = false
                        }
                    }
                }
            } else {
                tokenMap[tokenName][sourceTitle] = sourceExpansions;
                tokenMap[tokenName]['show'] = tokenName==this.props.clickedtoken ? true : false
                if(sourceTitle=='Machine Learning Enrichment') {
                    tokenMap[tokenName]['showRelated'] = false
                };
            }
        } else {
            tokenMap[tokenName] = {};
            tokenMap[tokenName][sourceTitle] = sourceExpansions;
            tokenMap[tokenName]['show'] = tokenName==this.props.clickedtoken ? true : false
            if(sourceTitle=='Machine Learning Enrichment') {
                tokenMap[tokenName]['showRelated'] = false
            }
        }
        this.setState({tokenMap: tokenMap})
    }

    containsKey = (object, key) => {
        return !!Object.keys(object).find(k => k.toLowerCase() === key.toLowerCase());
    }

    showTokenRow = (clickedToken) => {
        let tokenMap = this.state.tokenMap;
        Object.keys(tokenMap).map((token, keyIndex) => {
            if(clickedToken==token) {
                tokenMap[token].show = true;
            } else {
                tokenMap[token].show = false;
            }
        })
    }

    showHideTokenRow = (token) => {
        let tokenMap = this.state.tokenMap;
        tokenMap[token].show = !tokenMap[token].show;
        this.setState({tokenMap: tokenMap})
    }


    renderExpansions = () => {
        var tokenMap = this.state.tokenMap;
        return (
            <Modal
              {...this.props}
              aria-labelledby="contained-modal-title-vcenter"
              size="lg"
              centered
              style={{"top":"50px"}}
            >
              <Modal.Header closeButton className="Settingspopup">
                <Modal.Title id="contained-modal-title-vcenter" style={{"marginTop": "8px"}}>Semantic Search Scope</Modal.Title>
              </Modal.Header>
              <Modal.Body style={{"overflowY": "auto", "maxHeight": "500px"}}>
              <div className="col-md-2 pl-0 pr-0" style={{"marginTop": '-10px', "float": "right"}}>
                    <ul class="legend_list">
                    <li class="red_legend">
                    <div class="red legend_tooltip">&nbsp;</div>
                    <span class="red_tt tooltiptext">Exact Term Matches</span></li>
                    <li class="green_legend">
                    <div class="green legend_tooltip">&nbsp;</div>
                    <span class="green_tt tooltiptext">Term Synonyms</span></li>
                    <li class="blue_legend">
                    <div class="blue legend_tooltip">&nbsp;</div>
                    <span class="blue_tt tooltiptext">Sub Concepts</span></li>
                    <li class="yellow_legend">
                    <div class="yellow legend_tooltip">&nbsp;</div>
                    <span class="yellow_tt tooltiptext">Related</span></li>
                    </ul>
                </div>
              <div className="row" role="tablist" aria-multiselectable="true">

              {Object.keys(tokenMap).map((token, keyIndex) => (
                <div className="col-lg-12" style={{"borderLeft":"1px solid #e5e5e5", "borderRight":"1px solid #e5e5e" }}
                >
                    <h1 className="token_name" style={{"text-transform":"capitalize", "cursor":"pointer", "outline": "none"}} 
                        onClick={()=>{this.showHideTokenRow(token)}}
                        role="tab" tabIndex="0"> 
                        {token}
                        {tokenMap[token].show==false ?
                        <i title="Expand" className="fa fa-plus pull-right"></i> : 
                        <i title="Collapse" className="fa fa-minus pull-right"></i> 
                        }
                    </h1>
                     {Object.keys(tokenMap[token]).map((source, sourceIndex) => (
                        <div>
                        {source!=='show' && source!=='showRelated' && tokenMap[token].show==true &&
                        <div className={"src_row "}
                        >
                        <ul className="src_exp_list">
                            <li 
                                className={'src_name expansion_icon '} 
                                title={source}
                                style={{"textTransform":"capitalize","textOverflow":"ellipsis","overflow": "hidden"}}> 
                                {source}
                            </li>
                            {/* { source=='Machine Learning Enrichment' && tokenMap[token].showRelated==true ? 
                            <i  style={{"cursor":"pointer", "float":"left", "marginTop": "8px", "fontSize": "18px", "padding-left":"10px"}}
                                title="Click to hide related expansions" 
                                onClick={tokenMap[token].showRelated=false}
                                className="fa fa-minus-square-o" role="button" tabindex="0" 
                                aria-label="Click to hide related expansions"></i>  :
                            <i  //?????????????????&& (expansions| filter:{'type': 'related'}).length>0" 
                                style={{"cursor":"pointer", "float":"left", "marginTop": "8px", "fontSize": "18px", "paddingLeft":"10px"}}
                                title="Click to view related expansions" onClick={tokenMap[token].showRelated=true} 
                                className="fa fa-plus-square-o" role="button" tabindex="0" aria-label="Click to hide related expansions"></i>
                            
                            } */}
                            {Array.isArray(tokenMap[token][source]) && (tokenMap[token][source]).map((expansion, exIndex) => (
                             <li 
                                title={expansion.type}
                                //?????????????????ng-show="(item.type=='related' && sourceExpansions.showRelated==true && source=='Machine Learning Enrichment') || (source!=='Machine Learning Enrichment') || ( source=='Machine Learning Enrichment' && item.type!=='related')" 
                                className={(expansion.type=='standardname' || expansion.type=='synonyms') ? 'green_token ': null+
                                (expansion.type=='narrower') ? 'blue_token ' : null +(expansion.type=='related') ? 'yellow_token':null + " query_token"}
                            >
                                {expansion.label}
                            </li>
                            ))
                            }
                            <span></span>
                            {/* <span 
                            //???????????????????????ng-if="(expansions| filter:{'type': 'acronyms'}).length==0 && (expansions| filter:{'type': 'variants'}).length==0 && source=='Machine Learning Enrichment' && sourceExpansions.showRelated!==true" 
                                style={{"fontStyle":"italic", "float":"left", "margin":"4px 5px", "borderRadius":"20px", "padding":"0px 12px"}}>
                                No variants or acronyms found. 
                                Click on 
                                <i className="fa fa-plus-square-o" style={{"fontSize":"small"}}></i> 
                                icon to view related expansions. 
                            </span> */}
                        </ul>
                        </div>
                        }
                        </div>
                    ))
                    }
                    </div>
                 ))
                 }
                </div>
              </Modal.Body>
              <Modal.Footer>
                <Form.Group as={Row}>
                  <Col md={12} right>
                    <Button variant="secondary" onClick={this.props.onHide}>
                      Close
                    </Button>
                  </Col>
                </Form.Group>
              </Modal.Footer>
            </Modal>
          );
    }
    render() {
        return this.renderExpansions()
    }
}
export default AppExpansions;