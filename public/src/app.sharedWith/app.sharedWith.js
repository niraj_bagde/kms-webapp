import React, { Component } from 'react';
import FileShare from '../app.fileShare/app.fileShare';
import "./app.sharedWith.css";
import Axios from 'axios';

class SharedWith extends Component {
    constructor(props) {
      super(props);
      this.state = {
        sharedWithUsers: [],
        showShareModal: false,
        userList : []
        }
    }


    componentWillMount() {
        // this.getUsersList();
    }


    getUsersList = () => {
        var userList = [];
        let request;
        let requestUrl = "";
        if(this.props.source.indexOf('Sharepoint') >-1) {
          request = {
            "ConnectorSourceName":"SharePoint"
          }
          requestUrl = this.props.edocsBackendAPI+"api/v1/sharepointusers";
        }
        request["ClientName"]= this.props.client;
        Axios.post(requestUrl, request).then(response => {
          if(response.data) {
            userList = response.data
          }

        this.setState({
            userList: userList
        }, this.getSharedWithUsers())
      })
    }


    getSharedWithUsers = () => {
        let request;
        let requestUrl = "";
        let docId = this.props.documentId ? this.props.documentId : 
                        (this.props.selectedDoc && this.props.selectedDoc.sp_doc_id ? this.props.selectedDoc.sp_doc_id : "")
        if(this.props.source.indexOf('Sharepoint') >-1) {
          request = {
              "DocumentId": docId,
              "ConnectorSourceName":"SharePoint"
          }
          requestUrl = this.props.edocsBackendAPI+"api/v1/sharepointdocument/";
        }
        request["ClientName"]= this.props.client;
        Axios.post(requestUrl, request).then(response => {
            let sharedWith = [];
            let sharedWithUniqueUsers = [];
            if(response && response.data) {
                sharedWith = response.data
            }
            sharedWith.forEach((item, index) => {
                sharedWith[index].users = sharedWith[index].SharedTo.split(',');
                sharedWith[index].users.forEach((user, key) => {
                    var ind = this.state.userList.map(function (d) { return d["Email"]; }).indexOf(user);
                    var eInd = sharedWithUniqueUsers.map(function (d) { return d["Email"]; }).indexOf(user);
                    if (ind > -1 && eInd ==-1) {
                        sharedWithUniqueUsers.push(this.state.userList[ind]);
                    }
                })

                this.setState({
                    sharedWithUsers: sharedWithUniqueUsers
                })
            });
        });
    }

    shareDocument = () => {
        this.setState({
            showShareModal: true
        })
    }


    hideShareModal = () => {
        setTimeout(()=>{
            this.getSharedWithUsers();
        },1000)

        this.setState({
            showShareModal: false
        })
    }

    render() {
    return  (
        <div style={{'marginTop': '10px'}}>
        <div style={{'marginBottom': '10px', marginLeft:'15px'}}><h4 className="version-heading">Shared with
        <button className="share-button" title="Share this document">
                <i className="fa fa-share-alt share-icon" aria-hidden="true" 
                    onClick={(e) =>
                        this.shareDocument(e)
                    }
                    ></i>
            </button></h4>

            </div>

                <ul>
                {this.state.sharedWithUsers.map((item, index) => (
                    <li>
                        <div className="col-md-12 pl-0 shared-list">
                            <div className="col-md-2 shared-user-pic"><i class="fa fa-user" title="User"></i></div>
                            <div className="col-md-10 shared-username" title={item.Email} style={{"textTransform": "capitalize"}}>{item.Name}</div>
                        </div>
                    </li>
                    ))
                }
                </ul>
            {
                this.state.showShareModal &&
                <FileShare selectedDoc={this.props.selectedDoc} 
                           fileType={this.props.fileType}
                           sharedWithUsers= {this.state.sharedWithUsers}
                           userEmail={this.props.userEmail}
                           hideFileShare={()=>this.hideShareModal()}
                           source={this.props.source}
                           client={this.props.client}
                           edocsBackendAPI={this.props.edocsBackendAPI}
                />
            }
    </div>
    )
  }
};


export default SharedWith;