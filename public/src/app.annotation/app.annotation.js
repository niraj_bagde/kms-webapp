import React, { useState, useEffect } from "react";
import "./app.annotation.css";
import Axios from "axios";

function Annotation(props) {
  const [loading, setLoading] = useState(true);
  const doc_id = props.source_id ? props.source_id : props.item.DoucumentId
  const [autoText, setautoText] = useState([]);
  const [keyword, setKeyword] = useState("");
  const [inputStyle, setInputStyle] = useState("black");
  const dataSource = props.source;
  const edocsBackendAPI=props.edocsBackendAPI;
  const saveKeyword = function (keyword) {
    if (keyword.length > 0) {
      let prevArr = autoText;

      let canPush = prevArr.find(
        (item) => item.toLowerCase() == keyword.toLowerCase()
      );

      if (canPush == undefined) {
        prevArr.push(keyword);
        // let body = {
        //   DocumentId: doc_id,
        //   TagType: "manual",
        //   Operation: "Add",
        //   TagValue: keyword,
        // };
        let request;
        let requestUrl="";
        if(dataSource.indexOf('Sharepoint') >-1) {
          request = {
            "ConnectorSourceName":"SharePoint",
            "DocumentId": doc_id,
            "TagType": "manual",
            "Operation": "Add",
            "TagValue": keyword,
          };

            requestUrl = edocsBackendAPI+"api/v1/metadata";
        }
        request["ClientName"] = props.client;
        Axios.post(requestUrl, request).then(response => {
        // Axios.post("/annotation/save", body)
        //   .then((response) => {
            // console.log("success saving");
            setautoText(prevArr);
            setKeyword("");
            setInputStyle("black");
          })
          .catch((err) => {
            console.log("catch err in saving");
            console.log(err);
          });
      } else {
        // console.log("already exists");
        setInputStyle("red");
      }
    }
  };
  const removeKeyword = function (keyword) {
    let prevArr = autoText.filter((item) => {
      return item.toLowerCase() != keyword.toLowerCase();
    });
    // let body = {
    //   DocumentId: doc_id,
    //   TagType: "manual",
    //   Operation: "remove",
    //   TagValue: keyword,
    // };
    // Axios.post("/annotation/save", body)
    //   .then((response) => {
      let request;
      let requestUrl="";
      if(dataSource.indexOf('Sharepoint') >-1) {
        request = {
          "ConnectorSourceName":"SharePoint",
          "DocumentId": doc_id,
          "TagType": "manual",
          "Operation": "remove",
          "TagValue": keyword,
        };

          requestUrl =edocsBackendAPI+"api/v1/metadata";
      }
      request["ClientName"] = props.client;
      Axios.post(requestUrl, request).then(response => {
        setautoText(prevArr);
        setKeyword("");
      })
      .catch((err) => {
        console.log("catch err in removing");
        console.log(err);
      });
  };

  const autoAntn = autoText.map((item, index) => {
    return (
      <span className="antn-token" key={index}>
        <span
          className="fa fa-close pull-right"
          style={{ marginLeft: "4px", marginTop: "3px" }}
          onClick={() => {
            removeKeyword(item);
          }}
        ></span>
        {item}
      </span>
    );
  });
  useEffect(() => {
    var source_id = doc_id;
    let request;
    let requestUrl="";
    if(dataSource.indexOf('Sharepoint') >-1) {
      request = {
        "ConnectorSourceName":"SharePoint",
        "DocumentId": source_id,
      };

      requestUrl = edocsBackendAPI+"api/v1/getmetadata";
    }
    request["ClientName"] = props.client;
    console.log("props.selectedDocInfo")
    console.log(props.selectedDocInfo)
    let array = props.selectedDocInfo.tags ? props.selectedDocInfo.tags : [];
    array = array.map((elem) => elem.trim());
    array = array.filter((item) => item.length > 0);
    setautoText(array);
    setLoading(false);
    /* Axios.post(requestUrl, request).then(response => {
    // Axios.get("/annotation/fetch/"+ source_id)
    //   .then((response) => {
        var array = response.data.Tags.split(",");
        array = array.map((elem) => elem.trim());
        array = array.filter((item) => item.length > 0);
        setautoText(array);
        setLoading(false);
      })
      .catch((err) => {
        console.log(err);
      }); */
  }, []);

  return (
    <div className="annotation-root">
      {loading ? (
        <div>
          <img src={props.loaderIcon} className="loader"></img>
          </div>
      ) : (
        <div className="annotation-top">
          <div className="annotation-child">
            {/* <div className="top-text">Tags</div> */}
            <div style={{'marginBottom': '10px', marginLeft:'15px'}}>
              <h4 className="version-heading">Tags
              </h4>
            </div>
            <div className="atn-gc">
              {autoAntn}
              <input
                className="antn-input"
                type="text"
                value={keyword}
                onChange={(e) => {
                  setKeyword(e.target.value);
                }}
                onKeyPress={(e) => {
                  e.which === 13 ? saveKeyword(keyword) : null;
                }}
                style={{ borderColor: `${inputStyle}` }}
                autoFocus={true}
                placeholder={"Type here & Hit enter"}
              ></input>
            </div>
          </div>
          {/* <div className="annotation-child">
            <div className="top-text">Auto Tags</div>
            <div className="atn-gc">{autoAntn}</div>
          </div> */}
        </div>
      )}

      {/* <div className="annotation-bottom">
        <button className="antn-btn">Save</button>
        <button className="antn-btn">Close</button>
      </div> */}
    </div>
  );
}
export default Annotation;
