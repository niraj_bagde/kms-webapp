import React, { useState, useEffect } from "react";

function Attachment(props) {
  // const [Tab, setTab] = useState(1);
  // const [attachment, setAttachment] = useState(<div></div>);
  // const [attachments, setAttachment] = useState([]);
  const data = props.item;
  var myAttachments = [];

  const getFileExtension = (url) => {
    var matches = url.match(/\/([^\/?#]+)[^\/]*$/);
    if (matches && matches.length > 1) {
      let fileName = matches[1].split(".");
      if (fileName !== undefined && fileName.length > 0) {
        let fileExtension = fileName[fileName.length - 1];
        return fileExtension;
      } else {
        return matches[1];
      }
    }
    return null;
  }

  const getBreadCrum = (titleUrl) => {
    let urlParts = unescape(titleUrl).split("/");
    if(props.resultSource=='searchService') {
      urlParts.splice(0, 5);
    }
    // urlParts.splice(0, 5);
    // urlParts.splice(-1, 1);
    let breadkCrum = <div className="bread-crum-wrapper pull-left">{urlParts.map((folder, index) => {
        let crum = <span key={index} className="bread-crum-span">{folder} <i className="fa fa-caret-right" aria-hidden="true"></i> </span>
        if (index === (urlParts.length -1)) {
          crum = <span key={index} className="bread-crum-span">{folder}</span>
        }
        return crum
    })}
    </div>

    return breadkCrum;
  }

  const getFileName = (url) => {
    var matches = url.match(/\/([^\/?#]+)[^\/]*$/);
    if (matches && matches.length > 1) {
      let fileName = matches[1].split(".");
      if (fileName !== undefined && fileName.length > 0) {
        fileName = fileName[0];
        return fileName;
      } 
    }
    return null;
  }

  const setActiveTab = (index) => {
    let keywords, title, content, teams, players, officials, games, author, creator, drugs, health, anatomies, orgs;
    let dateIndex = `embd_${index}_creation_date`;
    let date = data[dateIndex] ? new Date(data[dateIndex]).toDateString(): "";
    let keywordsIndex = `embd_${index}_keywords`;
    let teamsIndex = `embd_${index}_teams`;
    let playersIndex = `embd_${index}_players`;
    let gamesIndex = `embd_${index}_games`;
    let anatomiesIndex = `embd_${index}_anatomies`;
    let drugIndex = `embd_${index}_drugs`;
    let healthIndex = `embd_${index}_health_conditions`;
    let OrgIndex = `embd_${index}_organizations`;
    let officialsIndex = `embd_${index}_officials`;
    let titleIndex = `embd_${index}_title`;
    let titleSubstituteIndex = `embd_${index}_title_substitute`;
    let contentIndex = props.resultSource=='searchService' ? `embd_${index}_content`:`embd_${index}_content_short`;
    let urlIndex = `embd_${index}_doc_resource_name`;
    let authorIndex = `embd_${index}_author`;
    let creatorIndex = `embd_${index}_creator`;
    
    let fileExtension = getFileExtension(data[urlIndex][0]);
    let fileName = unescape(getFileName(data[urlIndex][0]));
    let itemClass = "fa fa-bookmark";
    if (fileExtension == "pdf") {
      itemClass = "fa fa-file-pdf-o";
    } else if (fileExtension == "docx" || fileExtension == "doc") {
      itemClass = "fa fa-file-word-o";
    } else if (fileExtension == "msg") {
      itemClass = "fa fa-envelope";
    } else if (fileExtension == "xls" || fileExtension == "csv" || fileExtension == "xlsx") {
      itemClass = "fa fa-file-excel-o";
    } else if (fileExtension == "ppt" || fileExtension == "pptx") {
      itemClass = "fa fa-file-powerpoint-o";
    } 
    author = data[authorIndex];
    creator = data[creatorIndex];
    let breadkCrum = props.resultSource=='searchService' ? getBreadCrum(data[urlIndex][0]):getBreadCrum(data["doc_path"]) ;

    if (data.highlight[keywordsIndex] != undefined) {
      keywords = data.highlight[keywordsIndex];
    } else if (data[keywordsIndex] != undefined) {
      keywords = data[keywordsIndex];
    }
    if (data.highlight[titleIndex] != undefined) {
      title = data.highlight[titleIndex];
    } else if (data.highlight[titleSubstituteIndex] != undefined) {
      title = data.highlight[titleSubstituteIndex];
    }else if (data[titleIndex] != undefined) {
      title = data[titleIndex];
    }else if (data[titleSubstituteIndex] != undefined) {
      title = data[titleSubstituteIndex];
    }
  
    if (data.highlight[contentIndex] != undefined) {
      content = data.highlight[contentIndex];
    } else if (data[contentIndex] != undefined) {
      content = data[contentIndex];
    }
    content = content && content[0].substr(0, 999);
    if (data.highlight[teamsIndex] != undefined) {
      teams = data.highlight[teamsIndex];
    } else if (data[teamsIndex] != undefined) {
      teams = data[teamsIndex];
    }

    if (data.highlight[playersIndex] != undefined) {
      players = data.highlight[playersIndex];
    } else if (data[playersIndex] != undefined) {
      players = data[playersIndex];
    }
    if (data.highlight[gamesIndex] != undefined) {
      games = data.highlight[gamesIndex];
    } else if (data[gamesIndex] != undefined) {
      games = data[gamesIndex];
    }
    if (data.highlight[officialsIndex] != undefined) {
      officials = data.highlight[officialsIndex];
    } else if (data[officialsIndex] != undefined) {
      officials = data[officialsIndex];
    }

    if (data.highlight[drugIndex] != undefined) {
      drugs = data.highlight[drugIndex];
    } else if (data[drugIndex] != undefined) {
      drugs = data[drugIndex];
    }
    if (data.highlight[healthIndex] != undefined) {
      health = data.highlight[healthIndex];
    } else if (data[healthIndex] != undefined) {
      health = data[healthIndex];
    }
    if (data.highlight[anatomiesIndex] != undefined) {
      anatomies = data.highlight[anatomiesIndex];
    } else if (data[anatomiesIndex] != undefined) {
      anatomies = data[anatomiesIndex];
    }
    if (data.highlight[OrgIndex] != undefined) {
      orgs = data.highlight[OrgIndex];
    } else if (data[OrgIndex] != undefined) {
      orgs = data[OrgIndex];
    }
    myAttachments.push (
      <div
      className="result-child1"
        style={{
          display: "flex",
          flexDirection: "column",
          "boxShadow": "0px 0px 25px #ccc inset",
          "paddingTop": "15px"
        }}
      >
        <div
          style={{
            textTransform: "uppercase",
            // display: "flex",
            marginTop: "10px"
          }}
        >
        <a
          className={itemClass}
          style={{ color: "#1d4289", margin: "2px 6px", "cursor":"pointer!important", "fontWeight": "bold" }}
          aria-hidden="true"
        ></a>
          <b
            style={{
              fontSize: "14px",
              color: "#1d4289",
              margin: "0",
              fontWeight: "700",
            }}
            dangerouslySetInnerHTML={{ __html: title }}
          />
          {!title && fileName &&
            <b
            style={{
              fontSize: "14px",
              color: "#1d4289",
              margin: "0",
              fontWeight: "700",
            }}
            dangerouslySetInnerHTML={{
              __html: `${fileName}`,
            }}
            />
          }
          {" "}
          <div className="pull-right">
            {creator &&
              <p
                title="Author"
                className={(date) && "pipe"}
                style={{ display:"inline-block", fontSize:"12px", textTransform:"capitalize"}}
                dangerouslySetInnerHTML={{
                  __html: `${creator}`,
                }}
              /> 
              }
              {!creator && author &&
              <p
                title="Author"
                className={(date) && "pipe"}
                style={{ display:"inline-block", fontSize:"12px", textTransform:"capitalize"}}
                dangerouslySetInnerHTML={{
                  __html: `${author}`,
                }}
              /> 
              }
              {(date) && 
              <p style={{ marginLeft: "5px" , display:"inline-block", fontSize:"12px"}}
                title="Created date">
                {date}
              </p>
              }
            </div>

        </div>
        <div className="result-child2" style={{"padding": "0px 0px 10px"}}>
              {breadkCrum} 
        </div>
        <p
          style={{ marginRight: "10px" }}
          dangerouslySetInnerHTML={{
            __html: content,
          }}
        />
        <ul
          style={{
            display: "flex",
            justifyContent: "flex-start",
            marginTop: "2rem",
            marginBottom: "2rem",
            fontSize: "1.25rem",
          }}
          className="list-parent"
        >
          {keywords != undefined && (
            <li className="list-item">
              <b>Tags: </b>
              <span
                dangerouslySetInnerHTML={{
                  __html: `${keywords}`,
                }}
              />
            </li>
          )}
          {teams != undefined && (
            <li className="list-item">
              <b>Teams: </b>
              <span
                dangerouslySetInnerHTML={{
                  __html: `${teams}`,
                }}
              />
            </li>
          )}
          {players != undefined && (
            <li className="list-item">
              <b>Players: </b>
              <span
                dangerouslySetInnerHTML={{
                  __html: `${players}`,
                }}
              />
            </li>
          )}
          {games != undefined && (
            <li className="list-item">
              <b>Games: </b>
              <span
                dangerouslySetInnerHTML={{
                  __html: `${games}`,
                }}
              />
            </li>
          )}
          {officials != undefined && (
            <li className="list-item">
              <b>Officials: </b>
              <span
                dangerouslySetInnerHTML={{
                  __html: `${officials}`,
                }}
              />
            </li>
          )}
          {anatomies != undefined && (
            <li className="list-item">
              <b>Anatomies: </b>
              <span
                dangerouslySetInnerHTML={{
                  __html: `${anatomies}`,
                }}
              />
            </li>
          )}
          {health != undefined && (
            <li className="list-item">
              <b>Health Conditions: </b>
              <span
                dangerouslySetInnerHTML={{
                  __html: `${health}`,
                }}
              />
            </li>
          )}
          {drugs != undefined && (
            <li className="list-item">
              <b>Drugs: </b>
              <span
                dangerouslySetInnerHTML={{
                  __html: `${drugs}`,
                }}
              />
            </li>
          )}
          {orgs != undefined && (
            <li className="list-item">
              <b>Organizations: </b>
              <span
                dangerouslySetInnerHTML={{
                  __html: `${orgs}`,
                }}
              />
            </li>
          )}
        </ul>
      </div>
    );
    // setAttachment(myAttachment);
  };

  const createAttachmentArray = () => {
    let attachCount = props.item["embd_doc_num"] ? props.item["embd_doc_num"] : 0;
    for(let i=1; i<=attachCount.length; i++) {
      setActiveTab(i);
    }
  }
  createAttachmentArray();
  // useEffect(() => {
  //   //setActiveTab(1);
  //   createAttachmentArray()
  // }, []);

  return (
    <div className="attach-root" style={{"marginTop":"10px"}}>
    {myAttachments}
      {/* <ul className="nav nav-tabs">
        {data.embd_doc_num !== undefined && data.embd_doc_num.map((bar) => {
          return (
            <li className={bar == Tab ? "active" : ""}>
              <a
                data-toggle="tab"
                id="docPreview"
                onClick={() => setActiveTab(bar)}
                href="/"
              >
                {bar}
              </a>
            </li>
          );
        })}
      </ul> */}
      {/* <div className="tab-content tree-border">
        <div className="inner-attach">{attachment}</div>
      </div> */}
    </div>
  );
}
export default Attachment;
