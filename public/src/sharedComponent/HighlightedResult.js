import React, { Component } from 'react'

export default class HighlightedResult extends Component {
    render() {
        return (
            <div>
                <tr ng-repeat="searchrow in dataStore.searchResultdata " id="document_{{$index}}" ng-init="docposition = (dataStore.pageNo >= 1 ) ? (($index + 1) + ( dataStore.itemsPerPage * (dataStore.pageNo - 1) )) : ($index + 1)">
                    <td><div class="pull-left tablerow advancesearchpadleft">
                        <div class="col-lg-12 advancesearchpadleft">
                            <div class="result_row">
                                <div class="metadata_section" style="min-height:20px;">
                                    {((searchrow.app_key && searchrow.app_key!='') && searchrow.app_key.length > 0)? (
                                        <span
                                        class="basicsearchsubTitle" title="Application" style="margin-right:15px"> <span>{{ searchrow.app_key }}</span>
                                    </span>
                                    ):""}
                                    
                                    <span ng-if="searchrow.item_type && searchrow.item_type !==''" class="text-uppercase meta-date">
                                        <span title="Record Type">{{ searchrow.item_type }}</span>
                                    </span>
                                    <span ng-if="(searchrow.modified && searchrow.modified!='')"
                                        class="text-uppercase meta-date" title="Modified date"> <span>{{ searchrow.modified | date }}</span>
                                    </span>

                                </div>
                                <div class="bookmark_section result_tokan_span">
                                    <div ng-if="searchrow.title && searchrow.title!=''" class="basicsearchTitle">
                                        <span>{{ searchrow.displayCount }}.</span>
                                        <span ng-bind-html="to_trusted(searchrow.title)" ></span>
                                        <img class="player_image" ng-if="searchrow.image_url" src={{ searchrow.image_url }}>
                                           
                           
                        </div>
                                        <div class="url_section">
                                            <span ng-if="searchrow.url && searchrow.url!=''" class="meta-link  text_elipsis55">
                                                <a class="result_url" ng-href="{{searchrow.url}}" target="_blank"
                                                    title="{{searchrow.url}}"
                                                    ng-click="saveDocumentClickToSnowplow(searchrow,true);"
                                                > {{ searchrow.url }} </a>
                                            </span>
                                        </div>

                                        <div ng-if="searchrow.desc && searchrow.desc.length>0" class="result_info" >
                                            <p ng-bind-html="to_trusted(searchrow.desc)" ng-class="{'content-scroll': !searchrow.showMore}"></p>
                                            <button type="button" ng-if="searchrow.showMore==true" ng-click="toggleDesc_NBA(searchrow)" class="pull-right btn-link moreLessDesc" aria-label="click to show more information">Show More <i class="fa fa-angle-double-down"></i></button>
                                            <button type="button" ng-if="searchrow.desc.length > 300 && !searchrow.showMore" ng-click="toggleDesc_NBA(searchrow)" class="pull-right btn-link moreLessDesc" aria-label="click to hide information">Show Less <i class="fa fa-angle-double-up"></i></button>
                                        </div>

                                        <div ng-if="searchrow.authorsStr != undefined && searchrow.authorsStr !='' " class="result_info">
                                            <p>
                                                <div title="Creator"><em ng-bind-html="to_trusted(searchrow.authorsStr)" ></em></div>
                                            </p>
                                        </div>


                                        <ul class="metadata_row">
                                            <li ng-if="searchrow.highlightedKeyWords!= undefined && searchrow.displayKeywords.length > 0">
                                                <label>Tags:</label>
                                                <span ng-bind-html="to_trusted(searchrow.displayKeywords)"></span>
                                            </li>
                                            <li ng-if="searchrow.displayTeam && searchrow.displayTeam !==''">
                                                <label>Teams:</label>
                                                <span ng-bind-html="to_trusted(searchrow.displayTeam)"></span>
                                            </li>
                                            <li ng-if="searchrow.displayPlayers && searchrow.displayPlayers !==''">
                                                <label>Players:</label>
                                                <span ng-bind-html="to_trusted(searchrow.displayPlayers)"></span>
                                            </li>
                                            <li ng-if="searchrow.displayGames && searchrow.displayGames !==''">
                                                <label>Games:</label>
                                                <span ng-bind-html="to_trusted(searchrow.displayGames)"></span>
                                            </li>
                                            <li ng-if="searchrow.displayOfficials && searchrow.displayOfficials !==''">
                                                <label>Officials:</label>
                                                <span ng-bind-html="to_trusted(searchrow.displayOfficials)"></span>
                                            </li>
                                            <li ng-if="searchrow.displayPresident && searchrow.displayPresident !==''">
                                                <label>President:</label>
                                                <span ng-bind-html="to_trusted(searchrow.displayPresident)"></span>
                                            </li>
                                        </ul>


                                        <tr>
                                            <td class="green_box">
                                                <label>Teams:</label> <span ng-bind-html="to_trusted(searchrow.displayTeam)"></span>
                                            </td>
                                            <td class="blue_box">
                                                <label>Officials:</label> <span ng-bind-html="to_trusted(searchrow.displayOfficials)"></span>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td class="green_box">
                                                <label>Players:</label> <span ng-bind-html="to_trusted(searchrow.displayPlayers)"></span>
                                            </td>


                                            <td class="blue_box">
                                                <label>Games:</label> <span ng-bind-html="to_trusted(searchrow.displayGames)"></span>
                                            </td>
                                        </tr>
                                        </table> -->

                      </div>

                            </div>

                        </div>

                    </div></td>
                </tr>
            </div>
        )
    }
}
