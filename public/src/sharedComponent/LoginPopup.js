import React, { Component, useState } from "react";
import { Modal, Row, Container, Col, Button, Form } from "react-bootstrap";
import "./LoginPopup.css";

class Loginform extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      domain: "",
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
  handleSubmit() {
    this.props.onSuccess(this.state.username,this.state.domain);
  }

  componentWillMount() {}

  componentWillUnmount() {}

  render() {
    return (
      <Modal
        {...this.props}
        aria-labelledby="contained-modal-title-vcenter"
        size="sm"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">Login</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form className="inner-form">
            <Form.Group as={Row} controlId="formHorizontalEmail">
              <Form.Label column md={4}>
                Username
              </Form.Label>
              <Col sm={6}>
                <Form.Control
                  type="text"
                  placeholder=""
                  name="username"
                  value={this.state.username}
                  onChange={this.handleChange}
                />
              </Col>
            </Form.Group>
            {/* <Form.Group as={Row} controlId="formHorizontalPassword">
              <Form.Label column sm={3}>
                Password
              </Form.Label>
              <Col sm={6}>
                <Form.Control
                  type="password"
                  placeholder="anything"
                  name="password"
                  value={this.state.password}
                  onChange={this.handleChange}
                />
              </Col>
            </Form.Group> */}
            <Form.Group as={Row}>
              <Form.Label as="legend" column md={4}>
                Select Domain
              </Form.Label>
              <Col sm={6}>
                <Form.Control
                  as="select"
                  name="domain"
                  value={this.state.domain}
                  onChange={this.handleChange}
                >
                  <option value="">Select your domain here</option>
                  <option value="thedigitalgroup">THE DIGITAL GROUP</option>
                  <option value="nba">NBA</option>
                </Form.Control>
              </Col>
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Form.Group as={Row}>
            <Col md={12} right>
              <Button variant="primary" onClick={this.handleSubmit}>
                Login
              </Button>
              <Button variant="secondary" onClick={this.props.onHide}>
                Cancel
              </Button>
            </Col>
          </Form.Group>
        </Modal.Footer>
      </Modal>
    );
  }
}

export default Loginform;
