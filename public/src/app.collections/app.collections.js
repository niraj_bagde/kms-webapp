import React, { Component } from 'react';
import "./app.collections.css";
import { Modal, Row, Container, Col, Button, Form, InputGroup } from "react-bootstrap";
import Tree, { TreeNode } from 'rc-tree';
import TreeDataHelper from '../helpers/TreeDataHelper';
import CollectionHelper from '../helpers/CollectionHelper';
import axios from 'axios';

class Collections extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sharedWithUsers: [],
            userList: [],
            newCollectionName: "",
            selectedCollection: "",
            collectionTree: [],
            collectedDocName: props.selectedDoc.file_name,
            selectedDocument: props.selectedDoc,
            documentAddSuccessMessage: "",
            isLoading: false,
            selectedKeys: [],
            expandedKeys: [],
            showDocName: true
        }
    }


    componentWillMount() {
        this.getCollectionsTree(false)
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.selectedDoc.file_name !== this.props.selectedDoc.file_name) {
            this.setState({
                collectedDocName: this.props.selectedDoc.file_name,
                selectedDocument: this.props.selectedDoc,
            })
            this.getCollectionsTree(false)
        }
    }

    getCollectionsTree = (isCalledFromSaveCollection) => {
        this.setState({isLoading: true});
        let requestObj = {userEmail: this.props.userEmail}
        axios.post("/collections/getCollectionsTree", requestObj).then(collectionsData => {
            if (collectionsData !== undefined) {
                let collectionTreeData = CollectionHelper.collectionTreeAssembly(collectionsData.data, this.addRootCollection, this.addChildCollection, true);
                let selectedKeys = ""
                if (collectionTreeData.length > 2) {
                    let alreadySelectedCollection = localStorage.getItem("selectedCollection");
                    if (alreadySelectedCollection !== undefined && alreadySelectedCollection !== null && alreadySelectedCollection !== "") {
                        selectedKeys = alreadySelectedCollection;
                        collectionTreeData.forEach(collection => {
                            let collectionNode = CollectionHelper.searchTreeWithId(collection, selectedKeys);
                            if (collectionNode !== null) {
                                this.setState({selectedCollection: collectionNode})
                            }
                        })
                    } else {
                        selectedKeys = collectionTreeData[1]["key"];
                        this.setState({selectedCollection: collectionTreeData[1]})
                    }
                }
                this.setState({collectionTree: collectionTreeData, isLoading: false, selectedKeys: [selectedKeys], expandedKeys: [selectedKeys]});
                if (isCalledFromSaveCollection) {
                    this.saveCollectionDocuments();
                }
            }
        })
    }

    collectionNameOnChange = (event) => {
        this.setState({
            newCollectionName: event.currentTarget.value
        })
    }

    checkIfEnterHit = (event, parentId) => {
        if(event.keyCode == 13) {
            this.setState({isLoading: true});
            let collectionObject = {
                name: event.target.value,
                parent_id: parentId,
                document_info: "[]",
                owner: this.props.userEmail,
                created_date: new Date(),
                modified_by: this.props.userEmail,
                modified_date: new Date()
            }
            axios.post("/collections/addNewCollection", collectionObject).then(addResult => {
                let selectedKeys = addResult.data[0]["id"];
                localStorage.setItem("selectedCollection", selectedKeys);
                this.setState({
                    newCollectionName: "",
                    selectedKeys: [selectedKeys]
                })
                this.getCollectionsTree(true);
            })
        } else if (event.keyCode == 27) {
            this.getCollectionsTree(false);
        }
    }

    addRootCollection = () => {
        let collectionTreeData = [...this.state.collectionTree];
        let newRootCollection = <span><input 
                            type="text"
                            name="collectionName"
                            placeholder="Collection Name"
                            className="collection-input-boxes"
                            /* value={this.state.newCollectionName} */
                            onChange={this.collectionNameOnChange}
                            autoFocus
                            onKeyDown={(event) => this.checkIfEnterHit(event, 0)} /> 
            </span>
        let newKey = CollectionHelper.randomStringGenerator();
        
        collectionTreeData[0] = {
            "id": "0",
            "key": newKey,
            "title": newRootCollection, //collection.name,
            "parentId": "",
            "isLeaf": true,
            "isAddLeaf": true,
            "icon": <i className="fa fa-bars collection-bars" />
        }
        
        this.setState({
            selectedKeys: [String(newKey)],
            collectionTree: collectionTreeData,
        })
    }

    addChildCollection = (parentCollection) => {
        let collectionTreeData = [...this.state.collectionTree];
        let currentParentNode = {}; 
        collectionTreeData.forEach(collection => {
            currentParentNode = CollectionHelper.searchTreeWithId(collection, parentCollection.id)
            if (currentParentNode !== null) {
                if (currentParentNode !== null) {
                    let newCollection = <span><input 
                                    type="text"
                                    name="collectionName"
                                    placeholder="Collection Name"
                                    className="collection-input-boxes"
                                    onChange={this.collectionNameOnChange}
                                    autoFocus
                                    onKeyDown={(event) => this.checkIfEnterHit(event, parentCollection.id)} /> 
                    </span>
                    let newKey = CollectionHelper.randomStringGenerator();
                    let treeNodeForAdd = {
                        "id": "0",
                        "key": newKey,
                        "title": newCollection,
                        "parentId": "",
                        "isLeaf": true,
                        "isAddLeaf": true,
                        "icon": <i className="fa fa-bars collection-bars" />
                    }
                    currentParentNode.children[0] = treeNodeForAdd;
        
                    this.setState({selectedKeys: [String(newKey)], collectionTree: collectionTreeData});
                }
            }
        });
    }

    treeNodeSelected = (selectedKeys, selectedNode) => {
        if (selectedNode.node.isLeaf) {
            if (!selectedNode.node.isAddLeaf) {
                this.props.collectionNodeSelected(selectedNode.node);
            }
        } else {
            let expandedKeys = [...this.state.expandedKeys];
            if (expandedKeys.indexOf(selectedNode.node.key) === -1) {
                expandedKeys.push(selectedNode.node.key)
            }
            this.setState({expandedKeys: expandedKeys, selectedCollection: selectedNode.node, selectedKeys: [selectedNode.node.key]});
            localStorage.setItem("selectedCollection", selectedNode.node.key);
        }
        
    }

    collectedDocNameChange = (event) => {
        this.setState({collectedDocName: event.target.value})
    }

    saveCollectionDocuments = () => {
        this.setState({isLoading: true});
        let selectedCollection = this.state.selectedCollection;
        let collectionDocName = this.state.collectedDocName;
        let selectedDocument = this.state.selectedDocument
        selectedDocument.NameInCollection = collectionDocName;
        let collectionDocuments = selectedCollection.documentInfo;
        if (Object.keys(collectionDocuments).length === 0) {
            collectionDocuments = [selectedDocument]
        } else {
            collectionDocuments.push(selectedDocument);
        }
        
        let collectionObject = {
            id: selectedCollection.id,
            name: selectedCollection.title,
            parent_id: selectedCollection.parentId,
            document_info: JSON.stringify(collectionDocuments),
            owner: selectedCollection.owner,
            created_date: selectedCollection.createdDate,
            modified_by: this.props.userEmail,
            modified_date: new Date()
        };
        axios.post("/collections/updateNewCollection", collectionObject).then(updateResult => {
            if (updateResult.status === 200) {
                this.props.sendCollectionAddEvent(selectedDocument,selectedCollection);
                localStorage.setItem("selectedCollection", selectedCollection.id);
                this.setState({
                    newCollectionName: "",
                    documentAddSuccessMessage: "Document saved in collection.",
                    showDocName: false
                });
                var currentComp = this;
                setTimeout(function(){ currentComp.setState({
                    documentAddSuccessMessage: ""
                }) }, 5000);
                this.getCollectionsTree(false);
            }
            
        })
    }

    docNameOnFocus = () => {
        const input = document.getElementById('collectedDocNam');
        input.focus();
        input.select();
    }

    onExpandCollection = (expandedKeys, onExpandObj, isExpanded) => {
        if (onExpandObj.expanded) {
            expandedKeys.push(onExpandObj.node.id);
        } else {
            let itemToRemove = expandedKeys.indexOf(onExpandObj.node.id)
            expandedKeys.splice(itemToRemove, 1);
        }

        this.setState({expandedKeys: expandedKeys});
    }

    closeDocName = () => {
        this.setState({showDocName: false});
    }

    render() {
        let collectionTreeStruct = <Tree
            className="myCls"
            showLine
            autoExpandParent={true}
            selectedKeys={this.state.selectedKeys}
            treeData={this.state.collectionTree}
            expandedKeys={this.state.expandedKeys}
            onExpand={this.onExpandCollection}
            onSelect={this.treeNodeSelected}
        />
        return (
            <div className="collections-form">
                <Form>
                    {this.props.isLoading && <div className="row overlay-for-loader" style={{"width": "103%"}}>
                        <img src={this.props.loaderIcon} className="loader"></img>
                    </div>}
                    {this.state.documentAddSuccessMessage.length > 0 && <div class="alert alert-success  collection-save-alert">
                            {this.state.documentAddSuccessMessage}
                    </div>}
                    <div className="row collection-tree">
                        {collectionTreeStruct}
                    </div>
                    {this.state.showDocName && <div className="row collection-name-wrapper">
                        <div className="col-md-11">
                            <input
                            type="text"
                            placeholder="Name"
                            id="collectedDocNam"
                            value={this.state.collectedDocName}
                            onChange={this.collectedDocNameChange}
                            className="collection-input-boxes long-input-box"
                            onFocus={this.docNameOnFocus}
                            autoFocus
                            />
                            <Button variant="link" title="Close" className="collection-btns save-form-btn" onClick={this.closeDocName} >
                                <i className="fa fa-times" />
                            </Button>
                            <Button variant="link" title="save to collection" className="collection-btns save-form-btn" onClick={this.saveCollectionDocuments} >
                                <i className="fa fa-arrow-up" />
                            </Button>
                            <br />
                            <Form.Text className="text-muted">
                                Save document as.
                            </Form.Text>
                        </div>
                    </div>}
                </Form>
            </div>
        )
    }
};



export default Collections;