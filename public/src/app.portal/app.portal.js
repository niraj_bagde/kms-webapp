import React, { Component, useState } from 'react';
import { Modal, Row, Container, Col, Button } from 'react-bootstrap';
import Login from "../app.login/app.login"
class Portal extends Component {
    constructor(props) {
        super(props);
        this.state = {};

    }
    render() {
        return (
            <Modal {...this.props} aria-labelledby="contained-modal-title-vcenter" size="lg"
                backdrop="static"
                keyboard={false}
                centered dialogClassName="modal-90w">
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        Login Portal
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body><Container><Login /></Container></Modal.Body>
            </Modal>
        )
    }
}
export default Portal;