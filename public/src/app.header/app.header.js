import React, { Component } from 'react';
import LoginPopup from '../sharedComponent/LoginPopup.js';
// import React,{Component} from 'react';
// import {Route, Redirect} from 'react-router';
import Search from '../app.search/app.search';
import Dashboard from '../app.dashboard/app.dashboard'
import Form from 'react-bootstrap/Form'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Button from 'react-bootstrap/Button'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import "./app.header.css";
import { useHistory } from "react-router-dom";

// class AppHeader extends Component {
  // constructor(props) {
  //   super(props);
  //   this.state = {
  //     modalShow: false,
  //     isLoggedIn: true,
  //     userName: props.user,
  //     domain: props.userEmail
  //   }
  // }
const AppHeader = (props) => {
  const history = useHistory();
  const logoutClick = () => {
    history.push("/login");
}
  // render() {
    // let { isLoggedIn, userName, modalShow,domain } = this.state;
    let isLoggedIn = true;
    return (
      <div>
        <nav className="navbar navbar-autocollapse navbar-fixed-top navbar-default">
          <div className="container-fluid">
            <div className="navbar-header">
              <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar1">
                <span className="sr-only">Toggle navigation</span>
                <span className="icon-bar"></span>
                <span className="icon-bar"></span>
                <span className="icon-bar"></span>
              </button>
              {/* <div className="nba-icon">
                <img src={props.logo}/>
              {props.client=='nba' ? 
                <img src={logo} alt="National Basketball Academy"  /> :
                <img src={alnylamLogo} alt="Alnylam" style={{"marginRight": "20px"}} />
                } 
              </div>*/}
              <a className="navbar-brand">
              <img src={props.edocsLogo} className="site-logo-alnylam" />
              {/* {props.client=='nba' ? 
                <img src={edocslogo} alt="NBA eDocs" className="site-logo" />
                :
                <img src={edocsAlLogo} alt="NBA eDocs" className="site-logo-alnylam" />
              } */}
              </a>
              {
                props.showPilotSticker &&
                <span className="release-version-number">PILOT RELEASE V#0.23</span>
              }
              
            </div>
            <div id="navbar1" className="navbar-collapse collapse">
              <ul className="nav navbar-nav">
              </ul>
              <ul className="nav navbar-nav navbar-right rightHeader">
                {/* <li className="dropdown">
                  <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                    <i className="fa fa-bell shake" title="Alerts"></i>
                    <span className="badge">3</span>
                  </a>
                  <ul className="dropdown-menu alert-dropdown">
                    <li>
                      <a href="#">Alert One </a>
                    </li>
                    <li>
                      <a href="#">Alert Two</a>
                    </li>
                    <li>
                      <a href="#">Alert Three</a>
                    </li>
                    <li>
                      <a href="#">Alert Four</a>
                    </li>
                    <li>
                      <a href="#">Alert Five</a>
                    </li>
                    <li className="divider"></li>
                    <li>
                      <a href="#">View All</a>
                    </li>
                  </ul>
                </li> */}
                 {/* <button className="user-icon" data-toggle="dropdown"
                  onClick={() => { props.onShowNotes() }}>
                    <i className="fa fa-sticky-note-o" title="Notes"></i>
                  </button>  */}


                {isLoggedIn ? (
                  <li className="dropdown">
                    <button className="user-icon" data-toggle="dropdown"
                      onClick={() => { logoutClick(); }}
                    >
                      <i className="fa fa-user-times" title="User"></i>
                    </button>
                    <div className="user-desc pull-left">
                    <div className="edocsusername">{props.user}</div>
                    {props.userEmail && <div className="edocsemailid"><em>({props.userEmail})</em></div>}
                    </div>
                  </li>
                ) : <button className="user-icon" data-toggle="dropdown"
                  onClick={() => { this.setState({ modalShow: true }); }}>
                    <i className="fa fa-user" title="User"></i>
                  </button> }

                  <button className="bell-icon">
                    <i className="fa fa-bell" title="Notification"></i>
                  </button>

              </ul>
            </div>
          </div>
        </nav>
        {/* {modalShow &&
          <LoginPopup
            show={modalShow}
            onHide={() => { this.setState({ modalShow: false }); }}
            onSuccess={(username,domain) => {
              this.props.userDetails(username,domain);
              this.setState({ modalShow: false, isLoggedIn: true, userName: username,domain:domain })
            }}
          />
        } */}

      </div>
    );
  // }
}
export default AppHeader;