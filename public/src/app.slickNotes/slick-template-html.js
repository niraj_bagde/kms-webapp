const slickTemplateHTML = `<div class="slickPaper sp-container" ng-click="closeAllPopup($event)">
    <div class="sp-tree">
        <div class="sp-line-1" ng-if="verticalLines"></div>
        <div class="sp-line-2" ng-if="verticalLines"></div>
        <ul class="tree-scrollbar" dnd-list="tree" dnd-drop="dropCallback(event, index, item, external, type, 'containerType')" style="overflow: auto; max-height: 100%;">
            <li class="sp-child-tree-task sp-main-parent" ng-repeat="data in tree track by $index" ng-init="parentIndex = 0" ng-include="'/public/src/app.slickNotes/slick-paper-nested-template.html'" data-index="{{$index}}" dnd-draggable="data" dnd-moved="tree.splice($index, 1)" dnd-effect-allowed="move"></li>
        </ul>
        <ul class="tree-scrollbar" style="clear:both;height:100%;width:100%;overflow:hidden;">
            <li class="sp-child-tree-task sp-main-parent" ng-repeat="data in linesTree" ng-include="'/public/src/app.slickNotes/slick-paper-empty-lines-template.html'"></li>
        </ul>
    </div>
    <div class="sp-tree-userbox ">
        <div class="user-tabs active-user">Amit</div>
        <div class="user-tabs">Abhijit</div>
    </div>
    <div class="notes-tabs-wrapper">
        <div class="notes-tab-1 notes-tabs-active">Sheet 1</div>
        <div class="notes-tab-2">Sheet 2</div>
    </div>
    <div id="spOptionsPopup{{paperUniqueID}}" name=spOptionPopup ng-click="ClosePopup('idSlashPopup')" class="sp-modalSlash optionsPopup" ng-show="slashPopupData.length" style="position: absolute;">
        <div id="spSlashPopup" class="sp-modal-content">
            <div class="sp-modal-body options">
                <div id="{{data.type}}" class="popup-option-txt" ng-click="selectOption($event, data, false)" ng-repeat="data in slashPopupData | filter : { value : slashSearchKeyword }">
                    <span class="popup-option-span"><img ng-src="{{ImagePathPopup}}{{data.imagePath}}"></span>
                    <span class="gen-margin">{{data.value}}</span>
                </div>
            </div>
        </div>
    <input type="file" style="display: none;" accept="image/*" id="spfileUpload" class="InsertedToolBarFileButton">
    </div>

    <div id="spSelectionPopup{{paperUniqueID}}" ng-mouseleave="decorationMouseLeave()" ng-mouseenter="decorationMouseOver()" class="FontToolTip selectionPopup" style="position: absolute;">
        <button title="Bold (Ctrl+B)" ng-click="decorateText('bold')" style="font-weight: bold">B</button>
        <button title="Italic (Ctrl+I)" ng-click="decorateText('italic')" style="font-style: italic;">I</button>
        <button title="Underline (Ctrl+U)" ng-click="decorateText('underline')" style="text-decoration: underline">U</button>
        <button title="Strikethrough" ng-click="decorateText('strikethrough')" style="text-decoration: line-through">S</button>
        <button title="Large Header" ng-click="heading('H1')">H1</button>
        <button title="Medium Header" ng-click="heading('H2')">H2</button>
        <button title="Create Link" ng-click="HyperLink()">HL</button>
    </div>
        <!-- Modal content -->
        <div id="spMainImagePopup{{paperUniqueID}}" class="sp-modal-content-image">
            <span ng-click="closeImagePopup($event)" class="sp-close-img">x</span>
            <div id="spImageSubPopup{{paperUniqueID}}">
                <button class="sp-btn-img" title="Resize Full" ng-click="changeImageDimension($event, 100)">100%</button>
                <button class="sp-btn-img" title="Resize Half" ng-click="changeImageDimension($event, 50)">50%</button>
                <button class="sp-btn-img" title="Resize Quarter" ng-click="changeImageDimension($event, 25)">25%</button>
                <button class="sp-btn-img" title="Align Left" ng-click="alignImage('left')"><img class="btn-img-icon" ng-src="{{ImagePathPopup}}left-align.png"></button>
                <button class="sp-btn-img " title="Align Right" ng-click="alignImage('right')"><img class="btn-img-icon" ng-src="{{ImagePathPopup}}right-align.png"></button>
                <input id="spTxtImageSize{{paperUniqueID}}" ng-keydown="imageTxtEnter($event)" ng-click="spTextImageClick($event)" maxlength="2" placeholder="size" minlength="1" type="text" class="sp-txtImageSize"><button class="sp-btn-img " title="Change Image Size" ng-click="changeImageSize($event)"><img class="btn-img-icon" ng-src="{{ImagePathPopup}}resize.png"></button>
                <button class="sp-btn-img " title="Change Image" ng-click="ChangeImage()"><img class="btn-img-icon" ng-src="{{ImagePathPopup}}change.png"></button>
                <button class="sp-btn-img " title="Remove Image" ng-click="RemoveImage()"><img class="btn-img-icon" ng-src="{{ImagePathPopup}}remove.png"></button>
            </div>
        </div>
        <div id="spImagePopup{{paperUniqueID}}" class="sp-image-modal" ng-click="closeImagePopup($event)">
        </div>

    <div id="tile-overlay" ng-click="closeChangeTypePopup()" style="background:#000; opacity:.5; height:1800px; width:1800px; display:none; margin-top:-50px; z-index:999; position:fixed; top:0; left:0;"></div>
    <div id="changeSiblingsType{{paperUniqueID}}" class="modal closeChangeTypePopup fade in changeSiblingsTypePopupClass" role="dialog" style="display: none;">

      <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" ng-click="closeChangeTypePopup()">×</button>
                  <div style="float:left; font-size:18px; font-weight:bold;" class="modal-title">Change Type</div>
              </div>
              <div class="modal-body">
                  <div class="row">
                      <div class="col-lg-12 col-md-12 col-sm-12">
                          <label id="lblChangeSiblingsType{{paperUniqueID}}">Do you want to remove 'Sheet1'?</label>
                      </div>
                  </div>
              </div>
              <div class="modal-footer">
                  <button  style="width:45px;" type="button" ng-click="changAllSiblingsType()" class="btn btn-default">Ok</button>
                  <button  type="button" class="btn btn-default" ng-click="closeChangeTypePopup()">Cancel</button>
              </div>
          </div>
      </div>
  </div>

    <div id='slickPaperSpinLoading' class="sp-loader">
    </div>

    <script type="text/ng-template" id="popupTmpl.html">
        <div id="spWebLinkPopUp"  class="sp-popup">
            <!--<h2 style="text-align:center;">Create Link</h2>-->
            <div class="ngdialog-content crt-link-popup">
                <label>Display Text</label> </br>
                <input ng-keyup="showIntellisense($event)" id="spDisplayField" type="text" style="width:100%;" ng-model="item.displayText" />
                <label style="margin-top:10px;">URL</label>
                <input ng-keyup="showIntellisense($event)" type="text" style="width:100%;" ng-model="item.linkUrl" />
            </div>
            <div class="ngdialog-buttons crtlnkfooter" style="float: right;">
                <button id="spOkBtn" class="ngdialog-button ok-btn" type="button" ng-keyup="showIntellisense($event)" ng-click="closeThisDialog(item)" style="background-color:#64b64b !important;float: left;transition: all .2s ease-in-out;">
                    <img src="./images/ok.png"/> Ok</button>
                <button id ="spCancelBtn" class="ngdialog-button" type="button" ng-click="closeThisDialog()" style="float: left;transition: all .2s ease-in-out;">
                    <img src="./images/close.png"/> Cancel</button>
            </div>
            <div class="sp-close ngdialog-close">×</div>
        </div>
    </script>

    <script>
        $('#slickPaperSpinLoading').hide();
    </script>
</div>`

export default slickTemplateHTML
