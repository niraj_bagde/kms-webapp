import React, { useState, useEffect } from "react";
import axios from 'axios';
import SlidingPane from "react-sliding-pane";
import "react-sliding-pane/dist/react-sliding-pane.css";
import SlickNotes from "../app.slickNotes/app.slickNotes";
import NotesHelper from '../helpers/NotesHelper';

const userSportsNotes = `[{"id":1401,"name":"Sample Queries","nodes":[{"id":1402,
                        "name":"Basketball Sports Rules","nodes":[],"focus":true,"parentId":1401,"type":"text","eventId":"", "linkData":[],"index":0,"name1":"Basketball Sports Rules"},
                         {"id":1403,"name":"Golden State Warriors","parentId":1401,"focus":false,"type":"text","nodes":[],"name1":"Golden State Warriors","index":1},
                         {"id":1404,"name":"Lakers","parentId":1401,"focus":false,"type":"text","nodes":[],"name1":"Lakers","index":2},
                         {"id":1405,"name":"lebron","parentId":1401,"focus":false,"type":"text","nodes":[],"name1":"lebron","index":3},
                         {"id":1406,"name":"contracts worth more than $11m","parentId":1401,"focus":false,"type":"text","nodes":[],"name1":"contracts worth more than $11m","index":4},
                         {"id":1407,"name":"contracts worth less than $12k in last 5 months","parentId":1401,"focus":false,"type":"text","nodes":[],"name1":"contracts worth less than $12k in last 5 months","index":5},
                         {"id":1408,"name":"contracts signed in last 3 months","parentId":1401,"focus":false,"type":"text","nodes":[],"name1":"contracts signed in last 3 months","index":6},
                         {"id":1409,"name":"Thunder","parentId":1401,"focus":false,"type":"text","nodes":[],"name1":"Thunder","index":7},
                         {"id":1410,"name":"players contract","parentId":1401,"focus":false,"type":"text","nodes":[],"name1":"players contract","index":8},
                         {"id":1411,"name":"Julius Randle - Knicks","parentId":1401,"focus":false,"type":"text","nodes":[],"name1":"Julius Randle - Knicks","index":9},
                         {"id":1412,"name":"registrar","parentId":1401,"focus":false,"type":"text","nodes":[],"name1":"registrar","index":10},
                         {"id":1413,"name":"FIBA Asia Championships","parentId":1401,"focus":false,"type":"text","nodes":[],"name1":"FIBA Asia Championships","index":11},
                         {"id":1414,"name":"title: basketball","parentId":1401,"focus":false,"type":"text","nodes":[],"name1":"title: basketball","index":12}
                        ],
                    "focus":false,"type":"text","name1":"Sample Queries","index":0
            }]`;

const userMedicalNotes = `[{"id":1401,"name":"Sample Queries","nodes":[{"id":1402,"name":"Favourites","focus":false,"parentId":1401,"type":"text","eventId":"", "linkData":[],"index":0,"name1":"Favourites","nodes":[{"id":1501,"name":"RNAi Therapeutics","nodes":[],"focus":false,"parentId":1402,"type":"text","eventId":"","linkData":[],"index":0,"name1":"RNAi Therapeutics"},{"id":1503,"name":"Regeneron","nodes":[],"focus":false,"parentId":1402,"type":"text","eventId":"","linkData":[],"index":2,"name1":"Regeneron"},{"id":1504,"name":"","nodes":[],"focus":false,"parentId":1402,"type":"text","eventId":"","linkData":[],"index":3,"name1":""}]},{"id":1403,"name":"Word Docs","parentId":1401,"focus":false,"type":"text","nodes":[{"id":1601,"name":"Vir Biotechnology","nodes":[],"focus":false,"parentId":1403,"type":"text","eventId":"","linkData":[],"index":0,"name1":"Vir Biotechnology"},{"id":1602,"name":"Sinus Thrombosis","nodes":[],"focus":false,"parentId":1403,"type":"text","eventId":"","linkData":[],"index":1,"name1":"Sinus Thrombosis"},{"id":1603,"name":"","nodes":[],"focus":false,"parentId":1403,"type":"text","eventId":"","linkData":[],"index":2,"name1":""}],"name1":"Word Docs","index":1},
                     {"id":1404,"name":"Search within search","parentId":1401,"focus":false,"type":"text","nodes":[
                      {"id":14041,"name":"healthcare","nodes":[],"focus":false,"parentId":1404,"type":"text","eventId":"", 
                      "linkData":[],"index":0,"name1":"healthcare"},
                      {"id":14042,"name":"title: RNAi","nodes":[],"focus":false,"parentId":1404,"type":"text","eventId":"", 
                      "linkData":[],"index":1,"name1":"title: RNAi"},
                      {"id":14043,"name":"","nodes":[],"focus":false,"parentId":1404,"type":"text","eventId":"", 
                      "linkData":[],"index":2,"name1":""},
                      {"id":14044,"name":"apollo","nodes":[],"focus":false,"parentId":1404,"type":"text","eventId":"", 
                      "linkData":[],"index":3,"name1":"apollo"},
                      {"id":14045,"name":"Pharmacokinetics","nodes":[],"focus":false,"parentId":1404,"type":"text","eventId":"", 
                      "linkData":[],"index":4,"name1":"Pharmacokinetics"},
                      {"id":14046,"name":"","nodes":[],"focus":false,"parentId":1404,"type":"text","eventId":"", 
                      "linkData":[],"index":4,"name1":""}
                     ],"name1":"Search within search","index":2},
                     
                     {"id":1405,"name":"Emails with attachments","parentId":1401,"focus":false,"type":"text","nodes":[
                      {"id":14051,"name":"Medical Research","nodes":[],"focus":false,"parentId":1405,"type":"text","eventId":"", 
                      "linkData":[],"index":0,"name1":"Medical Research"},
                      {"id":14052,"name":"","nodes":[],"focus":false,"parentId":1405,"type":"text","eventId":"", 
                      "linkData":[],"index":1,"name1":""}
                     ],"name1":"Emails with attachments","index":3},
                     
                     {"id":1406,"name":"XLS","parentId":1401,"focus":false,"type":"text","nodes":[
                      {"id":14061,"name":"AHP","nodes":[],"focus":false,"parentId":1406,"type":"text","eventId":"", 
                      "linkData":[],"index":0,"name1":"AHP"},
                      {"id":14062,"name":"","nodes":[],"focus":false,"parentId":1406,"type":"text","eventId":"", 
                      "linkData":[],"index":1,"name1":""}
                     ],"name1":"XLS","index":4},

                     {"id":1407,"name":"PPT","parentId":1401,"focus":false,"type":"text","nodes":[
                      {"id":14071,"name":"Stage RNA","nodes":[],"focus":false,"parentId":1407,"type":"text","eventId":"", 
                      "linkData":[],"index":0,"name1":"Stage RNA"},
                      {"id":14072,"name":"","nodes":[],"focus":false,"parentId":1407,"type":"text","eventId":"", 
                      "linkData":[],"index":1,"name1":""}
                     ],"name1":"PPT","index":5}

                    ],
                "focus":false,"type":"text","name1":"Sample Queries","index":0
        }]`;

class noteSlider extends React.Component {
    constructor(props) {
        super(props);
        console.log('noteslider....' + props.userNotes);
        this.state = {
            notes: "", //props.client == 'alnylam' ? props.userNotes : userNotes
            notesLoading: true,
            isReponseReceived: true,
            notesUpsertArray: []
        }
    }
    componentDidMount() {
        this.getUserNotes();
        document.addEventListener(
            "updateNotes",
            (event) => {
                var notes = event.detail.notes;
                this.setState({
                    notes: JSON.stringify(notes)
                });
                if (this.state.isReponseReceived) {
                    this.updateAllNotes(event.detail.notes);
                }
            },
            false
        );
    }

    getUserNotes = () => {
        let noteRequest = {
            userEmail: this.props.userEmail
        };

        axios.post("/notes/getUserNote", noteRequest).then((res, error) => {
            if (res.data.length > 0) {
                let notesData = NotesHelper.formatNotesData(res.data);
                let noteDataStr = JSON.stringify(notesData);
                this.setState({ notes: noteDataStr, notesLoading: false });
            } else {
                this.setState({ notes: [], notesLoading: false });
            }
        })
    }

    closeNoteView = () => {
        this.props.onCloseNotesSlider(this.state.notes);
    }

    updateAllNotes = (allNotes) => {
        this.setState({isReponseReceived: false});
        let flatArray = [];
        // flatArray = NotesHelper.getFlatArray(allNotes[0], flatArray, this.state.notesUpsertArray);
        flatArray = NotesHelper.createFlatArray(allNotes[0], flatArray, this.state.notesUpsertArray);
        console.log("flatArray")
        console.log(flatArray)
        let notes = {allNotes: flatArray, userEmail: this.props.userEmail};
        axios.post("notes/updateInsertNotes", notes).then(response => {
            this.setState({notesUpsertArray: response.data}, function () {
                this.setState({isReponseReceived: true})
            });
            this.getUserNotes();
        })
    }

    render = () => (
        <SlidingPane
            id="noteSlider"
            className="right-note-panel"
            overlayClassName="some-custom-overlay-class"
            isOpen={this.props.isNotesOpen}
            width="85%"
            onRequestClose={() => {
                this.props.onCloseNotesSlider(this.state.notes);
            }}
        >
            {this.state.notesLoading && <div>Notes are being loaded</div>}
            {!this.state.notesLoading && <SlickNotes userNotes={this.state.notes} onClose={() => this.closeNoteView()} onPerformSearch={(data) => { this.props.onPerformSearch(data) }} />}
        </SlidingPane>
    );
}

export default noteSlider;


