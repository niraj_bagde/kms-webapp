// Add findDeep function in the underscore object
// Used to get the shared task's individual tree
// Reference: http://stackoverflow.com/a/17580000/2025923
_.findDeep = function(items, attrs) {
    'use strict';

    function match(value) {
        for (var key in attrs) {
            if (!_.isUndefined(value) && value !== null) {
                if (attrs[key] !== value[key]) {
                    return false;
                }
            } else {
                return false;
            }
        }
        return true;
    }

    function traverse(value) {
        var result;

        _.forEach(value, function(val) {
            if (match(val)) {
                result = val;
                return false;
            }

            if (_.isObject(val) || _.isArray(val)) {
                result = traverse(val);
            }

            if (result) {
                return false;
            }
        });

        return result;
    }
    return traverse(items);
};

slickPaperUtils = (function() {
    'use strict';

    // To get random MongoDB Object ID
    var getObjectId = function() {
        return new Meteor.Collection.ObjectID()._str;
    };

    // To get the tree controller scope object
    // from outside of the Angular Scope
    var getTreeScope = function() {
        return angular.element(document.querySelector('[ng-controller="TreeController"]')).scope();
    };

    // Get non-empty tasks count
    var getNonEmptyTask = function(arr) {
        return arr.filter(function(task) {
            return task.name.trim();
        });
    };

    var scrollIntoView = function(el) {
        // Reference: https://github.com/litera/jquery-scrollintoview
        return $(el).scrollintoview({
            direction: "vertical",
            duration: 50 // 50 ms
        });

        var view = $('.sp-tree').first(),
            elementTop = el.closest('li.sp-main-parent').position().top === 0 ? el.closest('li.sp-main-parent').offset().top : el.closest('li.sp-main-parent').position().top;

        if (view.outerHeight() < $('.sp-tree').outerHeight()) {
            return;
        }

        if (el.parents('li').length > 1) {
            var scrollTop = view.scrollTop();
            elementTop = el.closest('li').offset().top;
            var actualTop = elementTop - scrollTop;
            if (actualTop > view.outerHeight()) {
                return;
            }
        }

        if (view.outerHeight() - elementTop <= 0) {
            el[0].scrollIntoView(false);
        } else if (elementTop < 0) {
            el[0].scrollIntoView(true);
        }
    };

    // Reference: http://stackoverflow.com/a/3976125/2025923
    var getCaretPosition = function(editableDiv) {
            var caretPos = 0,
                sel, range;
            if (window.getSelection) {
                sel = window.getSelection();
                if (sel.rangeCount) {
                    range = sel.getRangeAt(0);
                    if (range.commonAncestorContainer.parentNode == editableDiv) {
                        caretPos = range.endOffset;
                    }
                }
            } else if (document.selection && document.selection.createRange) {
                range = document.selection.createRange();
                if (range.parentElement() == editableDiv) {
                    var tempEl = document.createElement("span");
                    editableDiv.insertBefore(tempEl, editableDiv.firstChild);
                    var tempRange = range.duplicate();
                    tempRange.moveToElementText(tempEl);
                    tempRange.setEndPoint("EndToEnd", range);
                    caretPos = tempRange.text.length;
                }
            }
            return caretPos;
        },

        // Reference: http://stackoverflow.com/a/6249440/2025923
        setCaretPosition = function(el, index) {

            index = typeof index === 'undefined' ? el && el.textContent && el.textContent.trim().length || 0 : index;

            var range = document.createRange(),
                sel = window.getSelection();

            try {
                range.setStart(el.childNodes[0] || el, index);
            } catch (e) {
                console.error('ERROR:', e);
                range.setStart(el.childNodes[0], 0);
            }
            range.collapse(true);
            sel.removeAllRanges();
            sel.addRange(range);
        };

    // Reference: http://stackoverflow.com/a/5379408/2025923
    function getSelectionText() {
        var text = "";
        if (window.getSelection) {
            text = window.getSelection().toString();
        } else if (document.selection && document.selection.type != "Control") {
            text = document.selection.createRange().text;
        }

        return text;
    }

    function clearSelection() {
        if (window.getSelection) {
            if (window.getSelection().empty) { // Chrome
                window.getSelection().empty();
            } else if (window.getSelection().removeAllRanges) { // Firefox
                window.getSelection().removeAllRanges();
            }
        } else if (document.selection) { // IE?
            document.selection.empty();
        }
    }
    // Mixpanel Tracking Events
    var trackEvents = function(action, property) {
        property = property || {};
        if (Session.get('username')) {
            property.username = Session.get('username');
        }

        mixpanel.track(action, property);
    };

    var getParentlength = function(nodes) {
        window.count++;

        for (var i = 0; i < nodes.length; i++) {
            getParentlength(nodes[i].nodes);
        }
        return count;
    };

    var getTaskSummary = function(taskId, userId) {
        var data = TasksData.find({
            userId: userId,
            _id: taskId
        }).fetch();

        return data;
    };
    // Takes an ISO time and returns a string representing how
    // long ago the date represents.
    function prettyDate(date) {
        var diff, day_diff;
        try {
            diff = (((new Date()).getTime() - date.getTime()) / 1000);
            day_diff = Math.floor(diff / 86400);
        } catch (e) {
            return date;
        }

        if (isNaN(day_diff) || day_diff < 0 || day_diff >= 31)
            return date;

        return day_diff === 0 && (
                diff < 60 && "just now" ||
                diff < 120 && "1 minute ago" ||
                diff < 3600 && Math.floor(diff / 60) + " minutes ago" ||
                diff < 7200 && "1 hour ago" ||
                diff < 86400 && Math.floor(diff / 3600) + " hours ago") ||
            day_diff == 1 && "Yesterday" ||
            day_diff < 7 && day_diff + " days ago" ||
            day_diff < 31 && Math.ceil(day_diff / 7) + " weeks ago";
    };

    function getCaretPixelPos($node, offsetx, offsety) {

        offsetx = offsetx || 0;
        offsety = offsety || 0;

        var nodeLeft = 0,
            nodeTop = 0;
        if ($node) {
            nodeLeft = $node.offsetLeft;
            nodeTop = $node.offsetTop;
        }

        var pos = {
            left: 0,
            top: 0
        };

        if (document.selection) {
            var range = document.selection.createRange();
            pos.left = range.offsetLeft + offsetx - nodeLeft + 'px';
            pos.top = range.offsetTop + offsety - nodeTop + 'px';
        } else if (window.getSelection) {
            var sel = window.getSelection();
            var range = sel.getRangeAt(0).cloneRange();
            try {
                range.setStart(range.startContainer, range.startOffset - 1);
            } catch (e) {}
            var rect = range.getBoundingClientRect();
            if (range.endOffset == 0 || range.toString() === '') {
                // first char of line
                if (range.startContainer == $node) {
                    // empty div
                    if (range.endOffset == 0) {
                        pos.top = '0px';
                        pos.left = '0px';
                    } else {
                        // firefox need this
                        var range2 = range.cloneRange();
                        range2.setStart(range2.startContainer, 0);
                        var rect2 = range2.getBoundingClientRect();
                        pos.left = rect2.left + offsetx - nodeLeft + 'px';
                        pos.top = rect2.top + rect2.height + offsety - nodeTop + 'px';
                    }
                } else {
                    pos.top = range.startContainer.offsetTop + 'px';
                    pos.left = range.startContainer.offsetLeft + 'px';
                }
            } else {
                pos.left = rect.left + rect.width + offsetx - nodeLeft + 'px';
                pos.top = rect.top + offsety - nodeTop + 'px';
            }
        }
        return pos;
    };


    // For Copy-Paste
    var createTreeFromHierarchy = (function() {
        'use strict';

        var parents = [];
        var prevLevel = 0;

        var createItem = function(name, parentId) {
            return {
                id: +new Date() + parseInt(Math.random() * 1000, 10),
                name: name,
                name1: name,
                parentId: parentId || 0,
                nodes: []
            }
        };

        function createHierarchy(lines, result, mainParentId) {
            function addParent(el) {
                result.push(el);
            }

            function createTree(lines, parent, parentId) {
                if (lines.length === 0) {
                    return parent;
                }

                var line = lines.shift();

                // Check if this line is parent
                var isParent = /^\s*-/.test(line);
                var element = createItem(line.replace(/^\s*-\s*/, '').trim(), parentId);

                var level = line.match(/^\t*/)[0].length;
                if (isParent) {
                    parents[level] = element;
                }

                if (level === 0) {
                    element.parentId = mainParentId;
                    addParent(element);
                    return createTree(lines, element.nodes, element.id);
                }
                if (level > prevLevel) {
                    // If level decreased
                    var prevParent = parents[prevLevel];
                    element.parentId = prevParent.id;
                    prevParent.nodes.push(element);
                    prevLevel = level;
                    return createTree(lines, prevParent.nodes, prevParent.id);
                } else if (level < prevLevel) {
                    var newParent = parents[level - 1];
                    element.parentId = newParent.id;
                    newParent.nodes.push(element);
                    prevLevel = level;
                    // If level increased
                    return createTree(lines, newParent.nodes, newParent.id);

                } else {
                    parent.push(element);
                    // Level same as prev level
                    if (lines.length) {
                        return createTree(lines, parent, parentId);
                    }
                    return parent;
                }
            }

            return createTree(lines, result);
        }


        return createHierarchy;
    }());

    return {
        getObjectId: getObjectId,

        getTreeScope: getTreeScope,
        getNonEmptyTask: getNonEmptyTask,
        scrollIntoView: scrollIntoView,

        // To get/set caret position in the textbox/textarea/contenteditable
        getCaretPosition: getCaretPosition,
        setCaretPosition: setCaretPosition,


        // Mixpanel events Tracking
        trackEvents: trackEvents,

        //Get count of the Children
        getParentlength: getParentlength,

        //Get summary of Task
        getTaskSummary: getTaskSummary,

        //Format Date
        prettyDate: prettyDate,

        // Text selection
        getSelectionText: getSelectionText,
        clearSelection: clearSelection,

        //getCaretPosinPixel
        getCaretPixelPos: getCaretPixelPos,

        createTreeFromHierarchy: createTreeFromHierarchy
    };
}());

function getFirstEmptyTask(tasks, index) {
    index = index || 0;
    for (var i = index, len = tasks.length; i < len; i++) {
        var e = tasks[i];

        if (e.name.trim() === '') {
            return e.id;
        }
    }
}

function SetCaretPosition(el, pos) {
    if (el && el.childNodes && el.childNodes.length) {
        pos = pos || el.textContent.length;

        //Loop through all child nodes
        for (var key in el.childNodes) {
            var node = el.childNodes[key];

            if (key != 'length') {
                if (node.nodeType === 3) {
                    // we have a text node
                    if (node.length >= pos) {
                        // finally add our range
                        var range = document.createRange(),
                            sel = window.getSelection();
                        range.setStart(node, pos);
                        range.collapse(true);
                        sel.removeAllRanges();
                        sel.addRange(range);
                        return -1; // we are done
                    } else {
                        pos -= node.length;
                    }
                } else {
                    pos = SetCaretPosition(node, pos);
                    if (pos == -1) {
                        return -1; // no need to finish the for loop
                    }
                }
            } else {
                return pos;
            }
        }
        return pos; // needed because of recursion stuff
    } else {
        slickPaperUtils.setCaretPosition(el);
    }
}

var getNewItem = function(id, parentId, name, type) {
    var item = {
        id: id || +new Date() + Math.floor(Math.random() * (1000 - 1)),
        name: name || ' ',
        parentId: parentId || 0,
        focus: true,
        type: type || 'text',
        nodes: []
    };
    return item;
};

angular.module('slickPaper', ['dndLists', 'ngDialog']);

angular.module('slickPaper').filter('spUnsafe', ['$sce', function($sce) {
    return function(val) {
        return $sce.trustAsHtml(val);
    };
}]);


angular.module('slickPaper').directive('slickPaper', ['$rootScope', '$timeout', '$sce', '$compile', 'ngDialog', function($rootScope, $timeout, $sce, $compile, ngDialog) {
    return {
        restrict: 'AE',
        scope: true,
        templateUrl: './scripts/3rdi-components/3rdi-note/slick-template.html',
        link: function(scope, element, attributes, paperService) {
            // To show non-editable lines
            var lineItems = [];
            for (var i = 0; i < 10; i++) {
                lineItems.push({
                    id: 'Foo',
                    name: '',
                    nodes: []
                });
            }
            scope.linesTree = lineItems;
            scope.usedInSlick = attributes.usedSlickPaper === undefined ? "false" : attributes.usedSlickPaper;;
            scope.isUsedInThirdI  = attributes.usedInThirdi === undefined ? "false" : attributes.usedInThirdi;;
            // Configurable options
            scope.verticalLines = attributes.verticalLines === 'true';
            scope.isCursorOnDecoration = false;
            scope.horizontalLinesClass = attributes.horizontalLines === 'true' ? 'sp-line' : '';

            window.getEmptyTree = function(len) {
               len = len || 10;
               var emptyNotes = [];
               for (var i = 0; i < len; i++) {
                   emptyNotes.push(getNewItem());
               }
               return emptyNotes;
           };

            ////comment because of mearge code
            // var data = [];
            // for (var i = 0; i < 10; i++) {
            //     data.push(getNewItem());
            // }

            if (attributes.background) {
                element.find('.sp-tree').css('background', attributes.background);
            }

            var className = attributes.hideOptionIcon === 'true' ? 'sp-hideOptionIcon' : '';
            if (className) {
                element.addClass(className);
                scope.verticalLines = false;
            }

            if (attributes.data) {
                try {
                    var updatedData = JSON.parse(attributes.data);
                    updatedData = updatedData.filter(function(e) {
                        return e;
                    });
                    scope.tree = updatedData.length ? updatedData : window.getEmptyTree();
                } catch (e) {
                    console.error('SlickPaper: Invalid JSON passed to slick-paper directive', e);
                }
            }
            scope.tree = scope.tree || window.getEmptyTree();


            // Update data dynamically
            $rootScope.$on('slickPaperDataLoad', function(e, args) {
                var notes = Session.get('notes');
                var sheetID = Session.get('currentSheet');
                var notesArray = _.find(notes, function(note) {
                    return note.id === sheetID;
                });

                setTimeout(function() {
                    $scope.$apply(function() {
                        scope.tree = notesArray && notesArray.notesData && notesArray.notesData.length > 0 ? notesArray.notesData : getEmptyTree();
                    });
                }, 100);

                // Hide Loader
                mainUtils.hideLoader();

            });

            window.compilePaperComponent = function(slickPaperID, scope) {
              $compile(angular.element($('[slick-paper-id="' + slickPaperID + '"]')[0]))(scope);
            };

            window.compileTileHeader = function() {
               $compile(angular.element($('.note-title')))(angular.element($('.note-title')).scope());
           };

           window.compileSingleTileHeader = function(slickPaperID, scope) {
               $compile(angular.element($('[tile-paper-id="' + slickPaperID + '"]')[0]))(scope);
           };

           window.setPaperDataNew = function(data) {
                var updatedData = JSON.parse(data);
                if(updatedData){
                  scope.tree  = updatedData;
                }else{
                  scope.tree = scope.tree || window.getEmptyTree();
                }
            }

            var isConditionTrue = false;
            // Set focus on first item by default only if current view is listView
            if(scope.usedInSlick === "true")
            {
              if (Session.get('notesView') !== "tileView" && Session.get('notesView') !== "listView" && Session.get('notesView') !== undefined) {
                if (scope.usedInSlick || ($(element[0]).is(':visible'))) {
                  isConditionTrue = true;
                }
              }
            }
            else {
              isConditionTrue = true;
            }
            if(isConditionTrue)
            {
              setTimeout(function() {
                var firstElement = element.find('div[sp-editable]:first').focus();
                slickPaperUtils.setCaretPosition(firstElement[0]);
              }, 1000);
            }

            if ($rootScope.IDForSlashpopup_Paper == undefined) {
                $rootScope.IDForSlashpopup_Paper = 1;
                scope.paperUniqueID = $rootScope.IDForSlashpopup_Paper;
            } else {
                $rootScope.IDForSlashpopup_Paper = ++$rootScope.IDForSlashpopup_Paper;
                scope.paperUniqueID = $rootScope.IDForSlashpopup_Paper;
            }

            var $scope = scope;

            scope.item = {
                displayText: "",
                linkUrl: "http://"
            };

            $rootScope.uniqueID = 0;
            scope.divToAdd = 2;

            scope.ImagePathPopup = "./images/"

            $scope.getPaperData = function() {
                return JSON.stringify(scope.tree);
            };

            window.spRemoveTask  = function(taskId) {

                var task = _.findDeep(scope.tree, {
                    id: taskId
                });

                var nodes;

                if (task && task.parentId > 0) {

                    parent = _.findDeep(scope.tree, {
                        id: task.parentId
                    });

                    nodes = parent.nodes;
                } else {
                    nodes = scope.tree;
                }

                var index = nodes.indexOf(task);
                nodes.splice(index, 1);

                if (scope.tree.length < 1) {
                    scope.tree = [getNewItem()];
                }
            };

            function getTreeScope() {
                return $scope;
            }

            scope.delete = function(data, $parent) {
              scope.dataChange = true;
                if (data.parentId === 0 || !$parent.$parent.data) {
                    var index = _.findIndex(scope.tree, {
                        id: data.id
                    });
                    scope.onNgblur(null, data);
                    scope.tree.splice(index, 1);
                } else {
                    scope.onNgblur(null, data);
                    var parent = $parent.$parent.$parent.data;
                    var index = parent.nodes.indexOf(data);
                    parent.nodes.splice(index, 1);
                }

                if (scope.tree.length < 1) {
                    scope.tree = [getNewItem()];
                }
            };

            scope.add = function(treeData, data, addChild, isCollapsed, changeDataType) {
                var newData = getNewItem(0, treeData.id, '', data.type);
                id = newData.id;

                if (changeDataType) {
                    newData.type = changeDataType;
                }

                if (addChild && !isCollapsed) {
                    // When having children, add new child at the end of the current childrens
                    if (getEmptyTasksCount(treeData.nodes), -1) {
                        treeData.nodes.unshift(newData);
                    } else {
                        id = getFirstEmptyTask(treeData.nodes);
                    }
                } else {
                    var itemIndex = treeData.nodes.indexOf(data);
                    // No childrens, then add new task as next task
                    if (getEmptyTasksCount(treeData.nodes, itemIndex)) {
                        treeData.nodes.splice(itemIndex + 1, 0, newData);

                    } else {
                        id = getFirstEmptyTask(treeData.nodes, itemIndex + 1);
                        treeData.nodes[itemIndex + 1].type = newData.type;
                    }
                }

                scope.newItemId = id;
                return id;
            };

            scope.addNext = function(data, $parent, isCollapsed, $element, type, isAddExtraDiv, changeDataType) {

                if (data.parentId === 0) {
                    // Add new items on the first level
                    var treeData = scope.tree,
                        treeLength = treeData.length;

                    var newData = getNewItem(0, 0, '', type || data.type);
                    var itemIndex = treeData.indexOf(data);
                    id = newData.id;

                    if ((data.nodes.length > 0) && !isCollapsed) {
                        newData.parentId = data.id;
                        if (getEmptyTasksCount(treeData[itemIndex].nodes, -1)) {
                            // treeData[itemIndex].nodes.push(newData); // Add new child node
                            treeData[itemIndex].nodes.unshift(newData);
                            //SAVING THE DATA TO DB
                            //scope.onNgblur({}, newData);
                        } else {
                            id = getFirstEmptyTask(treeData[itemIndex].nodes);
                        }
                    } else {
                        //scope.olElement(data,$parent, $element, type);
                        if (getEmptyTasksCount(treeData, itemIndex)) {
                            //SAVING THE DATA TO DB
                            scope.onNgblur({}, newData);

                            if (isAddExtraDiv == undefined) {
                                treeData.splice(itemIndex + 1, 0, newData);
                            } else {
                                treeData.splice(treeLength, 0, newData);
                            }
                        } else {
                            id = getFirstEmptyTask(treeData, itemIndex + 1);
                            treeData[itemIndex + 1].type = newData.type;
                        }
                    }
                    scope.newItemId = id;
                    scope.setNameProperty(scope.newItemId);
                    return id;
                } else {
                    // If already having children, then add new children. Else add new item in parent.
                    var parent = data.nodes.length > 0 ? data : $parent.data;
                    // If collapsed, then add to parent, else add new child
                    parent = isCollapsed ? $parent.data : parent;

                    var newItemId = scope.add(parent, data, data.nodes.length, isCollapsed, changeDataType);
                    scope.setNameProperty(newItemId);
                    return newItemId;

                }
            };

            scope.addItem = function($event, data, $parent, $element, type, isAddExtraDiv, isSetFocus) {

                var $element = $element || $($event.currentTarget);
                clearTimeout(timeout);

                if ($event && isAddExtraDiv == undefined) {
                    $event.preventDefault();
                }

                //Change data type for indent with enter item (image to text)
                var changeDataType;

                if ($event.type === "keydown" && type === "image") {
                    type = "text";
                    changeDataType = 'text';
                    //data.type = 'text';
                }

                var isCollapsed = $element.closest('li').children('ul').hasClass('hide');

                var id = scope.addNext(data, $parent.$parent, isCollapsed, $element, type || data.type, isAddExtraDiv, changeDataType);

                if (isAddExtraDiv) {
                    return;
                }

                $timeout(function() {
                    if (isSetFocus === true) {
                        focusItemById(id);
                    }
                });
                return id;
            };


            // To check if all the selected elements has the same parent
            // #Scenario 1
            scope.hasSameParent = function() {
                var combinedArr = _.uniq(scope.selectedTaskData.concat(scope.selectedTaskRangeData), 'data.id');

                return combinedArr.every(function(e) {
                    return combinedArr[0].data.parentId === e.data.parentId;
                });
            };

            // To sort the selected tasks by index
            scope.sortSelectedTasks = function() {
                return _.uniq(_.sortBy(scope.selectedTaskData, 'data.index'), 'data.id');
            };

            scope.sortByParent = function() {
                var sorted = _.sortBy(scope.selectedTaskData, function(item) {
                    var parents = 0,
                        currentParent = item.$parent;
                    while (currentParent) {
                        currentParent = currentPar.$parent;
                        parents++;
                    }

                    return -parents;
                });
                return sorted;
            };

            /**
             * Multiple selections indent/outdent helpers
             */
            scope.selectedTaskData = [];
            // To store the tasks which are selected by using CTRL/Click
            scope.addTaskToSelection = function(task, parent, parentIndex) {
                var taskData = {
                    data: task,
                    parent: parent,
                    parentIndex: parentIndex
                };

                // If not present in the selection array, then add it
                if (_.some(scope.selectedTaskData, function(el) {
                        return el.data.id === task.id;
                    }) === false) {
                    scope.selectedTaskData.push(taskData);
                }
            };

            scope.removeTaskFromSelection = function(task) {
                // Check if task is from the range selection
                if (scope.selectedTaskRangeData.length === 2) {
                    // Get individual items in the range
                    var selectedTasks = $('.selectedCopy');
                    var selectedItemsId = scope.selectedTaskData.map(function(item) {
                        return item.data.id;
                    });

                    selectedTasks.each(function() {
                        var taskId = $(this).find('div[sp-editable]:first').data('sptaskid');
                        if (selectedItemsId.indexOf(taskId) === -1) {
                            var $parent = angular.element($('[data-sptaskid="' + taskId + '"]').closest('li')[0]).scope().$parent;
                            var data = _.findDeep(scope.tree, {
                                id: taskId
                            });
                            data.index = $(this).index();

                            scope.addTaskToSelection(data, $parent);
                        }

                        $('.firstCopy, .currentCopy').removeClass('firstCopy currentCopy');
                    });

                    // Clear range array
                    scope.selectedTaskRangeData = [];
                    scope.rangeSelection = false;
                }

                _.remove(scope.selectedTaskData, function(el) {
                    return el.data.id === task.id;
                });
            };

            scope.selectedTaskRangeData = [];
            // To store the tasks which are selected by using SHIFT
            scope.addTaskToRangeSelection = function(task, parent, parentIndex) {
                if (scope.selectedTaskRangeData.length >= 2) {
                    scope.selectedTaskRangeData = 1;
                    scope.selectedTaskRangeData[1] = {
                        data: task,
                        parent: parent,
                        parentIndex: parentIndex
                    };
                } else {
                    scope.selectedTaskRangeData.push({
                        data: task,
                        parent: parent,
                        parentIndex: parentIndex
                    });
                }
            };

            scope.removeAllTasksFromSelection = function() {
                scope.selectedTaskData = [];
                scope.selectedTaskRangeData = [];
                firstSelectedTask = undefined;
                scope.shiftSelectionTask = undefined;
            };

            // To get the range of tasks from the first selected task to the last selected tasks
            scope.getTaskRange = function() {
                scope.selectedTaskRangeData = _.uniq(scope.selectedTaskRangeData, 'data.id');

                if (scope.selectedTaskRangeData.length === 2) {
                    // Sort by index
                    scope.selectedTaskRangeData = _.sortBy(scope.selectedTaskRangeData, 'data.index');

                    var firstTask = scope.selectedTaskRangeData[0].data,
                        lastTask = scope.selectedTaskRangeData[1].data;
                    var subtree = scope.tree;

                    if (firstTask.parentId !== 0) {
                        subtree = _.findDeep(scope.tree, {
                            id: firstTask.parentId
                        });
                        if (subtree && subtree.nodes) {
                            subtree = subtree.nodes;
                        }
                    }

                    // Get only selected tasks range out of subtree
                    var parent = scope.selectedTaskRangeData[0].parent;

                    var firstTaskAdded = false,
                        addTask = false;
                    subtree = subtree.filter(function(task) {
                        if (!firstTaskAdded && task.id === firstTask.id) {
                            firstTaskAdded = true;
                            return true; // First task
                        } else if (firstTaskAdded && task.id !== lastTask.id) {
                            return true;
                        } else if (task.id === lastTask.id) {
                            firstTaskAdded = false;
                            return true; // Last task
                        }
                        return false;
                    }).map(function(task) {
                        task.index = $('[data-sptaskid="' + task.id + '"]').closest('li').index();
                        return {
                            data: task,
                            parent: parent
                        };
                    });

                    return subtree;

                    // Get combined data
                    // scope.selectedTaskData = scope.sortSelectedTasks(scope.selectedTaskData.concat(subtree));
                } else if (scope.selectedTaskRangeData.length === 1) {
                    scope.selectedTaskData = scope.selectedTaskData.concat(scope.selectedTaskRangeData);
                }
            };

            scope.shiftLeft = function(data, $parent) {
                var parent;

                if (data.parentId === 0) {
                    return data;
                }

                var imPar = $parent.$parent.$parent.data;
                var parentNodeIndex = -1;

                if (imPar.parentId === 0) {
                    parent = scope.tree;
                } else {
                    parent = $parent.$parent.$parent.$parent.$parent.data.nodes;
                }

                var index = imPar.nodes.indexOf(data);
                var cutData = imPar.nodes.splice(index, 1);
                parentNodeIndex = parent.indexOf(imPar);
                parent.splice(parentNodeIndex + 1, 0, cutData[0]);

                data.parentId = imPar.parentId;

                return data;
            };

            scope.shiftRight = function(data, $parent) {
                var parent;

                if (data.parentId === 0) {
                    parent = scope.tree;
                } else {
                    parent = $parent.$parent.$parent.data.nodes;
                }

                var index = parent.indexOf(data);
                if (index > 0) {
                    var cutData = parent.splice(index, 1)[0];
                    var prevNode = parent[index - 1];
                    prevNode.nodes.push(cutData);

                    data.parentId = prevNode.id;
                }

                return data;
            };


            /**
             * For tasks selection and copying to the clipboard
             */
            var isIe = (navigator.userAgent.toLowerCase().indexOf('msie') != -1 || navigator.userAgent.toLowerCase().indexOf('trident') != -1);
              element[0].addEventListener('cut', cutToClipboard, false);
              element[0].addEventListener('copy', copyToClipboard, false);

              function cutToClipboard(e) {
                  // First copy the data to clipboard
                  copyToClipboard(e);

                  // Then delete selected data
                  scope.removeSelectedTasks(e);
              }


            function copyToClipboard(e) {
                $('.manualSelect').find('li:not(.manualSelect)').addClass('selectedCopy parent');
                $('.manualSelect').parents('li:not(.manualSelect)').addClass('selectedCopy parent');

                var textToPutOnClipboard = '';
                if ($('.selectedCopy').length) {
                    var selectedTasks = [],
                        copyAsHtml = '';
                    var prevLevel = -1,
                        openUlCount = 0;

                    $('.selectedCopy').each(function() {
                        var taskTitle = $(this).find('div[sp-editable]:first').text().trim(),
                            level = $(this).parents('ul').length - 1;
                        var parentPrefix = $(this).closest('li').find('.selectedCopy').not(this).length ? '- ' : '';

                        if (taskTitle.length) {
                            if (prevLevel < level) {
                                copyAsHtml += '<ul><li>' + taskTitle + '</li>';
                                openUlCount++;
                            } else if (prevLevel > level) {
                                copyAsHtml += '</ul><li>' + taskTitle + '</li>';
                                openUlCount--;
                            } else if (prevLevel === level) {
                                copyAsHtml += '<li>' + taskTitle + '</li>';
                            }

                            prevLevel = level;
                            taskTitle = '\t'.repeat(level) + parentPrefix + taskTitle;
                            selectedTasks.push(taskTitle);
                        }
                    });
                    copyAsHtml += '</ul>'.repeat(openUlCount);

                    textToPutOnClipboard = $('#copyAsHtml').is(':checked') ? copyAsHtml : selectedTasks.join('\t\r\n');

                    e.preventDefault();


                    $('.selectedCopy').find('li.selectedCopy:not(.manualSelect)').removeClass('selectedCopy parent');
                    $('.selectedCopy').parents('li.selectedCopy:not(.manualSelect)').removeClass('selectedCopy parent');
                }

                if (isIe) {
                    window.clipboardData.setData('Text', textToPutOnClipboard);
                } else {
                    e.clipboardData.setData('text/plain', textToPutOnClipboard);
                }
            }

            function getPastedUrl(e)
            {
                var pastedText = undefined;
                var anchorTag;
                var urlRegex = /((http|https):?\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})/;
                if (window.clipboardData && window.clipboardData.getData) { // IE
                        pastedText = window.clipboardData.getData('Text');
                    }
                else if (e.clipboardData && e.clipboardData.getData) {
                     pastedText = e.clipboardData.getData('text/plain');
                    }
                if(urlRegex.test(pastedText)){
                    return pastedText;
                }else{
                    return '';
                }
            }

            function pasteData(e) {
                $('.unsaved-popup').show();
                if ($(e.target).closest('[slick-paper]').length && scope.data) {
                    var pastedUrl = getPastedUrl(e);
                    var pastedText = undefined;
                    var self = this;
                    if(pastedUrl === ""){
                        if (window.clipboardData && window.clipboardData.getData) { // IE
                            pastedText = window.clipboardData.getData('Text');
                            window.clipboardData.setData('Text', '');
                        } else if (e.clipboardData && e.clipboardData.getData) {
                            pastedText = e.clipboardData.getData('text/plain');
                            e.clipboardData.setData('text/plain', '');
                        }
                    }else{
                        if (window.clipboardData && window.clipboardData.getData){
                           window.clipboardData.setData('Text', '');
                        } else if(e.clipboardData && e.clipboardData.getData){
                           e.clipboardData.setData('text/plain', '');
                        }
                        pastedText = pastedUrl;
                        //e.clipboardData.setData('text/plain', pastedText);
                        setTimeout(function() {
                            $(e.target).find('span').html(pastedText);
                            focusItemById(scope.data.id);
                        });
                    }

                    var lines = pastedText.split(/\n/);
                    if (lines.length > 1) {
                        if (scope.data.name.trim() === '') {
                            scope.delete(scope.data, scope.parent);
                            scope.data.index--;
                        }
                        var hierarchy = [];
                        slickPaperUtils.createTreeFromHierarchy(lines, hierarchy, scope.data.parentId);
                        if (scope.data.parentId == 0) {
                            // scope.tree.splice(scope.data.index + 1, 0, ...hierarchy);
                            [].splice.apply(scope.tree, [scope.data.index + 1, 0].concat(hierarchy));
                        } else {
                            var parent = scope.parent.$parent.$parent.data;
                            // parent.nodes.splice(scope.data.index + 1, 0, ...hierarchy);
                            [].splice.apply(parent.nodes, [scope.data.index + 1, 0].concat(hierarchy));
                        }
                        e.preventDefault();
                        e.stopPropagation();
                        setTimeout(function() {
                            scope.sendDataToParent();
                            $('.unsaved-popup').hide();
                        });
                        return false;
                    }
                }
            }

            function pasteDataOld(e) {
              if(scope.usedInSlick == "true") //Code is used in slick
                $('.unsaved-popup').show();
                if ($(e.target).closest('[slick-paper]').length && scope.data) {
                    var pastedUrl = getPastedUrl(e);
                    var pastedText = undefined;
                    if(pastedUrl === ""){
                        if (window.clipboardData && window.clipboardData.getData) { // IE
                            pastedText = window.clipboardData.getData('Text');
                            window.clipboardData.setData('Text', '');
                        } else if (e.clipboardData && e.clipboardData.getData) {
                            pastedText = e.clipboardData.getData('text/plain');
                            e.clipboardData.setData('text/plain', '');
                        }
                    } else{
                        if (window.clipboardData && window.clipboardData.getData){
                           window.clipboardData.setData('Text', '');
                        } else if(e.clipboardData && e.clipboardData.getData){
                           e.clipboardData.setData('text/plain', '');
                        }
                        pastedText = pastedUrl;
                        //e.clipboardData.setData('text/plain', pastedText);
                        setTimeout(function() {
                            $(e.target).find('span').html(pastedText);
                        });
                    }

                    var lines = pastedText.split(/\n/);
                    if (lines.length > 1) {
                        if (scope.data.name.trim() === '') {
                            scope.delete(scope.data, scope.parent);
                            scope.data.index--;
                        }
                        var hierarchy = [];
                        slickPaperUtils.createTreeFromHierarchy(lines, hierarchy, scope.data.parentId);
                        if (scope.data.parentId == 0) {
                            [].splice.apply(scope.tree, [scope.data.index + 1, 0].concat(hierarchy));
                        } else {
                            var parent = scope.parent.$parent.$parent.data;
                            [].splice.apply(parent.nodes, [scope.data.index + 1, 0].concat(hierarchy));
                        }
                        e.preventDefault();
                        e.stopPropagation();
                        setTimeout(function() {
                            scope.sendDataToParent();
                            $('.unsaved-popup').hide();
                        });
                        return false;
                    }
                    // else {
                    //     if (scope.data.name1 === undefined) {
                    //         scope.data.name1 = "";
                    //     }
                    //     scope.data.name += pastedText;
                    //     scope.data.name1 += pastedText;
                    //     setTimeout(function() {
                    //         SetCaretPosition(e.target);
                    //         scope.sendDataToParent();
                    //         $('.unsaved-popup').hide();
                    //     });
                    // }
                }
            }


            // To clear selection on ESCAPE press anywhere
            $(document.body).on('keydown', function(e) {
                if (e.keyCode === keyCodes.escape) {
                    clearSelection();

                    var treeScope = getTreeScope();
                    treeScope.removeAllTasksFromSelection();
                    treeScope.hideOptionsPopup();
                }
            });

            var firstSelectedTask;
            scope.rangeSelection = false;
            // For selection of items using SHIFT key
            var shiftSelection = function($element, data, parent) {
                scope.rangeSelection = true;
                // To maintain first selected task for range selection
                if (!firstSelectedTask) {
                    firstSelectedTask = {
                        data: data,
                        parent: parent
                    };

                    scope.addTaskToRangeSelection(data, parent, data.index);
                } else {
                    // Add current selected task as the last selected task
                    scope.selectedTaskRangeData.length = 1;
                    scope.addTaskToRangeSelection(data, parent);
                }

                // First, add the classes to current element.
                $element.addClass('selectedCopy firstCopy manualSelect').removeClass('parent ctrlSelect');
                setTimeout(function() {
                    $element.removeClass('parent ctrlSelect');
                });

                // Remove the selected class of all the elements than the boundary elements
                $('.selectedCopy:not(.firstCopy):not(.ctrlSelect)').removeClass('selectedCopy manualSelect parent');
                $('.currentCopy').removeClass('currentCopy');
                // If there are more than one boundary elements
                if ($('.selectedCopy:not(.ctrlSelect)').length >= 2) {
                    $element.addClass('currentCopy firstCopy manualSelect').removeClass('parent ctrlSelect');
                    // If selecting the same element with SHIFT
                    if ($('.firstCopy').length < 2) {
                        // Remove the added classes
                        // $('.firstCopy').removeClass('firstCopy currentCopy');
                        return false;
                    }

                    var firstEl = false;
                    element.find('.sp-tree div[sp-editable]', element).each(function() {
                        if (firstEl) {
                            // selectedTaskId.push($(this).data('sptaskid'));

                            $(this).closest('li').addClass('selectedCopy manualSelect').removeClass('parent ctrlSelect');

                            if ($(this).closest('li').hasClass('firstCopy')) {
                                // $('.firstCopy.currentCopy').removeClass('firstCopy currentCopy ctrlSelect');
                                $('.firstCopy.currentCopy').removeClass('firstCopy ctrlSelect'); // currentCopy
                                return false;
                            }
                        } else if ($(this).closest('li').hasClass('firstCopy')) {
                            // selectedTaskId.push($(this).data('sptaskid'));
                            firstEl = true;
                            $(this).closest('li').addClass('selectedCopy').removeClass('ctrlSelect');
                        }
                    });
                } else if ($('.selectedCopy').length === 1) {
                    $element.addClass('firstCopy manualSelect').removeClass('parent ctrlSelect');
                }
            };

            // To select all the items between the last selected item(irrespective of SHIFT) and current item
            scope.shiftRangeSelection = function() {
                // var firstTask = scope.shiftSelectionTask;
                // Get last selected item from the selected tasks array
                var lastSelected = scope.selectedTaskData.length > 0 ? scope.selectedTaskData.length - 1 : 0;
                var firstTask = scope.selectedTaskData[lastSelected] || scope.selectedTaskRangeData[0] || scope.shiftSelectionTask;

                if (firstTask) {
                    var taskData = firstTask.data,
                        taskParent = firstTask.parent;

                    shiftSelection(getItemById(taskData.id).closest('li'), taskData, taskParent);
                }
            };

            function removeSelectedRange() {
                // scope.getTaskRange();
                var selectedItems = element.find('.selectedCopy:not(.parent)').map(function(task) {
                    return $(this).find('.sp-input').data('sptaskid');
                }).get();

                var index = element.find('.selectedCopy:first').index('.sp-child-tree-task');

                // Sort according to the depth
                selectedItems = selectedItems.sort(function(a, b) {
                    return getItemById(b).parents('li.sp-child-tree-task').length - getItemById(a).parents('li.sp-child-tree-task').length;
                }).filter(function(elementId) {
                    var totalItems = getItemById(elementId).closest('li').find('.sp-input').filter(function() {
                        return $(this).text().trim();
                    }).length;

                    var totalSelectedItems = getItemById(elementId).closest('li').find('.selectedCopy:not(.parent)').filter(function() {
                        return $(this).find('.sp-input').text().trim();
                    }).length + getItemById(elementId).closest('li.selectedCopy').length;

                    return totalItems ? totalItems === totalSelectedItems : true;
                });

                selectedItems.forEach(function(e) {
                    var scope = angular.element(getItemById(e).closest('li')[0]).scope(),
                        parent = scope.$parent,
                        data = scope.data;
                    if (!data.status) {
                        scope.delete(data, parent);
                    }
                });
            }
            window.removeSelectedRange = removeSelectedRange;

            function clearSelection() {
                $('.selectedCopy, .manualSelect, .parent, .ctrlSelect').removeClass('selectedCopy parent manualSelect ctrlSelect');
                firstSelectedTask = undefined;
                scope.rangeSelection = false;
                scope.shiftSelectionTask = undefined;
            }
            // End of Copy/Selection functions

            // A recursive function to update the IDs of childrens when copying a hierarchy
            scope.updateIds = function(obj, parentId) {
                // Create ID from timestamp
                var id = +new Date() - Math.floor(Math.random() * (1000 - 1));
                obj.id = id;
                if (parentId !== undefined) {
                    obj.parentId = parentId;
                }

                obj.nodes = obj.nodes.map(function(o) {
                    o.parentId = id;
                    return o;
                });

                // obj = _.pick(obj, ['_id', 'name', 'nodes', 'parentId']);
                setTimeout(function() {
                    scope.onNgblur({}, _.pick(obj, ['id', 'name', 'userId', 'nodes', 'parentId']));
                });

                for (var i = 0, len = obj.nodes.length; i < len; i++) {
                    scope.updateIds(obj.nodes[i]);
                }
            };

            var keyCodes = {
                backspace: 8,
                tab: 9,
                enter: 13,
                escape: 27,
                spacebar: 32,
                pageUp: 33,
                pageDown: 34,
                end: 35,
                home: 36,
                leftArrow: 37,
                upArrow: 38,
                rightArrow: 39,
                downArrow: 40,
                delete: 46,
                a: 65,
                c: 67,
                x: 88,
                v: 86,
                forwardSlash: 191,
                f5: 116
            };

          function getItemById(id) {
              return element.find('.sp-tree [data-sptaskid="' + id + '"]');
          }

          function getItemByImageId(id) {
              return element.find('.sp-tree #' + id).closest('div');
          }

          function getItems() {
              return element.find('.sp-tree .sp-input[data-sptaskid]:visible');
          }

          function getIndex(el) {
              return getItems().index(el);
          }

          function getItemCount() {
              return getItems().length;
          }

          window.focusItem = function(index) {
              index = typeof index === 'undefined' ? 0 : index;
              setTimeout(function() {
                  var els = getItems();
                  try {

                      SetCaretPosition(els.eq(index)[0], els.eq(index)[0].textContent.length);
                  } catch (err) {
                      if (index < 0 || isNaN(index) || isNaN(Number(index)))
                          index = 1;
                      // If next item is not focus-able
                      // focus prev item
                      return focusItem(index - 1);
                  }

                  slickPaperUtils.scrollIntoView(els.eq(index));
              });
          }

          window.focusFromOutside = function(index) {
              $timeout(function() {
                  focusItem(index);
              }, 300);
          };

          function focusItemById(id) {
              focusItem(getIndex(getItemById(id)));
          }

          function focusItemByImageId(id) {
              focusItem(getIndex(getItemByImageId(id)));
          }

            scope.focusNextItem = function(event) {
                event.preventDefault();

                // Get Current Item Index
                var index = getIndex(event.currentTarget);
                index = (index + 1) % getItemCount();
                focusItem(index);
            };
            scope.focusPrevItem = function(event) {
                event.preventDefault();

                // Get Current Item Index
                var index = getIndex(event.currentTarget);
                index = index - 1 < 0 ? getItemCount() - 1 : index - 1;
                focusItem(index);
            };

            // For popup options
            var options = [{
                    imagePath: 'txt-icon.png',
                    type: 'text',
                    value: 'Text'
                }, {
                    imagePath: 'icon-4.png',
                    type: 'unorderedList',
                    value: 'BulletedList'
                },
                // {
                //     imagePath: 'icon-5.png',
                //     type: 'orderedList',
                //     value: 'NumberedList'
                // },
                {
                    imagePath: 'icon-6.png',
                    type: 'checkbox',
                    value: 'Checkbox'
                }, {
                    imagePath: 'img-icon.png',
                    type: 'image',
                    value: 'Image'
                }
            ];

            scope.slashPopupData = options;

            scope.filterOptions = function($event, data, slashData) {
                $timeout(function() {

                    if (/\/[a-zA-Z]*$/.test(slashData)) {
                        var search = (slashData.match(/\/([a-zA-Z]*)$/) || [''])[1];
                        var regex = new RegExp(search, 'i');

                        var filteredOptions = options.filter(function(option) {
                            return regex.test(option.value);
                        });

                        if (filteredOptions.length === 0) {
                            scope.hideOptionsPopup();
                        } else {
                            scope.showOptionsPopup($event);
                        }
                    } else {
                        scope.hideOptionsPopup();
                    }

                    scope.slashPopupData = filteredOptions;
                    scope.selectFirstOption();
                }, 100);
            };

            scope.hideOptionsPopup = function() {
                $('#spOptionsPopup' + $scope.paperUniqueID).hide();
            };

            // Set the options required when adding new elements
            // when adding the option using the individual icon
            scope.setDataOptions = function($event, $parent) {
                scope.data = $parent.data;
                scope.itemDataBeforeSlash = _.assign({}, scope.data);
                scope.event = $event;
                scope.event.currentTarget = $($event.currentTarget).next().find('div[sp-editable]:first')[0];
                scope.parent = $parent;
                scope.slashPopupData = options;

                $($event.currentTarget).focus();
                $event.stopPropagation();
            };

            scope.showOptionsPopup = function($event, data) {


              var parentPos = $($event.currentTarget).closest('[slick-paper]')[0].getBoundingClientRect();
                   var childPos = $event.currentTarget.getBoundingClientRect();
                   var left = childPos.left - parentPos.left;

                   var top = childPos.top - parentPos.top;
                   var elementHeight = $($event.currentTarget).closest('.sp-lineItem').outerHeight();


                setTimeout(function() {
                    // Get current element left, top

                    if(scope.isUsedInThirdI == "true")
                    {
                       left = $($event.currentTarget).offset().left,
                       top = $($event.currentTarget).offset().top;
                    }






                    var $optionsPopup = $('#spOptionsPopup' + $scope.paperUniqueID).show();
                    var popupWidth = $optionsPopup.outerWidth();

                    //top = $event.currentTarget.getBoundingClientRect().top;

                    if (top + $optionsPopup.outerHeight() > $(window).height()) {
                        // Position above the record
                        top -= $('#spOptionsPopup' + $scope.paperUniqueID).height() + elementHeight;
                    }

                    if(scope.isUsedInThirdI == "true")
                    {
                      if ($event.type == "keydown") {
                        var pos = slickPaperUtils.getCaretPixelPos();

                        if ((parseInt(pos.left) + popupWidth) > (screen.width - 10 - popupWidth)) {
                          left = (parseInt(pos.left) - 10 - popupWidth) + 'px';
                        } else {
                          left = pos.left;
                        }
                      }
                    }

                    // if ($($event.currentTarget).closest('.tile').length !== 0 && $($event.currentTarget).closest('.smallTile').length === 1) {
                    //     var offset = $($event.currentTarget).closest('[slick-paper]').offset();
                    //     left += offset.left;
                    //     top += offset.top;
                    //     $('#spOptionsPopup' + $scope.paperUniqueID).css({
                    //         position: "fixed"
                    //     });
                    // }

                    $optionsPopup.css({
                        left: left,
                        top: top + elementHeight,
                        display: 'block'
                    });

                    scope.selectFirstOption();
                }, 200);
            };

            scope.selectFirstOption = function() {
                setTimeout(function() {
                    $('#spOptionsPopup' + $scope.paperUniqueID + ' #spSlashPopup .popup-option-txt:first').addClass('selected-option').siblings('.selected-option').removeClass('selected-option');
                }, 100);
            };

            scope.navigateOptions = function(event) {
                var keyCode = event.keyCode || event.charCode || event.which;
                var options = $('#spOptionsPopup' + $scope.paperUniqueID + ' #spSlashPopup .options > div');

                var index = element.find('.selected-option').index(),
                    totalOptions = options.length;

                if (keyCode === keyCodes.downArrow) {
                    index = (index + 1) >= totalOptions ? 0 : index + 1;
                } else if (keyCode === keyCodes.upArrow) {
                    index = index - 1 < 0 ? totalOptions - 1 : index - 1;
                }

                options.eq(index).addClass('selected-option').siblings('.selected-option').removeClass('selected-option');
            };

            scope.changeNestedType;
            scope.changeOptionType = '';
            scope.changeSiblingsType = function(event, option) {
                if (option.type.toLowerCase() !== 'image') {
                    scope.changeOptionType = option.type;
                    scopeData = scope.parent.$parent;

                    var optionTypeText = option.type;
                    if ('unorderedList' === optionTypeText)
                        optionTypeText = "bulletedlist";

                    $('#lblChangeSiblingsType' + scope.paperUniqueID).text("Do you want to convert all siblings into " + optionTypeText + "?");

                    if (scopeData.data.parentId !== 0) {
                        if (scopeData.$parent.data.nodes.length >= 2 && scopeData.$parent.data.nodes.filter(function(o) {
                                return o.id != scope.changeTypeCurrentTargetId && o.name.trim() !== ""
                            }).length >= 1) {
                            scope.changeNestedType = true;
                            scope.changAllSiblingsType();
                            //$('#changeSiblingsType' + scope.paperUniqueID).modal('show');
                            // if($("[data-sptaskid="+scope.changeTypeCurrentTargetId+"]").parents('#popup-note-tile').length === 1)
                            //     $('#tile-overlay').show();
                        }
                    } else {
                        if (scope.tree.length >= 2 && scope.tree.filter(function(o) {
                                return o.id !== scope.changeTypeCurrentTargetId && o.name.trim() != ""
                            }).length >= 1) {
                            scope.changeNestedType = false;
                            scope.changAllSiblingsType();
                            //$('#changeSiblingsType' + scope.paperUniqueID).modal('show');
                            // if($("[data-sptaskid="+scope.changeTypeCurrentTargetId+"]").parents('#popup-note-tile').length === 1)
                            //     $('#tile-overlay').show();
                        }
                    }
                }
            }

            scope.changAllNestedSiblingsType = function(data)
            {
                    data.nodes.forEach(function(item) {
                                if (item.type !== 'image') {
                                    if (item.name.trim() !== "") {
                                        item.type = scope.changeOptionType;
                                        //$compile(element)(element.scope());
                                    }
                                }
                                if(item.nodes.length !== 0)
                                {
                                    scope.changAllNestedSiblingsType(item);
                                    console.warn('nested data',item);
                                }

                            });
            }

            scope.changAllSiblingsType = function() {
                if (scope.changeNestedType) {



                    $timeout(function() {
                        var scope1;
                        if ($('#popup-note-tile').is(':visible') || $('#list-view').is(':visible')) {
                            scope1 = $($("[data-sptaskid=" + scope.changeTypeCurrentTargetId + "]").parents('li:not(:hidden)')[1]).scope();
                        } else {
                            scope1 = $($("[data-sptaskid=" + scope.changeTypeCurrentTargetId + "]").first().parents('li:not(:hidden)')[1]).scope();
                        }


                        scope.$apply(function() {
                            scope1.data.nodes.forEach(function(item) {
                                if (item.type !== 'image') {
                                    if (item.name.trim() !== "") {
                                        item.type = scope.changeOptionType;
                                        //$compile(element)(element.scope());
                                    }
                                }
                                if(item.nodes.length !== 0)
                                {
                                    scope.changAllNestedSiblingsType(item);
                                    console.warn('nested data',item);
                                }

                            })
                        });
                        scope.sendDataToParent();
                        scope.dataChange = false;
                    });

                } else {
                    scope.tree.forEach(function(item) {
                        if (item.type !== 'image') {
                            if (item.name.trim() !== "")
                                item.type = scope.changeOptionType
                        }
                        if(item.nodes.length !== 0)
                        {
                            scope.changAllNestedSiblingsType(item);
                            console.warn('nested data',item);
                        }
                    })
                }
                $('#changeSiblingsType' + scope.paperUniqueID).modal('hide');
                //$('#tile-overlay').hide();
            }

            scope.closeChangeTypePopup = function() {
               $(".closeChangeTypePopup").modal("hide");
               //$('#tile-overlay').hide();
           }

           scope.selectOption = function(event, option) {

                 var currentTarget = scope.event.currentTarget,
                     ind = getIndex(currentTarget);
                  scope.changeTypeCurrentTargetId = $(scope.event.currentTarget).attr('data-sptaskid');

                 scope.data.name = scope.itemDataBeforeSlash.name;
                 scope.changeSiblingsType(event, option)
                 if ('image' !== option.type) {
                     scope.data.name1 = scope.itemDataBeforeSlash.name1 || scope.data.name;
                     scope.data.type = option.type;

                     $timeout(function() {
                         scope.hideOptionsPopup();
                         focusItem(ind);
                     }, 100);
                 } else if ('image' === option.type) {
                     if (scope.data.name.trim() === '') {
                         scope.data.type = 'image';
                         scope.newItemId = scope.data.id;
                     } else {
                         var parent = scope.parent;
                         if (event.type === 'keydown')
                             parent = scope.parent.$parent;

                         var id = scope.addItem(scope.event, scope.data, parent);
                         $timeout(function() {
                             getItemById(id).focus();
                             angular.element(document.querySelector('[data-sptaskid="' + id + '"]')).scope().data.type = 'image';
                         });

                     }
                     scope.showImageUploadPopup();
                     return;
                 }
                 // scope.data.type = option.type;

                 if (scope.data.name.trim() === '' || /^\/[a-zA-Z]*/.test(scope.data.name.trim())) {
                     scope.hideOptionsPopup();
                     focusItem(ind);
                     return;
                 }

                 // TODO: Add new item for toggle
                 if (option.type === 'toggle') {
                     scope.addItem(scope.event, scope.data, scope.parent.$parent, undefined, option.type);
                 }

                 scope.hideOptionsPopup();
                 focusItem(ind);
             };

            scope.checkTextSelection = function(event) {
                // Check if text is selected
                scope.event = event;
                if (slickPaperUtils.getSelectionText()) {
                    scope.showSelectionPopup(event);
                } else {
                    scope.hideSelectionPopup();
                }

                scope.closeAllPopup(event);
            };

            scope.showSelectionPopup = function(event) {
                // Position popup
                var sel = window.getSelection();
                var range = sel.getRangeAt(0).cloneRange();
                var rect = range.getBoundingClientRect();

                var parentPos = $(event.currentTarget).closest('[slick-paper]')[0].getBoundingClientRect();

                $('#spSelectionPopup' + scope.paperUniqueID).show();

                var left = rect.left - parentPos.left - $('#spSelectionPopup' + scope.paperUniqueID).width() / 2,
                    top = rect.top - $('#spSelectionPopup' + scope.paperUniqueID).outerHeight();

                // if(scope.isUsedInThirdI === "true")
                // {
                //   left = rect.left;
                // }


                if ($('#popup-note-tile').is(':visible') || scope.isUsedInThirdI === "true") {
                    top = top - parentPos.top;
                    if (left < 0)
                        left = 0;
                    if ($(event.currentTarget).closest('[slick-paper]').width() <= (left + ($('#spSelectionPopup' + $scope.paperUniqueID).width()))) {
                        left = left - ($('#spSelectionPopup' + $scope.paperUniqueID).width() / 2);
                    }
                } else {
                    if ($(event.currentTarget).closest('[slick-paper]').width() <= (left + ($('#spSelectionPopup' + $scope.paperUniqueID).width()))) {
                        left = left - ($('#spSelectionPopup' + $scope.paperUniqueID).width() / 2);
                    }
                }


                if (top < 0) {
                    top = rect.top + $(event.currentTarget).outerHeight();
                }



                var popupWidth = $('#spSelectionPopup' + scope.paperUniqueID).outerWidth();

                // if (left + popupWidth > $(event.currentTarget).closest('[slick-paper]').width()) {
                //     $('#spSelectionPopup' + scope.paperUniqueID).css({
                //         right: 0,
                //         left: 'initial',
                //         top: top
                //     });
                //     return;
                // } else if (left < 0) {
                //     left = 0;
                // }
                //slick-paper-id="listViewPaperId"
                if (scope.isUsedInThirdI === "true" || ($(event.currentTarget).closest('.tile').length !== 0 && $(event.currentTarget).closest('.smallTile').length === 1) || $(event.currentTarget).closest('[slick-paper-id="listViewPaperId"]').length !== 0) {
                    var offset = $(event.currentTarget).closest('[slick-paper]').offset();
                    left += offset.left;
                    //top += (offset.top - 46);
                    $('#spSelectionPopup' + $scope.paperUniqueID).css({
                        position: "fixed"
                    });



                }

// top = event.currentTarget.getBoundingClientRect().top - $(event.currentTarget).outerHeight();
// top = event.currentTarget.getBoundingClientRect().top - $(event.currentTarget).outerHeight();

                $('#spSelectionPopup' + scope.paperUniqueID).css({
                    left: left,
                    right: 'initial',
                    top: top,
                    //position: 'fixed'
                });
            };
            scope.hideSelectionPopup = function() {
                $('#spSelectionPopup' + scope.paperUniqueID).hide();
            };

            scope.closeImagePopup = function(e) {

                if (scope.event == undefined)
                    return;

                if (scope.event.currentTarget == undefined)
                    return;

                if (e) {
                    if ($('#file_' + scope.newItemId).length) {
                        var index = getIndex(getItemById(scope.newItemId));
                        $timeout(function() {
                            focusItem(index + 1);
                        });
                    }
                }

                if (e && e.type != 'keydown') {
                    if (e.target.id == "spTxtImageSize" + scope.paperUniqueID)
                        return;
                }
                $('[id^="spImagePopup"], [id^="spMainImagePopup"]').hide();
            };

            scope.closeAllPopup = function(e) {
                if (slickPaperUtils.getSelectionText().length == 0)
                    scope.hideSelectionPopup();
                scope.hideOptionsPopup();
            };

            scope.decorationMouseLeave = function() {
                scope.isCursorOnDecoration = false;
            };

            scope.decorationMouseOver = function() {
                scope.isCursorOnDecoration = true;
            };

            scope.decorateText = function(command) {
                $timeout(function() {
                  scope.dataChange = true;
                    document.execCommand(command, false, null);
                    var selection = window.getSelection();
                    var html = selection.baseNode.parentNode.closest(".sp-input").innerHTML;
                    scope.data.name1 = html;
                    slickPaperUtils.clearSelection();
                    scope.hideSelectionPopup();

                    setTimeout(function() {
                        SetCaretPosition(scope.event.target);
                    });
                });
            };

            scope.heading = function(head) {
                if (head == "H1") {
                    $(scope.event.currentTarget).removeClass("sp-headingSecond").toggleClass("sp-headingFirst");
                    scope.data.headingClass = 'sp-headingFirst';
                } else if (head == "H2") {
                    $(scope.event.currentTarget).removeClass("sp-headingFirst").toggleClass("sp-headingSecond");
                    scope.data.headingClass = 'sp-headingSecond';
                }
                scope.sendDataToParent();
            };

            scope.imageClick = function(e) {
                scope.event = e;
                scope.newItemId = e.currentTarget.id.match(/\d+/)[0];
                var left = $(e.currentTarget).offset().left,
                    top = $(e.currentTarget).offset().top;

                var parentPos = $(e.currentTarget).closest('[slick-paper]')[0].getBoundingClientRect();
                var childPos = e.currentTarget.getBoundingClientRect();
                var left = childPos.left - parentPos.left;
                var top = childPos.top - parentPos.top;
                $("#spImagePopup" + scope.paperUniqueID + ', #spMainImagePopup' + scope.paperUniqueID).show();
                $imagePopup = $("#spMainImagePopup" + scope.paperUniqueID);

                top = e.currentTarget.getBoundingClientRect().top;

                if (top + $imagePopup.outerHeight() > $(window).height()) {
                    // Position above the record
                    top -= $imagePopup.height() + 10 + 150;
                }

                if (scope.isUsedInThirdI === "true" || ($(event.currentTarget).closest('.tile').length !== 0 && $(event.currentTarget).closest('.smallTile').length === 1)) {
                    var offset = $(event.currentTarget).closest('[slick-paper]').offset();
                    left += (offset.left - 120);
                } else if (left + $('#spImageSubPopup' + $scope.paperUniqueID).width() > $(event.currentTarget).closest('[slick-paper]').width()) {
                    left = (left + 20) - $('#spImageSubPopup' + $scope.paperUniqueID).width();
                }

                if (scope.isUsedInThirdI === "true" || $('#tile-view').is(':visible')) {
                    if (left < 0)
                        left = 0;
                    if ($(event.currentTarget).closest('[slick-paper]').width() <= (left + ($('#spSelectionPopup' + $scope.paperUniqueID).width()))) {
                        left = left - ($('#spSelectionPopup' + $scope.paperUniqueID).width() / 2);
                    }
                }


                $imagePopup.css({
                    left: left,
                    top: top,
                    position: 'absolute'
                });

                if (scope.isUsedInThirdI === "true" || ($(e.currentTarget).closest('.tile').length !== 0 && $(e.currentTarget).closest('.smallTile').length === 1)) {
                    $('#spMainImagePopup' + $scope.paperUniqueID).css({
                        position: "fixed"
                    });
                }

                $("#spTxtImageSize" + scope.paperUniqueID).focus();
                $(scope.event.currentTarget).addClass("NotRemoveImage");
            };

            scope.imageTxtEnter = function(event) {
                if (event.keyCode == keyCodes.enter) {
                    scope.changeImageSize(event);
                    scope.closeImagePopup(event);
                }
            };

            scope.spTextImageClick = function(e) {
                e.stopPropagation();
            };

            var getImageData = function(event) {
                return getItemById(event.currentTarget.id.replace(/\D+/g, '')).scope().data;
            };
            // Set image widht and height as given percent
            scope.changeImageDimension = function(event, percent) {
                //scope.event.currentTarget
                var styles = {
                    width: percent + '%',
                    height: percent + '%'
                };
                $(scope.event.currentTarget).css(styles);

                // Get data of the element
                var imageData = getImageData(scope.event);
                imageData.styles = styles;
                scope.dataChange = true;
            };

            // Set image width and height as per user entered percent
            scope.changeImageSize = function(event) {
                var percent = Number(document.querySelector('#spTxtImageSize' + scope.paperUniqueID).value);

                if (percent >= 1 && percent <= 100) {
                    scope.changeImageDimension(event, percent);
                }
            };

            // Set the image alignment
            scope.alignImage = function(direction) {
                scope.dataChange = true;
                var imageData = getImageData(scope.event);
                imageData.classNames = 'sp-align-' + direction;
            };

            scope.RemoveImage = function() {
                 var $ele = $(scope.event.currentTarget).closest('li')[0];
                 var index = getIndex(getItemById(scope.newItemId));
                 var scope1 = angular.element($ele).scope();
                 var parent = scope1.$parent,
                     data = scope1.data;

                 if (!data.status) {
                     scope.dataChange = false;
                     scope.delete(data, parent);
                 }
                 scope.closeImagePopup();
                 // scope.sendDataToParent();
                 setTimeout(function() {
                     focusItem(Number(index));
                 }, 100);
             };

            function bindBodyFocusEvent() {
                document.body.onfocus = function() {
                    unbindBodyFocusEvent();
                };
            }

            function unbindBodyFocusEvent() {
                document.body.onfocus = null;

                $timeout(function() {
                    scope.hideOptionsPopup();

                    var id = scope.newItemId;
                    if (element.find('#file_' + id).attr('src') === undefined) {
                        // Change image type to text and focus
                        angular.element(getItemById(id)[0]).scope().data.type = 'text';
                        $timeout(function() {
                            focusItemById(id);
                        });
                    }
                }, 250);
            }

            scope.showImageUploadPopup = function() {
                setTimeout(function() {
                    element.find('#spfileUpload').click();
                    bindBodyFocusEvent();
                });
            };

            scope.ChangeImage = function() {
                scope.dataChange = true;
                //setTimeout(function() {
                element.find('#spfileUpload')[0].click();
                //});
                scope.closeImagePopup();
            };

            element.find('#spfileUpload')[0].addEventListener('change', function(e) {
                var file = element.find('#spfileUpload')[0].files[0];
                 if(file.size > 1000000){
                    $('#error_message').text("Image size must be under 1mb!");
                    $('#pageZeelayer').css('display', 'block');
                    $("#dialog").dialog({
                        create: function () { $(".ui-dialog-titlebar-close").attr("title", "Close") },
                        autoOpen: true,
                        buttons: {
                            "OK": function () {
                                $('#pageZeelayer').css('display', 'none');
                                $(this).dialog("close");
                            }
                        }
                    });
                    return;
                }
                this.value = '';
                var fileReader = new FileReader();

                fileReader.addEventListener('loadend', function(e) {
                    var scope = getTreeScope();
                    if (scope.newItemId !== undefined) {
                        var data = angular.element(document.querySelector('#file_' + scope.newItemId)).scope().data;
                        data.src = e.target.result;
                        var index = getIndex(getItemById(scope.newItemId));
                        //scope.CompileEditableDiv(scope.event.currentTarget.closest('div'));
                        $timeout(function() {
                            focusItem(index + 1);
                        });
                    }
                    file = null;
                    fileReader = null;
                    scope.sendDataToParent();
                }, false);
                fileReader.readAsDataURL(file);
                // file = null;
                e.stopPropagation();
            });

            scope.FindValidSlash = function(inputText, ele) {
                var caretPos = getCaretCharacterOffsetWithin(ele);
                var text = inputText.substring(0, caretPos);
                scope.slashTextData = text;

                return /\/[a-zA-Z]*$/.test(text);
            };

            scope.removeSelectedTasks = function($event) {
                if (scope.selectedTaskData.length || scope.selectedTaskRangeData.length) {
                    // Sort by index
                    scope.selectedTaskData = scope.sortSelectedTasks();

                    // If selection using SHIFT key, get all the tasks
                    if (scope.rangeSelection || scope.selectedTaskData.length) {
                        removeSelectedRange();
                        focusItem(getIndex($event.target));
                        scope.removeAllTasksFromSelection();
                        clearSelection();
                    } else {
                        scope.isContextMenu = true;
                        scope.deleteItem();
                    }

                    $event.preventDefault();
                }
            };

            $scope.selectedOtlineItem;
            $scope.selectedCount = 0;
            $scope.isUsedCtrlA = false;
            $scope.selectOutlineItem = function(currentTarget)
            {
                if($scope.selectedOtlineItem === undefined)
                {
                    $scope.selectedOtlineItem = $(currentTarget).parents('li:first');
                }
                else
                {
                        $scope.selectedOtlineItem = $($($scope.selectedOtlineItem).parents('li')[$scope.selectedCount++]).find('li');
                }

                $scope.isUsedCtrlA = true;
                if($scope.selectedCount > 0)
                {
                    console.warn($($scope.selectedOtlineItem).length === $($scope.selectedOtlineItem).closest('ul').find('li').length);
                    if($($scope.selectedOtlineItem).closest('ul').find('li').length === 0)
                    {
                        $(currentTarget).closest('.sp-tree').find('li').addClass('manualSelect selectedCopy ctrlSelect');
                    }
                    else
                    {
                        $($scope.selectedOtlineItem).closest('ul').find('li').addClass('manualSelect selectedCopy ctrlSelect').end().closest('li').addClass('manualSelect selectedCopy ctrlSelect');
                    }

                }
                else
                {
                    $($scope.selectedOtlineItem).addClass('manualSelect selectedCopy ctrlSelect selected');
                }
            }

            scope.setNameProperty = function(newItemId)
            {
             setTimeout(function() {
                            if(scope.isEnterPress === true)
                            {
                                var scopeNew = angular.element($('[data-sptaskid="'+newItemId+'"]')).scope();
                                scopeNew.$apply(function() {
                                    scopeNew.data.name = scope.nameText;
                                    scopeNew.data.name1 = scope.nameHtml;
                                });
                            }
                            scope.isEnterPress = false;
                    }, 0);
            }

            var timeout;
            //EVENTS
            scope.keyDown = function($event, data, $parent, $index, parentIndex) {

              if(scope.usedInSlick == "true") //Code is used in slick
                $('.unsaved-popup').show();

                setTimeout(function() {
                scope.data.name = $event.currentTarget.textContent;
              })

              scope.dataChange = true;
              var isforwardSlash = /\/[a-zA-Z]*/.test(data.name);
                if (!($event.ctrlKey || $event.shiftKey || isforwardSlash || data.type === 'image')) {
                 scope.hideOptionsPopup();
                 if(($event.keyCode >= 65 &&  $event.keyCode <= 90) || ($event.keyCode >= 97 &&  $event.keyCode <= 122 ) || ($event.keyCode >= 48 &&  $event.keyCode <= 57))
                 {
                   return;
                 }
                    if (!isforwardSlash) {
                        if ($event.keyCode === keyCodes.spacebar) {
                     return;
                   }
                    } else {

                 }
               }

                // Cache
                scope.data = data;
                scope.event = $event;
                scope.parent = $parent;

                var currentTarget = $event.currentTarget,
                    $input = getItems(),
                    ind = $input.index(currentTarget);
                var $element = $(currentTarget);
                var keyCode = $event.keyCode || $event.charCode || $event.which;
                var copiedString = "";
                data.index = $index;

                // For images
                if ($element.closest('.sp-input').attr('readonly') && !(keyCode === keyCodes.upArrow || keyCode === keyCodes.downArrow || keyCode === keyCodes.backspace || keyCode === keyCodes.f5) && keyCode !== keyCodes.enter) {
                    $event.preventDefault();
                    return false;
                }

                $timeout(function() {
                    // Check if text is selected
                    if (slickPaperUtils.getSelectionText()) {
                        scope.showSelectionPopup($event);
                    } else {
                        scope.hideSelectionPopup();
                    }
                });

                // If options popup is open
                if ($('#spOptionsPopup' + $scope.paperUniqueID).is(':visible')) {
                    if (keyCode === keyCodes.upArrow || keyCode === keyCodes.downArrow) {
                        scope.navigateOptions($event, keyCode);
                        $event.preventDefault();
                        return false;
                    } else if (keyCode === keyCodes.enter) {
                        var option = angular.element(element.find('.optionsPopup .selected-option')[0]).scope().data;

                        if (option.type === 'image') {
                          scope.data.name1 = $element.html().replace(/\/(image|imag|ima|im|i)?\b/gi, '');
                          scope.data.name = $element.text().replace(/\/(image|imag|ima|im|i)?\b/gi, '');

                          scope.itemDataBeforeSlash = _.assign({}, data, {
                             name: scope.data.name,
                              name1: scope.data.name1
                          });
                        }

                        if (scope.data.type === option.type) {
                          $($event.currentTarget).html(scope.itemDataBeforeSlash.name1);
                        } else {
                            if (scope.itemDataBeforeSlash.name1 == "") {
                            $($event.currentTarget).html(scope.itemDataBeforeSlash.name1);
                          }
                        }

                        scope.selectOption($event, option);
                        $event.preventDefault();
                        return false;
                    } else if (keyCode === keyCodes.escape) {
                        scope.hideOptionsPopup();
                        return false;
                    }
                }

                if (keyCode != keyCodes.leftArrow && keyCode != keyCodes.rightArrow) {

                    if (keyCode === keyCodes.forwardSlash || /\/[a-zA-Z]*/.test(data.name)) {
                        if (keyCode === keyCodes.forwardSlash) {
                            scope.itemDataBeforeSlash = _.assign({}, data, {
                              name: $element.text(),
                                name1: $element.html()
                            });
                        }

                        $timeout(function() {

                            if (scope.FindValidSlash(data.name, $event.currentTarget)) {
                                scope.filterOptions($event, data, scope.slashTextData);
                            } else {
                                scope.hideOptionsPopup();
                            }
                            // scope.showOptionsPopup($event, data);
                        }, 100);

                    } else if (/\/[a-zA-Z]*/.test(data.name) === false) {
                        scope.hideOptionsPopup();
                    }
                }

                if (keyCode === keyCodes.upArrow) {
                    scope.focusPrevItem($event);
                } else if (keyCode === keyCodes.downArrow) {
                    scope.focusNextItem($event);
                }

                if (keyCode === keyCodes.delete) {
                     scope.removeSelectedTasks($event);
                } else if (keyCode == keyCodes.tab) {
                    // TAB keydown handler

                    // If this item is not in the selected tasks array, then add it
                    if (scope.selectedTaskData.length === 0 && scope.selectedTaskRangeData.length === 0) {
                        scope.addTaskToSelection(data, $parent, parentIndex);
                    }
                    $event.preventDefault();

                    // Scenarios
                    // 1. Check if all the selected items parent is same
                    var hasSameParent = scope.hasSameParent();

                    if (!hasSameParent) {
                        console.error('Different parent');
                        return false;
                    }

                    // Sort by index
                    scope.selectedTaskData = scope.sortSelectedTasks();

                    // If selection using SHIFT key, get all the tasks
                    if (scope.selectedTaskRangeData.length) {
                        var selectedRange = scope.getTaskRange() || [];
                        scope.selectedTaskData = scope.selectedTaskData.concat(selectedRange);
                        scope.selectedTaskData = scope.sortSelectedTasks();
                    }

                     scope.dataChange = true;

                    if ($event.shiftKey) {
                        // Reverse the array to maintain the sequence when outdenting
                        scope.selectedTaskData = scope.selectedTaskData.reverse();

                        // Get parent Id
                        var firstTask = scope.shiftLeft(scope.selectedTaskData[0].data, scope.selectedTaskData[0].parent);
                          // var $parent = angular.element(getItemById(scope.selectedTaskData[0].data.id).closest('li')[0]).scope().$parent;

                          // var firstTask = scope.shiftLeft(scope.selectedTaskData[0].data, $parent);


                        if (firstTask) {
                            var parentId = firstTask.parentId;
                            var firstEl = scope.selectedTaskData.shift();
                            scope.onNgblur({}, firstEl.data);
                            scope.data.name1 = $event.currentTarget.innerHTML;

                            scope.selectedTaskData.forEach(function(e) {
                                e.data.name1 = getItemById(e.data.id).html();
                                // Get parent object dynamically
                                e.parent = angular.element(getItemById(e.data.id).closest('li')[0]).scope().$parent;

                                scope.shiftLeft(e.data, e.parent);
                                e.data.parentId = parentId;
                                scope.onNgblur({}, e.data);
                                scope.dataChange = true;
                            });

                            scope.removeAllTasksFromSelection();
                            clearSelection();
                        }
                    } else {
                        $parent = scope.selectedTaskData[0].parent;

                        // If fist element is also selected, don't do anything
                        if (scope.selectedTaskData[0].data.index <= 0) {
                            if (scope.selectedTaskData.length === 1) {
                                scope.removeAllTasksFromSelection();
                                clearSelection();
                            }
                            return false;
                        }

                        scope.selectedTaskData.forEach(function(e, i) {
                            e.data.name1 = getItemById(e.data.id).html();
                            data = e.data;

                            if (i && scope.selectedTaskData[i - 1].data.index !== data.index - 1) {
                                $parent = e.parent;
                            }

                            data = scope.shiftRight(data, $parent);
                            if (data) {
                                scope.onNgblur({}, data);
                            }
                        });

                        scope.removeAllTasksFromSelection();
                        clearSelection();
                    }

                    $timeout(function() {
                        var el = getItemById(data.id);
                        el.addClass('currentTarget');

                        // To place the cursor at the index
                        // slickPaperUtils.setCaretPosition(el[0]);
                        if (el && el[0]) {
                            SetCaretPosition(el[0], el[0].textContent.length);
                        }
                    });
                    return false;
                } else if ($event.ctrlKey || $event.shiftKey) {
                    var $input = getItems();
                    var ind = $input.index($element);
                    if (keyCode === keyCodes.upArrow) {
                        ind--;
                    } else if (keyCode === keyCodes.downArrow) {
                        ind = (ind + 1) % $input.length;
                    }

                    var $oldElement = $element;
                    $element = $input.eq(ind);

                    // For multiple tasks Selection
                    if ($event.ctrlKey) {
                      if(keyCode === keyCodes.a)
                      {
                          $scope.selectOutlineItem(currentTarget);
                          $event.preventDefault();
                      }
                      if (keyCode === keyCodes.leftArrow) {
                            return;
                        }
                        if (keyCode === keyCodes.rightArrow) {
                            return;
                        }
                        if (keyCode === keyCodes.v) {
                              event.currentTarget.addEventListener('paste', pasteData, false);
                              return;
                          }

                        if (keyCode === keyCodes.rightArrow) {
                            scope.rangeSelection = false;
                            $element.closest('li').addClass('manualSelect selectedCopy ctrlSelect').removeClass('parent');
                            scope.addTaskToSelection(data, $parent, parentIndex);

                            $event.preventDefault();
                        } else if (keyCode === keyCodes.leftArrow) {
                            $element.closest('li').removeClass('manualSelect selectedCopy parent ctrlSelect');
                            scope.removeTaskFromSelection(data);

                            $event.preventDefault();
                        }
                    } else if ($event.shiftKey) {
                        // if (scope.rangeSelection === false) {
                        //     scope.removeAllTasksFromSelection();
                        // }

                        if (keyCode === keyCodes.downArrow || keyCode === keyCodes.upArrow) {
                            if (scope.selectedTaskRangeData.length === 0) {
                                shiftSelection($oldElement.closest('li'), data, $parent);
                            }

                            if (scope.selectedTaskRangeData.length) {
                                // Get id of the current item
                                var id = $element.data('sptaskid');

                                data = _.findDeep(scope.tree, {
                                    id: id
                                });

                                scope.selectedTaskRangeData.length = 1;
                                scope.addTaskToRangeSelection(data);
                            }

                            shiftSelection($element.closest('li'), data, $parent);
                        } else if (scope.selectedTaskRangeData.length === 0) {
                            // Save the task in a variable
                            if (!scope.shiftSelectionTask) {
                                scope.shiftSelectionTask = {
                                    event: $event,
                                    data: data,
                                    parent: $parent,
                                    parentIndex: parentIndex
                                };
                            }
                        }
                    }
                } else {
                    if (keyCode !== keyCodes.downArrow && keyCode !== keyCodes.upArrow) {
                        // clear selected elements
                        // clearSelection();
                        $('.firstCopy').removeClass('firstCopy currentCopy');
                    }
                }

                //scope.addExtraDiv($event, data, $parent.$parent, $element);
                // Enter key down handler
                if (keyCode == keyCodes.enter) {

                    var urlRegx = /((http|https):?\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})/;
                    var url = data.name.match(urlRegx);
                    var index = event.target.getAttribute('data-sptaskid');
                    var ele = angular.element($('[data-sptaskid="' + index + '"]'));
                    var tags = $(ele).find('a');
                    var isSetFocus = true;

                    if (url && tags.length == 0) {

                        $event.preventDefault();
                        isSetFocus = false;
                        scope.openHyperLinkModal($event, data);
                    }

                    if (data.type !== 'image')
                      {
                          if(getCaretCharacterOffsetWithin($(currentTarget)[0]) === 0 && scope.data.name.trim() !== '' && scope.data.name !== undefined)
                          {
                              setTimeout(function() {
                                  var scopeNew = $('[data-sptaskid="' + scope.data.id + '"]').scope();
                                  scope.isEnterPress = true;
                                  scope.nameText =   scope.data.name;
                                  scope.nameHtml =   scope.data.name1;

                                  scopeNew.$apply(function() {
                                          scopeNew.data.name = '';
                                          scopeNew.data.name1 = '';
                                      });
                              },0);
                              //scope.currentTarget = currentTarget;
                          }
                          else
                          {
                              scope.isEnterPress = false;
                          }
                      }
                      else
                      {
                          scope.isEnterPress = false;
                      }

                    scope.nextLineItemID = scope.addItem($event, data, $parent.$parent, $element, data.type, undefined, isSetFocus);
                } else if (keyCode == keyCodes.backspace) {
                    history.forward();

                    if ((data.type === 'checkbox' || data.type === 'unorderedList') && getCaretCharacterOffsetWithin($element[0]) === 0) {
                        data.type = 'text';
                        data.name1 = $element.html();
                        $timeout(function() {
                            focusItem(ind);
                        });

                        return false;
                    }

                    // Backspace keydown handler
                    if (data.nodes.length > 0 && data.name.trim().length <= 1) {
                        // When having children then don't allow to remove the parent task
                        return false;
                    } else if ((!data.name || data.name.trim() === '') && scope.tree.length === 1 && scope.tree[0].nodes.length === 0) {
                         //data.name1 = '';
                        scope.delete(data, $parent);

                        $timeout(function() {
                            focusItem(ind - 1 < 0 ? 0 : ind - 1);
                        });
                    } else if (!data.name || data.name.trim() === '') {
                        data.name1 = '';
                        scope.delete(data, $parent);

                        $event.preventDefault();
                        if (data.type === 'image' && keyCode === keyCodes.backspace) {
                            scope.sendDataToParent();
                        }
                        $timeout(function() {
                            focusItem(ind - 1 < 0 ? 0 : ind - 1);
                        });
                    } else {
                        timeout = autoSave(data);
                    }

                }
            };

            scope.addExtraDiv = function(event, data, $parent, $element) {

                $input = getItems();

                if ($input) {
                    if ($input[$input.length - 1].textContent.trim() != "") {
                        for (var i = 0; i < scope.divToAdd; i++) {
                            scope.addItem(event, data, $parent, $element, 'text', true);
                        }
                        return;
                    }
                    if ($input[$input.length - 2].textContent.trim() != "") {
                        for (var i = 0; i < scope.divToAdd - 1; i++) {
                            scope.addItem(event, data, $parent, $element, 'text', true);
                        }
                    }

                }
            };


            // Add the task to selection for copy
            scope.addToSelection = function(event, task, $parent, $index, parentIndex) {

                task.index = $index;

                // scope.addTaskToSelection(task, $parent, parentIndex);

                var $element = $(event.currentTarget).closest('li');
                // Set focus on the clicked task
                $('[data-sptaskid="' + task.id + '"]').focus();

                if (event.shiftKey) {
                    if (scope.selectedTaskRangeData.length === 0) {
                        scope.shiftRangeSelection();
                    }

                    // SHIFT key handler
                    shiftSelection($element, task, $parent);
                    //scope.addTaskToSelection(task, $parent, parentIndex);
                } else if (event.ctrlKey) {
                    scope.addTaskToSelection(task, $parent, parentIndex);

                    // Normal click and/or CTRL selection
                    // $element.toggleClass('selectedCopy manualSelect');
                    if ($element.hasClass('parent')) {
                        $element.removeClass('parent').addClass('manualSelect ctrlSelect');
                    } else if ($element.hasClass('manualSelect')) {
                        // Already selected, then remove selection
                        $element.removeClass('manualSelect selectedCopy ctrlSelect');

                        scope.removeTaskFromSelection(task);
                    } else {
                        // Add the current task in the selectedTaskData array.
                        $element.addClass('manualSelect selectedCopy ctrlSelect');
                    }

                    // Clear previous selections
                    $('.selectedCopy.parent').removeClass('selectedCopy parent');
                    $('.manualSelect').each(function() {
                        $(this).parents('li:not(.manualSelect)').addClass('selectedCopy parent');
                        $(this).closest('li').find('li:not(.manualSelect)').addClass('selectedCopy parent');
                    });
                } else {
                    var selected = $element.hasClass('manualSelect');
                    // Remove previous selected items
                    scope.removeAllTasksFromSelection();
                    clearSelection();

                    $element.toggleClass('manualSelect selectedCopy ctrlSelect');
                    // Normal Individual selection
                    if (selected) {
                        // Already selected, then remove selection
                        scope.removeTaskFromSelection(task);
                    } else {
                        scope.addTaskToSelection(task, $parent, parentIndex);
                    }
                }
            };

            scope.dropCallback = function(event, index, item, external, type, allowedType, $parent) {
              scope.dataChange = true;
                if ($parent) {
                    item.parentId = $parent.data.id;
                } else {
                    item.parentId = 0;
                }

                //scope.logListEvent('dropped at', event, index, external, type);
                if (event.type == 'drop') {
                    var rootId = $(event.currentTarget).parents("li").find('.sp-input:first').data('sptaskid');
                    var nodeItems = [];

                    if (item.linkData && item.linkData.length > 0) {
                        item.name = item.linkData[0].displayText;
                    }

                    nodeItems.push(item);
                    updateRootIdForChildrens(nodeItems, item.userId, rootId);
                    setTimeout(function() {
                        scope.onNgblur(event, item);
                    }, 0);
                }

                if (external) {
                    if (allowedType === 'itemType' && !item.label) return false;
                    if (allowedType === 'containerType' && !angular.isArray(item)) return false;
                }
                return item;
            };

            scope.sendDataToParent = function() {
                $rootScope.$emit('slickPaperDataChange', angular.copy(scope.tree));
            };

            scope.onNgblur = function(e, data) {



                $timeout(function() {
                    if (scope.isCursorOnDecoration === false)
                        scope.hideSelectionPopup();
                });
                if (e && e.currentTarget) {
                    $(e.currentTarget).closest('.sp-lineItem').removeClass('focusedItem');
                    var displayEelectionPopup = $('#spSelectionPopup' + scope.paperUniqueID).css('display');
                    if (scope.isCursorOnDecoration == false || displayEelectionPopup == "none") {
                        data.name1 = e.currentTarget.innerHTML;
                    }
                }
                clearTimeout(timeout);

                if (e && e.currentTarget) {
                    $(e.currentTarget).addClass('currentTarget');
                }

                // Save data only when changed
                 //if (scope.usedInSlick || scope.dataChange) {
                 try {
                   scope.sendDataToParent();
                 } catch (e) {
                 }
                  scope.dataChange = false;
                //}

                //var updateValues = angular.copy(scope.tree);
                if (e && data.id > 0) {
                  var allSequences = $('.sp-child-tree-task').map(function() {
                        return $(this).find('.sp-input:first').data('sptaskid');
                    }).get();

                    if (scope.searchtext != undefined && scope.searchtext.name && scope.searchtext.name.length > 0) {
                        var orgSequence = TasksSeqData.find({
                            userId: user_id
                        }).fetch();
                        var sequence = orgSequence[0].sequence.split(",").map(Number);
                        if (e.type === 'drop') {

                            var currIndex = allSequences.indexOf(data.id);
                            var parentChildElement = allSequences[currIndex + 1];
                            var idx = sequence.indexOf(data.id);
                            sequence.splice(idx, 1);
                            var parentIndex = sequence.indexOf(parentChildElement);
                            if (allSequences.length == currIndex + 1) {
                                parentChildElement = allSequences[currIndex - 1];
                                parentIndex = sequence.indexOf(parentChildElement) + 1;
                            }

                            sequence.splice(parentIndex, 0, data.id);
                            //var newOrders = allSequences.split(",");
                            Meteor.call('saveTaskSequence', user_id, sequence.join(","));
                        } else {
                            if (allSequences.length < sequence.length) {
                                allSequences = sequence;
                            }
                            Meteor.call('saveTaskSequence', user_id, allSequences.join(","));
                        }
                    }
                }

                $('.unsaved-popup').hide();
            };

            scope.onDrop = function(data, $parent, $index) {
                $parent.$parent.data.nodes.splice($index, 1);
                scope.dataChange = true;

                $timeout(function() {
                    focusItemById(data.id);
                });
            };

            scope.openLinks = function(event, data) {

                if (event.ctrlKey) {
                    // $rootScope.$emit('spSearch', data.name.trim());
                    // event.preventDefault();

                    if (data.linkData && data.linkData.length) {
                        return false;
                    }
                    var pos = getCaretCharacterOffsetWithin(event.currentTarget);
                    var text = data.name.slice(0, pos);

                    // $rootScope.$emit('spSearch', text.trim() || data.name.trim());
                    var event = new CustomEvent(
                        "spSearch", 
                        {
                            detail: {
                                text: text.trim(),
                                data: data.name.trim(),
                            },
                            bubbles: true,
                            cancelable: true
                        }
                    );
                    document.dispatchEvent(event);
                    event.preventDefault();
                    return false;
                } else {
                    openNestedLinks(data.nodes);
                }

            };

            scope.ctrlSelection = function(event, data, parent, index, parentIndex) {
                $(".optionsPopup").hide();
                data.index = index;
                if (data.status) {
                    //scope.onNgblur(event, data);
                    //return false;
                }
                if (!event.shiftKey && !event.ctrlKey) {
                    $('.firstCopy').removeClass('firstCopy currentCopy');
                    clearSelection();
                    scope.removeAllTasksFromSelection();
                }
                if (event.ctrlKey) {
                    if ($(event.currentTarget).closest('li').hasClass('manualSelect')) {
                        $(event.currentTarget).closest('li').removeClass('manualSelect selectedCopy parent');
                        scope.removeTaskFromSelection(data);
                        return false;
                    } else {
                        $(event.currentTarget).closest('li').addClass('manualSelect selectedCopy ctrlSelect').removeClass('parent');
                        scope.addTaskToSelection(data, parent, parentIndex);

                        return false;
                    }
                } else if (event.shiftKey) {
                    if (scope.selectedTaskRangeData.length === 0) {
                        scope.shiftRangeSelection();
                    }

                    shiftSelection($(event.currentTarget).closest('li'), data, parent);
                }
            };

            scope.closeModal = function(data) {

                if (scope.dialogInstance) {
                    scope.dialogInstance.close(data);
                }
            };



            scope.showIntellisense = function(event, data) {

                if (event && event.keyCode == keyCodes.enter && scope.isPopUpopned) {
                    scope.closeModal(scope.item, event.target.id);
                }

                var urlRegx = /((http|https):?\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})/;
                var url = event.target.textContent.match(urlRegx);

                if(!(url && event))
                {
                  return;
                }

                scope.data.name = event.currentTarget.textContent;

                if (event && data) {
                    clearTimeout(timeout);
                    timeout = autoOpen(event, data);
                }
            };

            scope.onChange = function(data) {
                // Autosave the task when using intellisence when no activity for two seconds
                clearTimeout(timeout);
                timeout = autoSave(data);
            };

            function autoOpen(event, data) {

                if (event.keyCode === keyCodes.leftArrow || event.keyCode === keyCodes.rightArrow ||
                    event.keyCode === keyCodes.upArrow || event.keyCode === keyCodes.downArrow ||
                    event.keyCode === keyCodes.escape || event.keyCode === keyCodes.tab) {
                    return;
                }

                var urlRegx = /((http|https):?\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})/;
                var url = event.target.textContent.match(urlRegx);
                var index = event.target.getAttribute('data-sptaskid');
                var ele = angular.element($('[data-sptaskid="' + index + '"]'));
                var tags = $(ele).find('a');


                return setTimeout(function() {
                    if (url && event) {
                        if (tags.length == 0) {
                            var isOpen = $("#spWebLinkPopUp").is(":visible");
                            if (isOpen == false) {
                                scope.openHyperLinkModal(event, data);
                            }
                        }
                    }
                }, 1500);
            }

            function autoSave(data, event) {

              // if (!data.name || data.name.trim() === "") {
              //       data.linkData = [];
              //       data.name1 = '';
              //       scope.sendDataToParent();
              //       return;
              //   }

                return setTimeout(function() {
                    // Save only if the title contains text
                    if (data.name.trim()) {
                        scope.onNgblur({}, data);
                        //tskScheduler(data);
                    }
                }, 1500);
            }


            OpenHyperLink = function(obj) {

              // if (obj.ctrlKey) {
              //      // Don't open links when CTRL + Click
              //      return false;
              //  }

                var currentEle = obj.currentTarget;

                if (currentEle) {

                    var parentData = angular.element(obj.currentTarget).scope().data;
                    var redirectWindow;

                    var url = obj.currentTarget.getAttribute('href');
                    var checkPrefixRegx = /(http|https):?\/\//;

                    if (!checkPrefixRegx.test(url)) {
                        url = "http://".concat(url);
                    }

                    if (parentData.nodes.length > 0) {

                        redirectWindow = window.open(url, '_blank');
                        redirectWindow.location;

                        openNestedLinks(parentData.nodes);

                    } else {
                        redirectWindow = window.open(url, '_blank');
                        redirectWindow.location;
                    }
                }
            };

            function openNestedLinks(currentNodes) {
                var checkPrefixRegx = /(http|https):?\/\//;

                for (var i = 0; i < currentNodes.length; i++) {

                    if (currentNodes[i].linkData && currentNodes[i].linkData.length > 0) {
                        var link = currentNodes[i].linkData[0].link;
                        if (!checkPrefixRegx.test(link)) {
                            link = "http://".concat(link);
                        }
                        try {
                            window.open(link, '_blank');
                        } catch (e) {
                            console.warn(e);
                        }
                    }

                    if (currentNodes[i].nodes.length > 0) {
                        openNestedLinks(currentNodes[i].nodes);
                    }
                }
            }

            function getTitle(displayStr) {
                var urlRegx = /((http|https):?\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})/;
                if (urlRegx.test(displayStr)) {
                    return true;
                } else {
                    return false;
                }
            }

            scope.HyperLink = function() {

                scope.dialogInstance = {};

                scope.isPopUpopned = true;

                var elem = window.getSelection();
                var anchorElem = elem.anchorNode.parentElement;

                if (elem) {

                    var urlPattern = new RegExp(/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/);

                    scope.item.displayText = window.getSelection().toString();

                    if (urlPattern.test(scope.item.displayText)) {
                        scope.item.linkUrl = scope.item.displayText;
                    } else {
                        scope.item.linkUrl = "http://";
                    }
                    scope.prevText = window.getSelection().toString();

                    if (elem.focusNode.parentElement.tagName === "A") {
                        var prevUrl = elem.focusNode.parentElement.getAttribute('href');
                        $scope.id = elem.focusNode.parentElement.id;
                        scope.item.linkUrl = prevUrl;
                        scope.item.displayText = elem.focusNode.parentElement.textContent;
                    } else if (!anchorElem.hasAttribute("href")) {
                        $rootScope.uniqueID++;
                        $scope.id = 'selectedText' + $rootScope.uniqueID;

                        var spanEle = document.createElement('span');
                        spanEle.setAttribute('id', $scope.id);
                        spanEle.setAttribute('name', 'selectedText');
                        spanEle.textContent = window.getSelection().toString();

                        var range = window.getSelection().getRangeAt(0);
                        range.deleteContents();
                        range.insertNode(spanEle);

                    } else {
                        var prevUrl = anchorElem.getAttribute('href');
                        $scope.id = anchorElem.parentElement.id;
                        scope.item.linkUrl = prevUrl;
                    }
                }

                scope.dialogInstance = ngDialog.open({
                    template: 'popupTmpl.html',
                    className: 'ngdialog-theme-default',
                    appendClassName: 'sp-dialog',
                    closeByDocument: false,
                    scope: scope
                });

                $rootScope.$on('ngDialog.opened', function(e, $dialog) {
                    // Autofocus the first textbox in the dialog
                    $('#spDisplayField').focus().select();
                });

                scope.dialogInstance.closePromise.then(function(data) {

                    var newText = document.getElementById($scope.id);

                    if (data != undefined && data.value != undefined && data.value != "$escape" && data.value != "$closeButton" && data.value != "$document") {

                        var newDisplayText = "";
                        var newLinkUrl = "";
                        var linkTooltip = "";

                        if (data.value.displayText.trim() != "") {
                            newDisplayText = data.value.displayText;
                            newLinkUrl = data.value.linkUrl;
                        } else {
                            newDisplayText = scope.prevText;
                            newLinkUrl = data.value.linkUrl;
                        }

                        var isURL = getTitle(newDisplayText);

                        if (!isURL) {
                            linkTooltip = newLinkUrl;
                        }

                        if (newText && newText.tagName !== "A") {
                            newText.innerHTML = '<a id="' + $scope.id + '" href="' + newLinkUrl + '" style="cursor:pointer" title = "' + linkTooltip + '" onclick="OpenHyperLink(event)" target="_blank">' + newDisplayText + '</a> ';
                        } else {
                            $(newText).attr({
                                id: $scope.id,
                                href: newLinkUrl,
                                title: linkTooltip
                            }).html(newDisplayText);
                            //scope.CompileEditableDiv($(newText));
                        }

                        scope.hideSelectionPopup();
                        //scope.CompileEditableDiv(newText);

                        var $element = scope.event.currentTarget;
                        var id = $element.getAttribute('data-sptaskid');
                        //var eleData = angular.element(document.querySelector('.input[data-sptaskid="' + id + '"]')).scope().data;
                        var eleData = angular.element(getItemById(id).closest('li')[0]).scope().data;
                        var linkDetails = {
                            displayText: newDisplayText,
                            link: newLinkUrl
                        };
                        var linkArray = eleData.linkData;

                        if (linkArray && linkArray.length != 0) {
                            for (var i = 0; i < linkArray.length; i++) {
                                if (linkArray[i].displayText.toLowerCase() != newDisplayText || linkArray[i].link.toLowerCase() != newLinkUrl) {
                                    linkArray[i].displayText = newDisplayText;
                                    linkArray[i].link = newLinkUrl;
                                }
                            }
                        } else {
                            eleData.linkData = [];
                            eleData.linkData.push(linkDetails);
                        }

                        anchorElem.innerHTML = anchorElem.innerHTML.replace(/<\/?span[^>]*>/g, "");
                        // if(newText){
                        //     SetCaretPosition(newText);
                        // }else{
                        //     SetCaretPosition(anchorElem);
                        // }
                        SetCaretPosition(anchorElem);

                    } else {
                        anchorElem.innerHTML = anchorElem.innerHTML.replace(/<\/?span[^>]*>/g, "");
                        scope.hideSelectionPopup();
                        SetCaretPosition(anchorElem);
                    }
                    scope.dataChange = true;
                });
            };

            function getCaretCharacterOffsetWithin(element) {
                var caretOffset = 0;
                var doc = element.ownerDocument || element.document;
                var win = doc.defaultView || doc.parentWindow;
                var sel;
                if (typeof win.getSelection != "undefined") {
                    sel = win.getSelection();
                    if (sel.rangeCount > 0) {
                        var range = win.getSelection().getRangeAt(0);
                        var preCaretRange = range.cloneRange();
                        preCaretRange.selectNodeContents(element);
                        preCaretRange.setEnd(range.endContainer, range.endOffset);
                        caretOffset = preCaretRange.toString().length;
                    }
                } else if ((sel = doc.selection) && sel.type != "Control") {
                    var textRange = sel.createRange();
                    var preCaretTextRange = doc.body.createTextRange();
                    preCaretTextRange.moveToElementText(element);
                    preCaretTextRange.setEndPoint("EndToEnd", textRange);
                    caretOffset = preCaretTextRange.text.length;
                }
                return caretOffset;
            }

            function placeCaretAtEnd(el) {
                el.focus();
                if (typeof window.getSelection != "undefined" &&
                    typeof document.createRange != "undefined") {
                    var range = document.createRange();
                    range.selectNodeContents(el);
                    range.collapse(false);
                    var sel = window.getSelection();
                    sel.removeAllRanges();
                    sel.addRange(range);
                } else if (typeof document.body.createTextRange != "undefined") {
                    var textRange = document.body.createTextRange();
                    textRange.moveToElementText(el);
                    textRange.collapse(false);
                    textRange.select();
                }
            }

            scope.openHyperLinkModal = function(event, dataObj) {

                var currentElem = event.target;
                var isURL = true;
                var text = dataObj.name;
                var urlPattern = /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/;
                //var urlRegx = /\b(http|https)?(:\/\/)?(\S*)\.(\w{2,6})\b/ig;
                var urlRegx = /((http|https):?\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})/;

                scope.dialogInstance = {};

                var str = '';

                var pos = 0;

                pos = getCaretCharacterOffsetWithin(currentElem);

                var cursorPos = getCaretCharacterOffsetWithin(currentElem);

                while (/\s/.test(text[pos]) || text[pos] == undefined) {
                    pos--;
                }

                if (currentElem.children.length == 0) {
                    isURL = true;

                } else {

                    for (var i = pos; i >= 0; i--) {

                        if (text[i] != undefined) {
                            if (text[i].charCodeAt(0) == 160) {
                                newPos = i;
                                break;
                            }
                            str += text[i];
                        }
                    }

                    if (str) {
                        var newStr = str.split("").reverse().join("");
                        newStr = newStr.trim();
                    } else {
                        isURL = false;
                    }

                    if (urlPattern.test(newStr)) {
                        isURL = true;
                    }

                    for (var i = 0; i < currentElem.children[0].children.length; i++) {
                        if (currentElem.children[0].children[i].nodeName == "A" && currentElem.children[0].children[i].textContent == newStr) {
                            isURL = false;
                        }
                    }

                }

                if (currentElem) {

                    var url = '';
                    var newStr = '';
                    var newPos = 0;

                    str = "";

                    for (var i = pos; i >= 0; i--) {

                        if (text[i].charCodeAt(0) == 160) {
                            newPos = i;
                            break;
                        }
                        str += text[i];
                    }

                    newStr = str.split("").reverse().join("");

                    if (newPos > 0) {
                        url = newStr.match(urlRegx);
                        if (url) {
                            scope.item.displayText = url[0];
                            scope.prevText = url[0];
                        } else {
                            isURL = false;
                        }
                    } else {
                        url = text.match(urlRegx);
                        if (url) {
                            scope.item.displayText = url[0];
                            scope.prevText = url[0];
                        } else {
                            isURL = false;
                        }
                    }

                    var index = text.indexOf(scope.item.displayText);

                    if (urlRegx.test(scope.item.displayText)) {
                        scope.item.linkUrl = scope.item.displayText;
                    } else {
                        scope.item.linkUrl = "http://";
                    }


                    if (isURL) {

                        $rootScope.uniqueID++;
                        $scope.id = 'selectedText' + $rootScope.uniqueID;
                        var normalText = '';
                        var afterURLText = '';
                        var spanEle = document.createElement('span');

                        if (index > 0 || index == 0) {
                            afterURLText = text.substr(cursorPos);
                        }

                        normalText = text.substr(0, index);
                        spanEle.innerHTML = '<span id=' + $scope.id + ' name="selectedText">' + scope.prevText + '</span> ';
                        //currentElem.innerHTML = normalText + spanEle.innerHTML + afterURLText;
                        //currentElem.innerHTML = currentElem.innerHTML.replace(urlRegx,spanEle.innerHTML);
                        dataObj.name1 = currentElem.innerHTML.replace(urlRegx, spanEle.innerHTML);

                    }
                }

                if (isURL) {

                    openPopUp(currentElem, dataObj, event);
                    scope.isPopUpopned = false;
                    $('#slickPaperSpinLoading').css('display', 'flex');
                    // var checkPrefixRegx = /(http|https):?\/\//;
                    // var url = scope.item.displayText;
                    // if (!checkPrefixRegx.test(url)) {
                    //     url= "http://".concat(url);
                    // }
                   if(scope.usedInSlick == "true"){
                        Meteor.call('getTitleOfURL',url,function(err, response){
                              console.warn(response);
                              $('#slickPaperSpinLoading').hide();
                              var titleText = "";
                              if(response && response.content){
                                  titleText = response.content.match(/<title>(.*?)<\/title>/mi)[1];
                                  console.warn(titleText);
                                  if(titleText.indexOf("Access to this site is blocked") == -1){
                                    scope.item.displayText = titleText;
                                  } else{
                                    scope.item.displayText = getHostName(scope.item.displayText);
                                  }
                              }else{
                                  scope.item.displayText = getHostName(scope.item.displayText);
                              }
                              scope.$digest();
                              $('#spDisplayField').focus().select();
                              scope.isPopUpopned = true;
                        });
                  } else{
                        $.ajax({
                            url: "http://textance.herokuapp.com/title/" + scope.item.displayText,
                            async: true,
                            success: function(data) {
                                $('#slickPaperSpinLoading').hide();
                                //openPopUp(data, currentElem, dataObj);
                                if (data && data.responseText) {
                                    scope.item.displayText = data.responseText;
                                } else {
                                    scope.item.displayText = getHostName(scope.item.displayText);
                                }
                                scope.$digest();
                                $('#spDisplayField').focus().select();
                                scope.isPopUpopned = true;
                            },
                            error: function(request, status, error) {
                                $('#slickPaperSpinLoading').hide();
                                scope.item.displayText = getHostName(scope.item.displayText);
                                scope.$digest();
                                $('#spDisplayField').focus().select();
                                scope.isPopUpopned = true;
                            }
                        });
                    }
                }
            };

            function getHostName(url) {

                var domain = "";

                //remove "http://" and "https://"
                if (url.indexOf("http://") == 0) {
                    url = url.substr(7);
                } else if (url.indexOf("https://") == 0) {
                    url = url.substr(8);
                }
                //remove "www."
                if (url.indexOf("www.") == 0) {
                    url = url.substr(4);
                }
                domain = url.split('/')[0].split('.')[0];

                domain = domain.charAt(0).toUpperCase() + domain.slice(1);

                return domain;

            }


            function openPopUp(currentElem, dataObj, event) {

                scope.dialogInstance = ngDialog.open({
                    template: 'popupTmpl.html',
                    className: 'ngdialog-theme-default',
                    appendClassName: 'sp-dialog',
                    closeByDocument: false,
                    scope: scope
                });

                $rootScope.$on('ngDialog.opened', function(e, $dialog) {
                  // Autofocus the first textbox in the dialog
                    $('#spDisplayField').focus().select();
                });

                scope.dialogInstance.closePromise.then(function(data) {

                    if (data != undefined && data.value != undefined && data.value != "$escape" && data.value != "$closeButton" && data.value != "$document") {

                        var newText = document.getElementById($scope.id);
                        var newDisplayText = "";
                        var newLinkUrl = "";
                        var linkTooltip = "";

                        if (data.value.displayText.trim() != "") {
                            newDisplayText = data.value.displayText;
                            newLinkUrl = data.value.linkUrl;
                        } else {
                            newDisplayText = scope.prevText;
                            newLinkUrl = data.value.linkUrl;
                        }

                        var isURL = getTitle(newDisplayText);

                        if (!isURL) {
                            linkTooltip = newLinkUrl;
                        }

                        if (newText) {
                            newText.innerHTML = '<a id="' + $scope.id + '" href="' + newLinkUrl + '" style="cursor:pointer;" title = "' + linkTooltip + '" onclick="OpenHyperLink(event)" target="_blank">' + newDisplayText + '</a>';
                        }

                        dataObj.name = dataObj.name.replace(/\/$/, "");
                        dataObj.name1 = currentElem.innerHTML;
                        currentElem.innerHTML = currentElem.innerHTML.replace(/<\/?span[^>]*>/g, "");

                        scope.CompileEditableDiv(newText);

                        var linkDetails = {
                            displayText: newDisplayText,
                            link: newLinkUrl
                        };

                        var linkArray = dataObj.linkData;

                        if (linkArray && linkArray.length != 0) {
                            for (var i = 0; i < linkArray.length; i++) {
                                if (linkArray[i].displayText.toLowerCase() != newDisplayText || linkArray[i].link.toLowerCase() != newLinkUrl) {
                                    linkArray[i].displayText = newDisplayText;
                                    linkArray[i].link = newLinkUrl;
                                }
                            }
                        } else {
                            dataObj.linkData = [];
                            dataObj.linkData.push(linkDetails);
                        }

                    } else {
                      currentElem.innerHTML = currentElem.innerHTML.replace(/<\/?span[^>]*>/g, "");
                        isURL = true;
                    }

                    if (event.keyCode !== keyCodes.enter) {
                        //placeCaretAtEnd(currentElem);
                        focusItemById(dataObj.id);
                    } else {
                        focusItemById(scope.nextLineItemID);
                    }
                    scope.dataChange = true;
                });
            }


            scope.onNgFocus = function(data, e, $parent, $index) {
                element.find('.sp-lineItem.focusedItem').removeClass('focusedItem');
                  $(e.target).closest('.sp-lineItem').addClass('focusedItem');
                  scope.focusParent = $parent;
                  // Cache
                  scope.data = data;
                  scope.event = e;
                  scope.parent = $parent;

                  if(scope.usedInSlick == "true")
                  {
                    if (Session.get('notesView') === 'tileView') {
                      Session.set("currentSheet", $(e.currentTarget).closest('[slick-paper]').attr('slick-paper-id'));
                    }
                  }

                  if (e.currentTarget) {
                      $(e.currentTarget).addClass('currentTarget');
                  }
              };

            scope.CompileEditableDiv = function(ele) {
                $compile(angular.element(ele))(scope);
            };

            var updateRootIdForChildrens = function(arr, user_id, rootId) {
                $.each(arr, function(i, val) {
                    if (rootId == null) {
                        rootId = val.id;
                    }
                    if (val.nodes) {
                        updateRootIdForChildrens(val.nodes, user_id, rootId);
                    }
                });
            };

            scope.showDeleteModal = function(haveChildren) {
                if (haveChildren) {
                    //scope.customAlertMessage = 'You cannot delete a task that has children tasks.';
                    //toastr.warning("You cannot delete a task that has children tasks")
                } else {
                    scope.deleteItem();
                }
            };

            //start-------------------------DELETE TASK-----------------------------------------------------------------

            scope.isContextMenu = false;
            scope.setContextMenu = function() {
                scope.isContextMenu = true;
            };

            scope.setDeleteEvent = function($event) {
                scope.event1 = $event;
            };

            function compare(a, b) {
                if (a.parentIndex > b.parentIndex)
                    return -1;
                else if (a.parentIndex < b.parentIndex)
                    return 1;
                else
                    return 0;
            }

            function deleteSelectedTasks() {

                var isAnyChildPresent = false;
                scope.selectedTaskData.sort(compare);
                // scope.sortByParent();
                // scope.selectedTaskData = scope.sortByParent();

                scope.selectedTaskData.forEach(function(item) {
                    if (item.data.nodes.length < 1) {
                        deleteTaskAndRelatedResources(item.data, item.parent);
                    } else {
                        isAnyChildPresent = true;
                    }
                });

                scope.removeAllTasksFromSelection();
                clearSelection();
            }

            function deleteTaskAndRelatedResources(data, $parent) {

                scope.delete(data, $parent);

                var task = angular.copy(data);

                window.eventService.removeEvent(task); //delete scheduler event when tree task item is deleted
                sharedUtils.tasks.deleteShared(task, null, "all"); //delete shared task when tree task item is deleted
            }

            scope.deleteItem = function() {
                var currentTarget = scope.event1.currentTarget;

                deleteSelectedTasks();
                $timeout(function() {
                    var $input = getItems();
                    var ind = (scope.index1 >= $input.length ? $input.length - 1 : scope.index1) % $input.length;

                    // Focus next element
                    $input.eq(ind).focus();
                });
            };

            function getEmptyTasksCount(tasks, index) {
                var task = tasks[index + 1],
                    nextTask = tasks[index + 2];
                if (task && nextTask && (task.type === 'image' || nextTask.type === 'image')) {
                    return true;
                }

                return !((task && task.name.trim() === '') && (nextTask && nextTask.name.trim() === ''));
            }
        }
    }
}]);

angular.module('slickPaper').directive('spExpand', function() {
    return function(scope, element, attrs) {
        element.bind('click', function(event) {
            var list = element.closest('li').find('> ul');

            var link = $(this);
            if (link.is(':visible')) {
                list.toggleClass('hide');
                link.toggleClass('sp-expandChildren sp-collapseChildren');
            }
        });
    };
});
