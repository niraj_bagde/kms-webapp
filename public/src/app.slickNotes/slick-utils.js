// Add findDeep function in the underscore object
// Used to get the shared task's individual tree
// Reference: http://stackoverflow.com/a/17580000/2025923
_.findDeep = function(items, attrs) {
    'use strict';

    function match(value) {
        for (var key in attrs) {
            if (!_.isUndefined(value) && value !== null) {
                if (attrs[key] !== value[key]) {
                    return false;
                }
            } else {
                return false;
            }
        }
        return true;
    }

    function traverse(value) {
        var result;

        _.forEach(value, function(val) {
            if (match(val)) {
                result = val;
                return false;
            }

            if (_.isObject(val) || _.isArray(val)) {
                result = traverse(val);
            }

            if (result) {
                return false;
            }
        });

        return result;
    }
    return traverse(items);
};

var slickPaperUtils = (function() {
    'use strict';

    // To get random MongoDB Object ID
    var getObjectId = function() {
        return new Meteor.Collection.ObjectID()._str;
    };

    // To get the tree controller scope object
    // from outside of the Angular Scope
    var getTreeScope = function() {
        return angular.element(document.querySelector('[ng-controller="TreeController"]')).scope();
    };

    // Get non-empty tasks count
    var getNonEmptyTask = function(arr) {
        return arr.filter(function(task) {
            return task.name.trim();
        });
    };

    var scrollIntoView = function(el) {
        // Reference: https://github.com/litera/jquery-scrollintoview
        return el.scrollintoview({
            direction: "vertical",
            duration: 50 // 50 ms
        });

        var view = $('.sp-tree').first(),
            elementTop = el.closest('li.main-parent').position().top === 0 ? el.closest('li.main-parent').offset().top : el.closest('li.main-parent').position().top;

        if (view.outerHeight() < $('.sp-tree').outerHeight()) {
            console.warn('Return from 27');
            return;
        }

        if (el.parents('li').length > 1) {
            var scrollTop = view.scrollTop();
            elementTop = el.closest('li').offset().top;
            var actualTop = elementTop - scrollTop;
            if (actualTop > view.outerHeight()) {
                console.warn('Return from 35');
                return;
            }
        }

        if (view.outerHeight() - elementTop <= 0) {
            el[0].scrollIntoView(false);
        } else if (elementTop < 0) {
            el[0].scrollIntoView(true);
        }
    };

    // Reference: http://stackoverflow.com/a/3976125/2025923
    var getCaretPosition = function(editableDiv) {
            var caretPos = 0,
                sel, range;
            if (window.getSelection) {
                sel = window.getSelection();
                if (sel.rangeCount) {
                    range = sel.getRangeAt(0);
                    if (range.commonAncestorContainer.parentNode == editableDiv) {
                        caretPos = range.endOffset;
                    }
                }
            } else if (document.selection && document.selection.createRange) {
                range = document.selection.createRange();
                if (range.parentElement() == editableDiv) {
                    var tempEl = document.createElement("span");
                    editableDiv.insertBefore(tempEl, editableDiv.firstChild);
                    var tempRange = range.duplicate();
                    tempRange.moveToElementText(tempEl);
                    tempRange.setEndPoint("EndToEnd", range);
                    caretPos = tempRange.text.length;
                }
            }
            return caretPos;
        },

        // Reference: http://stackoverflow.com/a/6249440/2025923
        setCaretPosition = function(el, index) {

            index = typeof index === 'undefined' ? el && el.textContent && el.textContent.trim().length || 0 : index;

            var range = document.createRange(),
                sel = window.getSelection();

            try {
                range.setStart(el.childNodes[0] || el, index);
            } catch (e) {
                console.error('ERROR:', e);
                range.setStart(el.childNodes[0], 0);
            }
            range.collapse(true);
            sel.removeAllRanges();
            sel.addRange(range);
        };

    // Reference: http://stackoverflow.com/a/5379408/2025923
    function getSelectionText() {
        var text = "";
        if (window.getSelection) {
            text = window.getSelection().toString();
        } else if (document.selection && document.selection.type != "Control") {
            text = document.selection.createRange().text;
        }

        return text;
    }

    function clearSelection() {
        if (window.getSelection) {
            if (window.getSelection().empty) { // Chrome
                window.getSelection().empty();
            } else if (window.getSelection().removeAllRanges) { // Firefox
                window.getSelection().removeAllRanges();
            }
        } else if (document.selection) { // IE?
            document.selection.empty();
        }
    }
    // Mixpanel Tracking Events
    var trackEvents = function(action, property) {
        property = property || {};
        if (Session.get('username')) {
            property.username = Session.get('username');
        }

        mixpanel.track(action, property);
    };

    var getParentlength = function(nodes) {
        window.count++;

        for (var i = 0; i < nodes.length; i++) {
            getParentlength(nodes[i].nodes);
        }
        return count;
    };

    var getTaskSummary = function(taskId, userId) {
        console.log("Task Id", taskId, "User Id", userId);
        var data = TasksData.find({
            userId: userId,
            _id: taskId
        }).fetch();

        return data;
    };
    // Takes an ISO time and returns a string representing how
    // long ago the date represents.
    function prettyDate(date) {
        var diff, day_diff;
        try {
            diff = (((new Date()).getTime() - date.getTime()) / 1000);
            day_diff = Math.floor(diff / 86400);
        } catch (e) {
            return date;
        }

        if (isNaN(day_diff) || day_diff < 0 || day_diff >= 31)
            return date;

        return day_diff === 0 && (
                diff < 60 && "just now" ||
                diff < 120 && "1 minute ago" ||
                diff < 3600 && Math.floor(diff / 60) + " minutes ago" ||
                diff < 7200 && "1 hour ago" ||
                diff < 86400 && Math.floor(diff / 3600) + " hours ago") ||
            day_diff == 1 && "Yesterday" ||
            day_diff < 7 && day_diff + " days ago" ||
            day_diff < 31 && Math.ceil(day_diff / 7) + " weeks ago";
    };

    //Check eleemnt presnt in visible area or notificationfunction isScrolledIntoView(elem)
    function isScrolledIntoView(elem) {
        var docViewTop = $(window).scrollTop();
        var docViewBottom = docViewTop + $(window).height();

        var elemTop = $(elem).offset().top;
        var elemBottom = elemTop + $(elem).height();

        return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    }

    function viewSelectedElement(customId) {
        var id = Session.get('selectedTaksId');

        if (id != undefined && customId == undefined) {

            var $task = $('div[data-taskid="' + id + '"]').closest('li').find('.input');

            var checkVisiblity = isScrolledIntoView($task[0]);

            if (!checkVisiblity)
                $('div[sp-editable]').eq($('div.input').index($task) + 5)[0].scrollIntoView(false);
            //$($task).fadeIn("slow");

        } else if (customId != undefined) {
            var $task = $('div[data-taskid="' + customId + '"]').closest('li').find('.input');
            if ($task[0] == undefined)
                return;
            var checkVisiblity = isScrolledIntoView($task[0]);

            if (!checkVisiblity)
                $('div[sp-editable]').eq($('div.input').index($task) + 5)[0].scrollIntoView(false);
        }
    }
    
   function getCaretPixelPos($node, offsetx, offsety) {
                
                offsetx = offsetx || 0;
                offsety = offsety || 0;

                var nodeLeft = 0,
                    nodeTop = 0;
                if ($node) {
                    nodeLeft = $node.offsetLeft;
                    nodeTop = $node.offsetTop;
                }

                var pos = {
                    left: 0,
                    top: 0
                };

                if (document.selection) {
                    var range = document.selection.createRange();
                    pos.left = range.offsetLeft + offsetx - nodeLeft + 'px';
                    pos.top = range.offsetTop + offsety - nodeTop + 'px';
                } else if (window.getSelection) {
                    var sel = window.getSelection();
                    var range = sel.getRangeAt(0).cloneRange();
                    try {
                        range.setStart(range.startContainer, range.startOffset - 1);
                    } catch (e) {}
                    var rect = range.getBoundingClientRect();
                    if (range.endOffset == 0 || range.toString() === '') {
                        // first char of line
                        if (range.startContainer == $node) {
                            // empty div
                            if (range.endOffset == 0) {
                                pos.top = '0px';
                                pos.left = '0px';
                            } else {
                                // firefox need this
                                var range2 = range.cloneRange();
                                range2.setStart(range2.startContainer, 0);
                                var rect2 = range2.getBoundingClientRect();
                                pos.left = rect2.left + offsetx - nodeLeft + 'px';
                                pos.top = rect2.top + rect2.height + offsety - nodeTop + 'px';
                            }
                        } else {
                            pos.top = range.startContainer.offsetTop + 'px';
                            pos.left = range.startContainer.offsetLeft + 'px';
                        }
                    } else {
                        pos.left = rect.left + rect.width + offsetx - nodeLeft + 'px';
                        pos.top = rect.top + offsety - nodeTop + 'px';
                    }
                }
                return pos;
            };

    return {
        getObjectId: getObjectId,

        getTreeScope: getTreeScope,
        getNonEmptyTask: getNonEmptyTask,
        scrollIntoView: scrollIntoView,

        // To get/set caret position in the textbox/textarea/contenteditable
        getCaretPosition: getCaretPosition,
        setCaretPosition: setCaretPosition,


        // Mixpanel events Tracking
        trackEvents: trackEvents,

        //Get count of the Children
        getParentlength: getParentlength,

        //Get summary of Task
        getTaskSummary: getTaskSummary,

        //Format Date
        prettyDate: prettyDate,

        //Click on Element
        viewSelectedElement: viewSelectedElement,

        // Text selection
        getSelectionText: getSelectionText,
        clearSelection: clearSelection,
        
        //getCaretPosinPixel
        
        getCaretPixelPos : getCaretPixelPos
    };
}());
