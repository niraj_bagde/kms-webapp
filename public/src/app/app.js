import React, { Component } from 'react';
import './app.css';
import './style.css';
import { BrowserRouter as Router, Link} from 'react-router-dom';
// import {Route, Redirect} from 'react-router';
// import {MainMenu} from '../app.route/app.nav.js';
import AppRouter from '../app.route/app.route-NBA.js';
// import AppHeader from '../app.header/app.header';


class App extends Component {

  render() {
    return (
      <Router>
        <AppRouter />
      </Router>
    );
  }
}

export default App;
