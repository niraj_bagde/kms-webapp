import React, { Component } from 'react';
import MiniVersions from "../app.miniVersions/app.miniVersions";
import Axios from 'axios';
import MiniDocPreview from '../app.miniPreview/app.miniPreview';
import SharedWith from '../app.sharedWith/app.sharedWith';
import DocumentProperties from '../app.documentProperties/app.documentProperties';
import Collections from '../app.collections/app.collections';

class AppFileInfoPanel extends Component {
    constructor(props) {
      super(props);
      let activeTab = "Properties"
      if (props.showCollection !== undefined && props.showCollection === true) {
        activeTab = "Collections"
      }
      this.state = {
            source_type:this.props.source_type,
            accessToken:this.props.accessToken,
            refreshToken:this.props.refreshToken,
            fileId:props.selectedDoc.file_id,
            documentId: props.selectedDoc.sp_doc_id && props.selectedDoc.sp_doc_id ,
            //   activeTab: 'Versions',
            activeTab: activeTab,
            versionArray: [],
        }
    }

    componentDidMount() {
      
        let documentId  = this.props.selectedDoc.sp_doc_id ? this.props.selectedDoc.sp_doc_id  : null
        this.getDocVersions(documentId);
    }

    componentDidUpdate (prevProps, prevState, snapshot) {
        if (prevProps.showCollection !== this.props.showCollection) {
            if (this.props.showCollection) {
                this.setState({activeTab: "Collections"})
            }
        }
    }



    getDocVersions = (documentId) => {
        if(documentId) {
            let request;
            let requestUrl="";
            if(this.props.source.indexOf('Sharepoint') >-1) {
                request = {
                    "ConnectorSourceName":"SharePoint",
                    "DocumentId": documentId
                }
                requestUrl = this.props.edocsBackendAPI+"api/v1/docversionhistory/";
            }
            request["ClientName"]= this.props.client;
            //get doc versions
            Axios.post(requestUrl, request).then(response => {
            // Axios.get("/docTree/getDocVersions/" + documentId).then(response => {
                let versionArray = response && response.data ? response.data : [];
                this.setState({
                    versionArray: versionArray
                })
            })
        }

    }

    openRightPane = (e) => {
        this.setState({
            activeTab: 'Properties'
        })
        this.props.openRightPane(e, this.props.selectedDoc.url, "docProperties", this.props.selectedDoc)
    }

    editDocument = () => {
        var documentId = this.props.selectedDoc.sp_doc_id;
        //  var url = "https://sp16-nba.thedigitalgroup.com:9091/api/v1/downloaddocument/" + documentId;

        // Axios.get(url).then(response => {
            let request;
            let requestUrl="";
            if(this.props.source.indexOf('Sharepoint') >-1) {
                request = {
                    "ConnectorSourceName":"SharePoint",
                    "DocumentId": documentId
                };

                requestUrl = this.props.edocsBackendAPI+"api/v1/downloaddocument/";
            }
            request["ClientName"]= this.props.client;
            Axios.post(requestUrl, request).then(response => {
            if(response && response.data && response.data.FilePath) {
                var url = "";
                if(this.props.fileType=='doc' || this.props.fileType=='docx') {
                    url ='ms-word:ofe|u|'+ response.data.FilePath;
                } else if (this.props.fileType=='xls' || this.props.fileType=='xlsx') {
                    url ='ms-excel:ofe|u|'+ response.data.FilePath;
                } else if (this.props.fileType=='ppt' || this.props.fileType=='pptx') {
                    url ='ms-powerpoint:ofe|u|'+ response.data.FilePath;
                }
                var mylink = document.getElementById("MyLink");
                mylink.setAttribute("href", url);
                mylink.click();
             }

        })
    }


    openToEditDocument = (fileID) => {
        //var newWin = window.open('https://app.box.com/file/'+fileID , 'Edit', 'width=1250,height=580,left=235,top=150');
        var newWin = window.open(fileID , 'Edit', 'width=1250,height=580,left=235,top=150');
    }

    render() {
        let selectedCollectionDoc = this.props.selectedDoc;
        if (Object.keys(this.props.itemToAddToCollection).length > 0 ) {
            selectedCollectionDoc = this.props.itemToAddToCollection;
        }
    return  (
        <div>
            <a id="MyLink"  style={{"display":"none"}}></a>
            <ul className="nav nav-tabs" style={{'marginTop': '13px'}}>
            <li
            onClick={()=>this.setState({activeTab: 'Properties'})}
            className={this.state.activeTab == 'Properties' ? "active" : ""}
            title="Properties"
            // style={{"float": "right", "marginRight": "-15px"}}
            >
                {/* <a><i class="fa fa-share-alt" aria-hidden="true" style={{"fontSize": "20px"}}></i></a> */}
                <a><i className="fa fa-align-left" aria-hidden="true"></i></a>
            </li>
            
            <li onClick={()=>this.setState({activeTab: 'Preview'})}
                className={this.state.activeTab == 'Preview' ? "active" : ""}
                title="Preview"
                >
                <a><i className="fa fa-eye" aria-hidden="true"></i></a>
            </li>
            <li onClick={()=>this.setState({activeTab: 'Collections'})}
                className={this.state.activeTab == 'Collections' ? "active" : ""}
                title="Collections"
                >
                <a><i className="fa fa-bars" aria-hidden="true"></i></a>
            </li>
            {/* <li onClick={()=>this.setState({activeTab: 'Versions'})}
                className={this.state.activeTab == 'Versions' ? "active" : ""}
                >
                <a>Versions</a>
            </li> */}
            <li
            onClick={()=>this.setState({activeTab: 'Share'})}
            className={this.state.activeTab == 'Share' ? "active" : ""}
            title="Sharing/Shared with"
            // style={{"float": "right", "marginRight": "-15px"}}
            >
                {/* <a><i class="fa fa-share-alt" aria-hidden="true" style={{"fontSize": "20px"}}></i></a> */}
                <a><i className="fa fa-share-alt" aria-hidden="true"></i></a>
            </li>
            <li onClick={()=>this.setState({activeTab: 'Versions'})}
                className={this.state.activeTab == 'Versions' ? "active" : ""}
                title="Versions"
                >
                <a><i className="fa fa-code-fork" aria-hidden="true"style={{"fontWeight": "bold"}}></i></a>
            </li>
                <li
                    onClick={(e) =>
                        this.openRightPane(e)
                    }
                    title="Expand"
                    style={{"float": "right"}}
                    className={this.state.activeTab == 'Expand' ? "active" : ""}
                    >
                    <a><i class="fa fa-expand" aria-hidden="true" style={{"fontSize": "19px"}}></i></a>
                </li>
                {/* { this.props.fileType!=='pdf' && this.props.fileType!=='msg' && */}
                    <li
                        onClick={(e) => this.openToEditDocument(this.props.selectedDoc.url)}
                            //this.openToEditDocument(this.state.fileId)}
                        title="Edit"
                        style={{"float": "right", "marginRight": "-15px"}}
                    >
                        <a><i class="fa fa-pencil-square-o" aria-hidden="true" style={{"fontSize": "20px"}}></i></a>
                    </li>
                {/* } */}

            </ul>
            {this.state.activeTab == "Collections" && 
                // <span> Coming soon !</span>
                <Collections   
                    versions={this.state.versionArray}
                    selectedDoc={selectedCollectionDoc}
                    fileType={this.props.fileType}
                    documentId={this.props.selectedDoc.sp_doc_id} 
                    source={this.props.source}
                    client={this.props.client}
                    fileId={this.state.fileId}
                    accessToken={this.state.accessToken}
                    refreshToken={this.state.refreshToken}
                    source_type={this.state.source_type}
                    edocsBackendAPI={this.props.edocsBackendAPI}
                    userEmail={this.props.userEmail}
                    closeCollection={()=>this.setState({activeTab: 'Properties'})}
                    loaderIcon={this.props.loaderIcon}
                    collectionNodeSelected={this.props.collectionNodeSelected}
                    sendCollectionAddEvent = {this.props.sendCollectionAddEvent}
                />
            }
            {this.state.activeTab == "Properties" && 
                // <span> Coming soon !</span>
                <DocumentProperties   
                    versions={this.state.versionArray}
                    selectedDoc={this.props.selectedDoc}
                    fileType={this.props.fileType}
                    documentId={this.props.selectedDoc.sp_doc_id} 
                    source={this.props.source}
                    client={this.props.client}
                    fileId={this.state.fileId}
                    accessToken={this.state.accessToken}
                    refreshToken={this.state.refreshToken}
                    source_type={this.state.source_type}
                    edocsBackendAPI={this.props.edocsBackendAPI}
                />
            }
            {this.state.activeTab == "Versions" && 
                // <span> Coming soon !</span>
                <MiniVersions   
                    versions={this.state.versionArray}
                    selectedDoc={this.props.selectedDoc}
                    fileType={this.props.fileType}
                    documentId={this.props.selectedDoc.sp_doc_id} 
                    source={this.props.source}
                    client={this.props.client}
                    fileId={this.state.fileId}
                    accessToken={this.state.accessToken}
                    refreshToken={this.state.refreshToken}
                    source_type={this.state.source_type}
                    edocsBackendAPI={this.props.edocsBackendAPI}
                />
            }
            {this.state.activeTab == "Share" && 
                <SharedWith 
                    selectedDoc={this.props.selectedDoc} 
                    fileType={this.props.fileType}
                    documentId={this.props.selectedDoc.sp_doc_id} 
                    userEmail={this.props.userEmail}
                    source={this.props.source}
                    client={this.props.client}
                    edocsBackendAPI={this.props.edocsBackendAPI}
                />
            }
            {this.state.activeTab == "Preview" && 
               <MiniDocPreview item={this.props.selectedDoc} 
                               loaderIcon={this.props.loaderIcon}
                               eventAppId={this.props.eventAppId}
                               solrCollection={this.props.solrCollection}
                               fileType={this.props.fileType}
                               documentId={this.props.selectedDoc.sp_doc_id} 
                               userEmail={this.props.userEmail}
                               source={this.props.source}
                               client={this.props.client}
                               fileId={this.state.fileId}
                               accessToken={this.state.accessToken}
                               refreshToken={this.state.refreshToken}
                               pageNo={this.props.pageNo}
                               timeCaptureTotal={this.props.timeCaptureTotal}
                               searchQuery={this.props.searchQuery}
                               totalCount={this.props.totalCount}
                               docUrl={this.props.docUrl}
                               userIP={this.props.userIP}
                               sessionId={this.props.sessionId}
                               document_ID={this.props.selectedDoc.file_id}
                               pageSize={this.props.pageSize}
                               source_type={this.state.source_type}
                               edocsBackendAPI={this.props.edocsBackendAPI}
                />
            }

        </div>
    )
  }
};


export default AppFileInfoPanel;