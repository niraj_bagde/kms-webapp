import React, { Component } from "react";
import "./app.pipelineTokens.css";

class PipelineTokens extends Component {
    constructor(props) {
      super(props);
      this.state = {
        queryTokens: props.tokens,
        autoPhraseSuggestions: [],
        pipelineTokens: props.pipelinetokens,
        tokenMap: props.tokenMap ? props.tokenMap : {},
      };
    }

    componentWillMount() {
        if(this.props.pipelinetokens && this.props.pipelinetokens.length>0)
            this.createTokenStructure(this.props.pipelinetokens)
    }


    createTokenStructure = (pipelineTokens) => {
        let autoPhraseSuggestions = [];
        let pinnedConcepts = this.props.pinnedconcepts ? this.props.pinnedconcepts : [];
        if (pipelineTokens && pipelineTokens.length > 0) {
            autoPhraseSuggestions = pipelineTokens.filter((token) => {
                if (token.tokenType == 'SYMBOL') {
                    if (/[^a-zA-Z0-9]/.test(token.token) && token.token.length === 1) {
                        return false;
                    }
                    return true;
                } else {
                    return true;
                }
            });

            autoPhraseSuggestions = this.toObject(autoPhraseSuggestions);
            if (pinnedConcepts && pinnedConcepts.length > 0) {
                autoPhraseSuggestions = this.getPinnedConcept(autoPhraseSuggestions, this.props.pinnedconcepts);
            }
            autoPhraseSuggestions = this.getIsExpandableTokens(autoPhraseSuggestions);
            
        }
        this.setState({autoPhraseSuggestions: autoPhraseSuggestions})
    }


    toObject = (listArr) => {
        var listObjArr = [];

         for(var i = 0;i < listArr.length;i++){
             var obj = {};
             obj['name'] = listArr[i].token;
             obj['isPinnedConcept'] = false;
             listObjArr.push(obj);
         }
      return listObjArr;
    }


    getPinnedConcept = (autoSuggestion,pinnedConceptsList) => {
        autoSuggestion.forEach(function(item,index){
              var isPresent = pinnedConceptsList.includes(item.name);
              if(isPresent){
                 item.isPinnedConcept = true;
              }
        });
        return autoSuggestion;
    }

    getIsExpandableTokens = (autoSuggestion) => {
        autoSuggestion.forEach((item,index) => {
              if(this.state.tokenMap[item.name]) {
                  item.isExpandable = true;
              } else {
                item.isExpandable = false;
              }
        });
        return autoSuggestion;
    }

    renderTokens = () => {
        var phrases = this.state.autoPhraseSuggestions;
        return (
            <div>
                {Array.isArray(phrases) && phrases.map((phrase, pIndex) => (
                <span className="text_18">
                    <span tabIndex="0" aria-label="" 
                        // ng-style="getCursorStyle(phrase.name);" 
                        // onClick={this.props.displayExpansions(phrase.name)}
                        // onMouseEnter
                        className={"query_token "+  ""}
                        
                        // ng-keydown="$event.keyCode==13 && displayExpansions(phrase.name)"
                    >
                    {phrase.name }
                    {phrase.isPinnedConcept &&
                        <i className="fa fa-thumb-tack" title="Pinned Concept"
                            style={{"color":"#a42038", "marginLeft": "3px", "marginRight": "3px"}} aria-hidden="true"></i> 
                    }
                    {phrase.isExpandable &&
                        <i className="fa fa-hand-pointer-o" title="Click to see Semantic Expansions"
                            style={{"color":"#a42038", "marginRight": "3px", "cursor":"pointer", "marginLeft": "3px",}} aria-hidden="true"
                            onClick={(e) => this.props.displayExpansions(phrase.name)}
                        ></i> 
                    }
                    </span>
                </span>
                ))}
            </div>
        );
    }

    render() {
        return this.renderTokens()
    }
}

export default PipelineTokens;