import React,{Component} from 'react';
// import {Route, Redirect} from 'react-router';
import axios from "axios";
import Search from '../app.search/app.search';
import Dashboard from '../app.dashboard/app.dashboard'
import Login from '../app.login/app.login'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    Redirect
  } from "react-router-dom";
import LoginForm from "../app.loginForm/app.loginForm-NBA.js"

class AppRouter extends Component {
    constructor(props) {
        super(props);
        this.state={
            userName : "",
            userEmail : "",
            client : "nba",
            azureClientId: "",
            redirectUri:""
        }
        this.getAzureClientId();
    }

    getAzureClientId = () => {
        axios.get("/getEnvConfigs").then((response, error) => {
            if(response) {
                this.setState({
                    "azureClientId": response.data.azureClientId,
                    "redirectUri": response.data.redirectUri
                  });
            }

          })
    }

    handleLogin = (name, username, client) => {
        this.setState({ userName: name, userEmail: username, client: client });
    }
    render() {
        return (
            <div>
                {/* <Route exact path="/search" component={Search} />
                <Route exact path="/login" component={LoginForm} />
                <Route path="*"><Redirect to="/" /></Route> */}
                <Switch>
                    <Route exact path="/">
                        <LoginForm onLogin={this.handleLogin} client={"nba"} 
                            azureClientId ={this.state.azureClientId}
                            redirectUri ={this.state.redirectUri}
                            />
                    </Route>
                    {/* <Route exact path="/alnylam">
                        <LoginForm onLogin={this.handleLogin} client={"alnylam"}/>
                    </Route>
                    <Route exact path="/nba">
                        <LoginForm onLogin={this.handleLogin} client={"nba"}/>
                    </Route> */}
                    <Route exact path="/search">
                        <Search user={this.state.userName} userEmail={this.state.userEmail} client={this.state.client}/>
                    </Route>
                    <Route path="/login" >
                        <LoginForm onLogin={this.handleLogin} client={this.state.client} 
                            azureClientId ={this.state.azureClientId}
                            redirectUri ={this.state.redirectUri}
                            />
                    </Route>
                    <Route path="*"><Redirect to="/" /></Route>
                </Switch>
            </div>

        );
    }
}

export default AppRouter;