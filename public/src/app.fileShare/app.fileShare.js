import React, { Component } from 'react';
import { Modal, Row, Container, Col, Button, Form } from "react-bootstrap";
import "./app.fileShare.css";
import Select from 'react-select';
import Axios from 'axios';

class FileShare extends Component {

    constructor(props) {
      super(props);
      this.state = {
        documentId: props.selectedDoc.sp_doc_id && props.selectedDoc.sp_doc_id ,
        showShareModal: false,
        activeTab: 'Share',
        userList: {},
        filePath: "",
        selectAllUsers: false, 
        selectedUsers: [],
        sharedWithUsers: this.props.sharedWithUsers
      }
    }

    componentWillMount() {
      this.getFilepath();
      // this.getUsersList();
    }

    getUsersList = () => {
        var userList = [];
        let request;
        let requestUrl = "";
        if(this.props.source.indexOf('Sharepoint') >-1) {
          request = {
            "ConnectorSourceName":"SharePoint"
          }
          requestUrl = this.props.edocsBackendAPI+"api/v1/sharepointusers";
        }
        request["ClientName"]= this.props.client;
        Axios.post(requestUrl, request).then(response => {
          if(response.data) {
            userList = response.data
          }
          userList.forEach(function(item) {
            item.value = item.Email;
            item.label = item.Name;
          });
        this.setState({
            userList: userList
        })
      })
    }

    handleChange = (value) => {
      this.setState({selectedUsers: value})
    }


    getFilepath = () => {
        let request;
        let requestUrl = "";
        if(this.props.source.indexOf('Sharepoint') >-1) {
            request = {
              "ConnectorSourceName":"SharePoint",
              "DocumentId": this.state.documentId
            };

            requestUrl = this.props.edocsBackendAPI+"api/v1/document/getdocpath";
        }
        request["ClientName"]= this.props.client;
        Axios.post(requestUrl, request).then(response => {
        // Axios.get("https://sp16-nba.thedigitalgroup.com:9091/api/v1/document/" + this.state.documentId).then((response) => {
          let filePath = "";
          if(response && response.data && response.data.FilePath) {
            filePath = response.data.FilePath;
          }

          this.setState({ filePath: filePath });
        });
    }

    onSelectAll = (e) => {
        this.setState({
            selectAllUsers: e.target.value
        })
    }

    handleClose = () => {
        this.props.hideFileShare()
    }

    handleSubmit = () => {
      var selectedUsersEmail = this.state.selectedUsers.map(function (d) {
        return d["Email"];
      })
      var request = {
        "sharedTo": selectedUsersEmail.join(','),
        "sharedBy": this.props.userEmail,
        "FilePath": this.state.filePath,
        "SharedByEmail": this.props.userEmail,
        "DocumentId": this.state.documentId
      }
      let requestUrl = "";

      this.props.hideFileShare();
      if(this.props.source.indexOf('Sharepoint') >-1) {
        request.ConnectorSourceName = "SharePoint";
        requestUrl = this.props.edocsBackendAPI+"api/v1/sharedocument/";
      }
      request["ClientName"]= this.props.client;
      // Axios.post(requestUrl, request)
    }


    render() {
    return  (
      <Modal
        show={true}
        onHide={() => this.handleClose()}
        aria-labelledby="contained-modal-title-vcenter"
        size="lg"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">Share Document</Modal.Title>
        </Modal.Header>
        <Modal.Body className="share-file">
        <ul className="nav nav-tabs" style={{"marginTop": "-10px"}}>
            <li onClick={()=>this.setState({activeTab: 'Share'})}
                className={this.state.activeTab == 'Share' ? "active" : ""}
                style={{"textAlign":"center", "width":"200px"}}
                >
                <a>Invite People</a>
            </li>
            <li onClick={()=>this.setState({activeTab: 'Shared'})}
                className={this.state.activeTab == 'Shared' ? "active" : ""}
                style={{"textAlign":"center", "width":"200px"}}
                >
                <a>Shared With</a>
            </li>
        </ul>
          <Form className="share-form">
          {this.state.activeTab == 'Share' &&
            <Form.Group as={Row} controlId="formHorizontalEmail">
            <Col md={2} className="user-label">
            <Form.Label >
              Select Users:
            </Form.Label>
            </Col>
            <Col md={9} className="pl-0">
                <Select
                    placeholder="Enter names.."
                    isMulti
                    name="users"
                    inputValue={this.state.inputValue}
                    onChange={(e, ref)=>{this.handleChange(e, ref);}}
                    options={this.state.userList}
                    className="basic-multi-select"
                    classNamePrefix="select"
                />
            </Col>
            {/* <Col md={1} className="pl-0 pr-0 chkbx-share-user">
                <input
                    title="Select All"
                    type="checkbox"
                    name="allcheckbox"
                    value={this.state.selectAllUsers}
                    onClick={(e)=> this.onSelectAll(e)}
                />
            </Col> */}
            {/* <Col md={4}>
                <Select
                    name="permission"
                    defaultValue={[{value:'edit', label:'can edit'}]}
                    options={[{value:'read', label:'can view'}, {value:'edit', label:'can edit'}]}
                    className="basic-multi-select"
                    classNamePrefix="select"
                />
            </Col> */}
          </Form.Group>
          }
          {this.state.activeTab == 'Shared' &&
                <Form.Group as={Row}>
                {/* <Form.Label column md={4}>
                Share with
 
                </Form.Label> */}
                <Col md={5}>
                <ul>
                    {this.state.sharedWithUsers.map((item, index) => (
                        <li>
                            <div className="col-md-12 pl-0 shared-list">
                                <div className="col-md-2 shared-user-pic"><i class="fa fa-user" title="User"></i></div>
                                <div className="col-md-10 shared-username" title={item.Email} style={{"textTransform": "capitalize"}}>{item.Name}</div>
                            </div>
                        </li>
                        ))
                    }
                    {/* <li>
                        <div className="col-md-12 pl-0">
                            <div className="col-md-2 shared-user-pic"><i class="fa fa-user" title="User"></i></div>
                            <div className="col-md-10 shared-user-name">Peter P.</div>
                        </div>
                    </li>
                    <li>
                        <div className="col-md-12 pl-0">
                            <div className="col-md-2 shared-user-pic"><i class="fa fa-user" title="User"></i></div>
                            <div className="col-md-10 shared-user-name">Tom C.</div>
                        </div>
                    </li> */}
                </ul>
                </Col>
                {/* <Col md={5}>
                <ul>
                    <li>
                        <div className="col-md-12 pl-0">
                            <div className="col-md-2 shared-user-pic"><i class="fa fa-user" title="User"></i></div>
                            <div className="col-md-10 shared-user-name">Edward J.</div>
                        </div>
                    </li>
                    <li>
                        <div className="col-md-12 pl-0">
                            <div className="col-md-2 shared-user-pic"><i class="fa fa-user" title="User"></i></div>
                            <div className="col-md-10 shared-user-name">James E.</div>
                        </div>
                    </li>
                </ul>
                </Col> */}
            </Form.Group>
          }
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Form.Group as={Row}>
            <Col md={12} right>
              <Button variant="primary" onClick={this.handleSubmit}>
                Share
              </Button>
              <Button variant="secondary" onClick={() => this.handleClose()}>
                Cancel
              </Button>
            </Col>
          </Form.Group>
        </Modal.Footer>
      </Modal>
    )
  }
};


export default FileShare;