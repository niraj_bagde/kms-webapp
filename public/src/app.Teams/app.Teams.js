import React from 'react';
import "./app.Teams.css";
import logo from '../images/NBA.png';
import gleauge from '../images/nba-g-leauge.png';

class Teams extends React.Component {
    render() {
        return (
            <div className="col-md-12 col-sm-12 col-xs-12 col-lg-12 pl-0 pr-0 teamssection">
                <div className="col-md-6 col-sm-6 col-xs-6 col-lg-6 pl-0">
                    <h4><span className="team-nba-icon"><img src={logo} alt="National Basketball Academy"  /></span>NBA League Teams</h4>
                    <div className="Team-Box">
                    <div className="teams-panel-heading">Atlantic</div>
                    <div className="team-panel-content">
                    <ul>
                        <li><span className="nba-leauge-team-1-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Boston_Celtics" target="_blank">Boston Celtics</a></span></li>
                        <li><span className="nba-leauge-team-2-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Brooklyn_Nets" target="_blank">Brooklyn Nets</a></span></li>
                        <li><span className="nba-leauge-team-3-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/New_York_Knicks" target="_blank">New York Knicks</a></span></li>
                        <li><span className="nba-leauge-team-4-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Philadelphia_76ers" target="_blank">Philadelphia 76ers</a></span></li>
                        <li><span className="nba-leauge-team-5-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Toronto_Raptors" target="_blank">Toronto Raptors</a></span></li>
                    </ul>
                    </div>
                    </div>
                    <div className="Team-Box">
                    <div className="teams-panel-heading teams-panel-heading-1">Central</div>
                    <div className="team-panel-content">
                    <ul>
                        <li><span className="nba-leauge-team-6-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Chicago_Bulls" target="_blank">Chicago Bulls</a></span></li>
                        <li><span className="nba-leauge-team-7-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Cleveland_Cavaliers" target="_blank">Cleveland Cavaliers</a></span></li>
                        <li><span className="nba-leauge-team-8-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Detroit_Pistons" target="_blank">Detroit Pistons</a></span></li>
                        <li><span className="nba-leauge-team-9-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Indiana_Pacers" target="_blank">Indiana Pacers</a></span></li>
                        <li><span className="nba-leauge-team-10-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Milwaukee_Bucks" target="_blank">Milwaukee Bucks</a></span></li>
                    </ul>
                    </div>
                    </div>
                    <div className="Team-Box">
                    <div className="teams-panel-heading teams-panel-heading-2">Southeast</div>
                    <div className="team-panel-content">
                    <ul>
                        <li><span className="nba-leauge-team-11-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Atlanta_Hawks" target="_blank">Atlanta Hawks</a></span></li>
                        <li><span className="nba-leauge-team-12-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Charlotte_Hornets" target="_blank">Charlotte Hornets</a></span></li>
                        <li><span className="nba-leauge-team-13-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Miami_Heat" target="_blank">Miami Heat</a></span></li>
                        <li><span className="nba-leauge-team-14-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Orlando_Magic" target="_blank">Orlando Magic</a></span></li>
                        <li><span className="nba-leauge-team-15-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Washington_Wizards" target="_blank">Washington Wizards</a></span></li>
                    </ul>
                    </div>
                    </div>
                    <div className="Team-Box">
                    <div className="teams-panel-heading teams-panel-heading-3">Northwest</div>
                    <div className="team-panel-content">
                    <ul>
                        <li><span className="nba-leauge-team-16-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Denver_Nuggets" target="_blank">Denver Nuggets</a></span></li>
                        <li><span className="nba-leauge-team-17-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Minnesota_Timberwolves" target="_blank">Minnesota Timberwolves</a></span></li>
                        <li><span className="nba-leauge-team-18-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Oklahoma_City_Thunder" target="_blank">Oklahoma City Thunder</a></span></li>
                        <li><span className="nba-leauge-team-19-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Portland_Trail_Blazers" target="_blank">Portland Trail Blazers</a></span></li>
                        <li><span className="nba-leauge-team-20-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Utah_Jazz" target="_blank">Utah Jazz</a></span></li>
                    </ul>
                    </div>
                    </div>
                    <div className="Team-Box">
                    <div className="teams-panel-heading teams-panel-heading-4">Pacific</div>
                    <div className="team-panel-content">
                    <ul>
                        <li><span className="nba-leauge-team-21-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Golden_State_Warriors" target="_blank">Golden State Warriors</a></span></li>
                        <li><span className="nba-leauge-team-22-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Los_Angeles_Clippers" target="_blank">Los Angeles Clippers</a></span></li>
                        <li><span className="nba-leauge-team-23-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Los_Angeles_Lakers" target="_blank">Los Angeles Lakers</a></span></li>
                        <li><span className="nba-leauge-team-24-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Phoenix_Suns" target="_blank">Phoenix Suns</a></span></li>
                        <li><span className="nba-leauge-team-25-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Sacramento_Kings" target="_blank">Sacramento Kings</a></span></li>
                    </ul>
                    </div>
                    </div>
                    <div className="Team-Box">
                    <div className="teams-panel-heading teams-panel-heading-5">Southwest</div>
                    <div className="team-panel-content">
                    <ul>
                        <li><span className="nba-leauge-team-26-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Dallas_Mavericks" target="_blank">Dallas Mavericks</a></span></li>
                        <li><span className="nba-leauge-team-27-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Houston_Rockets" target="_blank">Houston Rockets</a></span></li>
                        <li><span className="nba-leauge-team-28-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Memphis_Grizzlies" target="_blank">Memphis Grizzlies</a></span></li>
                        <li><span className="nba-leauge-team-29-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/New_Orleans_Pelicans" target="_blank">New Orleans Pelicans</a></span></li>
                        <li><span className="nba-leauge-team-30-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/San_Antonio_Spurs" target="_blank">San Antonio Spurs</a></span></li>
                    </ul>
                    </div>
                    </div>
                </div>
                <div className="col-md-6 col-sm-6 col-xs-6 col-lg-6 pr-0">
                    <h4><span className="team-gleauge-icon"><img src={gleauge} alt="National Basketball Academy"  /></span>NBA G League Teams</h4>
                    <div className="Team-Box">
                    <div className="teams-panel-heading teams-panel-heading-6">Atlantic</div>
                    <div className="team-panel-content">
                    <ul>
                        <li><span className="g-leauge-team-1-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Delaware_Blue_Coats" target="_blank">Delaware Blue Coats</a></span></li>
                        <li><span className="g-leauge-team-2-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Long_Island_Nets" target="_blank">Long Island Nets</a></span></li>
                        <li><span className="g-leauge-team-3-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Maine_Red_Claws" target="_blank">Maine Red Claws</a></span></li>
                        <li><span className="g-leauge-team-4-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Raptors_905" target="_blank">Raptors 905</a></span></li>
                        <li><span className="g-leauge-team-5-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Westchester_Knicks" target="_blank">Westchester Knicks</a></span></li>
                    </ul>
                    </div>
                    </div>
                    <div className="Team-Box">
                    <div className="teams-panel-heading teams-panel-heading-7">Central</div>
                    <div className="team-panel-content">
                    <ul>
                        <li><span className="g-leauge-team-6-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Canton_Charge" target="_blank">Canton Charge</a></span></li>
                        <li><span className="g-leauge-team-7-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Fort_Wayne_Mad_Ants" target="_blank">Fort Wayne Mad Ants</a></span></li>
                        <li><span className="g-leauge-team-8-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Grand_Rapids_Drive" target="_blank">Grand Rapids Drive</a></span></li>
                        <li><span className="g-leauge-team-9-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Windy_City_Bulls" target="_blank">Windy City Bulls</a></span></li>
                        <li><span className="g-leauge-team-10-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Wisconsin_Herd" target="_blank">Wisconsin Herd</a></span></li>
                    </ul>
                    </div>
                    </div>
                    <div className="Team-Box">
                    <div className="teams-panel-heading teams-panel-heading-8">Southeast</div>
                    <div className="team-panel-content">
                    <ul>
                        <li><span className="g-leauge-team-11-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Capital_City_Go-Go" target="_blank">Capital City Go-Go</a></span></li>
                        <li><span className="g-leauge-team-12-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/College_Park_Skyhawks" target="_blank">College Park Skyhawks</a></span></li>
                        <li><span className="g-leauge-team-13-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Erie_BayHawks_(2019%E2%80%93)" target="_blank">Erie BayHawks</a></span></li>
                        <li><span className="g-leauge-team-14-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Greensboro_Swarm" target="_blank">Greensboro Swarm</a></span></li>
                        <li><span className="g-leauge-team-15-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Lakeland_Magic" target="_blank">Lakeland Magic</a></span></li>
                    </ul>
                    </div>
                    </div>
                    <div className="Team-Box">
                    <div className="teams-panel-heading teams-panel-heading-9">Midwest</div>
                    <div className="team-panel-content">
                    <ul>
                        <li><span className="g-leauge-team-16-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Iowa_Wolves" target="_blank">Iowa Wolves</a></span></li>
                        <li><span className="g-leauge-team-17-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Memphis_Hustle" target="_blank">Memphis Hustle</a></span></li>
                        <li><span className="g-leauge-team-18-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Oklahoma_City_Blue" target="_blank">Oklahoma City Blue</a></span></li>
                        <li><span className="g-leauge-team-19-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Sioux_Falls_Skyforce" target="_blank">Sioux Falls Skyforce</a></span></li>
                    </ul>
                    </div>
                    </div>
                    <div className="Team-Box">
                    <div className="teams-panel-heading teams-panel-heading-10">Pacific</div>
                    <div className="team-panel-content">
                    <ul>
                        <li><span className="g-leauge-team-20-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Agua_Caliente_Clippers" target="_blank">Agua Caliente Clippers</a></span></li>
                        <li><span className="g-leauge-team-21-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Northern_Arizona_Suns" target="_blank">Northern Arizona Suns</a></span></li>
                        <li><span className="g-leauge-team-22-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Santa_Cruz_Warriors" target="_blank">Santa Cruz Warriors</a></span></li>
                        <li><span className="g-leauge-team-23-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/South_Bay_Lakers" target="_blank">South Bay Lakers</a></span></li>
                        <li><span className="g-leauge-team-24-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Stockton_Kings" target="_blank">Stockton Kings</a></span></li>
                    </ul>
                    </div>
                    </div>
                    <div className="Team-Box">
                    <div className="teams-panel-heading teams-panel-heading-11">Southwest</div>
                    <div className="team-panel-content">
                    <ul>
                        <li><span className="g-leauge-team-25-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Austin_Spurs" target="_blank">Austin Spurs</a></span></li>
                        <li><span className="g-leauge-team-26-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Rio_Grande_Valley_Vipers" target="_blank">Rio Grande Valley Vipers</a></span></li>
                        <li><span className="g-leauge-team-27-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Salt_Lake_City_Stars" target="_blank">Salt Lake City Stars</a></span></li>
                        <li><span className="g-leauge-team-28-logo">&nbsp;</span>&nbsp;<span className="leauge-team-text"><a href="https://en.wikipedia.org/wiki/Texas_Legends" target="_blank">Texas Legends</a></span></li>
                    </ul>
                    </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Teams;