var BoxSDK = require("box-node-sdk");
var fs = require("fs");
var app = require('./../server.js');
// var configFile = fs.readFileSync("./config/config.json", "utf8");
// var config = JSON.parse(configFile);
var env = app.get("env")
var fileName = './config/'+env + ".json";
var envFile = fs.readFileSync(fileName, 'utf8');
var config = JSON.parse(envFile);

var sdk = new BoxSDK({
  clientID: config.clientID,
  clientSecret: config.clientSecret,
});

var connectionMap = [];

 var boxRouter = function (router) {
    router.get('/authorizeUser', authorizeUser);
    router.post('/getAuthTokenByCode', getAuthTokenByCode);
    router.post('/getAuthTokenByRefreshToken', getAuthTokenByRefreshToken)
    router.post("/getDownloadFileURL", getDownloadFileURL);
    router.post("/getSingleFileInfo", getSingleFileInfo);
    router.post("/updateFileInfo", updateFileInfo);
    router.post("/getAllFiles", getAllFiles);
    router.post("/getVersions", getVersions);
    router.get("/getCurrentUserId/:email", getCurrentUserId);
    router.post("/getUserGroups", getUserGroups);
    router.post("/uploadFile", uploadFile);
    router.post("/downloadFile", downloadFile);  
    router.post("/getFileTags", getFileTags);  
    return router;
};

var authorizeUser = function (req, res, next) {
    var authorize_url = sdk.getAuthorizeURL({
        response_type: "code"
      });

      res.send(authorize_url);
};


var getAuthTokenByCode = function (req, res, next) {
  let code= req.body.code;
  let userEmail = req.body.user;
  sdk.getTokensAuthorizationCodeGrant(code, null, function(err, tokenInfo) {
    if(tokenInfo) {
    var userClient = sdk.getPersistentClient(tokenInfo);
    if(tokenInfo.accessToken) {
      let tokens = {
        "accessToken": tokenInfo.accessToken,
        "refreshToken": tokenInfo.refreshToken
        
      };
      updateConnectionMap(userEmail, userClient)
      res.send(tokens)
    } 
  } else {
    res.send(err)
  }
  });
};

var updateConnectionMap = function (userEmail, userClient) {
  var ind = connectionMap.map(function (d) {return d["email"].toLowerCase()}).indexOf(userEmail.toLowerCase());
  if(ind>-1) {
    connectionMap[ind].client = userClient;
  } else {
    connectionMap.push({"email": userEmail, "client": userClient})
  }
}

var getCurrentUserId = function (req, res, next) {
  var email = req.params.email;
  var client;
  var ind = connectionMap.map(function (d) {return d["email"].toLowerCase()}).indexOf(email.toLowerCase());
  if(ind>-1) {
    client = connectionMap[ind].client;
  }
  if(client) {
    client.users.get(client.CURRENT_USER_ID).then((user) => {
      let userId = user.id;
      if(userId) {
        res.json({"userid": userId, "boxUser": user.login});
      } else {
        res.json({"userid": ""});
      }
    });
  } else {
    res.json({"userid": ""});
  }

}

var getUserGroups = function (req, res, next) {
  let id= req.body.userid;
  var email = req.body.userEmail;
  var client;
  var ind = connectionMap.map(function (d) {return d["email"].toLowerCase()}).indexOf(email.toLowerCase());
  if(ind>-1) {
    client = connectionMap[ind].client;
  }
  if(client) {
    client.users.getGroupMemberships(id).then((memberships) => {
      res.json({"memberships": memberships});
    });
  } else {
    res.json({"memberships": []});
  }
}

//Single file download url
var getDownloadFileURL = function (req, res, next) {
  var email = req.body.email;
  var client;
  var ind = connectionMap.map(function (d) {return d["email"].toLowerCase()}).indexOf(email.toLowerCase());
  if(ind>-1) {
    client = connectionMap[ind].client;
  }
  if(client) {
    client.files.getDownloadURL(req.body.fileID).then((downloadURL) => {
      res.json(downloadURL);
    });
  } else {
    res.json("Error");
  }
};

//Get Single file information
var getSingleFileInfo = function (req, res, next) {

  var email = req.body.email;
  var client;
  var ind = connectionMap.map(function (d) {return d["email"].toLowerCase()}).indexOf(email.toLowerCase());
  if(ind>-1) {
    client = connectionMap[ind].client;
  }
  if(client) {
    client.files.get(req.body.fileID).then((file) => {
      res.json(file);
    });
  } else {
    res.json("Error");
  }
  
};

//Get all files folder wise 
var getAllFiles = function (req, res, next) {
  client.folders
  .getItems(req.body.FolderID, { usemarker: "false",fields: "name",offset: 0})
  .then((items) => {
    res.json(items);
  });
};

//Get single file version 
var getVersions = function (req, res, next) {
  client.files.getVersions(req.body.FileID).then((versions) => {
    res.json(versions);
  });
};

var getAuthTokenByRefreshToken = function (req, res, next) {
  let refreshToken= req.body.refreshToken;
  let userEmail = req.body.user;
  sdk.getTokensRefreshGrant(refreshToken, null, function(err, tokenInfo) {
    if(tokenInfo && tokenInfo.accessToken) {
        var userClient = sdk.getPersistentClient(tokenInfo);
        let tokens = {
          "accessToken": tokenInfo.accessToken,
          "refreshToken": tokenInfo.refreshToken,
        };
        updateConnectionMap(userEmail, userClient)
        res.send(tokens)
      } else {
        res.send(err)
      }
  });
};

//Download the file
var downloadFile = function (req, res, next) {
  var email =req.body.email;
  var client;
  var ind = connectionMap.map(function (d)  {
    return d["email"].toLowerCase()}).indexOf(email.toLowerCase());
    if(ind>-1) { client = connectionMap[ind].client;
    }
  
  if(client) {
    client.files.getReadStream(req.body.fileID, null, function(error, stream) {
      if (error) {
        console.log(error);
      }
      const data = [];      
      stream.on('data', (chunk) => {
          data.push(chunk);
      });      
      stream.on('end', () => {   
          const buffer  = Buffer.concat(data);
          const str = buffer.toString("utf-8")
          let base64data = buffer.toString('base64');
        res.send(base64data);
      })      
      stream.on('error', (err) => {
          console.log('error :', err)
      })
    });
  } 
  else {
    res.json({"userid": ""});
  }
}

//Upload the file
var uploadFile = function (req, res, next) {
  var email =req.body.email;
  var client;
  var ind = connectionMap.map(function (d) 
  {
    return d["email"].toLowerCase()
  }).indexOf(email.toLowerCase());

  if(ind>-1) {
    client = connectionMap[ind].client;
  }  

let bufferObj = Buffer.from(req.body.stream, "base64"); 
  if(client) {
    client.files.uploadFile(req.body.folderID, req.body.fileName , bufferObj).then(file => {
      res.json(file)
    	}); 
  } 
  else {
    res.json({"userid": ""});
  }

}

var getSingleFileInfo = function (req, res, next) {

  var email = req.body.email;
  var client;
  var ind = connectionMap.map(function (d) {return d["email"].toLowerCase()}).indexOf(email.toLowerCase());
  if(ind>-1) {
    client = connectionMap[ind].client;
  }
  if(client) {
    client.files.get(req.body.fileID).then((file) => {
      res.json(file);
    });
  } else {
    res.json("Error");
  }
  
};

var getFileTags = function (req, res, next) {

  var email = req.body.email;
  var client;
  var ind = connectionMap.map(function (d) {return d["email"].toLowerCase()}).indexOf(email.toLowerCase());
  if(ind>-1) {
    client = connectionMap[ind].client;
  }
  if(client) {
    client.files.get(req.body.fileID,{fields: 'tags'}).then((file) => {
      res.json(file);
    });
  } else {
    res.json("Error");
  }
};

//Update file information
var updateFileInfo = function (req, res, next) {
  var email = req.body.email;
  var client;
  var ind = connectionMap.map(function (d) {return d["email"].toLowerCase()}).indexOf(email.toLowerCase());
  if(ind>-1) {
    client = connectionMap[ind].client;
  }
  if(client) {
    client.files.update(req.body.fileID,{ tags : req.body.tags }).
    then((file) => {
      res.json("ok");
    });
  } else {
    res.json("Error");
  }
};



module.exports = boxRouter;